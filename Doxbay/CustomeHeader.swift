//
//  HeaderView.swift
//  DoxbaySalman
//
//  Created by AppsInvo  on 20/05/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit

class CustomeHeader: UIView {
    @IBOutlet weak var headerName: UILabel!
    @IBOutlet weak var headerAddButton: UIButton!   
}
