//
//  Utility.swift
//  Doxbay
//
//  Created by Luminous on 27/04/18.
//  Copyright © 2018 Yuji Hato. All rights reserved.
//

import Foundation
class Utility {

    
    
    func validateEmail(enteredEmail:String) -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
    }
    
    
    
//    class  func getHTMLToString(titleStr : String)  -> NSAttributedString
//    {
//        let htmlString = "<html><body>\(titleStr ?? "")</body></html>"
//        let data = htmlString.data(using: String.Encoding.unicode)! // mind "!"
//        let attrStr = try? NSAttributedString( // do catch
//            data: data,
//            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
//            documentAttributes: nil)
//        return  attrStr!;
//    }
    
    class  func getHTMLToString(titleStr : String)  -> NSAttributedString
    {
        let htmlString = "<html><body>\(titleStr )</body></html>"
        let data = htmlString.data(using: String.Encoding.unicode)! // mind "!"
        let attrStr = try? NSAttributedString( // do catch
            data: data,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil)
        return  attrStr!;
    }
    
    
    
  class func heightForView(text:String) -> CGFloat{
        let font = UIFont(name: "Helvetica", size: 12.0)
        let bounds: CGRect = UIScreen.main.bounds
        let width : CGFloat = bounds.size.width
        
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    
}
