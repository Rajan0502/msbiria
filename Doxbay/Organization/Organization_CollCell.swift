//
//  Organization_CollCell.swift
//  CollectionViewDataSourceBlog
//
//  Created by Luminous on 08/05/18.
//  Copyright © 2018 Erica Millado. All rights reserved.
//

import UIKit

class Organization_CollCell: UICollectionViewCell {
    
    @IBOutlet var bookImage: UIImageView!
    @IBOutlet var bookLabel: UILabel!
 //   @IBOutlet var orgChnnelCount: UILabel!
    
    func displayContent( title: String, chnalCount : String , imageName :String) {
        let image : UIImage = UIImage(named: imageName)!
        bookImage.image = image;
        bookLabel.text = title
       // orgChnnelCount.text = chnalCount + "Channels"
    }
}
