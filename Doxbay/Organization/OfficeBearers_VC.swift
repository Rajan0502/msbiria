//
//  OfficeBearers_VC.swift
//  Doxbay
//
//  Created by Luminous on 13/05/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON
import MBProgressHUD

class OfficeBearers_VC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var offList : [OfficeBcModal] = [];
    var AboutOffice : String = "";
    
   // @IBOutlet var labAboutOffice: UITextView!
    @IBOutlet var tableView: UITableView!
    
    
    
    
    
    
    @IBAction func btnBack(_ sender: Any) {
     //    self.navigationController?.popViewController(animated: false)
         self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self;
        tableView.delegate = self
        getOfficeBarData();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section == 0)
        {
            return Utility.heightForView(text: AboutOffice) + 30;
        }
        else{
            return CGFloat(135);
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0)
        {
            return 1
        }
        else{
            return offList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.section == 0)
        {
           
            let cell = tableView.dequeueReusableCell(withIdentifier: "OfficeBCellAbout", for: indexPath) as! OfficeBCellAbout
            cell.officeAbout.attributedText =  Utility.getHTMLToString(titleStr: AboutOffice)
            return cell
        }
        else
        {
            let rowDara : OfficeBcModal = offList[indexPath.row];
            let cell = tableView.dequeueReusableCell(withIdentifier: "OfficeBCell", for: indexPath) as! OfficeBCell
            cell.labName.text = rowDara.name;
            cell.labDestion.text = rowDara.designation;
            cell.labEmail.text = rowDara.email;
            cell.labPlace.text = rowDara.place
            cell.labCont.text = rowDara.mobile
            return cell;
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    
    
    
    
    
    func getOfficeBarData()
    {
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Loading"
        
        let requestURL =  BASEURL + channelOffice;
        let parameters: [String:String] = ["token": token , "channel_id": channel_id]
        
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            MBProgressHUD.hide(for: self.view, animated: false)
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)-- \(response)")
            if success
            {
                self.parseJSONData(json: response)
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
        })
    }
    
    
    
    
    func parseJSONData(json: JSON)
    {
        AboutOffice = json["data"]["office_text"].stringValue;
       // labAboutOffice.attributedText =  Utility.getHTMLToString(titleStr: AboutOffice)
        
        
        for result in json["data"]["members"].arrayValue
        {
            print(result)
            let id = result["id"].stringValue
            let name = result["name"].stringValue
            let designation = result["designation"].stringValue
            let email = result["email"].stringValue
            let mobile = result["mobile"].stringValue
            let place = result["place"].stringValue
            let bearer_id = result["bearer_id"].stringValue
            let photo = result["photo"].stringValue
            
            let aData : OfficeBcModal = OfficeBcModal(id: id, name: name, designation: designation, email: email,  mobile: mobile  , place: place , bearer_id: bearer_id , photo: photo);
            self.offList.append(aData);
        }
        
        self.tableView.reloadData();
    }
}


class OfficeBcModal{
    let id : String;
    let name: String;
    let designation : String;
    let email : String;
    let mobile: String;
    let place: String;
    let bearer_id : String;
    let photo: String;
    init(id: String , name : String , designation : String , email : String , mobile : String , place : String , bearer_id : String , photo : String)
    {
        self.id = id;
        self.name = name;
        self.designation = designation;
        self.email = email;
        self.mobile = mobile;
        self.place = place;
        self.bearer_id = bearer_id;
        self.photo = photo
    }
}


class OfficeBCell : UITableViewCell {
    
    @IBOutlet var profilePic: UIImageView!
    @IBOutlet var labName: UILabel!
    @IBOutlet var labDestion: UILabel!
    @IBOutlet var labEmail: UILabel!
    @IBOutlet var labPlace: UILabel!
    @IBOutlet var labCont: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

class OfficeBCellAbout : UITableViewCell {

    @IBOutlet var officeAbout: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
