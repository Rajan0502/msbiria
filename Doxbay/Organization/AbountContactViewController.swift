//
//  AbountContactViewController.swift
//  Doxbay
//
//  Created by Luminous on 13/05/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit
import WebKit
import MBProgressHUD


class AbountContactViewController: UIViewController, WKNavigationDelegate {

    var pageType : String = "";
    var pageTitle :  String = "";
   // var webView: WKWebView!
    
    @IBOutlet var webview: UIWebView!
    @IBOutlet var headerTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.headerTitle.text = pageType
        getExploreListData();
   
    }

    @IBAction func btnBack(_ sender: Any) {
      //  self.navigationController?.popViewController(animated: false)
         self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    
    func getExploreListData()
    {
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Loading"
        var requestURL = ""
        if(pageType == "Contact Us")
        {
            requestURL =  BASEURL + channelContact;
        }
        else if(pageType == "About Us")
        {
            requestURL =  BASEURL + channelAbout;
        }

        
        let parameters: [String:String] = ["token": token , "channel_id": channel_id]
        
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
           
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)-- \(response)")
            if success
            {
                var loadHTMLVal = "";
                if(self.pageType == "Contact Us")
                {
                     let contact_address = response["data"]["contact_address"].stringValue
                     let map_url = response["data"]["map_url"].stringValue
                    
                     loadHTMLVal = "<html><head><title>\(self.pageType)web</title></head> <body><p>\(contact_address)</p><p>\(map_url)</p></body>"
                }
                else if(self.pageType == "About Us")
                {
                     let about_text = response["data"]["about_text"].stringValue
                    loadHTMLVal = "<html><head><title>\(self.pageType)web</title></head> <body><p>\(about_text)</p></body>"
                }
              self.webview.loadHTMLString(loadHTMLVal, baseURL: nil)
              MBProgressHUD.hide(for: self.view, animated: false)
            }
            else
            {
                 MBProgressHUD.hide(for: self.view, animated: false)
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
        })
    }
}
