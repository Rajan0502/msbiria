//
//  OrganizationTimeLIne_VC_CellTableViewCell.swift
//  Doxbay
//
//  Created by Luminous on 11/05/18.
//  Copyright © 2018 Yuji Hato. All rights reserved.
//

import UIKit

class OrganizationTimeLIne_VC_Cell: UITableViewCell {

    @IBOutlet var userProfilePic: UIImageView!
    @IBOutlet var postImageView: UIImageView!
    @IBOutlet var isFavrateImgView: UIImageView!
    @IBOutlet var isLikeImgView: UIImageView!
    
    @IBOutlet var labUserName: UILabel!
    @IBOutlet var postTime: UILabel!
    @IBOutlet var postDescription: UITextView!
    @IBOutlet var labLikeCount: UILabel!
    @IBOutlet var labCommentCount: UILabel!
    
    @IBOutlet var like_Dislike: UIView!
    @IBOutlet var comment: UIView!
    
    @IBOutlet var postImgUIView : UIView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
       // layoutMargins = UIEdgeInsetsMake(20, 0, 20, 0)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
