//
//  Organization_CollModal.swift
//  CollectionViewDataSourceBlog
//
//  Created by Luminous on 08/05/18.
//  Copyright © 2018 Erica Millado. All rights reserved.
//

import Foundation
struct Organization_CollModal {
    let CatID: String
    let CatName: String
    let catImg: String
    let totalChannel: String;
    
    
    init(catID : String, catName: String , umgName : String , totalChannel:String)
    {
        self.CatID = catID
        self.CatName = catName
        self.catImg = umgName
        self.totalChannel = totalChannel
    }
    
    
}
