//
//  Organization_CollViewViewController.swift
//  CollectionViewDataSourceBlog
//
//  Created by Luminous on 08/05/18.
//  Copyright © 2018 Erica Millado. All rights reserved.
//

import UIKit

class Organization_CollViewViewController: UIViewController , UICollectionViewDataSource {
    
    
    
    let sampleTextField =  UILabel(frame: CGRect(x: 10, y: 200, width: UIScreen.main.bounds.size.width - 20 , height: 50))
    
    
    @IBOutlet var collectionView: UICollectionView!
    
    var catListArr : [Organization_CollModal] = [];
    
    
    
    @IBAction func backClk(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnConnectedBranch(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ConnectedBranch_VC") as! ConnectedBranch_VC
        self.present(vc, animated:true, completion:nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        
        
        sampleTextField.font = UIFont.systemFont(ofSize: 30)
        sampleTextField.textColor = UIColor.black
        sampleTextField.textAlignment = .center
        sampleTextField.lineBreakMode = .byTruncatingMiddle
        sampleTextField.lineBreakMode = .byWordWrapping
        sampleTextField.numberOfLines = 0;
        sampleTextField.text = "Your subscription request send to admin, Please wait for admin confirmation. Thanks"
        let greet4Height = sampleTextField.optimalHeight
        sampleTextField.frame = CGRect(x: sampleTextField.frame.origin.x, y: sampleTextField.frame.origin.y, width: sampleTextField.frame.width, height: greet4Height)
        self.view.addSubview(sampleTextField)
        sampleTextField.isHidden = true
        
        
        
        var sWidth : Int = Int(screenWidth);
        var xxx : Double = 0.0
        if sWidth % 2 == 0 {
            sWidth = sWidth - 12
            xxx =  Double(sWidth / 3)
            print("Sell widht  odd \(xxx)")
            print("Sell widht odd \(sWidth)")
        } else {
            sWidth = sWidth - 13
            sWidth =  sWidth / 3
            xxx =  Double(sWidth / 3)
            print("Sell widht  odd \(xxx)")
            print("Sell widht  odd \(sWidth)")
        }
        
        var cellSize = CGSize(width: 100  , height:115)
        if(UIDevice.current.screenTypeWithHeight.rawValue == "1136")
        {
            cellSize = CGSize(width: 100  , height:105)
        }
        else
        {
            cellSize = CGSize(width: 115  , height:115)
        }
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical //.horizontal
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
        layout.minimumLineSpacing = 1.0
        layout.minimumInteritemSpacing = 1.0
        collectionView.setCollectionViewLayout(layout, animated: true)
        
        
        
        
       
        
        let Form : Organization_CollModal = Organization_CollModal(catID: "Form", catName: "Form" , umgName: "vc_ops_forms" , totalChannel: "");
        let News : Organization_CollModal = Organization_CollModal(catID: "News",catName: "News" , umgName: "vc_ops_news" , totalChannel: "");
    
        let Events : Organization_CollModal = Organization_CollModal(catID: "Events",catName: "Events" , umgName: "vc_ops_events" , totalChannel: "");
        let PressRelease : Organization_CollModal = Organization_CollModal(catID: "Press Release",catName: "Press Release" , umgName: "vc_ops_press_release" , totalChannel: "");
        let Gallery : Organization_CollModal = Organization_CollModal(catID: "Gallery",catName: "Gallery" , umgName: "vc_ops_gallery" , totalChannel: "");
        let MedicoLegal : Organization_CollModal = Organization_CollModal(catID: "Medico-Legal",catName: "Medico-Legal" , umgName: "vc_ops_medico" , totalChannel: "");
        let eVoting : Organization_CollModal = Organization_CollModal(catID: "eVoting",catName: "eVoting" , umgName: "vc_ops_evoting" , totalChannel: "");
        let MemberSeatch : Organization_CollModal = Organization_CollModal(catID: "Member Search",catName: "Member Search" , umgName: "vc_ops_member" , totalChannel: "");
        
        let AboutUS : Organization_CollModal = Organization_CollModal(catID: "About Us",catName: "About Us" , umgName: "vc_ops_about" , totalChannel: "");
        
        let OfficeBearers : Organization_CollModal = Organization_CollModal(catID: "Office Bearers",catName: "Office Bearers" , umgName: "vc_ops_connected" , totalChannel: "");
        
        let ContactUS : Organization_CollModal = Organization_CollModal(catID: "Contact Us",catName: "Contact Us" , umgName: "vc_ops_contact" , totalChannel: "");
        
        let Quiz : Organization_CollModal = Organization_CollModal(catID: "Quiz",catName: "Quiz" , umgName: "vc_quiz" , totalChannel: "");
        
        let Updates : Organization_CollModal = Organization_CollModal(catID: "Updates",catName: "Updates" , umgName: "vc_ops_updates" , totalChannel: "");
        
        let SocialInitiatives : Organization_CollModal = Organization_CollModal(catID: "Social Initiatives",catName: "Social Initiatives" , umgName: "vc_ops_social" , totalChannel: "");
        
        let Downloads : Organization_CollModal = Organization_CollModal(catID: "Downloads",catName: "Downloads" , umgName: "vc_ops_download" , totalChannel: "");
        
        
        
        
        if(is_subscribeStatus == "2")
        {
            catListArr.append(AboutUS);
            catListArr.append(OfficeBearers);
            catListArr.append(ContactUS);
            catListArr.append(MemberSeatch);
            catListArr.append(Quiz);
            catListArr.append(Form);
            catListArr.append(eVoting);
            catListArr.append(Updates);
            catListArr.append(News);
            catListArr.append(Events);
            catListArr.append(PressRelease);
            catListArr.append(Gallery);
            catListArr.append(MedicoLegal);
            catListArr.append(SocialInitiatives);
            catListArr.append(Downloads);
        }
        else{
            self.collectionView.isHidden = true
            self.sampleTextField.isHidden = false
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.parent?.view.frame = CGRect(x:0, y:64, width:self.view.frame.width, height: UIScreen.main.bounds.size.height-64)
            // floatingActionButton.isHidden = true
            
        }
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  catListArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Organization_CollCell", for: indexPath) as! Organization_CollCell
        
        
        cell.contentView.layer.borderWidth = 1.0
        cell.contentView.layer.borderColor = UIColor.clear.cgColor
        cell.contentView.layer.masksToBounds = true
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cell.layer.shadowRadius = 2.0
        cell.layer.shadowOpacity = 1.0
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
        
        
        
        let Organization_CollModal = catListArr[indexPath.row]
        
        cell.displayContent(title: Organization_CollModal.CatName , chnalCount: Organization_CollModal.totalChannel , imageName: Organization_CollModal.catImg)
        let tap = UITapGestureRecognizer(target: self, action: #selector(clickPos))
        cell.bookImage.addGestureRecognizer(tap)
        cell.bookImage.isUserInteractionEnabled = true
        
        return cell
    }
    
    
    @objc func clickPos(_ sender: UITapGestureRecognizer)
    {
        let touch = sender.location(in: self.collectionView)
        let indexPathx =  self.collectionView.indexPathForItem(at: touch)
        let indx = indexPathx?.row;
        let xx : Organization_CollModal = catListArr[indx!]
        
        if(indx == 0 ||  indx == 2)
        {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "AbountContactViewController") as! AbountContactViewController
            vc.pageType = xx.CatName
            vc.pageTitle = xx.CatID
            self.present(vc, animated:true, completion:nil)
        }
        else if(indx == 1)
        {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "OfficeBearers_VC") as! OfficeBearers_VC
            self.present(vc, animated:true, completion:nil)
        }
        else if(indx == 3)
        {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ChannelSubscriberList_VC") as! ChannelSubscriberList_VC
            vc.chanalId = channel_id;
            vc.FromPage = "Member Search" //xx.catName;
            self.present(vc, animated:true, completion:nil)
        }
        else if(indx == 4)
        {
            //ShowToast.showPositiveMessage(message: "Comming Soon...")
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "QuizLIstViewController") as! QuizLIstViewController
            self.present(vc, animated:true, completion:nil)
        }
        else if(indx == 5 )
        {
            isFromChannelID = channel_id
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "FormList_VC") as! FormList_VC
            self.present(vc, animated:true, completion:nil)
        }
        else if(indx == 7)
        {
            ShowToast.showPositiveMessage(message: "Comming Soon...")
        }
        else if(indx == 13)
        {
            ShowToast.showPositiveMessage(message: "Comming Soon...")
        }
        else if(indx == 14)
        {
            ShowToast.showPositiveMessage(message: "Comming Soon...")
        }
        else
        {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "OrganizationTimeLine_VC") as! OrganizationTimeLine_VC
            vc.expID = xx.CatID
            vc.orgChannelName = xx.CatName
            self.present(vc, animated:true, completion:nil)
        }
        
    }
    
    
    
}
