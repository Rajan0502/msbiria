 //
//  LeftViewController.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 12/3/14.
//

import UIKit
import Alamofire


enum LeftMenu: Int {
    case main = 0
    case swift
    case java
    case go
    case nonMenu
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}

 
 
class LeftViewController : UIViewController, LeftMenuProtocol,UITableViewDelegate,UITableViewDataSource {
    
    
    
    
    @IBOutlet weak var tableView: UITableView!
    var sectionArray = ["","Main","Support US"]

    
    
    var mainSection = ["Home", "Profile", "Messenger", "Favorites",  "Admin", "Setting", "Log Out"]
     var mainSectionIcon = ["menu_home", "menu_profile", "menu_chhat", "menufav",  "menu_admin", "menu_setting", "menu_logout"]
    
    var yourAcoountArray = ["Rate", "Invite Friends","Terms & Conditions", "Privacy Policy" , "Help Center"]
     var yourAcoountArrayIocn = ["menu_rate", "menu_share", "term_condi", "privacy_policy" , "help_center"]
    
    var mainViewController: UIViewController!
    var swiftViewController: UIViewController!
    var javaViewController: UIViewController!
    var goViewController: UIViewController!
    var nonMenuViewController: UIViewController!
    
    
    
    
 //   var imageHeaderView: ImageHeaderView!
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
   
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.separatorColor = UIColor(red: 25.0/255.0, green: 28.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let swiftViewController = storyboard.instantiateViewController(withIdentifier: "SwiftViewController") as! SwiftViewController
        self.swiftViewController = UINavigationController(rootViewController: swiftViewController)
        
        let javaViewController = storyboard.instantiateViewController(withIdentifier: "JavaViewController") as! JavaViewController
        self.javaViewController = UINavigationController(rootViewController: javaViewController)
        
        let goViewController = storyboard.instantiateViewController(withIdentifier: "GoViewController") as! GoViewController
        self.goViewController = UINavigationController(rootViewController: goViewController)
        
        self.getProfile();
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        self.view.layoutIfNeeded()
    }
    
    
    
    
    func changeViewController(_ menu: LeftMenu) {
        switch menu {
        case .main:            self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
        case .swift:            self.slideMenuController()?.changeMainViewController(self.swiftViewController, close: true)
        case .java:            self.slideMenuController()?.changeMainViewController(self.javaViewController, close: true)
        case .go:            self.slideMenuController()?.changeMainViewController(self.goViewController, close: true)
        case .nonMenu:            self.slideMenuController()?.changeMainViewController(self.nonMenuViewController, close: true)
        }
    }


    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tableView == scrollView {
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 3;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section==0
        {
            return 1;
        }
        else if  section==1
        {
             return mainSection.count
        }
        else if  section==2
        {
            return yourAcoountArray.count
        }
        else
        {
            return  0 ;
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section==0
        {
            let cellIdentifier = "MenuProfileTableViewCell"
            var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? MenuProfileTableViewCell
            if cell == nil {
                var topLevelObjects = Bundle.main.loadNibNamed("MenuProfileTableViewCell", owner: self, options: nil)
                cell = topLevelObjects?[0] as? MenuProfileTableViewCell
                cell?.selectionStyle = .none
            }
            

            cell?.labName.text = displayName
            cell?.labEmail.text = email
            
            cell?.imgProfilePic.layer.cornerRadius =  (cell?.imgProfilePic.frame.size.width)! / 2;
            cell?.imgProfilePic.layer.masksToBounds = true
            if(!photoUrl.isEmpty && photoUrl != "")
            {
                cell?.imgProfileBg.sd_setImage(with: URL(string: photoUrl), placeholderImage: UIImage(named: "vc_user"))
                cell?.imgProfilePic.sd_setImage(with: URL(string: photoUrl), placeholderImage: UIImage(named: "vc_user"))
            }
            

            
            return cell!
        }
        else if  indexPath.section==1
        {
            let cellIdentifier = "MenuTableViewCell"
            var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? MenuTableViewCell
            if cell == nil {
                var topLevelObjects = Bundle.main.loadNibNamed("MenuTableViewCell", owner: self, options: nil)
                cell = topLevelObjects?[0] as? MenuTableViewCell
                cell?.selectionStyle = .none
            }
            cell?.lbl.frame = CGRect(x:71,y:0,width:(self.view.frame.size.width-71),height:(cell?.lbl.frame.size.height)!)
            cell?.imgVw.isHidden = false
            cell?.imgVw.image = UIImage(named: mainSectionIcon[indexPath.row]) // UIImageView(name : )
            cell?.lbl.text = mainSection[indexPath.row]
            cell?.lbl.textColor = UCUtil.colorWithHexString(hex: "9a9fbf")
            
            return cell!;
        }
        else if  indexPath.section==2
        {
            let cellIdentifier = "MenuTableViewCell"
            var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? MenuTableViewCell
            if cell == nil {
                var topLevelObjects = Bundle.main.loadNibNamed("MenuTableViewCell", owner: self, options: nil)
                cell = topLevelObjects?[0] as? MenuTableViewCell
                cell?.selectionStyle = .none
            }
            cell?.lbl.text = yourAcoountArray[indexPath.row]
            cell?.lbl.textColor = UIColor.white
            cell?.lbl.frame = CGRect(x:71,y:0,width:(self.view.frame.size.width-71),height:(cell?.lbl.frame.size.height)!)
            cell?.imgVw.isHidden = false
            cell?.imgVw.image = UIImage(named: yourAcoountArrayIocn[indexPath.row])
            return cell!;
        }
        else
        {
            let cellIdentifier = "MenuTableViewCell"
            var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? MenuTableViewCell
            return cell!;
        }
  
       
    }
 
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
            let sectionView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
            sectionView.backgroundColor = UIColor(red: 25.0/255.0, green: 28.0/255.0, blue: 118.0/255.0, alpha: 1.0) // UIColor.white
            sectionView.tag = section
     
            let separatorLineView1 = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 0.5))
            separatorLineView1.backgroundColor = UIColor(red: 25.0/255.0, green: 28.0/255.0, blue: 118.0/255.0, alpha: 1.0) //UCUtil.colorWithHexString(hex: "9a9fbf")
            sectionView.addSubview(separatorLineView1)
        
        
            let viewLabel = UILabel(frame: CGRect(x: 0, y: 1, width: tableView.frame.size.width, height: 39.5))
            viewLabel.backgroundColor = UIColor(red: 25.0/255.0, green: 28.0/255.0, blue: 118.0/255.0, alpha: 1.0) //UIColor.white
            viewLabel.textColor = UIColor.white // UCUtil.colorWithHexString(hex: "9a9fbf")
            viewLabel.font = UIFont.boldSystemFont(ofSize: 15)
            viewLabel.text = "  " + sectionArray[section]
        
            sectionView.addSubview(viewLabel)
            let separatorLineView = UIView(frame: CGRect(x: 0, y: 39.5, width: tableView.frame.size.width , height: 0.5))
           separatorLineView.backgroundColor = UCUtil.colorWithHexString(hex: "9a9fbf")
           sectionView.addSubview(separatorLineView)

        return sectionView
        
        }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if section==0
        {
            return 0
        }
        else
        {
            return 40;
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section==0
        {
            return 170
        }
        else
        {
            if indexPath.section==3
            {
                return 40
            }
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexPath = tableView.indexPathForSelectedRow
        let sectionInx = indexPath?.section;
        let indAtRow = indexPath?.row;
        
        if(sectionInx == 0)
        {
            if indexPath?.row == 0
            {
                isFromMenu=""
            }
        }
        else if(sectionInx == 1)
        {
            if indexPath?.row == 0
            {
                isFromMenu="Home"
                self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
                return;
            }
            else if indexPath?.row == 1
            {
                isFromMenu="Profile"
            }
            else if indexPath?.row == 2
            {
                isFromMenu="Messenger"
            }
            else if indexPath?.row == 3
            {
                isFromMenu="Favorites"
            }
            else if indexPath?.row == 4
            {
                isFromMenu="Admin"
            }
            else if indexPath?.row == 5
            {
                isFromMenu="Setting"
            }
            else if indexPath?.row == 6
            {
                let alert = UIAlertController(title: "MSBIRIA App", message: "Do you want to logout from App.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                    UserDefaults.standard.removeObject(forKey: "tokenID");
                    UserDefaults.standard.removeObject(forKey:  "uid");
                    UserDefaults.standard.removeObject(forKey:  "email");
                    UserDefaults.standard.removeObject(forKey: "deviceId");
                    UserDefaults.standard.removeObject(forKey:  "islogin");
                    UserDefaults.standard.removeObject(forKey: "isRemindMe");
                    
                    ShowToast.showPositiveMessage(message: "Thanks. You have been successfully logged out!");
                    
                    let storyBoard: UIStoryboard = UIStoryboard(name: "LoginSearch", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    self.present(newViewController, animated: true, completion: nil)
                }))
                alert.addAction(UIAlertAction(title: "No, Thank", style: .default, handler: { action in
                    print("Yay! You brought your towel!")
                }))
                
                self.present(alert, animated: true)
                
                

            }
            
           if indexPath?.row != 6
            {
                self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
                NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "initalizeController"),object:nil))
            }
        }
       else if(sectionInx == 2)
        {
            if indexPath?.row == 2
            {
                let storyBoard: UIStoryboard = UIStoryboard(name: "LoginSearch", bundle: nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "TermCond_PrivacyPoli_VC") as! TermCond_PrivacyPoli_VC
                vc.isFromPage = "Menu"
                vc.PageType = "Term_Condition"
                self.present(vc, animated: true, completion: nil)
            }
            else if indexPath?.row == 3
            {
                let storyBoard: UIStoryboard = UIStoryboard(name: "LoginSearch", bundle: nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "TermCond_PrivacyPoli_VC") as! TermCond_PrivacyPoli_VC
                vc.isFromPage = "Menu"
                vc.PageType = "Privacy_Policy"
                self.present(vc, animated: true, completion: nil)
            }
            else if indexPath?.row == 4
            {
                let storyBoard: UIStoryboard = UIStoryboard(name: "LoginSearch", bundle: nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "TermCond_PrivacyPoli_VC") as! TermCond_PrivacyPoli_VC
                vc.isFromPage = "Menu"
                vc.PageType = "Help_Center"
                self.present(vc, animated: true, completion: nil)
            }
            else
            {
                ShowToast.showPositiveMessage(message: "Comming Soon...")
            }
    }
        
        
    }
    
    
  
    
    
    
    func getProfile() {
        let params = ["token":token, "for_uuid": for_uuid, Constant.kAPI_NAME: getUser]
        
        SGServiceController.instance().hitPostService(params, unReachable: {
            print("No internet")
        }) { (response) in
            if response == nil {
                return
            }
            let data = response?["data"] as? [String: Any]
            allDetail = data!
            self.setData(info: allDetail)
        }
    }
    
    var displayName : String = ""
    var email : String = ""
    var photoUrl : String = ""
    func setData(info: [String: Any])
    {
        displayName =  info["displayName"] as? String ?? ""
        email = info["email"] as? String ?? ""
        photoUrl = info["photoUrl"] as? String ?? ""
        
        profileData["displayName"] = info["displayName"] as? String ?? ""
        profileData["email"] = info["email"] as? String ?? ""
        profileData["photoUrl"] = info["photoUrl"] as? String ?? ""
        profileData["mobile"] = info["mobile"] as? String ?? ""
        
        profileData["doxbay_landline"] = info["doxbay_landline"] as? String ?? ""
        profileData["registration_number"] = info["registration_number"] as? String ?? ""
        
        profileData["doxbay_qualification"] = info["doxbay_qualification"] as? String ?? ""
        profileData["displayName"] = info["displayName"] as? String ?? ""
        profileData["doxbay_dob"] = info["doxbay_dob"] as? String ?? ""
        profileData["doxbay_speciality"] = info["doxbay_speciality"] as? String ?? ""
        profileData["doxbay_designation"] = info["doxbay_designation"] as? String ?? ""
        profileData["doxbay_address"] = info["doxbay_address"] as? String ?? ""
        profileData["doxbay_pincode"] = info["doxbay_pincode"] as? String ?? ""
        profileData["dox_id"] = info["dox_id"] as? String ?? ""
        
        self.tableView.reloadData();
    }
    
    
    
}
 
 
