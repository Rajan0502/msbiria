//
//  MenuProfileTableViewCell.swift
//  Doxbay
//
//  Created by Unify on 23/04/18.
//  Copyright © 2018 Yuji Hato. All rights reserved.
//

import UIKit

class MenuProfileTableViewCell: UITableViewCell {

    
  //  @IBOutlet weak var imgVw: UIImageView!
    
    @IBOutlet weak var imgProfileBg: UIImageView!
    @IBOutlet weak var imgProfilePic: UIImageView!
    @IBOutlet weak var labName: UILabel!
    @IBOutlet weak var labEmail: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
