

let screenFrame = UIScreen.main.bounds
let floatingFrame = CGRect(x: screenFrame.size.width-80 , y: screenFrame.size.height-70, width: 60, height: 60)
let floatingActionButton = LiquidFloatingActionButton(frame: floatingFrame)





import UIKit
import SnapKit

class MainViewController: UIViewController, CAPSPageMenuDelegate {
    
    @IBAction func sideBarBtnClk(_ sender: Any) {
        slideMenuController()?.toggleLeft()
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var red4Vw: UIView!
    @IBOutlet weak var red2Vw: UIView!
    @IBOutlet weak var red3Vw: UIView!
    @IBOutlet weak var red1Vw: UIView!
    
    @IBOutlet var MainHeaderTitle: UILabel!
    
  
    
    
    
    
    var cells = [LiquidFloatingCell]()
    var pageMenu: CAPSPageMenu?
    var controllerArray = [UIViewController]()
    var selectedindex = 0
    
    override func viewDidLoad() {
        self.navigationController?.isNavigationBarHidden = true
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(initalizeController(notification:)), name: Notification.Name("initalizeController"), object: nil)
        makeControllerArray()
        let width = self.view.frame.size.width
        
        let color = UIColor(red: 25.0/255.0, green: 28.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        let parameters: [CAPSPageMenuOption] = [
            .MenuItemSeparatorWidth(0),
            .UseMenuLikeSegmentedControl(false),
            .MenuItemSeparatorPercentageHeight(0.1),
            .ScrollMenuBackgroundColor(color),
            .SelectedMenuItemLabelColor(UIColor.white),
            .UnselectedMenuItemLabelColor(UIColor.white),
            .BottomMenuHairlineColor(UIColor.red),
            .MenuItemWidth(width/5-18),
            .MenuHeight(40)
        ]
        
        
        
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x:0, y:64, width:self.view.frame.width, height: self.view.frame.height), pageMenuOptions: parameters)
        pageMenu?.applyImage()
        pageMenu?.delegate = self
        
        cellFactory()
        
        DispatchQueue.main.async {
            floatingActionButton.color = UIColor(red: 25.0/255.0, green: 28.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        }
        
        floatingActionButton.dataSource = self
        floatingActionButton.delegate = self
        DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
            self.view.addSubview(self.pageMenu!.view)
            self.view.addSubview(floatingActionButton)
            self.view.bringSubview(toFront: floatingActionButton)
        }
        
        
        UIApplication.shared.setStatusBarStyle(.lightContent, animated: true)
        floatingActionButton.isHidden = false
        
    }
    
    
    
   
    
   
    
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            self.pageMenu?.view.layoutSubviews()
            self.pageMenu?.view.layoutIfNeeded()
        }
    }
    
    func cellFactory() {
        cells.append(CustomLiquidFloatingCell(icon: UIImage(named: "vc_event")!, name: "Publish a event"))
        cells.append(CustomLiquidFloatingCell(icon: UIImage(named: "vc_post_pdf")!, name: "Post a Thought"))
        cells.append(CustomLiquidFloatingCell(icon: UIImage(named: "vc_post_pdf")!, name: "Post to MSBIRIA"))
    }
    
    
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    
    
    
    func makeControllerArray() {
//        let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
//        home.title = "MSBIRIA Timeline"
//        var image = UIImage(named: "ic_tab_feeds_selected")
//        home.navigationItem.titleView = UIImageView(image: image)
        
        let home = HomeTimelineViewController.GetHomeTimelineViewController()
        home.title = "MSBIRIA Timeline"
        var image = UIImage(named: "ic_tab_feeds_selected")
        home.navigationItem.titleView = UIImageView(image: image)

        
        
        image = UIImage(named: "vc_tab_organization_selected")
        let organise = self.storyboard?.instantiateViewController(withIdentifier: "Organization_CollViewViewController") as! Organization_CollViewViewController
        organise.title = "Organization"
         organise.navigationItem.titleView = UIImageView(image: image)
        
        image = UIImage(named: "ic_tab_channel_selected")
        let emptyVC = self.storyboard?.instantiateViewController(withIdentifier: "SubscribedChannel_VC") as! SubscribedChannel_VC
        emptyVC.title = "Subscribe Channel"
        emptyVC.navigationItem.titleView = UIImageView(image: image)
        
        
        image = UIImage(named: "ic_tab_explore_selected")
        let exploreCollection = self.storyboard?.instantiateViewController(withIdentifier: "ExploreCollectionView") as! ExploreCollectionView
        exploreCollection.title = "Explore"
        exploreCollection.navigationItem.titleView = UIImageView(image: image)
        
        
        image = UIImage(named: "vc_tab_national_selected")
        let emptyVC2 = self.storyboard?.instantiateViewController(withIdentifier: "OrganizationTimeLine") as! OrganizationTimeLine
        emptyVC2.title = "My Timeline"
        emptyVC2.navigationItem.titleView = UIImageView(image: image)
        
        controllerArray.append(home)
        controllerArray.append(organise)
        controllerArray.append(emptyVC)
        controllerArray.append(exploreCollection)
        controllerArray.append(emptyVC2)
        
         MainHeaderTitle.text = "MSBIRIA Timeline"
    }
    
    
    
   
    
    @objc func initalizeController(notification: Notification)
    {
        red1Vw.isHidden=true;
        red2Vw.isHidden=true;
        red3Vw.isHidden=true
        red4Vw.isHidden=true
        callPages(ifrom: isFromMenu)
    }
    
    
    // Open View Controller after click on menu section or items
    func callPages(ifrom:String)
    {
        switch ifrom
        {
        case "Profile" :
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ProfileViewTimeLine") as! ProfileViewTimeLine
            isForEdit = true;
            vc.profileID = for_uuid
           // self.present(vc, animated:true, completion:nil)
            self.navigationController?.pushViewController(vc, animated: true)
//
            
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewTimeLine") as! ProfileViewTimeLine
//            isForEdit = true
//            vc.profileID = for_uuid
//            // self.navigationController?.pushViewController(vc, animated: true)
//            self.present(vc, animated: true, completion: nil)
        case "Messenger" :
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Messenger_FriendList") as! Messenger_FriendList
             self.present(vc, animated: true, completion: nil)
        case "Favorites" :
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OrganizationTimeLine_VC") as! OrganizationTimeLine_VC
            vc.expID = "Favorites"
            vc.orgChannelName = "Favorites"
           // self.navigationController?.pushViewController(vc, animated: true)
            self.present(vc, animated: true, completion: nil)
        case "Admin" :
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Admin_VC") as! Admin_VC
            self.navigationController?.pushViewController(vc, animated: true)
        case "Setting" :
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Setting_VC") as! Setting_VC
            self.navigationController?.pushViewController(vc, animated: true)
        default:
//            let controller = HomeViewController(nibName:"HomeViewController",bundle: nil)
            let controller = HomeTimelineViewController.GetHomeTimelineViewController()
            addChildViewController(controller)
            controller.view.frame = CGRect(x: 0, y: 10, width: self.view.frame.size.width, height: self.view.frame.size.height-100)
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
        }
        
    }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    var PageIndex : Int = 0;
    @IBAction func btnSearch(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var pageName : String = "";
        var pageSearchName : String = "";
        if(PageIndex == 0 )
        {
           let vc = storyboard.instantiateViewController(withIdentifier: "SearchTimeLine_VC") as! SearchTimeLine_VC
            pageName = "OrganizationTimeLine"
            pageSearchName = "Type here to search "
            vc.isFromPage = pageName
            vc.searchHeader = pageSearchName
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if(PageIndex == 1 )
        {
            let vc = storyboard.instantiateViewController(withIdentifier: "SearchChannelUser") as! SearchChannelUser
            pageName = "OrganizationChannels"
            pageSearchName = "Type here to search channel"
            vc.isFromPage = pageName
            vc.searchHeader = pageSearchName
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if(PageIndex == 2 )
        {
           let  vc = storyboard.instantiateViewController(withIdentifier: "SearchChannelUser") as! SearchChannelUser
            pageName = "SubscribeChannels"
            pageSearchName = "Type here to search subscribed channel"
            vc.isFromPage = pageName
            vc.searchHeader = pageSearchName
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if(PageIndex == 3 )
        {
           let  vc = storyboard.instantiateViewController(withIdentifier: "SearchChannelUser") as! SearchChannelUser
             pageName = "ExploreChannels"
            pageSearchName = "Type here to search channel"
            vc.isFromPage = pageName
            vc.searchHeader = pageSearchName
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if(PageIndex == 4 )
        {
           let  vc = storyboard.instantiateViewController(withIdentifier: "SearchTimeLine_VC") as! SearchTimeLine_VC
            pageName = "AllOrganizationTimeLine"
            pageSearchName = "Type here to search"
            vc.isFromPage = pageName
            vc.searchHeader = pageSearchName
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    func willMoveToPage(controller: UIViewController, index: Int){
        print("willMoveToPage---\(index)")
        var PageTitle : String = ""
        if(index == 0)
        {
            PageTitle = "MSBIRIA Timeline"
            floatingActionButton.isHidden =  false
        }
        else if(index == 1)
        {
            PageTitle = "Organization"
            floatingActionButton.isHidden =  true
        }
        else if(index == 2)
        {
            PageTitle = "Subscribe Channels"
            floatingActionButton.isHidden =  true
        }
        else if(index == 3)
        {
            PageTitle = "Explore"
            floatingActionButton.isHidden =  true
        }
        else if(index == 4)
        {
            PageTitle = "My Timeline"
            floatingActionButton.isHidden =  false
        }
        MainHeaderTitle.text = PageTitle;
    }
    
    
    func didMoveToPage(controller: UIViewController, index: Int){
         print("didMoveToPage---\(index)")
        var PageTitle : String = ""
        PageIndex = index
        if(index == 0)
        {
            PageTitle = "MSBIRIA Timeline"
            floatingActionButton.isHidden =  false
        }
        else if(index == 1)
        {
            PageTitle = "Organization"
            floatingActionButton.isHidden = true
        }
        else if(index == 2)
        {
            PageTitle = "Subscribe Channels"
            floatingActionButton.isHidden =  true
        }
        else if(index == 3)
        {
            PageTitle = "Explore"
            floatingActionButton.isHidden =  true
        }
        else if(index == 4)
        {
            PageTitle = "My Timeline"
            floatingActionButton.isHidden =  false
        }
        MainHeaderTitle.text = PageTitle;
    }
    
    
  
    @IBAction func fourthTabClk(_ sender: Any) {
        red1Vw.isHidden=true;
        red2Vw.isHidden=true;
        red3Vw.isHidden=true
        red4Vw.isHidden=false
    }
    
    @IBAction func leftBtnClk(_ sender: Any) {
    }
    
    
    
}



extension MainViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}







extension MainViewController: LiquidFloatingActionButtonDelegate {
    func liquidFloatingActionButton(_ liquidFloatingActionButton: LiquidFloatingActionButton, didSelectItemAtIndex index: Int) {
        liquidFloatingActionButton.close()
       
         if index == 0
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EventListViewController") as! EventListViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if index == 1 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PostAThoughtVC") as! PostAThoughtVC
            vc.isFromPage = "Home"
            vc.isFromPageType = "UserPost"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if index == 2
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PostAThoughtVC") as! PostAThoughtVC
            vc.isFromPage = "Home"
            vc.isFromPageType = "ChannelPost"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
}




extension MainViewController: LiquidFloatingActionButtonDataSource {
    func numberOfCells(_ liquidFloatingActionButton: LiquidFloatingActionButton) -> Int {
        return cells.count
    }
    
    
    func cellForIndex(_ index: Int) -> LiquidFloatingCell {
        return cells[index]
    }
    
   
}


extension CAPSPageMenu {
    func applyImage(){
        for (index,controller) in self.controllerArray.enumerated() {
            let titleImage = controller.navigationItem.titleView as! UIImageView
            let imageView = UIImageView(image: titleImage.image)
            imageView.contentMode = .scaleAspectFit
            let item = self.menuItems[index]
            item.titleLabel?.isHidden = true
           // item.titleLabel?.text = item.titleLabel
            let frame = item.titleLabel?.frame
            imageView.frame = item.titleLabel!.frame
            item.addSubview(imageView)
        }
    }
}



    public class CustomLiquidFloatingCell : LiquidFloatingCell {
        var name: String = "sample"
        
        init(icon: UIImage, name: String) {
            self.name = name
            super.init(icon: icon)
        }
        required public init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        public override func setupView(_ view: UIView) {
            super.setupView(view)
            let label = UILabel()
            label.text = name
            label.textColor = UIColor.white
            label.backgroundColor = UIColor(red: 25.0/255.0, green: 28.0/255.0, blue: 118.0/255.0, alpha: 1.0)
            label.makeRoundCorner(4)
            label.font = UIFont(name: "Helvetica-Neue", size: 10)
            label.textAlignment = .center
            addSubview(label)
            label.snp_makeConstraints { make in
                make.left.equalTo(self).offset(-135)
                make.size.equalTo(CGSize(width: 130, height: 25))
                make.top.equalTo(self).offset(12)
            }
        }
    }


