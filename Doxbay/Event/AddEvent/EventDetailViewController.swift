//
//  EventDetailViewController.swift
//  Doxbay
//
//  Created by kavi yadav on 17/06/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import AVKit
import AVFoundation
import MobileCoreServices
import SwiftyJSON
import DKImagePickerController
import DatePickerDialog




class EventDetailViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate {
    
    var isFromPage : String = ""
    @IBOutlet weak var SpecialityLbl: UILabel!
    @IBOutlet weak var DegreeLbl: UILabel!
    @IBOutlet weak var NavTitleLbl: UILabel!
    
    @IBOutlet weak var InstituteHeightCons: NSLayoutConstraint!
    
    
    @IBOutlet weak var CollectionView: UICollectionView!
    @IBOutlet weak var ScrollView: UIScrollView!
    
  
    
    @IBOutlet weak var InstituteView: UIView!
    @IBOutlet weak var DegreeView: UIView!
    @IBOutlet weak var SpecialityView: UIView!
    @IBOutlet weak var Commentview: UIView!
    @IBOutlet weak var DateView: UIView!
    @IBOutlet weak var CityView: UIView!
   
   
    
    
    @IBOutlet weak var InstituteTxt: UITextField!
    @IBOutlet weak var SpecialityTxt: UITextField!
    @IBOutlet weak var DegreeTxt: UITextField!
    @IBOutlet weak var CountryTxt: UITextField!
    @IBOutlet weak var DateTxt: UITextField!
    @IBOutlet weak var CommentTxtView: UITextView!
    
    
   
   
  

    
   
    
   
    
    
    var NavTitle = String()
    var imageNameUploadArray = [Any]()
    var imagePicker = UIImagePickerController()
    let pickerController = DKImagePickerController()
    var imagePickerArray = [Any]()

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NavTitleLbl.text = NavTitle
        CollectionView.delegate = self
        CollectionView.dataSource = self
        CollectionView.isHidden = true
        CommentTxtView.delegate = self
        
        CommentTxtView.text = "Any thoughts on this event"
        CommentTxtView.textColor = UIColor.lightGray
         // self.InstituteHeightCons.constant = 0
        self.loadDetailView ()
        // Do any additional setup after loading the view.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setObjectInDict(){
        
    }
    
    
    //CollectionView Delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imageNameUploadArray.count
    }

    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ColCell", for: indexPath as IndexPath) as! AddImageCell

        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        let selectedImage = self.imageNameUploadArray[indexPath.item]as! NSDictionary
        let uploadedimage = selectedImage.value(forKey: "image") as? UIImage
        var image = UIImage()
        if uploadedimage != nil {
            image = uploadedimage!

            cell.imgOutlet.image = image

        }
        cell.DeleteButton.tag = indexPath.row
        return cell
    }
    //TEXTVIEW DELEGATES
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Any thoughts on this event"
            textView.textColor = UIColor.lightGray
        }
    }

    @IBOutlet weak var SetImage: UIButton!
    @IBAction func TapONImageButton(_ sender: Any) {
        self.imagePicker.delegate = self
        self.imagePicker.mediaTypes = [kUTTypeImage as String]
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Please select", message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            print("Cancel")
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: "Camera", style: .default)
        { _ in
            self.imagePicker.allowsEditing = false
            
            self.imagePicker.sourceType = .camera
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        actionSheetControllerIOS8.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Gallery", style: .default)
        { _ in
            
            self.pickerController.sourceType = .photo
            self.pickerController.assetType = .allPhotos
            self.pickerController.didSelectAssets = { (assets: [DKAsset]) in
                print("didSelectAssets")
                for asset in assets {
                    asset.fetchOriginalImage(true, completeBlock: { (pickedImage, info) in
                        print(pickedImage)
                        self.imagePickerArray.append(pickedImage)
                        let imageDic: [String:Any] = ["name":"image.png","image":pickedImage]
                        self.CollectionView.isHidden = false
                        
                        self.imageNameUploadArray.append(imageDic)
                        self.CollectionView.reloadData()
                        // self.tblVw.reloadData()
                    })
                }
            }
            self.present(self.pickerController, animated: true) {}
        }
        actionSheetControllerIOS8.addAction(deleteActionButton)
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }

    
    @IBAction func TapOnDeleteBtn(_ sender: UIButton) {
        let index = sender.tag
        imageNameUploadArray.remove(at: index)
        if imageNameUploadArray.count == 0 {
            CollectionView.isHidden = true
        }
        CollectionView.reloadData()
        
        
    }
    
    @IBOutlet weak var TapOnSubmit: UIButton!
    @IBAction func Submit(_ sender: Any) {

        var strEmployer_Degree : String = "";
        var strDesigination_Speciality : String = "";
        var strCityCountry : String = "";
        var strComments : String = "";
        var strDate : String = "";
        var strInstitute : String = "";
     
        
        let inputName = DegreeLbl.text!   //  Employer  , Degree
        let inputName2 = SpecialityLbl.text!   // Designation , Speciality
        
        
    strEmployer_Degree = DegreeTxt.text!    // use for get Employer   OR   degrees   impurt value
    strDesigination_Speciality = SpecialityTxt.text!// use for get desigination OR  speciality  imput value
    strCityCountry = CountryTxt.text!   // use for get city and country imput value
    strComments = CommentTxtView.text!   // use for get comment input value value
    strDate = DateTxt.text!    // use for get Date input value
    strInstitute = InstituteTxt.text!    //  use for get institute input value
        
   
        
        print("strComments-> \(strComments) ----- NavTitledata>-\(NavTitle) ----strDesigination_Speciality --> \(strDesigination_Speciality) ---- strInstitute-->\(strInstitute)-----strEmployer_Degree-->\(strEmployer_Degree)-----inputName2-->\(inputName2)-----strCityCountry-->\(strCityCountry)------strDate-->\(strDate)")
        
        
        if (strComments == "Any thoughts on this event")
        {
            //ShowToast.showNegativeMessage(message: "Please enter text");
            strComments = ""
            //return;
        }
        
       
       if (strFlag == "0")
        {
            if (strInstitute == "")
            {
                ShowToast.showNegativeMessage(message: "Please enter institute name");
                return;
            }
        }
        if (strFlag == "0" || strFlag == "1")
        {
            if (strEmployer_Degree == "")
            {
                ShowToast.showNegativeMessage(message: "Please enter employee name");
                return;
            }
        }
        if (strDesigination_Speciality == "")
        {
            ShowToast.showNegativeMessage(message: "Please enter designation");
            return;
        }
        if (strCityCountry == "")
        {
            ShowToast.showNegativeMessage(message: "Please enter city & country");
            return;
        }
        
        
        if (strDate == "")
        {
            ShowToast.showNegativeMessage(message: "Please set date of joining");
            return;
        }

        
        
        
      
        
        let enentTitle = (NavTitle).data(using: String.Encoding.utf8)
        let enentTitle_base64 = enentTitle!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        
        
        let empyoyeer_degree = (strEmployer_Degree).data(using: String.Encoding.utf8)
        let empyoyeer_degree_base64 = empyoyeer_degree!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        
        
        let designition_speciality = (strDesigination_Speciality).data(using: String.Encoding.utf8)
        let designition_speciality_base64 = designition_speciality!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        
        
        let institute = (strInstitute).data(using: String.Encoding.utf8)
        let institute_base64 = institute!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        
        let CityCountry = (strCityCountry).data(using: String.Encoding.utf8)
        let CityCountry_base64 = CityCountry!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        
        let DatedVal = (strDate).data(using: String.Encoding.utf8)
        let Date_base64 = DatedVal!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        

        let CommentVal = (strComments).data(using: String.Encoding.utf8)
        let Comments_base64 = CommentVal!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        
      
        
        var dataParam: [String:String] = ["":""];
        
        
        // EventListArray = ["Joined new Job","Left job","Started new clinic/hospital","Attended conference","Cleared an exam","Joined residency","Complete residency","Joined fellowship/Observership","Complete fellowship/Observership","Started Internhip","Complete Internhip","Moved abroad","Published paper/thesis/article","Started social service","Create your own event"]
        if (strFlag == "0")
        {
            dataParam = ["uuid" : for_uuid , "event": enentTitle_base64  , "event_heading": enentTitle_base64 , "title": institute_base64 , "title_heading":"",   "as_event": empyoyeer_degree_base64 , "as_heading":"" , "with_event": designition_speciality_base64 ,"with_heading":"", "at_event": CityCountry_base64 , "at_heading": "" , "on_event": Date_base64 , "comment":Comments_base64]
        }
        else if(strFlag == "1")
        {
            dataParam = ["uuid" : for_uuid , "event": enentTitle_base64  , "event_heading": enentTitle_base64 , "title": empyoyeer_degree_base64 , "title_heading":"",   "as_event": designition_speciality_base64 , "as_heading":"" , "with_event": "" ,"with_heading":"", "at_event": CityCountry_base64 , "at_heading": "" , "on_event": Date_base64 , "comment": Comments_base64]
        }
        else if (strFlag == "2")
        {
            dataParam = ["uuid" : for_uuid , "event": enentTitle_base64  , "event_heading": enentTitle_base64 , "title": designition_speciality_base64 , "title_heading":"",   "as_event": "" , "as_heading":"" , "with_event": "" ,"with_heading":"", "at_event": CityCountry_base64 , "at_heading": "" , "on_event": Date_base64 , "comment":Comments_base64]
        }
        else if (strFlag == "3")
        {
            ShowToast.showPositiveMessage(message: "Comming soon..")
            return
        }
        
        print(dataParam)
        
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Loading"
        
        var dataStr = ""
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dataParam, options: .prettyPrinted)
            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: [])
            if let dictFromJSON = decoded as? [String:String] {
                print(dictFromJSON)
                dataStr = String(describing: dictFromJSON)
            }
        } catch {
            print(error.localizedDescription)
        }
        
        dataStr = dataStr.replacingOccurrences(of: "[", with: "{")
        dataStr = dataStr.replacingOccurrences(of: "]", with: "}")
        
        let requestURL = BASEURL + createUserEvent
        let parameters: [String:String] = ["data":dataStr,"token": token]
        
        print("-----------POST THOUGHT START-----------------")
        print(" Request URL- \(requestURL)  Param --- \(parameters) ----")
        print(" Image Array========== -----\(imagePickerArray) ---")
        
        
        UCUtil.requestPOSTURLWithFormDataAndImageArrayAndPDFArray(requestURL, params: parameters, headers: nil,imageArray:imagePickerArray,pdfArray:[], success: {(response) -> Void in

            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--")

            if success
            {
                // self.callapi()
                self.CommentTxtView.text = "Any thoughts on this event"


            }
           // ShowToast.showPositiveMessage(message: response["message"].string!);
            self.imagePickerArray.removeAll()
            self.imageNameUploadArray.removeAll()
            self.CollectionView.isHidden = true
            self.CollectionView.reloadData()
            MBProgressHUD.hide(for: self.view, animated: false)
            
            if self.isFromPage == "profile"
            {
                self.dismiss(animated: true, completion: nil)
            }
            else
            {
                self.navigationController?.popViewController(animated: true)
            }
            

        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
        })

        
    }
    @IBAction func TapOnBack(_ sender: Any) {
        if isFromPage == "profile"
        {
            self.dismiss(animated: true, completion: nil)
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func TapOnDateBtn(_ sender: Any) {
        datePickerTapped()
    }
    func datePickerTapped() {
        let currentDate = Date()
        var dateComponents = DateComponents()
        dateComponents.month = -3
        let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
        
        let datePicker = DatePickerDialog(textColor: .black,
                                          buttonColor: .black,
                                          font: UIFont.boldSystemFont(ofSize: 17),
                                          showCancelButton: true)
        datePicker.show("",
                        doneButtonTitle: "Done",
                        cancelButtonTitle: "Cancel",
                        minimumDate: threeMonthAgo,
                        maximumDate: currentDate,
                        datePickerMode: .date) { (date) in
                            if let dt = date {
                                let formatter = DateFormatter()
                                formatter.dateFormat = "dd MMM yyyy"
                                self.DateTxt.text = formatter.string(from: dt)
                            }
        }
    }
    
    
    var strFlag : String = "";
    func loadDetailView ()
    {
        if (NavTitle == "Joined residency" || NavTitle == "Complete residency" || NavTitle == "Joined fellowship/Observership" || NavTitle == "Complete fellowship/Observership")
        {
            strFlag = "0"
            ScrollView.contentInset = UIEdgeInsets(top: 100,left: 0,bottom: 0,right: 0)
        }
        else if(NavTitle == "Joined new Job" || NavTitle == "Left job")
        {
            strFlag = "1"
            ScrollView.contentInset = UIEdgeInsets(top: 60,left: 0,bottom: 0,right: 0)
            InstituteHeightCons.constant = 0
            DegreeLbl.text = "Employer"
            SpecialityLbl.text = "Designation"
            
            DegreeTxt.placeholder = "Employer"
            SpecialityTxt.placeholder = "Designation"
        }
        else if(NavTitle == "Published paper/thesis/article")
        {
            strFlag = "3"
            ShowToast.showPositiveMessage(message: "Comming soon")
        }
        else
        {
            strFlag = "2"
            self.DegreeView.isHidden = true
            self.InstituteView.isHidden = true
            
            ScrollView.contentInset = UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
            InstituteHeightCons.constant = 0
            var strTitle : String =  ""
            if (NavTitle == "Started new clinic/hospital")
            {
                strTitle = "Name of clinic/Hospital"
            }
            else  if (NavTitle == "Attended conference")
            {
                strTitle = "Name of conference"
            }
            else  if (NavTitle == "Cleared an exam")
            {
                strTitle = "Which Exam?"
            }
            else  if (NavTitle == "Started Internhip" || NavTitle == "Complete Internhip")
            {
                strTitle = "Name of Institute"
            }
            else  if (NavTitle == "Moved abroad")
            {
                strTitle = "Institute Joined"
            }
            else  if (NavTitle == "Started social service")
            {
                strTitle = "Name of social organization"
            }
            else  if (NavTitle == "Create your own event")
            {
                strTitle = "Title of event"
            }
            SpecialityLbl.text = strTitle
            SpecialityTxt.placeholder = strTitle
        }
        
    }

}

class ShadowViewInEventDetail: UIView {
    override var bounds: CGRect {
        didSet {
            setupShadow()
        }
    }
    
    private func setupShadow() {
        //self.layer.cornerRadius = 8
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowRadius = 3
        self.layer.shadowOpacity = 0.3
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 8, height: 8)).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
}
