//
//  EventListViewController.swift
//  Doxbay
//
//  Created by kavi yadav on 17/06/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit

class EventListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    var EventListTypeArray = NSMutableArray ()
    var EventListArray : NSArray  = ["Joined new Job","Left job","Started new clinic/hospital","Attended conference","Cleared an exam","Joined residency","Complete residency","Joined fellowship/Observership","Complete fellowship/Observership","Started Internhip","Complete Internhip","Moved abroad","Published paper/thesis/article","Started social service","Create your own event"]
    
    
    
    var isFromPage : String =  "";
    @IBOutlet weak var eventTable: UITableView!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        eventTable.delegate = self
        eventTable.dataSource = self
 
        // Do any additional setup after loading the view.
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 15
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! EventListCell
        let eventName = EventListArray.object(at: indexPath.row) as! String
        let firstChar = String(eventName.characters.prefix(1))
        cell.firstCharacterLbl.text = firstChar
        cell.eventNameLbl.text = eventName

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let eventName = EventListArray.object(at: indexPath.row)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailViewController") as! EventDetailViewController
        vc.NavTitle = eventName as! String
        vc.isFromPage = isFromPage

        if isFromPage == "profile"
        {
            self.dismiss(animated: true, completion: nil)
            self.navigationController?.popViewController(animated: true)
            self.present(vc, animated:true, completion:nil)
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    @IBAction func back(_ sender: Any) {
        if isFromPage == "profile"
        {
            self.dismiss(animated: true, completion: nil)
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
}



class ShadowView: UIView {
    override var bounds: CGRect {
        didSet {
            setupShadow()
        }
    }
    
    private func setupShadow() {
        self.layer.cornerRadius = 8
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowRadius = 3
        self.layer.shadowOpacity = 0.3
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 8, height: 8)).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
}



class ShadowViewTimeLine: UIView {
    override var bounds: CGRect {
        didSet {
            setupShadow()
        }
    }
    
    private func setupShadow() {
        self.layer.cornerRadius = 5
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowRadius = 2
        self.layer.shadowOpacity = 0.3
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 5, height: 5)).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
}

