//
//  EventListCell.swift
//  Doxbay
//
//  Created by kavi yadav on 17/06/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit

class EventListCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var DataView: UIView!
    
    @IBOutlet weak var firstCharacterLbl: UILabel!
    
    
    @IBOutlet weak var eventNameLbl: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
