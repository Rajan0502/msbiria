//
//  ExploreChannelVC.swift
//  Doxbay
//
//  Created by Luminous on 09/05/18.
//  Copyright © 2018 Yuji Hato. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireImage
import MBProgressHUD

class ExploreChannelVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var expID : String = "";
    var ExploreChannelCatName = "";
    var expChData : [ExploreChannelListModal] = [];
    
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var headerTitle: UILabel!
    @IBAction func backClk(_ sender: Any)
    {
        // self.navigationController?.popViewController(animated: false)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //self.tableView.addSubview(self.refreshControl)
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.red
        return refreshControl
    }()
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        //self.getExploreChannels();
        refreshControl.endRefreshing()
    }
    
    
    
    @IBOutlet var searchExploreChannels: UIButton!
    @IBAction func searchExploreChannel(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchChannelUser") as! SearchChannelUser
        vc.isFromPage =  "ExploreChannelsList"
        vc.searchHeader =  "Type here to search channel " + ExploreChannelCatName
        vc.SearchID = expID;
        self.present(vc, animated:true, completion:nil)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self;
        tableView.dataSource = self
        self.tableView.addSubview(self.refreshControl)
        getExploreChannels();
        headerTitle.text = ExploreChannelCatName;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 95;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return expChData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExploreChannelListCellTableViewCell", for: indexPath) as! ExploreChannelListCellTableViewCell
        
        
//        cell.contentView.layer.borderWidth = 1.0
//        cell.contentView.layer.borderColor = UIColor.clear.cgColor
//        cell.contentView.layer.masksToBounds = true
//        cell.layer.shadowColor = UIColor.gray.cgColor
//        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
//        cell.layer.shadowRadius = 1.0
//        cell.layer.shadowOpacity = 1.0
//        cell.layer.masksToBounds = false
//        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
// cell.chanelName.text = Explore.name;
        
        let Explore : ExploreChannelListModal = expChData[indexPath.row];
        cell.chanelName.lineBreakMode = .byTruncatingMiddle
        cell.chanelName.lineBreakMode = .byWordWrapping
        cell.chanelName.numberOfLines = 0;
        cell.chanelName.text = Explore.name;
        let greet4Height = cell.chanelName.optimalHeight
        cell.chanelName.frame = CGRect(x: cell.chanelName.frame.origin.x, y: cell.chanelName.frame.origin.y, width: cell.chanelName.frame.width, height: greet4Height)
        
        cell.totalChannelPost.text = Explore.count_post + " Channel Post";
        if(Explore.photoUrl == nil)
        {
        }
        else
        {
            do {
                try   cell.channelImg.sd_setImage(with:URL(string: Explore.photoUrl), placeholderImage: UIImage(named: "vc_channel"))
            }catch {
                print("Unexpected error: \(error).")
            }
            
        }
        let tap = UITapGestureRecognizer(target: self, action: #selector(clickPos))
        cell.channelImg.addGestureRecognizer(tap)
        cell.channelImg.isUserInteractionEnabled = true
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let xyz : ExploreChannelListModal = expChData[(indexPath.row)]
        
        /*
         let signUp = self.storyboard?.instantiateViewController(withIdentifier: "ExploreChannelDetailsViewController") as! ExploreChannelDetailsViewController
         signUp.chanalID = xyz.id;
         signUp.expID = expID;
         self.navigationController?.pushViewController(signUp, animated: true)
         */
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ExploreChannelDetailsViewController") as! ExploreChannelDetailsViewController
        nextViewController.chanalID = xyz.id;
        nextViewController.expID = expID;
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    
    @objc func clickPos(_ sender: UITapGestureRecognizer)
    {
        let touch = sender.location(in: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: touch)
        let xyz : ExploreChannelListModal = expChData[(indexPath?.row)!]
        
        /*
         let signUp = self.storyboard?.instantiateViewController(withIdentifier: "ExploreChannelDetailsViewController") as! ExploreChannelDetailsViewController
         signUp.chanalID = xyz.id;
         signUp.expID = expID;
         self.navigationController?.pushViewController(signUp, animated: true)
         */
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ExploreChannelDetailsViewController") as! ExploreChannelDetailsViewController
        nextViewController.chanalID = xyz.id;
        nextViewController.expID = expID;
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    
    
    
    
    
    func getExploreChannels()
    {
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Loading"
        //MBProgressHUD.hide(for: self.view, animated: false)
        
        let requestURL =  BASEURL + getChannelList;
        let parameters: [String:String] = ["category_id": expID ,"token": token , "currentpage": String(self.pageNo)]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            MBProgressHUD.hide(for: self.view, animated: false)
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)-- \(response)")
            if success
            {
                let abc = response["data"].arrayObject;
                self.parse(json: response)
                
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
        })
    }
    
    func parse(json: JSON) {
        //self.expChData.removeAll();
        if(json["data"].arrayValue.count < 10)
        {
            isAvilableForLoad = false
        }
        for result in json["data"].arrayValue {
            let id = result["id"].stringValue
            let name = result["name"].stringValue
            let photoUrl = result["photoUrl"].stringValue
            let count_post = String(result["count_post"].intValue)
            let catNAme1 : ExploreChannelListModal = ExploreChannelListModal(id: id , name: name,  photoUrl: photoUrl, count_post: count_post , count_newpost: "");
            expChData.append(catNAme1);
        }
        
        self.tableView.reloadData();
        MBProgressHUD.hide(for: self.view, animated: false)
    }
    
    var pageNo : Int = 0;
    var loadingData = false;
    var isAvilableForLoad = true;
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = expChData.count - 3
        if !loadingData && indexPath.row == lastElement {
            loadingData = true
            loadMoreData()
        }
    }
    func loadMoreData() {
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                self.pageNo = self.pageNo + 1
                if(self.isAvilableForLoad)
                {
                    self.getExploreChannels();
                }
                self.loadingData = false
            }
        }
    }
    
}

class ExploreChannelListModal
{
    let id : String;
    let name: String;
    let photoUrl : String;
    let count_post: String;
    var count_newpost : String;
    
    init(id: String, name: String , photoUrl : String , count_post: String, count_newpost : String)
    {
        self.id = id;
        self.name = name;
        self.photoUrl = photoUrl;
        self.count_post = count_post;
        self.count_newpost = count_newpost;
    }
}

