//
//  AddSpecilityView.swift
//  DoxbaySalman
//
//  Created by AppsInvo  on 20/05/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit

class AddSpecilityView: UIView {
    @IBOutlet weak var txtSpeciality: UITextField!
    @IBAction func tapSave(_ sender: Any) {
    
        let params = ["speciality": txtSpeciality.text!, "no_of_year":"", "token": token, Constant.kAPI_NAME: addUserSpeciality]

        
        if(params["speciality"] == "")
        {
            ShowToast.showPositiveMessage(message: "Please enter your speciality")
        }
        else
        {
            SwiftLoader.show(title: "Loading...", animated: true)
       
            SGServiceController.instance().hitPostService(params, unReachable: {
                print("no internet")
                SwiftLoader.hide()
            }) { (response) in
                SwiftLoader.hide()
                if response == nil {
                    return
                }
                profileInstance.getProfile()
                UIView.animate(withDuration: 0.5, animations: {
                    self.alpha = 0.0
                }) { (complete) in
                    self.removeFromSuperview()
                }
            }
        }
    }
    
    @IBAction func tapCancel(_ sender: Any) {
        UIView.animate(withDuration: 0.5, animations: {
            self.alpha = 0.0
        }) { (complete) in
            self.removeFromSuperview()
        }
    }
    
    
}
