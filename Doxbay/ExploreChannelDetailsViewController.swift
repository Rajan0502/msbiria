//
//  ExploreChannelDetailsViewController.swift
//  Doxbay
//
//  Created by Luminous on 10/05/18.
//  Copyright © 2018 Yuji Hato. All rights reserved.
//

import UIKit
import SwiftyJSON
import AlamofireImage
import Alamofire
import MBProgressHUD

class ExploreChannelDetailsViewController: UIViewController {
    
    var chanalID : String = "";
    var expID : String = ""
    
    
    
    @IBOutlet var chImageView: UIImageView!
    @IBOutlet var chName: UILabel!
    @IBOutlet var chDescription: UITextView!
    @IBOutlet var chinviteURl: UILabel!
    @IBOutlet var chPostCount: UILabel!
    @IBOutlet var chUserCount: UILabel!
    @IBOutlet var lableHeaderBarTitle: UILabel!
    @IBOutlet var shareUIView: UIView!
    @IBOutlet var lableSubscribeUnScribe: UIButton!
    @IBOutlet var subscriberUserUIView: UIView!
    @IBOutlet var userPostUIView: UIView!
    @IBOutlet var subscribeUSerImg: UIImageView!
    
    
    @IBAction func btnBackClick(_ sender: Any) {
        // self.navigationController?.popViewController(animated: false)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    var subscribe = "";
    var admin = "";
    @IBAction func btnSubscribeUnSubscribeChannel(_ sender: Any) {
        if(admin == "0")
        {
            if(self.is_subscribed == "2")
            {
                subscribe = "0"
            }
            else{
                subscribe = "2"
            }
        }
        else
        {
            if(is_subscribed == "0" || is_subscribed == "")
            {
                subscribe = "1"
            }
            else if (is_subscribed == "1")
            {
                ShowToast.showPositiveMessage(message: "You have already sent the subscription request for this channel")
                return
            }
            else if(is_subscribed == "2")
            {
                subscribe = "0"
            }
        }
        
        let requestURL = BASEURL + subsribeUnsubscribeChannel;
        let parameters: [String:String] = ["channel_id": chhID ,"token": token , "is_subscribe" : subscribe]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            MBProgressHUD.hide(for: self.view, animated: false)
            let success = response["success"].boolValue
            let message = response["message"].stringValue
            print("xxxxxxxx   ---- \(success)-- \(response)")
            if success
            {
                if(self.admin == "0")
                {
                    if(self.is_subscribed == "2")
                    {
                        self.is_subscribed = "0"
                        self.lableSubscribeUnScribe.setTitle("Subscribe Now",for: .normal)
                    }
                    else{
                        self.is_subscribed = "2"
                        self.lableSubscribeUnScribe.setTitle("UnSubscribe",for: .normal)
                    }
                }
                else
                {
                    if(self.is_subscribed == "0" || self.is_subscribed == "")
                    {
                        self.is_subscribed = "1"
                        self.lableSubscribeUnScribe.setTitle("Subscribe Request Sent",for: .normal)
                    }
                    else if(self.is_subscribed == "2")
                    {
                        self.is_subscribed = "0"
                        self.lableSubscribeUnScribe.setTitle("Subscribe Now",for: .normal)
                    }
                }
                
                ShowToast.showPositiveMessage(message: message)
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
        })
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(clickPos))
        self.shareUIView.addGestureRecognizer(tap)
        self.shareUIView.isUserInteractionEnabled = true
        
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(clickPosSubs))
        self.subscriberUserUIView.addGestureRecognizer(tap2)
        self.subscriberUserUIView.isUserInteractionEnabled = true
        self.subscribeUSerImg.addGestureRecognizer(tap2)
        self.subscribeUSerImg.isUserInteractionEnabled = true
        
        
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(clickPosPost))
        self.userPostUIView.addGestureRecognizer(tap3)
        self.userPostUIView.isUserInteractionEnabled = true
        
        
        getExploreListData()
    }
    
    
    @objc func clickPosSubs(_ sender: UITapGestureRecognizer)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ChannelSubscriberList_VC") as! ChannelSubscriberList_VC
        vc.chanalId = chhID;
        self.present(vc, animated:true, completion:nil)
    }
    
    
    
    @objc func clickPosPost(_ sender: UITapGestureRecognizer)
    {
        if(self.is_subscribed == "2")
        {
            print("chanalID --- \(chanalID) ---- expID - \(expID)");
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ChannelPostTM_VC") as! ChannelPostTM_VC
            vc.expID = chhID;
            vc.orgChannelName = chhName;
            vc.orgChannelImgLogo = photoUrl;
            self.present(vc, animated:true, completion:nil)
        }
        else{
            ShowToast.showPositiveMessage(message: "Plesae subscribe the channel.")
        }
    }
    
    
    @objc func clickPos(_ sender: UITapGestureRecognizer)
    {
        let text = "https://app.doxbay.com/channels/\(chanalID)"
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    func getExploreListData()
    {
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Loading"
        //MBProgressHUD.hide(for: self.view, animated: false)
        
        let requestURL = BASEURL + getExpChannel;
        let parameters: [String:String] = ["id": chanalID ,"token": token]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            MBProgressHUD.hide(for: self.view, animated: false)
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)-- \(response)")
            if success
            {
                let abc = response["data"].arrayObject;
                self.parse(json: response)
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
        })
    }
    
    
    
    
    
    var is_subscribed = "";
    var chhID = "" ; var chhName = "" , photoUrl = "";
    func parse(json: JSON) {
        chhName = json["data"]["name"].stringValue
        let description = json["data"]["description"].stringValue
        photoUrl = json["data"]["photoUrl"].stringValue
        let count_post = json["data"]["count_post"].stringValue
        let count_subscribers = json["data"]["count_subscribers"].stringValue
        is_subscribed = json["data"]["is_subscribed"].stringValue;
        chhID = json["data"]["id"].stringValue;
        admin = json["data"]["admin"].stringValue;
        
        chName.text = chhName;
        lableHeaderBarTitle.text = chhName;
        chDescription.text = description;
        chPostCount.text = count_post + " Post";
        chUserCount.text = count_subscribers + " Users"
        
        
        
        
        
        chinviteURl.text = "https://app.doxbay.com/channels/\(chanalID)"
        if(is_subscribed == "0" || is_subscribed == "")
        {
            self.lableSubscribeUnScribe.setTitle("Subscribe Now",for: .normal)
        }
        else if (is_subscribed == "1")
        {
            self.lableSubscribeUnScribe.setTitle("Subscribe Request sent",for: .normal)
        }
        else if(is_subscribed == "2")
        {
            self.lableSubscribeUnScribe.setTitle("Unscribe Now",for: .normal)
        }
        if(!photoUrl.isEmpty)
        {
            do {
                try   self.chImageView.sd_setImage(with:URL(string:  photoUrl ), placeholderImage: UIImage(named: "vc_channel"))
            }catch {
                print("Unexpected error: \(error).")
            }
            
        }
        MBProgressHUD.hide(for: self.view, animated: false)
    }
    
}



