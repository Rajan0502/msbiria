//
//  CellExperience.swift
//  DoxbaySalman
//
//  Created by AppsInvo  on 20/05/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit

class CellExperience: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesignation: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblName: UILabel!
    var id = ""
    @IBOutlet weak var btnDelete: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    
    func configureView(info: [String: Any]) {
        btnDelete.isHidden = !isForEdit
        id = info["id"] as? String ?? ""
        if let isCurrent = info["is_current"] as? String, isCurrent == "Working" {
            lblDate.text = "\(info["start_year"]!) to Pressent"
        }
        else {
            lblDate.text = "\(info["start_year"]!) to \(info["end_year"]!)"
        }
        let firstCharacter = info["hospital_name"] as! String
        lblTitle.text = "\(firstCharacter.characters.first!)"
        lblDesignation.text = info["designation_name"] as? String
        lblName.text = info["hospital_name"] as? String
        
    }
    
    @IBAction func tapDelete(_ sender: Any) {
        profileInstance.showOkCancelAlertWithAction("Are you sure delete?") { (isOk) in
            if isOk {
                let params = ["id":self.id, "token": token, Constant.kAPI_NAME: deleteUserJob]
                SGServiceController.instance().hitPostService(params, unReachable: {
                    print("Not reachable")
                }) { (response) in
                    print(response)
                    if let index = profileInstance.tableView.indexPath(for: self) {
                        print(index.section)
                        print(index.row)
                        profileInstance.tableView.beginUpdates()
                        profileInstance.arrjobDetail.remove(at: index.row)
                        profileInstance.tableView.deleteRows(at: [index], with: .none)
                        profileInstance.tableView.endUpdates()
                        
                    }
                }
            }
            
        }
        
    
    }
    
    
    
}
