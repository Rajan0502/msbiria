//
//  OTLModal.swift
//  Doxbay
//
//  Created by Luminous on 11/05/18.
//  Copyright © 2018 Yuji Hato. All rights reserved.
//

import Foundation

struct OTLModal {
    let id: String
    let title: String
    let description : String
    let category: String
    let pdf: String;
    let pdf_name : String;
    let image:String
    let video : String;
    let video_thumb : String
    let video_url : String
    let video_url_oe : String
    let time : String
    var count_like : String
    let count_comment : String;
    var is_favorite:String;
    var is_liked : String;
    let content_type : String
    let userID : String
    let userName : String
    let userImage:String
    let channel_name : String
    let channel_id : String
    
    init(id : String, title: String , description : String,  category : String , pdf: String , pdf_name: String, image : String , video : String , video_thumb: String, video_url: String , video_url_oe :String , time:String , count_like : String , count_comment : String , is_favorite:String , is_liked : String , content_type: String , userID : String , userName: String , userImage: String , channel_name : String , channel_id : String)
    {
        self.id = id
        self.title = title
        self.description = description
        self.category = category
        self.pdf = pdf
        self.pdf_name = pdf_name
        self.image = image;
        self.video = video
        self.video_thumb = video_thumb
        self.video_url = video_url;
        self.video_url_oe = video_url_oe;
        self.time = time;
        self.count_like = count_like;
        self.count_comment = count_comment;
        self.is_favorite = is_favorite;
        self.is_liked = is_liked;
        self.content_type = content_type;
        self.userID = userID
        self.userName = userName
        self.userImage = userImage;
        self.channel_name =  channel_name
        self.channel_id = channel_id;
    }
}


struct ReportResaonList {
    let id: String
    let name: String
    init(id : String , name : String)
    {
        self.id = id
        self.name = name
    }
}

