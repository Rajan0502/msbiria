//
//  Home_New_VC.swift
//  DoxbaySalman
//
//  Created by Luminous on 26/05/18.
//  Copyright © 2018 Luminous. All rights reserved.
//


var is_subscribeStatus : String = "";
import UIKit
import MBProgressHUD
import SDWebImage
import AVKit
import AVFoundation
import MobileCoreServices
import Alamofire
import SwiftyJSON



import UIKit

class HomeViewController: UIViewController ,UITextViewDelegate, UITableViewDataSource,UITableViewDelegate,UINavigationControllerDelegate{
    
    var postContentType = String ()
    var post_id = String ()
    var User_UUid = String ()
    var channelID = String ()

    @IBOutlet var reportView: UIView!
    @IBOutlet weak var reportTable: UITableView!
    @IBOutlet var tableView: UITableView!
    
    var timeLimeData : [OTLModal] = []
    
    var refreshControl = UIRefreshControl()
   
    
    var profileDataArray = [Any]()
    var ReportListDataArray = [Any]()

    
    // 1
    var indexOfCellToExpand: Int?
    
    //3
    var isShow : Bool = false;
    var currentExpendRow : Int = -1;
    var expandedRows = Set<Int>()
    
    var isForReloadIndex : Int  = -1
    
    
    
    let sampleTextField =  UILabel(frame: CGRect(x: 10, y: 200, width: UIScreen.main.bounds.size.width - 20 , height: 50))
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
        
        sampleTextField.font = UIFont.systemFont(ofSize: 30)
        sampleTextField.textColor = UIColor.black
        sampleTextField.textAlignment = .center
        sampleTextField.lineBreakMode = .byTruncatingMiddle
        sampleTextField.lineBreakMode = .byWordWrapping
        sampleTextField.numberOfLines = 0;
        sampleTextField.text = "Your subscription request send to admin, Please wait for admin confirmation. Thanks"
        let greet4Height = sampleTextField.optimalHeight
        sampleTextField.frame = CGRect(x: sampleTextField.frame.origin.x, y: sampleTextField.frame.origin.y, width: sampleTextField.frame.width, height: greet4Height)
        self.view.addSubview(sampleTextField)
        sampleTextField.isHidden = true
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tag=1;
        self.tableView.reloadData()
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        self.hideKeyboardOnTap(#selector(self.dismissKeyboard))
        
        
        
        if((UserDefaults.standard.string(forKey: "is_subscribe")) != nil)
        {
            is_subscribeStatus = UserDefaults.standard.string(forKey: "is_subscribe")!
        }
        print("----is_subscribeStatus \(is_subscribeStatus)")
       
         self.getSubCribeChhanelStatus();
        
//        if(is_subscribeStatus.isEmpty || is_subscribeStatus == "" || is_subscribeStatus == "0")
//        {
//            subCribeChhanel();
//        }
//        else
//        {
//            getSubCribeChhanelStatus();
//        }

       
      //  self.showAttDetails();
        
    }
    
    
   
    func showAttDetails()
    {
        
        PopupController
            .create(self)
            .customize(
                [
                    .animation(.slideUp),
                    .scrollable(false),
                    .backgroundStyle(.blackFilter(alpha: 0.7))
                ]
            )
            .didShowHandler { popup in
                print("showed popup!")
            }
            .didCloseHandler { _ in
                print("closed popup!")
            }
            .show(DemoPopupViewController.instance())
    }
    
    
    
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
        // do aditional stuff
    }
    
    @objc func refresh(sender:AnyObject)
    {
        refreshControl.endRefreshing()
        getTimeLineData()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.parent?.view.frame = CGRect(x:0, y:64, width:self.view.frame.width, height: UIScreen.main.bounds.size.height-64)
            // floatingActionButton.isHidden = false
            
            if(self.isForReloadIndex > 0)
            {
                self.getTimeLineData();
                self.tableView.scrollToRow(at: IndexPath(row: self.isForReloadIndex, section: 0), at: .top, animated: true)
                self.isForReloadIndex = -1
            }
        }
        
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func subCribeChhanel()
    {
        let requestURL = BASEURL + subsribeUnsubscribeChannel;
        let parameters: [String:String] = ["channel_id": channel_id ,"token": token,"is_subscribe": "1"]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            print("subsribeUnsubscribeChannel Request   ---- \(response)--")
            
            if success
            {
                UserDefaults.standard.set("1", forKey: "is_subscribe")
                is_subscribeStatus = "1"
                
                //self.getSubCribeChhanelStatus()
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
        })
    }
    
    func getSubCribeChhanelStatus()
    {
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Loading"
        let requestURL = BASEURL + getExpChannel;
        let parameters: [String:String] = ["id": channel_id ,"token": token]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            MBProgressHUD.hide(for: self.view, animated: false)
            let success = response["success"].boolValue
            
            print("get Subscribe channel Status   ---- \(success)-- \(response)")
            if success
            {
                let abc = response["data"].arrayObject;
                
                is_subscribeStatus = response["data"]["is_subscribed"].stringValue;
                UserDefaults.standard.set(is_subscribeStatus, forKey: "is_subscribe")
                
                if(is_subscribeStatus == "2")
                {
                    self.getTimeLineData();
                }
                else{
                    self.subCribeChhanel();
                    self.tableView.isHidden = true
                    self.sampleTextField.isHidden = false
                    
                }
            }
            else{
                
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
        })
    }
  
    func getUserDetails()
    {
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Loading"
        //MBProgressHUD.hide(for: self.view, animated: false)
        let requestURL = BASEURL + getUser;
        let parameters: [String:String] = ["for_uuid": for_uuid ,"token": token]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--")
            
            if success
            {
                userPhotoUrl = response["data"]["photoUrl"].stringValue
                UserDefaults.standard.set(userPhotoUrl, forKey: "userPhotoUrl")
                print("userPhotoUrl----\(userPhotoUrl)")
            }
            // self.getTimeLineData();
            MBProgressHUD.hide(for: self.view, animated: false)
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
        })
    }
  
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == reportTable {
            return ReportListDataArray.count
        }else{
        return timeLimeData.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == reportTable {
            return 40
        }
        else{
        let rowData : OTLModal = timeLimeData[indexPath.row]
        let isVideoImgPdf = checkForImgVideoPdf(index : indexPath.row)
        
        if (isVideoImgPdf)
        {
            if let _ = indexOfCellToExpand, indexPath.row == indexOfCellToExpand
            {
                let xxx =  Utility.heightForView(text: rowData.title) + 190 ;
                return   xxx
            }
            else
            {
                return  440;
            }
        }
        else
        {
            if let _ = indexOfCellToExpand,  indexPath.row == indexOfCellToExpand
            {
                let xxx =  Utility.heightForView(text: rowData.title);
                return   xxx
            }
            else
            {
                return 230
            }
        }
        }
    }
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == reportTable {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! reportCell
            let reportdict = ReportListDataArray[indexPath.row] as! NSDictionary
            print(reportdict)
            let reasonName = reportdict["name"]
            cell.reasonLbl.text = reasonName as? String

            return cell

        }else{
        
        
        let rowData : OTLModal = timeLimeData[indexPath.row]
        let isVideoImgPdf = checkForImgVideoPdf(index : indexPath.row)
        
        if (isVideoImgPdf)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TM_Image_Cell") as! TM_Image_Cell
            var titleStr = rowData.title
            titleStr = titleStr.trimmingCharacters(in: .whitespacesAndNewlines)
            if let _ = indexOfCellToExpand, indexPath.row == indexOfCellToExpand
            {
                cell.postDescription.attributedText = Utility.getHTMLToString(titleStr: titleStr);
            }
            else
            {
                if((titleStr.count)>350)
                {
                    titleStr = (titleStr.substring(with: 0..<350)) + "...<strong><font color=red>Read More</font></strong>"
                }
                cell.postDescription.attributedText =  Utility.getHTMLToString(titleStr: titleStr);
            }
            
            
            cell.postDescription.tag = indexPath.row
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.expandCell(sender:)))
            cell.postDescription.addGestureRecognizer(tap)
            cell.postDescription.isUserInteractionEnabled = true
            
            
            cell.userName.text = rowData.userName
            cell.userPTitle.text = rowData.channel_name
            let miliSecondsTime:Double? = Double(rowData.time)
            let date = Date(timeIntervalSince1970: (miliSecondsTime! / 1000.0))
            
            cell.userPostTypeDateTime.attributedText =  Utility.getHTMLToString(titleStr: "<strong><font color=red>" + rowData.category + " </font></strong>" + date.getElapsedInterval() + " ago")
            
            
            cell.likeCount.text = rowData.count_like + " Likes"
            cell.commentCount.text = rowData.count_comment + " Comments"
            if(rowData.is_liked == "0")
            {
                cell.imgIsPostLike.image =  UIImage(named: "vc_like_false")
            }
            else
            {
                cell.imgIsPostLike.image =  UIImage(named: "vc_like_true")
            }
            if(rowData.is_favorite == "0")
            {
                cell.imgIsFavorites.image =  UIImage(named: "vc_favorite_normal")
            }
            else
            {
                cell.imgIsFavorites.image =  UIImage(named: "vc_favorite")
            }
            
            
            
            cell.userProfilePic.layer.cornerRadius =  (cell.userProfilePic.frame.size.width) / 2;
            cell.userProfilePic.layer.masksToBounds = true
            
            cell.userProfilePic.sd_setImage(with: URL(string: String(rowData.userImage)), placeholderImage: UIImage(named: "vc_user"))
            let tapProfile = UITapGestureRecognizer(target: self, action: #selector(self.viewUserProfile(sender:)))
            cell.userProfilePic.addGestureRecognizer(tapProfile)
            cell.userProfilePic.isUserInteractionEnabled = true
            cell.userProfilePic.tag = indexPath.row
            
            
            
            let postimg = rowData.image.split(separator: ",")
            if (postimg.count > 0)
            {
                cell.postImage.sd_setImage(with: URL(string: String(postimg[0])), placeholderImage: UIImage(named: "vc_ops_gallery"))
                let tapImg = UITapGestureRecognizer(target: self, action: #selector(viewImage))
                cell.postImage.addGestureRecognizer(tapImg)
                cell.postImage.isUserInteractionEnabled = true
                cell.postImage.tag = indexPath.row
                cell.vc_Play.isHidden = true
            }
            
            if (rowData.video_thumb.count > 0)
            {
                cell.vc_Play.isHidden = false
                let video_thumb = rowData.video_thumb.split(separator: ",")
                if (video_thumb.count > 0)
                {
                    cell.postImage.sd_setImage(with: URL(string: String(video_thumb[0])), placeholderImage: UIImage(named: "vc_play_circle_trans"))
                    let tapImg = UITapGestureRecognizer(target: self, action: #selector(plsyPostVideo))
                    cell.postImage.addGestureRecognizer(tapImg)
                    cell.postImage.isUserInteractionEnabled = true
                    cell.postImage.tag = indexPath.row
                }
                else
                {
                    cell.postImage.image = UIImage(named: "vc_play_circle_trans");
                    let tapImg = UITapGestureRecognizer(target: self, action: #selector(plsyPostVideo))
                    cell.postImage.addGestureRecognizer(tapImg)
                    cell.postImage.isUserInteractionEnabled = true
                    cell.postImage.tag = indexPath.row
                }
            }
            
            let postPdf = rowData.pdf.split(separator: ",")
            if (postPdf.count > 0)
            {
                cell.vc_Play.isHidden = false
                cell.postImage.image = UIImage(named: "ic_type_pdf");
                let tapImg = UITapGestureRecognizer(target: self, action: #selector(viewPDFFile(_:)))
                cell.postImage.addGestureRecognizer(tapImg)
                cell.postImage.isUserInteractionEnabled = true
                cell.postImage.tag = indexPath.row
            }
            
            
            
            
            
            let tapLike = UITapGestureRecognizer(target: self, action: #selector(self.btnLikePost(sender:)))
            cell.imgIsPostLike.addGestureRecognizer(tapLike)
            cell.imgIsPostLike.isUserInteractionEnabled = true
            cell.imgIsPostLike.tag = indexPath.row
            
            
            
            let tapComment = UITapGestureRecognizer(target: self, action: #selector(self.btnPostViewComment(sender:)))
            cell.imgPostComment.addGestureRecognizer(tapComment)
            cell.imgPostComment.isUserInteractionEnabled = true
            cell.imgPostComment.tag = indexPath.row
            
            cell.commentCount.tag = indexPath.row
            let tapCom = UITapGestureRecognizer(target: self, action: #selector(self.ViewComments(sender:)))
            cell.commentCount.addGestureRecognizer(tapCom)
            cell.commentCount.isUserInteractionEnabled = true
            
            
            let tapfavv = UITapGestureRecognizer(target: self, action: #selector(self.clickisFav(sender:)))
            cell.imgIsFavorites.addGestureRecognizer(tapfavv)
            cell.imgIsFavorites.isUserInteractionEnabled = true
            cell.imgIsFavorites.tag = indexPath.row
            
            
            let tapReport = UITapGestureRecognizer(target: self, action: #selector(self.clickisReport(sender:)))
            cell.imgReport.addGestureRecognizer(tapReport)
            cell.imgReport.isUserInteractionEnabled = true
            cell.imgReport.tag = indexPath.row

            
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TLN_Cell") as! TLN_Cell
            var titleStr = rowData.title
            titleStr = titleStr.trimmingCharacters(in: .whitespacesAndNewlines)
            if let _ = indexOfCellToExpand, indexPath.row == indexOfCellToExpand
            {
                cell.postDescription.attributedText = Utility.getHTMLToString(titleStr: titleStr);
            }
            else
            {
                if((titleStr.count)>350)
                {
                    titleStr = (titleStr.substring(with: 0..<350)) + "...<strong><font color=red>Read More</font></strong>"
                }
                cell.postDescription.attributedText =  Utility.getHTMLToString(titleStr: titleStr);
            }
            
            cell.postDescription.tag = indexPath.row
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.expandCell(sender:)))
            cell.postDescription.addGestureRecognizer(tap)
            cell.postDescription.isUserInteractionEnabled = true
            
            
            cell.userName.text = rowData.userName
            cell.userPTitle.text = rowData.channel_name
            let miliSecondsTime:Double? = Double(rowData.time)
            let date = Date(timeIntervalSince1970: (miliSecondsTime! / 1000.0))
            
            cell.userPostTypeDateTime.attributedText =  Utility.getHTMLToString(titleStr: "<strong><font color=red>" + rowData.category + " </font></strong>" + date.getElapsedInterval() + " ago")
            
            
            cell.likeCount.text = rowData.count_like + " Likes"
            cell.commentCount.text = rowData.count_comment + " Comments"
            if(rowData.is_liked == "0")
            {
                cell.imgIsPostLike.image =  UIImage(named: "vc_like_false")
            }
            else
            {
                cell.imgIsPostLike.image =  UIImage(named: "vc_like_true")
            }
            if(rowData.is_favorite == "0")
            {
                cell.imgIsFavorites.image =  UIImage(named: "vc_favorite_normal")
            }
            else
            {
                cell.imgIsFavorites.image =  UIImage(named: "vc_favorite")
            }
            
            
            
            cell.userProfilePic.layer.cornerRadius =  (cell.userProfilePic.frame.size.width) / 2;
            cell.userProfilePic.layer.masksToBounds = true
            
            cell.userProfilePic.sd_setImage(with: URL(string: String(rowData.userImage)), placeholderImage: UIImage(named: "vc_user"))
            
            let tapProfile = UITapGestureRecognizer(target: self, action: #selector(self.viewUserProfile(sender:)))
            cell.userProfilePic.addGestureRecognizer(tapProfile)
            cell.userProfilePic.isUserInteractionEnabled = true
            cell.userProfilePic.tag = indexPath.row
            
            let tapLike = UITapGestureRecognizer(target: self, action: #selector(self.btnLikePost(sender:)))
            cell.imgIsPostLike.addGestureRecognizer(tapLike)
            cell.imgIsPostLike.isUserInteractionEnabled = true
            cell.imgIsPostLike.tag = indexPath.row
            
            let tapComment = UITapGestureRecognizer(target: self, action: #selector(self.btnPostViewComment(sender:)))
            cell.imgPostComment.addGestureRecognizer(tapComment)
            cell.imgPostComment.isUserInteractionEnabled = true
            cell.imgPostComment.tag = indexPath.row
            
            cell.commentCount.tag = indexPath.row
            let tapCom = UITapGestureRecognizer(target: self, action: #selector(self.ViewComments(sender:)))
            cell.commentCount.addGestureRecognizer(tapCom)
            cell.commentCount.isUserInteractionEnabled = true
            
            let tapfavv = UITapGestureRecognizer(target: self, action: #selector(self.clickisFav(sender:)))
            cell.imgIsFavorites.addGestureRecognizer(tapfavv)
            cell.imgIsFavorites.isUserInteractionEnabled = true
            cell.imgIsFavorites.tag = indexPath.row
            
            let tapReport = UITapGestureRecognizer(target: self, action: #selector(self.clickisReport(sender:)))
            cell.imgReport.addGestureRecognizer(tapReport)
            cell.imgReport.isUserInteractionEnabled = true
            cell.imgReport.tag = indexPath.row

            return cell
        }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if tableView == reportTable {
            let reportdict = ReportListDataArray[indexPath.row] as! NSDictionary
            print(reportdict)
            let reasonId = reportdict["id"]
            self.submitReport(ReasonId: reasonId as! String)
        }
    }
    
    
    @objc func clickisReport(sender: UITapGestureRecognizer!)
    {
        let touch = sender.location(in: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: touch)
        let indx : Int = (indexPath?.row)!
        postContentType = timeLimeData[indx].content_type
        post_id = timeLimeData[indx].id
        User_UUid = timeLimeData[indx].userID
        channelID = timeLimeData[indx].channel_id

        let requestURL = BASEURL +  getUserReportReasonList
        let parameters: [String:String] = ["token": token ]
        
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--")
            
            if success
            {
               // self.ReportListDataArray = response["data"].arrayObject!
               // print(self.ReportListDataArray)
//                self.reportView.frame = CGRect(x: 0, y: self.view.frame.origin.y-35, width:self.view.frame.size.width, height: self.view.frame.size.height)
//                self.view.addSubview(self.reportView)
//                self.reportTable.reloadData()
                 self.showReportResion(response: response);
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
        })
    }
    
    
    
    var actions : [(String, UIAlertActionStyle)] = []
    var reportList : [ReportResaonList] = []
    func showReportResion(response : JSON)
    {
        self.actions  = []
        for result in response["data"].arrayValue
        {
            let id = result["id"].stringValue
            let nameVal = result["name"].stringValue
            let re : ReportResaonList = ReportResaonList(id: id , name: nameVal);
            reportList.append(re);
            self.actions.append((nameVal , UIAlertActionStyle.destructive))
        }
        actions.append(("Dismiss", UIAlertActionStyle.cancel))
        
        
        Alerts.showActionsheet(viewController: self, title: "DoxBay MSBIRIA", message: "Please select user report reason", actions: actions) { (index) in
            print("call action \(index)")
            if(index < self.reportList.count)
            {
                let reasonID = self.reportList[index].id
                self.reportUserNow(ReasonId: reasonID );
            }
        }
    }
    func reportUserNow(ReasonId : String)
    {
        SwiftLoader.show(title: "Loading..", animated: true)
        var requestURL = ""
        var parameters = [String:String]()
        parameters = ["token": token ,"post_id": post_id , "report_reason_id" : ReasonId , "for_uuid" : User_UUid ,"channel_id":channel_id]
        
        if postContentType == "user_post" {
            requestURL = BASEURL + reportUserPost
        }
        else if postContentType == "channel_post" {
            requestURL = BASEURL + reportChannelPost
        }
        else if(postContentType == "user_event")
        {
            requestURL = BASEURL + reportUserEvent
        }
        
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--response --\(response)")
            SwiftLoader.hide()
            if success
            {
                ShowToast.showPositiveMessage(message: "Successfully reported")
            }
            else
            {
                ShowToast.showPositiveMessage(message: "Somthing went worng, Please try again")
            }
        }, failure:  {
            (error) -> Void in
            SwiftLoader.hide()
            print("error===\(error)")
            ShowToast.showPositiveMessage(message: "Somthing went worng, Please try again")
        })
    }
    

    func submitReport(ReasonId : String) {
        print(postContentType)
        var requestURL = ""
        var parameters = [String:String]()
         parameters = ["token": token ,"post_id": post_id , "report_reason_id" : ReasonId , "for_uuid" : User_UUid ,"channel_id":channel_id]
        
        if postContentType == "user_post" {
            requestURL = BASEURL + reportUserPost
        }
        else if postContentType == "channel_post" {
           requestURL = BASEURL + reportChannelPost
        }
        else if(postContentType == "user_event")
        {
            requestURL = BASEURL + reportUserEvent
        }
        
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Loading"
        
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
             print("xxxxxxxx   ---- \(response)--")
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--")
            self.reportView.removeFromSuperview()
            MBProgressHUD.hide(for: self.view, animated: false)
            
            if success
            {
              ShowToast.showPositiveMessage(message: "Successfully Reported")
            }
            else
            {
                ShowToast.showPositiveMessage(message: "Something went wrong, Please try again.")
            }
        }, failure:  {
            (error) -> Void in
            MBProgressHUD.hide(for: self.view, animated: false)
            self.reportView.removeFromSuperview()
            ShowToast.showPositiveMessage(message: "Something went wrong")
            print("error===\(error)")
        })
    }
    
    @objc func expandCell(sender: UITapGestureRecognizer)
    {
        let label = sender.view as! UILabel
        
        let rowData : OTLModal = timeLimeData[label.tag]
        let titleStr = rowData.title
        
        if((titleStr.count)<350)
        {
            return
        }
        
        let isVideoImgPdf = checkForImgVideoPdf(index : label.tag)
        if (isVideoImgPdf)
        {
            let cell : TM_Image_Cell = tableView.cellForRow(at: IndexPath(row: label.tag, section: 0)) as!TM_Image_Cell
            
            cell.postDescription.sizeToFit()
            indexOfCellToExpand = label.tag
            cell.postDescription.attributedText =  Utility.getHTMLToString(titleStr: titleStr)
        }
        else
        {
            let cell : TLN_Cell = tableView.cellForRow(at: IndexPath(row: label.tag, section: 0)) as! TLN_Cell
            
            cell.postDescription.sizeToFit()
            indexOfCellToExpand = label.tag
            cell.postDescription.attributedText =  Utility.getHTMLToString(titleStr: titleStr)
        }
        
        if(isShow && label.tag == currentExpendRow)
        {
            indexOfCellToExpand = -1
            currentExpendRow =  -1
            isShow = false
        }
        else
        {
            isShow = true
            currentExpendRow = label.tag;
        }
        
        tableView.reloadRows(at: [IndexPath(row: label.tag, section: 0)], with: .fade)
        tableView.scrollToRow(at: IndexPath(row: label.tag, section: 0), at: .top, animated: true)
    }

    
    @objc func viewUserProfile(sender: UITapGestureRecognizer!)
    {
        let touch = sender.location(in: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: touch)
        {
            let indx = indexPath.row;
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ProfileViewTimeLine") as! ProfileViewTimeLine
            isForEdit = false;
            vc.profileID = self.timeLimeData[indx].userID;
            self.present(vc, animated:true, completion:nil)
            
            
        }
    }
    
    
    @objc func viewImage(_ sender: UITapGestureRecognizer)
    {
        let touch = sender.location(in: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: touch)
        {
            //  let dic = self.timeLineArray1[indexPath.row] as! Dictionary<String,Any>
            let imageStr =  self.timeLimeData[indexPath.row].image // dic["image"] as? String
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "ImagePagerViewControllerPager") as! ImagePagerViewControllerPager
            newViewController.imageURL = imageStr
            newViewController.requestType = "image"
            self.present(newViewController, animated: true, completion: nil)
        }
    }
    
    
    @objc func viewPDFFile(_ sender: UITapGestureRecognizer)
    {
        let touch = sender.location(in: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: touch)
        {
            //   let dic = self.timeLineArray1[indexPath.row] as! Dictionary<String,Any>
            let imageStr =   self.timeLimeData[indexPath.row].pdf // dic["pdf"] as? String
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "ImagePagerViewControllerPager") as! ImagePagerViewControllerPager
            newViewController.imageURL = imageStr
            newViewController.requestType = "pdf"
            self.present(newViewController, animated: true, completion: nil)
        }
    }
    
    @objc func plsyPostVideo(_ sender: UITapGestureRecognizer)
    {
        let touch = sender.location(in: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: touch)
        {
            //  let dic = self.timeLineArray1[indexPath.row] as! Dictionary<String,Any>
            let video_url = NSURL(string: String(describing:  self.timeLimeData[indexPath.row].video_url))
            play(url: video_url!)
        }
    }
    func play(url:NSURL) {
        print("playing \(url)")
        let player = AVPlayer(url: (url as URL))
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    func getThumbnailImage(forUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(1, 60) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
        }
        return nil
    }
    
    // like API
    @objc func btnLikePost(sender: UITapGestureRecognizer!)
    {
        
        let label = sender.view as! UIImageView
        
        let tagIndex : Int = label.tag
        
        // let dic = self.timeLineArray1[tagIndex] as! Dictionary<String,Any>
        let id = timeLimeData[tagIndex].id
        var is_liked = timeLimeData[tagIndex].is_liked
        
        if is_liked == "0"
        {
            is_liked = "1"
            timeLimeData[tagIndex].is_liked = "1"
            
            let aa : Int = Int(timeLimeData[tagIndex].count_like)! + 1
            timeLimeData[tagIndex].count_like = String(aa)
            
            let indexPath = IndexPath(item: tagIndex, section: 0)
            if(checkForImgVideoPdf(index : tagIndex))
            {
                let cell = self.tableView.cellForRow(at: indexPath) as! TM_Image_Cell
                cell.imgIsPostLike.image =  UIImage(named:
                    "vc_like_true")
                cell.likeCount.text = timeLimeData[tagIndex].count_like + " Likes"
            }
            else
            {
                let cell = self.tableView.cellForRow(at: indexPath) as! TLN_Cell
                cell.imgIsPostLike.image =  UIImage(named: "vc_like_true")
                cell.likeCount.text = timeLimeData[tagIndex].count_like  + " Likes"
            }
        }
        else
        {
            is_liked = "0"
            timeLimeData[tagIndex].is_liked = "0"
            let aa : Int = Int(timeLimeData[tagIndex].count_like)! - 1
            timeLimeData[tagIndex].count_like = String(aa)
            
            let indexPath = IndexPath(item: tagIndex, section: 0)
            if(checkForImgVideoPdf(index : tagIndex))
            {
                let cell = self.tableView.cellForRow(at: indexPath) as! TM_Image_Cell
                cell.imgIsPostLike.image =  UIImage(named: "vc_like_false")
                cell.likeCount.text = timeLimeData[tagIndex].count_like  + " Likes"
            }
            else
            {
                let cell = self.tableView.cellForRow(at: indexPath) as! TLN_Cell
                cell.imgIsPostLike.image =  UIImage(named: "vc_like_false")
                cell.likeCount.text = timeLimeData[tagIndex].count_like  + " Likes"
            }
        }
        
        
        let contentType = timeLimeData[tagIndex].content_type // String(describing: dic["content_type"]!)
        var requestURL = "";
        if(contentType == "user_post")
        {
            requestURL = BASEURL + createUserPostLike
        }
        else if(contentType == "user_event")
        {
            requestURL = BASEURL + createEventLike
        }
        else if(contentType == "channel_post")
        {
            requestURL = BASEURL + postLike
        }
        
        let parameters: [String:String] = ["id":id,"channel_id":channel_id ,"token": token ,"is_liked":is_liked]
        
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--")
            
            if success
            {
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
        })
    }

    // View comment history
    @objc func btnPostViewComment(sender: UITapGestureRecognizer!)
    {
        let imgv  = sender.view as! UIImageView
        let indx : Int = imgv.tag
        self.isForReloadIndex = indx
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Comments_VC") as! Comments_VC
        vc.id =  timeLimeData[indx].id
        vc.contentType = timeLimeData[indx].content_type
        self.present(vc, animated:true, completion:nil)
    }
    
    @objc func ViewComments(sender: UITapGestureRecognizer)
    {
        let label = sender.view as! UILabel
        let indx : Int = label.tag
        isForReloadIndex = indx
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Comments_VC") as! Comments_VC
        vc.id =  timeLimeData[indx].id
        vc.contentType = timeLimeData[indx].content_type
        self.present(vc, animated:true, completion:nil)
    }
    
    
    
    @objc func clickisFav(sender: UITapGestureRecognizer!)
    {
        let touch = sender.location(in: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: touch)
        let indx : Int = (indexPath?.row)!
        var isFavarity =  timeLimeData[indx].is_favorite
        
        if(isFavarity == "1")
        {
            isFavarity = "0";
            timeLimeData[indx].is_favorite = "0"
            
            let indexPath = IndexPath(item: indx, section: 0)
            if(checkForImgVideoPdf(index : indx))
            {
                let cell = self.tableView.cellForRow(at: indexPath) as! TM_Image_Cell
                cell.imgIsFavorites.image =  UIImage(named:
                    "vc_favorite_normal")
            }
            else
            {
                let cell = self.tableView.cellForRow(at: indexPath) as! TLN_Cell
                cell.imgIsFavorites.image =  UIImage(named: "vc_favorite_normal")
            }
        }
        else
        {
            isFavarity = "1";
            timeLimeData[indx].is_favorite = "1"
            let indexPath = IndexPath(item: indx, section: 0)
            if(checkForImgVideoPdf(index : indx))
            {
                let cell = self.tableView.cellForRow(at: indexPath) as! TM_Image_Cell
                cell.imgIsFavorites.image =  UIImage(named:
                    "vc_favorite")
            }
            else
            {
                let cell = self.tableView.cellForRow(at: indexPath) as! TLN_Cell
                cell.imgIsFavorites.image =  UIImage(named: "vc_favorite")
            }
        }
        
        
        let requestURL = BASEURL + postFavorite
        let parameters: [String:String] = ["id":timeLimeData[indx].id,"token": token ,"is_favorite": isFavarity]
        
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--")
            
            if success
            {
                
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
        })
    }
    
    
    func checkForImgVideoPdf(index : Int) -> Bool
    {
        let rowData : OTLModal = timeLimeData[index]
        if (rowData.image.split(separator: ",").count > 0)
        {
            return true
        }
        else if (rowData.video_url.split(separator: ",").count > 0)
        {
            return true
        }
        else if (rowData.pdf.split(separator: ",").count > 0)
        {
            return true
        }
        else
        {
            return false
        }
    }

    func getTimeLineData()
    {
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Loading"
        
        let requestURL = BASEURL + getOrganizationTimeline
        let parameters: [String:String] = ["channel_id": channel_id ,"token": token,"currentpage":  String(self.pageNo)]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--  \(response)")
            
            if success
            {
                self.parsex(json: response)
                MBProgressHUD.hide(for: self.view, animated: false)
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
            
        })
    }
    
    
    func parsex(json: JSON)
    {
        if(json["data"].arrayValue.count < 10)
        {
            isAvilableForLoad = false
        }
        for result in json["data"].arrayValue {
            let id = result["id"].stringValue
            let title = result["title"].stringValue
            let description = result["description"].stringValue
            let category = result["category"].stringValue
            let pdf = result["count_channpdfel"].stringValue
            let pdf_name = result["pdf_name"].stringValue
            let image = result["image"].stringValue
            let video = result["video"].stringValue
            let video_thumb = result["video_thumb"].stringValue
            let video_url = result["video_url"].stringValue
            let video_url_oe = result["video_url_oe"].stringValue
            let time = result["time"].stringValue
            let count_like = result["count_like"].stringValue
            let count_comment = result["count_comment"].stringValue
            let is_favorite = result["is_favorite"].stringValue
            let is_liked = result["is_liked"].stringValue
            let content_type = result["content_type"].stringValue
            let userID = result["user"]["id"].stringValue
            let userName = result["user"]["name"].stringValue
            let userImage = result["user"]["image"].stringValue
            let channel_name = result["channel_name"].stringValue
            let channel_id = result["channel_id"].stringValue
            
            let mData : OTLModal = OTLModal(id: id , title: title , description: description  , category: category , pdf: pdf , pdf_name: pdf_name , image: image , video: video , video_thumb: video_thumb , video_url: video_url , video_url_oe: video_url_oe , time: time , count_like: count_like , count_comment: count_comment , is_favorite: is_favorite , is_liked: is_liked , content_type: content_type , userID: userID , userName: userName , userImage: userImage , channel_name: channel_name , channel_id: channel_id)
            timeLimeData.append(mData)
        }
         self.tableView.reloadData()
        
    }
    
    var pageNo : Int = 1;
    var loadingData = false
    var isAvilableForLoad = true;
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = timeLimeData.count - 1
        if !loadingData && indexPath.row == lastElement {
            loadingData = true
            loadMoreData()
        }
    }
    
    func loadMoreData() {
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                self.pageNo = self.pageNo + 1
                if(self.isAvilableForLoad)
                {
                    self.getTimeLineData();
                }
                self.loadingData = false
            }
        }
    }
}
