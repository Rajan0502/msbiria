//
//  SuggChannelTableViewCell.swift
//  Doxbay
//
//  Created by Unify on 24/04/18.
//  Copyright © 2018 Yuji Hato. All rights reserved.
//

import UIKit

class SuggChannelTableViewCell: UITableViewCell {
    @IBOutlet weak var sChannelTblVw: UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
