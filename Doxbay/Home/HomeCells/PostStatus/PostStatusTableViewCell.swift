//
//  PostStatusTableViewCell.swift
//  Doxbay
//
//  Created by Unify on 24/04/18.
//  Copyright © 2018 Yuji Hato. All rights reserved.
//

import UIKit

class PostStatusTableViewCell: UITableViewCell {
    @IBOutlet weak var thoughtImgVw: UIImageView!
    
    @IBOutlet weak var postContVw: UIView!
    @IBOutlet weak var contVw: UIView!
    @IBOutlet weak var imgNameLbl: UILabel!
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var attachContVw: UIView!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var eventLbl: UILabel!
    @IBOutlet weak var eventImgVw: UIImageView!
    @IBOutlet weak var thoughtLbl: UILabel!
    @IBOutlet weak var eventBtn: UIButton!
    @IBOutlet weak var thoughtBtn: UIButton!
    @IBOutlet weak var postBtn: UIButton!
    @IBOutlet weak var txtVwUserPost: UITextView!
    @IBOutlet weak var imgVw: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
