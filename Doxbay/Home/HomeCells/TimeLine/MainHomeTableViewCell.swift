//
//  MainHomeTableViewCell.swift
//  Doxbay
//
//  Created by Unify on 24/04/18.
//  Copyright © 2018 Yuji Hato. All rights reserved.
//

import UIKit

class MainHomeTableViewCell: UITableViewCell {
   
    @IBOutlet weak var imgVw: UIImageView!
    
    @IBOutlet weak var bottomVwPost: UIView!
    @IBOutlet weak var commentBtn: UIButton!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var userImgVw: UIImageView!
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userHoursLbl: UILabel!
    
    @IBOutlet weak var contVw: UIView!
    @IBOutlet weak var bottomVw: UIView!
    @IBOutlet weak var countCommentLbl: UILabel!
    @IBOutlet weak var CountLikeLbl: UILabel!
    @IBOutlet weak var titleTxtVw: UITextView!
   
   
    
    /*
    @IBOutlet weak var postImgVw: UIImageView!
    
    @IBOutlet weak var postTxtVw: UITextView!
    
    
    
    @IBOutlet weak var cameraBtn: UIButton!
    @IBOutlet weak var postBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    @IBOutlet var commentView: UIView!
    */
    override func awakeFromNib() {
        super.awakeFromNib()
     //   commentView.isHidden = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
