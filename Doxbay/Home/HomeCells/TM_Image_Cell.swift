//
//  TM_Image_Cell.swift
//  DoxbaySalman
//
//  Created by Luminous on 26/05/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit

class TM_Image_Cell: UITableViewCell {

    
    
  
    @IBOutlet weak var reportLbl: UILabel!
    @IBOutlet weak var imgReport: UIImageView!
    @IBOutlet weak var userProfilePic: UIImageView!
    
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var userPTitle: UILabel!
    
    @IBOutlet weak var userPostTypeDateTime: UILabel!
    
    @IBOutlet weak var postDescription: UILabel!
    
    @IBOutlet weak var imgIsFavorites: UIImageView!
    
    @IBOutlet weak var imgIsPostLike: UIImageView!
    
    @IBOutlet weak var likeCount: UILabel!
    
    @IBOutlet weak var imgPostComment: UIImageView!
    
    @IBOutlet weak var commentCount: UILabel!
    
    
    
    @IBOutlet weak var postImage: UIImageView!
    
    
    @IBOutlet var vc_Play: UIImageView!
    
    
    
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
