

var userPhotoUrl = "";

import UIKit
import MBProgressHUD
import SDWebImage
import AVKit
import AVFoundation
import MobileCoreServices
import Alamofire
import SwiftyJSON




class HomeViewController_OLD: UIViewController,UITextViewDelegate, UITableViewDataSource,UITableViewDelegate,UINavigationControllerDelegate{
    
    
    var timeLimeData : [OTLModal] = []
    
    var refreshControl = UIRefreshControl()

    
    var profileDataArray = [Any]()
    var timeLineArray1 = [Any]()

    
    
    
    @IBOutlet weak var tblVw: UITableView!
    
    
    
    
    override func viewDidLoad() {
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.tblVw.addSubview(refreshControl)
        super.viewDidLoad()
        self.tblVw.delegate = self
        self.tblVw.dataSource = self
     //   self.tblVw.rowHeight = UITableViewAutomaticDimension
        self.tblVw.tag=1;
        self.tblVw.reloadData()
        self.tblVw.contentInset = UIEdgeInsetsMake(-30, 0, 0, 0)
        self.hideKeyboardOnTap(#selector(self.dismissKeyboard))
        
        subCribeChhanel();
        getUserDetails();
        // callapi()
        // Do any additional setup after loading the view.
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
        // do aditional stuff
    }
    
    @objc func refresh(sender:AnyObject)
    {
        refreshControl.endRefreshing()
        callapi()
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.parent?.view.frame = CGRect(x:0, y:64, width:self.view.frame.width, height: UIScreen.main.bounds.size.height-64)
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func subCribeChhanel()
    {
        let requestURL = BASEURL + subsribeUnsubscribeChannel;
        let parameters: [String:String] = ["channel_id": channel_id ,"token": token,"is_subscribe": "2"]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--")
            
            if success
            {
                self.getUserDetails();
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
        })
    }
    
    
    
    
    
    func getUserDetails()
    {
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Loading"
        //MBProgressHUD.hide(for: self.view, animated: false)
        let requestURL = BASEURL + getUser;
        let parameters: [String:String] = ["for_uuid": for_uuid ,"token": token]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--")
            
            if success
            {
                userPhotoUrl = response["data"]["photoUrl"].stringValue
                UserDefaults.standard.set(userPhotoUrl, forKey: "userPhotoUrl")
                print("userPhotoUrl----\(userPhotoUrl)")
                self.callapi();
            }
            MBProgressHUD.hide(for: self.view, animated: false)
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
        })
    }
    
    func callapi()
    {
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Loading"
        
        let requestURL = BASEURL + getOrganizationTimeline
        let parameters: [String:String] = ["channel_id": channel_id ,"token": token,"currentpage":""]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--  \(response)")
            
            if success
            {
                self.timeLineArray1 = response["data"].arrayObject!
                self.parsex(json: response)
                self.tblVw.reloadData()
            }
            MBProgressHUD.hide(for: self.view, animated: false)
            
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
        })
    }
    
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if tableView.tag == 1
        {
            return 2
        }
        else
        {
            return 0
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView.tag == 1
        {
            if section==1
            {
                return  self.timeLineArray1.count
            }
            else
            {
                return 0
            }
            
        }
        else
        {
            return 0
        }
        
    }
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
   //  func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
     {
        if tableView.tag == 1
        {
            if indexPath.section==0
            {
                var yAxis = 0
                return CGFloat(465 + yAxis) //post status
            }
            else  if indexPath.section==1
            {
                
                 print("---- Reload Index 000  -- \(indexPath.row)");
                
                let cellIdentifier = "MainHomeTableViewCell"
                var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? MainHomeTableViewCell
                if cell == nil {
                    var topLevelObjects = Bundle.main.loadNibNamed("MainHomeTableViewCell", owner: self, options: nil)
                    cell = topLevelObjects?[0] as? MainHomeTableViewCell
                    cell?.selectionStyle = .none
                }
                
                
                let dic = self.timeLineArray1[indexPath.row] as! Dictionary<String,Any>
                
                
                var titleStr = dic["title"] as? String
                titleStr = titleStr?.trimmingCharacters(in: .whitespacesAndNewlines)
                // cell?.titleTxtVw.text = titleStr
               if indexPath.row == selectedRowIndex && thereIsCellTapped {
                //if(clickIndexPos == indexPath.row  && isClickForShowHide )
                //{
                   
                }
                else
                {
                    if((titleStr?.count)!>230)
                    {
                        titleStr = (titleStr?.substring(with: 0..<230))! + "... Read More"
                    }
                }
                
                //  let mySubstring = titleStr?.substring(with: 0..<250)
                let htmlString = "<html><body>\(titleStr ?? "")</body></html>"
                // works even without <html><body> </body></html> tags, BTW
                let data = htmlString.data(using: String.Encoding.unicode)! // mind "!"
                let attrStr = try? NSAttributedString( // do catch
                    data: data,
                    options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                    documentAttributes: nil)
                cell?.titleTxtVw.attributedText = attrStr//titleStr
                
                cell?.titleTxtVw.text = titleStr
                
                
                
                let sizeThatShouldFitTheContent = cell?.titleTxtVw.sizeThatFits((cell?.titleTxtVw.frame.size)!)
                var height = sizeThatShouldFitTheContent?.height
                
                
                print("text height",height)
                if  let imageStr =  dic["image"] as? String
                {
                    
                    print("imageStr",imageStr)
                    let imageArray = imageStr.split(separator: ",")
                    if (imageArray.count > 0)
                    {
                        // for _ in imageArray
                        //{
                        
                          height = height!+60.00;
                    
                        //}
                    }
                }
                
                if  let videoStr =  dic["video_url"] as? String
                {
                    print("videoStr",videoStr)
                    let videoArray = videoStr.split(separator: ",")
                    if (videoArray.count > 0)
                    {
                        // for _ in videoArray
                        // {
                        height = height!+130.00;
                        // }
                    }
                }
                
                if  let pdfStr =  dic["pdf"] as? String
                {
                    print("pdfStr",pdfStr)
                    let pdfArray = pdfStr.split(separator: ",")
                    if (pdfArray.count > 0)
                    {
                        // for _ in imageArray
                        //{
                        height = height!+130.00;
                        //}
                    }
                }
                
                
                
                print(height! + 354)
                print("text: ",cell?.titleTxtVw.text)
                if  let pdf =  dic["pdf"] as? String, pdf != "" {
                    print(pdf)
                    return height! + 354
                }
                if let video =  dic["video_url"] as? String, video != "" {
                    return height! + 354
                }
                if let image =  dic["image"] as? String, image != "" {
                    return height! + 354
                }
                return height! + 180
            }
            else
            {
                return 190*10+80
            }
        }
        else
        {
            return 100
        }
    }
    
  
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView.tag == 1
        {
            if indexPath.section==0
            {
                let cellIdentifier = "PostStatusTableViewCell"
                var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? PostStatusTableViewCell
                return cell!
            }
            if indexPath.section==1
            {
                let cellIdentifier = "MainHomeTableViewCell"
                var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? MainHomeTableViewCell
                if cell == nil {
                    var topLevelObjects = Bundle.main.loadNibNamed("MainHomeTableViewCell", owner: self, options: nil)
                    cell = topLevelObjects?[0] as? MainHomeTableViewCell
                    cell?.selectionStyle = .none
                }
                cell?.imgVw.layer.cornerRadius =  (cell?.imgVw.frame.size.width)! / 2;
                cell?.imgVw.layer.masksToBounds = true
                
                

                let dic = self.timeLineArray1[indexPath.row] as! Dictionary<String,Any>
                
                let userDic = dic["user"] as! Dictionary<String,Any>
                cell?.userName.text = userDic["name"] as? String
                cell?.userImgVw.sd_setImage(with: URL(string: (userDic["image"] as? String)!), placeholderImage: UIImage(named: "auther"))
                
                let miliSecondsTime:Double? = Double((dic["time"] as? String)!)
                let date = Date(timeIntervalSince1970: (miliSecondsTime! / 1000.0))
                cell?.userHoursLbl.text = date.getElapsedInterval() + " ago"
                
                var titleStr = dic["title"] as? String
                titleStr = titleStr?.trimmingCharacters(in: .whitespacesAndNewlines)
                
                // cell?.titleTxtVw.text = titleStr
                var extHeight : CGFloat = 10.00;
                //if(clickIndexPos == indexPath.row  && isClickForShowHide )
                //{
                if indexPath.row == selectedRowIndex && thereIsCellTapped {
                    isClicked = false
                    isClickForShowHide = false
                    clickIndexPos = -1
                    //titleStr = titleStr;
                    extHeight = 120;
                }
                else
                {
                    if((titleStr?.count)!>350)
                    {
                        //titleStr = (titleStr?.substring(with: 0..<230))! + "... Read More"
                         titleStr = (titleStr?.substring(with: 0..<350))! + "...<strong><font color=red>Read More</font></strong>"
                    }
                }
                /*
                if((titleStr?.count)!>350)
                {
                    titleStr = (titleStr?.substring(with: 0..<350))! + "...<strong><font color=red>Read More</font></strong>"
                }
                */
                
                
                //  let mySubstring = titleStr?.substring(with: 0..<250)
                let htmlString = "<html><body>\(titleStr ?? "")</body></html>"
                // works even without <html><body> </body></html> tags, BTW
                let data = htmlString.data(using: String.Encoding.unicode)! // mind "!"
                let attrStr = try? NSAttributedString( // do catch
                    data: data,
                    options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                    documentAttributes: nil)
                cell?.titleTxtVw.attributedText = attrStr//titleStr
               
                let textViewRecognizer = UITapGestureRecognizer()
                textViewRecognizer.addTarget(self, action: #selector(readMoreText(_:)))
                cell?.titleTxtVw.addGestureRecognizer(textViewRecognizer)
                print("---- Reload Index 11 -- \(indexPath.row)");
              
               

                
                
                let tmLineData : OTLModal = timeLimeData[indexPath.row];
                cell?.countCommentLbl.text = tmLineData.count_comment  //String(describing: dic["count_comment"]!)
                cell?.CountLikeLbl.text = tmLineData.count_like // String(describing: dic["count_like"]!)
                let is_liked = tmLineData.is_liked //String(describing: dic["is_liked"]!)
                if is_liked == "0"
                {
                    cell?.CountLikeLbl.textColor = UIColor.colorWithHexString(hexStr: "#888da8")
                    let image = UIImage(named: "like")?.withRenderingMode(.alwaysTemplate)
                    cell?.likeBtn.setImage(image, for: .normal)
                    cell?.likeBtn.tintColor = UIColor.colorWithHexString(hexStr: "#888da8")
                }
                else
                {
                    cell?.CountLikeLbl.textColor = UIColor.red
                    let image = UIImage(named: "like")?.withRenderingMode(.alwaysTemplate)
                    cell?.likeBtn.setImage(image, for: .normal)
                    cell?.likeBtn.tintColor = UIColor.red
                }
                
                cell?.likeBtn.tag = indexPath.row
                cell?.likeBtn.addTarget(self, action: #selector(btnLikePost), for: .touchUpInside)
                
                cell?.commentBtn.tag = indexPath.row
                cell?.commentBtn.addTarget(self, action: #selector(btnPostViewComment), for: .touchUpInside)
                
                
                
                let sizeThatShouldFitTheContent = cell?.titleTxtVw.sizeThatFits((cell?.titleTxtVw.frame.size)!)
                let height = sizeThatShouldFitTheContent?.height
                
                
                
                let value1 : CGFloat = (cell?.titleTxtVw.frame.origin.y)!
                let value2 : CGFloat = height!
                var videoYAxis    =   value1 + value2
                
                if  let imageStr =  dic["image"] as? String
                {
                    let imageArray = imageStr.split(separator: ",")
                    if (imageArray.count > 0)
                    {
                        var count=0;
                        // for imgStr in imageArray
                        // {
                        let imgStrTemp = imageArray[0].trimmingCharacters(in: .whitespacesAndNewlines)
                        
                        let value1 : CGFloat = (cell?.titleTxtVw.frame.origin.y)!
                        let value2 : CGFloat = height!
                        let yAxis   =  value1 + value2 + extHeight   // set the image marging from top (text description)
                        
                        let imageView = UIImageView()
                        imageView.frame = CGRect(x: 5, y: yAxis, width: (self.view.frame.size.width-30), height: 220)
                        imageView.contentMode = .scaleAspectFit
                        cell?.contVw.addSubview(imageView)
                        imageView.sd_setImage(with: URL(string: String(imgStrTemp)), placeholderImage: UIImage(named: "Exbg"))
                        
                        
                        videoYAxis = yAxis+200
                        count += 1;
                        
                        let tap = UITapGestureRecognizer(target: self, action: #selector(viewImage))
                        imageView.addGestureRecognizer(tap)
                        imageView.isUserInteractionEnabled = true
                        
                        // }
                    }
                }
                else if  let videoStr =  dic["video_url"] as? String
                {
                    let videoArray = videoStr.split(separator: ",")
                    if (videoArray.count > 0)
                    {
                        var count=0;
                        // for var i in 0..<videoArray.count
                        // {
                        let videoStrTemp = videoStr.trimmingCharacters(in: .whitespacesAndNewlines)
                        
                        videoYAxis = CGFloat(videoYAxis) + 200*CGFloat(count)
                        
                        let imageView = UIImageView()
                        imageView.frame = CGRect(x: 15, y: Int(CGFloat(videoYAxis)), width: Int((self.view.frame.size.width-60)), height: 200)
                        imageView.contentMode = .scaleAspectFit
                        cell?.contVw.addSubview(imageView)
                        
                        if  let video_thumbStr =  dic["video_thumb"] as? String
                        {
                            let videothumbArray = video_thumbStr.split(separator: ",")
                            
                            if(videothumbArray.count>0)
                            {
                                imageView.sd_setImage(with:URL(string: String(videothumbArray[0])), placeholderImage: UIImage(named: "Exbg"))
                            }
                            else
                            {
                                imageView.sd_setImage(with:URL(string: String(videoArray[0]))!, placeholderImage: UIImage(named: "Exbg"))
                                DispatchQueue.global(qos: .background).async {
                                    
                                    let thumbnailImage = self.getThumbnailImage(forUrl: URL(string: String(videoArray[0]))!)
                                    
                                    DispatchQueue.main.async
                                        {
                                            imageView.image = thumbnailImage
                                    }
                                }
                                
                                
                                
                                
                            }
                            let tap = UITapGestureRecognizer(target: self, action: #selector(plsyPostVideo))
                            imageView.addGestureRecognizer(tap)
                            imageView.isUserInteractionEnabled = true
                        }
                        count += 1;
                        
                        //  }
                    }
                }
                else if  let pdfURl =  dic["pdf"] as? String
                {
                    let pdfArray = pdfURl.split(separator: ",")
                    if (pdfArray.count > 0)
                    {
                        var count=0;
                        let imgStrTemp = pdfArray[0].trimmingCharacters(in: .whitespacesAndNewlines)
                        
                        let value1 : CGFloat = (cell?.titleTxtVw.frame.origin.y)!
                        let value2 : CGFloat = height!
                        let yAxis   = value1 + value2 + 200*CGFloat(count)
                        
                        let imageView = UIImageView()
                        imageView.frame = CGRect(x: 15, y: yAxis, width: (self.view.frame.size.width-60), height: 200)
                        imageView.contentMode = .scaleAspectFit
                        cell?.contVw.addSubview(imageView)
                        // imageView.sd_setImage(with: URL(string: String(imgStrTemp)), placeholderImage: UIImage(named: "Exbg"))
                        imageView.image = UIImage(named: "pdf_icon");
                        videoYAxis = yAxis+200
                        
                        let tap = UITapGestureRecognizer(target: self, action: #selector(viewPDFFile))
                        imageView.addGestureRecognizer(tap)
                        imageView.isUserInteractionEnabled = true
                    }
                }
                return cell!
            }
            else
            {
                let cellIdentifier = "SuggChannelTableViewCell"
                var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? SuggChannelTableViewCell
                return cell!
            }
        }
        else
        {
            let cellIdentifier = "TrendingTableViewCell"
            var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? TrendingTableViewCell
            return cell!
        }
    }
    
    
    
    var expandedRows = Set<Int>()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        
       }
    
       func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
       

    }
    
    var thereIsCellTapped = false
    var selectedRowIndex = -1
    var clickIndexPos : Int =  -1
    var isClickForShowHide = false , isClicked = false;
    
    
    
    @objc func readMoreText(_ sender: UITapGestureRecognizer) {
        let touch = sender.location(in: self.tblVw)
        let indexPath = self.tblVw.indexPathForRow(at: touch)
        
        isClickForShowHide = ((indexPath?.row) != nil);
        isClicked = true;
        if(isClickForShowHide)
        {
            isClickForShowHide = false
        }
        else
        {
            isClickForShowHide = true
        }
        
        // let indexPathx = IndexPath(item: (indexPath?.row)!, section: 1)
        // tblVw.reloadRows(at: [indexPathx], with: .none)
        // tblVw.beginUpdates()
        // tblVw.endUpdates()
        
        
        
        if selectedRowIndex != indexPath?.row {
            self.thereIsCellTapped = true
            self.selectedRowIndex = (indexPath?.row)!
        }
        else {
            // there is no cell selected anymore
            self.thereIsCellTapped = false
            self.selectedRowIndex = -1
        }
        
      
        
         //tblVw.beginUpdates()
         tblVw.reloadRows(at: [indexPath!], with: .automatic)
         //tblVw.endUpdates()
       
    }

    
    
    
    
    
    
   
    
   
    
    @objc func viewImage(_ sender: UITapGestureRecognizer)
    {
        let touch = sender.location(in: self.tblVw)
        if let indexPath = self.tblVw.indexPathForRow(at: touch)
        {
            let dic = self.timeLineArray1[indexPath.row] as! Dictionary<String,Any>
            let imageStr =  dic["image"] as? String
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "ImagePagerViewControllerPager") as! ImagePagerViewControllerPager
            newViewController.imageURL = imageStr!
            newViewController.requestType = "image"
            self.present(newViewController, animated: true, completion: nil)
        }
    }
    
    
    @objc func viewPDFFile(_ sender: UITapGestureRecognizer)
    {
        let touch = sender.location(in: self.tblVw)
        if let indexPath = self.tblVw.indexPathForRow(at: touch)
        {
            let dic = self.timeLineArray1[indexPath.row] as! Dictionary<String,Any>
            let imageStr =  dic["pdf"] as? String
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "ImagePagerViewControllerPager") as! ImagePagerViewControllerPager
            newViewController.imageURL = imageStr!
            newViewController.requestType = "pdf"
            self.present(newViewController, animated: true, completion: nil)
        }
    }
    
    
    
    
    @objc func plsyPostVideo(_ sender: UITapGestureRecognizer)
    {
        let touch = sender.location(in: self.tblVw)
        if let indexPath = self.tblVw.indexPathForRow(at: touch)
        {
            let dic = self.timeLineArray1[indexPath.row] as! Dictionary<String,Any>
            let video_url = NSURL(string: String(describing: dic["video_url"]!))
            play(url: video_url!)
        }
    }
    func play(url:NSURL) {
        print("playing \(url)")
        let player = AVPlayer(url: (url as URL))
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
  
    
    func getThumbnailImage(forUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(1, 60) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
        }
        return nil
    }
    
   
    
  
    // like API
    @objc func btnLikePost(sender: UIButton!)
    {
         selectedRowIndex = -1
         clickIndexPos =  -1
        
        let dic = self.timeLineArray1[sender.tag] as! Dictionary<String,Any>
        let id = timeLimeData[sender.tag].id //String(describing: dic["id"]!)
        var is_liked = timeLimeData[sender.tag].is_liked // String(describing: dic["is_liked"]!)
        
        if is_liked == "0"
        {
            is_liked = "1"
            timeLimeData[sender.tag].is_liked = "1"
            
            let aa : Int = Int(timeLimeData[sender.tag].count_like)! + 1
            timeLimeData[sender.tag].count_like = String(aa)
            //self.tblVw.reloadData();
            let indexPath = IndexPath(item: sender.tag, section: 1)
           // tblVw.reloadRows(at: [indexPath], with: .none)
            
             let cell = self.tblVw.cellForRow(at: indexPath) as! MainHomeTableViewCell
            cell.CountLikeLbl.textColor = UIColor.colorWithHexString(hexStr: "#888da8")
            let image = UIImage(named: "like")?.withRenderingMode(.alwaysTemplate)
            cell.likeBtn.setImage(image, for: .normal)
            cell.likeBtn.tintColor = UIColor.colorWithHexString(hexStr: "#888da8")
            
        }
        else
        {
            is_liked = "0"
            timeLimeData[sender.tag].is_liked = "0"
            let aa : Int = Int(timeLimeData[sender.tag].count_like)! - 1
            timeLimeData[sender.tag].count_like = String(aa)
            // self.tblVw.reloadData();
            
            let indexPath = IndexPath(item: sender.tag, section: 1)
           // tblVw.reloadRows(at: [indexPath], with: .none)
            
             let cell = self.tblVw.cellForRow(at: indexPath) as! MainHomeTableViewCell
            cell.CountLikeLbl.textColor = UIColor.red
            let image = UIImage(named: "like")?.withRenderingMode(.alwaysTemplate)
            cell.likeBtn.setImage(image, for: .normal)
            cell.likeBtn.tintColor = UIColor.red
        }
        
        
        let contentType = String(describing: dic["content_type"]!)
        var requestURL = "";
        if(contentType == "user_post")
        {
            requestURL = BASEURL + createUserPostLike
        }
        else if(contentType == "user_event")
        {
            requestURL = BASEURL + createEventLike
        }
        else if(contentType == "channel_post")
        {
            requestURL = BASEURL + postLike
        }
        
        let parameters: [String:String] = ["id":id,"channel_id":channel_id ,"token": token ,"is_liked":is_liked]
        
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--")
            
            if success
            {
                //self.callapi()
                // self.tblVw.reloadData();
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
        })
    }
    
    
    
    // View comment history
    @objc func btnPostViewComment(sender: UIButton!)
    {
        let dic = self.timeLineArray1[sender.tag] as! Dictionary<String,Any>
       let vc = CommentsViewController(nibName: "CommentsViewController", bundle: nil)
        vc.id = String(describing: dic["id"]!)
        vc.contentType = String(describing: dic["content_type"]!)
        self.present(vc, animated:true, completion:nil)
    }
    
    
    
    
    func parsex(json: JSON) {
         self.timeLimeData.removeAll();
        for result in json["data"].arrayValue {
            let id = result["id"].stringValue
            let title = result["title"].stringValue
            let description = result["description"].stringValue
            let category = result["category"].stringValue
            let pdf = result["count_channpdfel"].stringValue
            let pdf_name = result["pdf_name"].stringValue
            let image = result["image"].stringValue
            let video = result["video"].stringValue
            let video_thumb = result["video_thumb"].stringValue
            let video_url = result["video_url"].stringValue
            let video_url_oe = result["video_url_oe"].stringValue
            let time = result["time"].stringValue
            let count_like = result["count_like"].stringValue
            let count_comment = result["count_comment"].stringValue
            let is_favorite = result["is_favorite"].stringValue
            let is_liked = result["is_liked"].stringValue
            let content_type = result["content_type"].stringValue
            let userID = result["user"]["id"].stringValue
            let userName = result["user"]["name"].stringValue
            let userImage = result["user"]["image"].stringValue
            let channel_name = result["user"]["channel_name"].stringValue
            let channel_id = result["user"]["channel_id"].stringValue
            
            let mData : OTLModal = OTLModal(id: id , title: title , description: description  , category: category , pdf: pdf , pdf_name: pdf_name , image: image , video: video , video_thumb: video_thumb , video_url: video_url , video_url_oe: video_url_oe , time: time , count_like: count_like , count_comment: count_comment , is_favorite: is_favorite , is_liked: is_liked , content_type: content_type , userID: userID , userName: userName , userImage: userImage , channel_name: channel_name , channel_id: channel_id)
            
            timeLimeData.append(mData)
        }
        
        //  self.collectionView.reloadData();
        
    }
    
    
    
    
}








//extension String {
//    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
//        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
//        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
//        
//        return ceil(boundingBox.height)
//    }
//    
//    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
//        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
//        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
//        
//        return ceil(boundingBox.width)
//    }
//}






extension Date {
    func getElapsedInterval() -> String {
        let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: self, to: Date())
        
        if let year = interval.year, year > 0 {
            return year == 1 ? "\(year)" + " " + "year" :
                "\(year)" + " " + "years"
        } else if let month = interval.month, month > 0 {
            return month == 1 ? "\(month)" + " " + "month" :
                "\(month)" + " " + "months"
        } else if let day = interval.day, day > 0 {
            return day == 1 ? "\(day)" + " " + "day" :
                "\(day)" + " " + "days"
        } else if let hour = interval.hour, hour > 0 {
            return hour == 1 ? "\(hour)" + " " + "hour" :
                "\(hour)" + " " + "hours"
        } else if let minute = interval.minute, minute > 0 {
            return minute == 1 ? "\(minute)" + " " + "minute" :
                "\(minute)" + " " + "minutes"
        } else if let second = interval.second, second > 0 {
            return second == 1 ? "\(second)" + " " + "second" :
                "\(second)" + " " + "seconds"
        } else {
            return "a moment"
        }
    }
}




extension String {
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }
    
    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return substring(from: fromIndex)
    }
    
    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return substring(to: toIndex)
    }
    
    func substring(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return substring(with: startIndex..<endIndex)
    }
}



