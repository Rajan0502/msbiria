//
//  CommentsViewController.swift
//  Doxbay
//
//  Created by Unify on 30/04/18.
//  Copyright © 2018 Yuji Hato. All rights reserved.
//

import UIKit
import MBProgressHUD
import SwiftyJSON

class CommentsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource , UITextFieldDelegate{
    
    
    var timeLineArray1 = [Any]()
    
    var id:String = ""
    var contentType :String = ""
    let sampleTextField =  UITextField(frame: CGRect(x: 10, y: UIScreen.main.bounds.size.height - 60, width: UIScreen.main.bounds.size.width - 70 , height: 50))
    
    @IBAction func backClk(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    
    
    @IBOutlet weak var tblVw: UITableView!
    @IBOutlet var commentBox: UITextView!
    @IBOutlet var btnPostComment: UIButton!
    
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.hideKeyboardOnTap(#selector(self.dismissKeyboard))
  
        
        
        commentBox.layer.borderColor = UIColor.colorWithHexString(hexStr: "#888da8").cgColor
        commentBox.layer.borderWidth = 0.5
        commentBox.layer.cornerRadius = 3
        commentBox.isHidden = true
        btnPostComment.isHidden = true
        
        
        //self.tblVw.contentInset = UIEdgeInsetsMake(5, 60, UIScreen.main.bounds.size.width - 10, UIScreen.main.bounds.size.height - 100)
        self.tblVw.frame = CGRect(x: 10   , y: 70  , width: UIScreen.main.bounds.size.width - 20, height: UIScreen.main.bounds.size.height - 100)
        
        self.hideKeyboardOnTap(#selector(self.dismissKeyboard))
        
        
        
        let button:UIButton = UIButton(frame: CGRect(x: UIScreen.main.bounds.size.width - 70 , y: UIScreen.main.bounds.size.height - 60 , width: 60, height: 50))
        button.tintColor = UIColor.white
        button.setImage(UIImage(named: "send_comment"), for: .normal)
        button.addTarget(self, action:#selector(self.postCommentt), for: .touchUpInside)
        self.view.addSubview(button)
        
        
        
        
        
        
        
        sampleTextField.placeholder = "Write your comments"
        sampleTextField.font = UIFont.systemFont(ofSize: 15)
        sampleTextField.borderStyle = UITextBorderStyle.roundedRect
        sampleTextField.autocorrectionType = UITextAutocorrectionType.no
        sampleTextField.keyboardType = UIKeyboardType.default
        sampleTextField.returnKeyType = UIReturnKeyType.done
        sampleTextField.clearButtonMode = UITextFieldViewMode.whileEditing;
        sampleTextField.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        sampleTextField.delegate = self
        self.view.addSubview(sampleTextField)
        
        
        
         callapi()
        // Do any additional setup after loading the view.
    }
    
    
   
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // return NO to not change text
        commenttext = textField.text!
        print("While entering the characters this method gets called")
        return true
    }
    
    
    
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
        // do aditional stuff
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func callapi()
    {
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Loading"
        var requestURL = "";
        if(contentType == "user_post" )
        {
            requestURL = BASEURL + getUserPostComment
        }
        else if(contentType == "user_event")
        {
            requestURL = BASEURL + getEventComment
        }
       // else if(contentType == "channel_post"  || contentType == "Post")
        else
        {
            requestURL = BASEURL + getComment
        }
        
        
        let parameters: [String:String] = ["id":id,"token": token]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--")
            MBProgressHUD.hide(for: self.view, animated: false)
            if success
            {
                self.timeLineArray1 = response["data"].arrayObject!
                self.parsex(json: response);
                self.tblVw.reloadData()
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
        })
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.CommentHistoryData.count;
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellIdentifier = "CommentTableViewCell"
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? CommentTableViewCell
        if cell == nil {
            var topLevelObjects = Bundle.main.loadNibNamed("CommentTableViewCell", owner: self, options: nil)
            cell = topLevelObjects?[0] as? CommentTableViewCell
            cell?.selectionStyle = .none
        }
        cell?.imgVw.layer.cornerRadius =  (cell?.imgVw.frame.size.width)! / 2;
        cell?.imgVw.layer.masksToBounds = true
        
        
        let CPosData : CommentHistoryList = CommentHistoryData[indexPath.row];
        cell?.nameLbl.text = CPosData.userName //userDic["name"] as? String
        cell?.titleLbl.text = CPosData.comment;
        let miliSecondsTime:Double? = Double((CPosData.time))
        if(miliSecondsTime != nil)
        {
            let date = Date(timeIntervalSince1970: (miliSecondsTime! / 1000.0))
            
            cell?.subTitleLbl.text = date.getElapsedInterval() + " ago"
        }
        else
        {
            cell?.subTitleLbl.text = CPosData.time;
        }
        cell?.imgVw.sd_setImage(with: URL(string: CPosData.userImage) , placeholderImage: UIImage(named: "auther"));
        
        return cell!
    }
    
 
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    
    
    var commenttext : String = "";
   @objc func postCommentt()
    {
        let postTxt =  commenttext
        if (postTxt == "")
        {
            ShowToast.showNegativeMessage(message:"Enter Text");
            return;
        }
        var requestURL = "";
        if(contentType == "user_post")
        {
            requestURL = BASEURL + createUserPostComment
        }
        else if(contentType == "user_event")
        {
            requestURL = BASEURL + createEventComment
        }
       // else if(contentType == "channel_post" || contentType == "Post")
        else
        {
            requestURL = BASEURL +  postComment
        }
        
        let cData : CommentHistoryList = CommentHistoryList(id: id, comment: postTxt, time: "Just Now!"  , userID: "" , userName: "" , userImage: "");
        CommentHistoryData.append(cData);
        self.tblVw.reloadData();
        
        
        let parameters: [String:String] = ["id":id, "channel_id":channel_id , "token":token,"comment":postTxt]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--")
            
            if success
            {
                self.commentBox.text = "";
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
        })
    }
    
    @IBAction func btnPostComment(_ sender: Any) {
        let postTxt =  commentBox.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        if (postTxt == "")
        {
            ShowToast.showNegativeMessage(message:"Enter Text");
            return;
        }
        var requestURL = "";
        if(contentType == "user_post")
        {
            requestURL = BASEURL + createUserPostComment
        }
        else if(contentType == "user_event")
        {
            requestURL = BASEURL + createEventComment
        }
        else if(contentType == "channel_post" || contentType == "Post")
        {
            requestURL = ("\(BASEURL) \(postComment)")
        }
        
        let cData : CommentHistoryList = CommentHistoryList(id: id, comment: postTxt, time: "Just Now!"  , userID: "" , userName: "" , userImage: "");
        CommentHistoryData.append(cData);
        self.tblVw.reloadData();
        
        
        let parameters: [String:String] = ["id":id, "channel_id":channel_id , "token":token,"comment":postTxt]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--")
            self.sampleTextField.text = "";
            self.commentBox.text = "";
            
            if success
            {
                
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
        })
    }
    
    
    var CommentHistoryData : [CommentHistoryList] = [];
    func parsex(json: JSON) {
        for result in json["data"].arrayValue {
            let id = result["id"].stringValue
            let comment = result["comment"].stringValue
            let time = result["time"].stringValue
            let uid = result["user"]["id"].stringValue
            let uname = result["user"]["name"].stringValue
            let image = result["user"]["image"].stringValue
            let cData : CommentHistoryList = CommentHistoryList(id: id, comment: comment, time: time  , userID: uid , userName: uname , userImage: image);
            CommentHistoryData.append(cData);
        }
    }
}

class CommentHistoryList
{
    let id: String;
    let comment : String;
    let time : String;
    let userID : String;
    let userName : String;
    let userImage : String;
    
    init(id:String, comment:String , time:String , userID: String , userName: String , userImage: String)
    {
        self.id = id;
        self.comment = comment;
        self.time = time
        self.userID = userID
        self.userName = userName
        self.userImage = userImage;
    }
    
}
