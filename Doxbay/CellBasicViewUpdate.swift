//
//  CellBasicViewUpdate.swift
//  Doxbay
//
//  Created by Luminous on 16/06/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit

class CellBasicViewUpdate: UITableViewCell {

    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtSpeciality: UITextField!
    @IBOutlet weak var txtDesignation: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtQualification: UITextField!
    @IBOutlet weak var txtRegNum: UITextField!
     @IBOutlet weak var txtemail: UITextField!
     @IBOutlet weak var txtMobile: UITextField!

    @IBOutlet var uiViewEmail: UIView!
    @IBOutlet var uiviewMobile: UIView!
    
    
    
    
    func configureView(info: [String: Any]) {
        txtName.text = info["displayName"] as? String
        txtSpeciality.text = info["doxbay_speciality"] as? String
        txtDesignation.text = info["doxbay_designation"] as? String
        txtAddress.text = info["doxbay_address"] as? String
        txtQualification.text = info["doxbay_qualification"] as? String
        txtRegNum.text = info["registration_number"] as? String
        txtemail.text = info["email"] as? String
        txtMobile.text = info["mobile"] as? String
        
        
        txtemail.isUserInteractionEnabled = false
        txtMobile.isUserInteractionEnabled = false
        
        
       
        
        if !isForEdit {
            txtName.isUserInteractionEnabled = false
            txtDesignation.isUserInteractionEnabled = false
            txtQualification.isUserInteractionEnabled = false
            txtAddress.isUserInteractionEnabled = false
            txtSpeciality.isUserInteractionEnabled = false
            txtRegNum.isUserInteractionEnabled = false
            
            uiViewEmail.isHidden = true
            uiviewMobile.isHidden = true
            
        } else {
            txtName.isUserInteractionEnabled = true
            txtDesignation.isUserInteractionEnabled = true
            txtQualification.isUserInteractionEnabled = true
            txtAddress.isUserInteractionEnabled = true
            txtSpeciality.isUserInteractionEnabled = true
            txtRegNum.isUserInteractionEnabled = true
        }
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
  
    
    @IBAction func didChanged(_ sender: UITextField) {
        if sender == txtName {
            profileData["displayName"] = txtName.text!
        }
        if sender == txtSpeciality {
            profileData["doxbay_speciality"] = txtSpeciality.text!
        }
        if sender == txtDesignation {
            profileData["doxbay_designation"] = txtDesignation.text!
        }
        if sender == txtAddress {
            profileData["doxbay_address"] = txtAddress.text!
        }
        if sender == txtQualification {
            profileData["doxbay_qualification"] = txtQualification.text!
        }
        if sender == txtRegNum {
            profileData["registration_number"] = txtQualification.text!
        }
    }
}
