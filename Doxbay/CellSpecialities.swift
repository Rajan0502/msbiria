//
//  CellSpecialities.swift
//  DoxbaySalman
//
//  Created by AppsInvo  on 20/05/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit

class CellSpecialities: UITableViewCell {

    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    var id = ""
    
    @IBOutlet weak var btnDelete: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func configureView(info: [String: Any]) {
        btnDelete.isHidden = !isForEdit
        id = info["id"] as? String ?? ""
        let firstCharacter = info["speciality"] as! String
        lblDesc.text = info["speciality"] as? String
        lblTitle.text = "\(firstCharacter.characters.first!)"
    }
    
    @IBAction func tapDelete(_ sender: Any) {
        profileInstance.showOkCancelAlertWithAction("Are you sure delete?") { (isOk) in
            if isOk {
                if let index = profileInstance.tableView.indexPath(for: self) {
                    
                }
                let params = ["id":self.id, "token": token, Constant.kAPI_NAME: deleteUspeciality]
                SGServiceController.instance().hitPostService(params, unReachable: {
                    print("Not reachable")
                }) { (response) in
                    print(response)
                    if let index = profileInstance.tableView.indexPath(for: self) {
                        print(index.section)
                        print(index.row)
                        profileInstance.tableView.beginUpdates()
                        profileInstance.arrSpecialities.remove(at: index.row)
                        profileInstance.tableView.deleteRows(at: [index], with: .none)
                        profileInstance.tableView.endUpdates()
                        
                    }
                }
            }
        }
    }
    
    
    
}
