//
//  ConnectedBranch_VC.swift
//  Doxbay
//
//  Created by Luminous on 16/05/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit
import SwiftyJSON
import AlamofireImage
import MBProgressHUD


class ConnectedBranch_VC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var segmentIndex : Int = 0
    var expID : String = "";
    var ExploreChannelCatName = "";
    var channelArray : [ExploreChannelListModal] = [];
    
    var arrBranches : [ExploreChannelListModal] = [];
    var arrOfficeChannel : [ExploreChannelListModal] = [];
    var pageNoBranch : Int = 1
    var pageNoOffiChan : Int = 1
    
    
    
   
      @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var headerTitle: UILabel!
    
    
    @IBAction func backClk(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)

    }
    
    //self.tableView.addSubview(self.refreshControl)
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.red
        return refreshControl
    }()
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
       // self.getConnectedChannel();
        refreshControl.endRefreshing()
    }
    
    
    
    
    
    
    
    @IBAction func searchConnectedChannels(_ sender: Any) {
        
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchChannelUser") as! SearchChannelUser
            if(segmentIndex == 0){
                    vc.isFromPage =  "ConnectedBranchChannles"
            }
            else
            {
                    vc.isFromPage =  "ContectedOfficeChannels"
            }
        vc.searchHeader =  "Type here to search channel"
         self.present(vc, animated:true, completion:nil)
        
        
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self;
        tableView.dataSource = self
        self.tableView.addSubview(self.refreshControl)
        
        
      
        segmentedControl.addTarget(self, action: #selector(self.segmentedValueChanged(_:)), for: .valueChanged)
        
        
        getConnectedChannel();
       // headerTitle.text = ExploreChannelCatName;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @objc func segmentedValueChanged(_ sender:UISegmentedControl!)
    {
        segmentIndex = sender.selectedSegmentIndex;
        if(segmentIndex == 0){
            DispatchQueue.main.async{
                self.channelArray.removeAll();
                self.channelArray = self.arrBranches;
                if(self.channelArray.isEmpty)
                {
                    self.getConnectedChannel();
                }
                else{
                    self.tableView.reloadData()
                }
            }
        }
        else
        {
            DispatchQueue.main.async{
                self.channelArray.removeAll();
                self.channelArray = self.arrOfficeChannel;
                if(self.channelArray.isEmpty)
                {
                    self.getConnectedChannel();
                }
                else{
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 95;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return channelArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExploreChannelListCellTableViewCell", for: indexPath) as! ExploreChannelListCellTableViewCell
        
//
//        cell.contentView.layer.borderWidth = 1.0
//        cell.contentView.layer.borderColor = UIColor.clear.cgColor
//        cell.contentView.layer.masksToBounds = true
//        cell.layer.shadowColor = UIColor.gray.cgColor
//        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
//        cell.layer.shadowRadius = 1.0
//        cell.layer.shadowOpacity = 1.0
//        cell.layer.masksToBounds = false
//        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
// // cell.chanelName.text = Explore.name;
//
        
        let Explore : ExploreChannelListModal = channelArray[indexPath.row];
       
        cell.chanelName.lineBreakMode = .byTruncatingMiddle
        cell.chanelName.lineBreakMode = .byWordWrapping
        cell.chanelName.numberOfLines = 0;
        cell.chanelName.text = Explore.name;
        let greet4Height = cell.chanelName.optimalHeight
        cell.chanelName.frame = CGRect(x: cell.chanelName.frame.origin.x, y: cell.chanelName.frame.origin.y, width: cell.chanelName.frame.width, height: greet4Height)
        
        
        cell.totalChannelPost.text = Explore.count_post + " Channel Post";
        if(Explore.photoUrl == nil)
        {
        }
        else
        {
            do {
                try   cell.channelImg.sd_setImage(with:URL(string: Explore.photoUrl), placeholderImage: UIImage(named: "vc_channel"))
            }catch {
                print("Unexpected error: \(error).")
            }
            
        }
        let tap = UITapGestureRecognizer(target: self, action: #selector(clickPos))
        cell.channelImg.addGestureRecognizer(tap)
        cell.channelImg.isUserInteractionEnabled = true
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let xyz : ExploreChannelListModal = channelArray[(indexPath.row)]
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ExploreChannelDetailsViewController") as! ExploreChannelDetailsViewController
        vc.chanalID = xyz.id;
        vc.expID = expID;
        self.present(vc, animated:true, completion:nil)
    }
    
    
    
    
    
    
    
    
    @objc func clickPos(_ sender: UITapGestureRecognizer)
    {
        let touch = sender.location(in: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: touch)
        let xyz : ExploreChannelListModal = channelArray[(indexPath?.row)!]
        
        let signUp = self.storyboard?.instantiateViewController(withIdentifier: "ExploreChannelDetailsViewController") as! ExploreChannelDetailsViewController
        signUp.chanalID = xyz.id;
        signUp.expID = expID;
        self.navigationController?.pushViewController(signUp, animated: true)
    }
    
    
    
    
    
    
    func getConnectedChannel()
    {
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Loading"
        //MBProgressHUD.hide(for: self.view, animated: false)
        
        var requestURL =  ""
        var parameters: [String:String] = ["": ""]
        if(segmentIndex == 0)
        {
            requestURL =  BASEURL + getConnectedChannels ;
            parameters = ["channel_id": channel_id ,"token": token , "currentpage": String(pageNoBranch)]
        }
        else{
            requestURL =  BASEURL + getConnectedChannelExtra;
            parameters = ["channel_id": channel_id ,"token": token , "currentpage":String(pageNoOffiChan)]
        }
        
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            MBProgressHUD.hide(for: self.view, animated: false)
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)-- \(response)")
            if success
            {
                let abc = response["data"].arrayObject;
                self.parse(json: response)
                
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
        })
    }
    
    func parse(json: JSON)
    {
        
        if(segmentIndex == 0)
        {
            channelArray = arrBranches;
            if(json["data"].arrayValue.count < 10)
            {
                isAvilableForLoadBranch = false
            }
        }
        else
        {
            channelArray = arrOfficeChannel;
            if(json["data"].arrayValue.count < 10)
            {
                isAvilableForLoadOfficeChan = false
            }
        }
        
        for result in json["data"].arrayValue {
            let id = result["id"].stringValue
            let name = result["name"].stringValue
            let photoUrl = result["photoUrl"].stringValue
            let count_post = String(result["count_post"].intValue)
            let is_extra_branch = result["is_extra_branch"].stringValue
            let catNAme1 : ExploreChannelListModal = ExploreChannelListModal(id: id , name: name,  photoUrl: photoUrl, count_post: count_post , count_newpost: "");
            if(segmentIndex == 0)
            {
                if is_extra_branch == "0"
                {
                    arrBranches.append(catNAme1);
                }
            }
            else
            {
                arrOfficeChannel.append(catNAme1);
            }
            channelArray.append(catNAme1);
        }
        
        self.tableView.reloadData();
        MBProgressHUD.hide(for: self.view, animated: false)
    }
    
    
    
    var loadingData = false;
    var isAvilableForLoadBranch = true;
    var isAvilableForLoadOfficeChan = true;
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = channelArray.count - 3
        if !loadingData && indexPath.row == lastElement {
            loadingData = true
            loadMoreData()
        }
    }
    func loadMoreData() {
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                if(self.segmentIndex == 0)
                {
                    self.pageNoBranch =  self.pageNoBranch + 1
                    if(self.isAvilableForLoadBranch)
                    {
                        self.getConnectedChannel();
                    }
                }
                else{
                    self.pageNoOffiChan =  self.pageNoOffiChan + 1
                    if(self.isAvilableForLoadOfficeChan)
                    {
                        self.getConnectedChannel();
                    }
                }
                self.loadingData = false
            }
        }
    }
    
    
    
}
