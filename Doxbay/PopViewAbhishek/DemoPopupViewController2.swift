//
//  DemoPopupViewController2.swift
//  PopupController
//
//  Created by 佐藤 大輔 on 2/4/16.
//  Copyright © 2016 Daisuke Sato. All rights reserved.
//

var  AttRepView : Attendence_Model? = nil

import UIKit

class DemoPopupViewController2: UIViewController, PopupContentViewController {
    
    
    @IBOutlet var pDate: UILabel!
    @IBOutlet var pDayType: UILabel!
    @IBOutlet var pTotalHr: UILabel!
    
    @IBOutlet var pInTime: UILabel!
    @IBOutlet var pInCirRa: UILabel!
    @IBOutlet var pIsInCerRa: UILabel!
    @IBOutlet var pInAddress: UILabel!

    @IBOutlet var pOutTime: UILabel!
    @IBOutlet var pOutCerRa: UILabel!
    @IBOutlet var pIsInOutCerRa: UILabel!
    @IBOutlet var pOutAdd: UILabel!

    @IBAction func closedPoup(_ sender: Any) {
        popup.dismiss()
    }
    
    var closeHandler: (() -> Void)?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.layer.cornerRadius = 4
        
       pDate.text = AttRepView?.date
       pDayType.text = AttRepView?.date_type
       pTotalHr.text = AttRepView?.Total_Working
        pInTime.text = AttRepView?.In_Time
        pInCirRa.text = (AttRepView?.INRadius)! + " Meters"
        if(AttRepView?.INIsRadius == "1")
        {
            pIsInCerRa.text = "Yes"
        }
        else
        {
           pIsInCerRa.text = "No"
        }

        //pInAddress.text = AttRepView?.INPlace
        
        pOutTime.text = AttRepView?.Out_Time
        pOutCerRa.text = (AttRepView?.OUTRadius)! + " Meters"
        if(AttRepView?.OUTIsRadius == "1")
        {
            pIsInOutCerRa.text = "Yes"
        }
        else
        {
            pIsInOutCerRa.text = "No"
        }
       // pOutAdd.text = AttRepView?.OUTPlace
        
        
        
        
        pOutAdd.lineBreakMode = .byTruncatingMiddle
        pOutAdd.lineBreakMode = .byWordWrapping
        pOutAdd.numberOfLines = 0;
        pOutAdd.text = AttRepView?.OUTPlace
        let greet4Height = pOutAdd.optimalHeight
        pOutAdd.frame = CGRect(x: pOutAdd.frame.origin.x, y: pOutAdd.frame.origin.y, width: pOutAdd.frame.width, height: greet4Height)
        
        
        pInAddress.lineBreakMode = .byTruncatingMiddle
        pInAddress.lineBreakMode = .byWordWrapping
        pInAddress.numberOfLines = 0;
        pInAddress.text = AttRepView?.OUTPlace
        
        let greet4Heightx = pInAddress.optimalHeight
        pInAddress.frame = CGRect(x: pInAddress.frame.origin.x, y: pInAddress.frame.origin.y, width: pInAddress.frame.width, height: greet4Heightx)
        
    }
    
    class func instance() -> DemoPopupViewController2 {
        let storyboard = UIStoryboard(name: "DemoPopupViewController2", bundle: nil)
        return storyboard.instantiateInitialViewController() as! DemoPopupViewController2
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    var popup = PopupController()
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        popup = popupController
        return CGSize(width: 320, height: 400)
    }
    
    
    
    
    
    
}


extension UILabel {
    var optimalHeight : CGFloat {
        get
        {
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: CGFloat.greatestFiniteMagnitude))
            label.numberOfLines = 0
            label.lineBreakMode = NSLineBreakMode.byWordWrapping
            label.font = self.font
            label.text = self.text
            label.sizeToFit()
            return label.frame.height
        }
        
    }
}
