//
//  HelperClass.swift
//  ECG Live
//
//  Created by Salman Ghumsani on 6/30/16.
//  Copyright © 2016 Affle AppStudioz. All rights reserved.
//

import UIKit
import CoreLocation


var myBundle:Bundle?
var viewBackground:UIView?
var mainNavigationController:UINavigationController?

struct Constant {
    static let APP_BLUE_COLOR               = UIColor.getRGBColor(52, g: 120, b: 223)
    static let kAPPNAME                     = "Doxbay"
    static let IS_FIRST_LAUNCH              = "isFirstLaunch"
    static let kPAGE_LIMIT                  = "ReadPageCount"
    static let kIS_LOGIN                    = "isLogin"
    static let kAPI_NAME                    = "apiName"
    static let kERROR_MSG                   = "errorMsg"
    static let kERROR_CODE                  = "errorCoed"
}


class RestrictTextView: UITextView{
    override public func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        
        return false
    }
}

//MARK: *************** Extension UIColor ***************
extension UIColor{
    
    var r: CGFloat{
        return self.cgColor.components![0]
    }
    
    var g: CGFloat{
        return self.cgColor.components![1]
    }
    
    var b: CGFloat{
        return self.cgColor.components![2]
    }
    
    var alpha: CGFloat{
        return self.cgColor.components![3]
    }
    class func getRGBColor(_ r:CGFloat,g:CGFloat,b:CGFloat)-> UIColor {
        return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: 1.0)
    }
}

//MARK: *************** Extension UIImage ***************
extension UIImage {
    func imageWithColor(_ color1: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        color1.setFill()
        let context = UIGraphicsGetCurrentContext()
        context!.translateBy(x: 0, y: self.size.height)
        context!.scaleBy(x: 1.0, y: -1.0);
        context!.setBlendMode(CGBlendMode.normal)
        let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
        context!.clip(to: rect, mask: self.cgImage!)
        context!.fill(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()! as UIImage
        UIGraphicsEndImageContext()
        return newImage
    }
  
    func resizeImage(targetSize: CGSize) -> UIImage {
        let size = self.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
        }
        let rect = CGRect(x: 0, y:0 , width: newSize.width, height: newSize.height)
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    
    
}

extension UIImage {
     func fromColor(_ color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
}

//MARK: *************** Extension UITabBar ***************
extension UITabBar {
    
    override open func sizeThatFits(_ size: CGSize) -> CGSize {
        super.sizeThatFits(size)
        var sizeThatFits = super.sizeThatFits(size)
        if UI_USER_INTERFACE_IDIOM() == .pad{
            sizeThatFits.height = 100
        }
        return sizeThatFits
    }
}

//MARK: *************** Extension UIViewController ***************
extension UIViewController
{
    
    func otherStoryboard()-> UIStoryboard {
        return UIStoryboard(name: "Other", bundle: nil)
    }
    
    func navigateScreenTo(name: String, storyBoard:String = "Main") {
        let storyboard = UIStoryboard(name: storyBoard, bundle: nil)
        let instance = storyboard.instantiateViewController(withIdentifier: name)
        self.navigationController?.pushViewController(instance, animated: true)
    }
    
    
    func showDescriptionInAlert(_ desc:String,title:String,animationTime:Double,descFont:UIFont) {
        viewBackground?.removeFromSuperview()
        viewBackground = UIView(frame: UIScreen.main.bounds)
        let viewAlertContainer = UIView()
        let txtViewDesc = UITextView()
        viewAlertContainer.backgroundColor = UIColor.white
        viewBackground!.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        txtViewDesc.backgroundColor = UIColor.white
        viewBackground!.alpha = 0.0
        txtViewDesc.text = desc
        txtViewDesc.isEditable = false
        txtViewDesc.font = descFont
        viewAlertContainer.makeRoundCorner(10)
        txtViewDesc.textAlignment = .center
        var txtViewHeight = CGRect(x: 0, y: 30, width: UIScreen.main.bounds.width-80, height: 100)
        let height = heightForLabel(desc, width: UIScreen.main.bounds.width-80, font: descFont)
        if height > 80 && height < 300 {
            txtViewHeight.size.height = height+20
        }
        let lblTitle = UILabel(frame: CGRect(x: 0, y: 8, width: UIScreen.main.bounds.width-80, height: 20))
        lblTitle.text = title
        lblTitle.textColor = UIColor.black
        lblTitle.font = UIFont.systemFont(ofSize: 18)
        lblTitle.backgroundColor = UIColor.white
        lblTitle.textAlignment = .center
        txtViewDesc.frame = txtViewHeight
        viewAlertContainer.frame = CGRect(x: 40, y: 100, width: UIScreen.main.bounds.width-80, height: txtViewHeight.size.height+30)
        viewAlertContainer.center = viewBackground!.center
        viewAlertContainer.addSubview(lblTitle)
        viewAlertContainer.addSubview(txtViewDesc)
        viewBackground!.addSubview(viewAlertContainer)
        self.view.addSubview(viewBackground!)
        UIView.animate(withDuration: animationTime) {
            viewBackground!.alpha = 1.0
        }
    }
    
    func hideDescriptionAlert(_ animationTime:Double) {
        UIView.animate(withDuration: animationTime, animations: { 
            viewBackground?.alpha = 0.0
        }) { (compelete) in
            if compelete {
                viewBackground?.removeFromSuperview()
            }
        }
    }
    
    func showOkAlert(_ msg: String) {
        let alert = UIAlertController(title:
            Constant.kAPPNAME, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    func showOkAlertWithHandler(_ msg: String,handler: @escaping ()->Void){
        let alert = UIAlertController(title: Constant.kAPPNAME, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (type) -> Void in
            handler()
        }
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    
    
    func showAlertWithActions(msg: String,titles:[String], handler:@escaping (_ clickedIndex: Int) -> Void) {
        
        
        if titles.last?.lowercased() == "cancel" {
            let alert = UIAlertController(title: Constant.kAPPNAME, message: msg, preferredStyle: .actionSheet)
            for title in titles {
                if title.lowercased() == "cancel" {
                    let action  = UIAlertAction(title: title, style: .cancel, handler: { (alertAction) in
                        //Call back fall when user clicked
                        let index = titles.index(of: alertAction.title!)
                        if index != nil {
                            handler(index!+1)
                        }
                        else {
                            handler(0)
                        }
                        
                    })
                    alert.addAction(action)
                }
                else{
                    let action  = UIAlertAction(title: title, style: .default, handler: { (alertAction) in
                        //Call back fall when user clicked
                        let index = titles.index(of: alertAction.title!)
                        if index != nil {
                            handler(index!+1)
                        }
                        else {
                            handler(0)
                        }
                    })
                    alert.addAction(action)
                }
            }
            present(alert, animated: true, completion: nil)
        }
        else {
            let alert = UIAlertController(title: Constant.kAPPNAME, message: msg, preferredStyle: .alert)
            
            for title in titles {
                
                let action  = UIAlertAction(title: title, style: .default, handler: { (alertAction) in
                    //Call back fall when user clicked
                    let index = titles.index(of: alertAction.title!)
                    if index != nil {
                        handler(index!+1)
                    }
                    else {
                        handler(0)
                    }
                    
                })
                alert.addAction(action)
            }
            present(alert, animated: true, completion: nil)
        }
    }

    
    func showOkCancelAlertWithAction(_ msg: String, handler:@escaping (_ isOkAction: Bool) -> Void) {
        let alert = UIAlertController(title: Constant.kAPPNAME, message: msg, preferredStyle: .alert)
        let okAction =  UIAlertAction(title: "OK", style: .default) { (action) -> Void in
            return handler(true)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
            return handler(false)
        }
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
}

//MARK: *************** Extension NSDate ***************
extension Date{
    
    var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var today: Date {
        return Calendar.current.date(byAdding: .day, value: 0, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return tomorrow.month != month
    }
    
    var day: Int {
        return Calendar.current.component(.day,  from: self)
    }
    
    func convertToString(_ validDateFormatter:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = validDateFormatter //"dd MMM yyyy" //yyyy-mm-dd hh:mm 
        return dateFormatter.string(from: self as Date)
        
    }
    
    func getSuffixDay()->String {
        let n = self.day
        var suffix = ""
        switch n%10 {
            case 1:  suffix = "st";
            case 2:  suffix = "nd";
            case 3:  suffix = "rd";
            default: suffix = "th";
        }
        return "\(n)\(suffix)"
    }
    
    var age: Int {
        return Calendar.current.dateComponents([.year], from: self, to: Date()).year!
    }
    
    func dayOfWeek() -> String? {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "EEEE"
            return dateFormatter.string(from: self).capitalized
        }
}

//MARK: *************** Extension CLLocation ***************

extension CLLocation{
    func getCityName(_ City:@escaping (_ city:String)->Void){
        
        let geoCoder = CLGeocoder()
        
        geoCoder.reverseGeocodeLocation(self, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            
            // Address dictionary
            print(placeMark.addressDictionary ?? "")
            
            // Location name
            if let locationName = placeMark.addressDictionary!["Name"] as? NSString {
                print(locationName)
            }
            
            // Street address
            if let street = placeMark.addressDictionary!["Thoroughfare"] as? NSString {
                print(street)
            }
            
            // City
            if let city = placeMark.addressDictionary!["City"] as? NSString {
                City(city as String)
                print(city)
            }
            
            // Zip code
            if let zip = placeMark.addressDictionary!["ZIP"] as? NSString {
                print(zip)
            }
            
            // Country
            if let country = placeMark.addressDictionary!["Country"] as? NSString {
                print(country)
            }
        })
    }
    
    func getDistance(location: CLLocation, withMiles:Bool)-> String {
        if withMiles {
            return "\(round(100*(self.distance(from: location).inMiles()))/100)"
        }else {
            return "\(round(100*(self.distance(from: location).inKilometers()))/100)"
        }
        
        
    }
    
}

extension CLLocationDistance {
    func inMiles() -> CLLocationDistance {
        return self*0.00062137
    }
    
    func inKilometers() -> CLLocationDistance {
        return self/1000
    }
}

//MARK: *************** Extension String ***************

extension String {
    
    func getDateInstance(validFormate:String)-> Date? {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = validFormate
        dateFormater.date(from:self)
        return dateFormater.date(from:self)
    }
    
    func hasSpecialCharacter() -> Bool {
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        if self.rangeOfCharacter(from: characterset.inverted) != nil {
            return true
        }
        return false
    }
    
   
    
    var isNumber : Bool {
        get{
            return !self.isEmpty && self.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
        }
    }
    
    func trim() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    var isContainAnyNumber : Bool{
        let decimalCharacters = CharacterSet.decimalDigits
        let decimalRange = self.rangeOfCharacter(from: decimalCharacters)
        if decimalRange != nil {
            return true
        }
        else{
            return false
        }
    }
    
    var isContainAnyAlfabits : Bool {
        let str1 = self
        let str2 = self
        let capitalizedLetters = CharacterSet.capitalizedLetters
        let lowercaseLetters   = CharacterSet.lowercaseLetters
        let capitalRange = str1.rangeOfCharacter(from: capitalizedLetters)
        let lowerRange = str2.rangeOfCharacter(from: lowercaseLetters)

        if capitalRange != nil{
            return true
        }
        else if  lowerRange != nil{
            return true
        }
        else{
            return false
        }
    }
    
    
    func fromBase64() -> String?{
        let sDecode = self.removingPercentEncoding
        return sDecode
    }
}

//MARK: *************** Extension UIView ***************
extension UIView
{
    class func fromNib<T : UIView>(xibName: String) -> T {
        return Bundle.main.loadNibNamed(xibName, owner: nil, options: nil )![0] as! T
    }
    
    func typeName(_ some: Any) -> String {
        return (some is Any.Type) ? "\(some)" : "\(type(of: some))"
    }
    
    func makeRounded(){
        DispatchQueue.main.async {
            self.layer.cornerRadius = self.frame.size.width/2
            self.clipsToBounds = true
        }
    }
    
    func makeRoundCorner(_ radius:CGFloat){
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
    
    func makeBorder(_ width:CGFloat,color:UIColor)
    {
        self.layer.borderWidth = width
        self.layer.borderColor = color.cgColor
        self.clipsToBounds = true
    }
    
    //give the border to UIView
    func border(radius : CGFloat,borderWidth : CGFloat,color :CGColor){
        self.layer.masksToBounds = true
        self.layer.cornerRadius  = radius
        self.layer.borderWidth   = borderWidth
        self.layer.borderColor   = color
    }
    
    //give the circle border to UIView
    func circleBorder(){
        let hight = self.layer.frame.height
        let width = self.layer.frame.width
        if hight < width{
            self.layer.cornerRadius = hight/2
            self.layer.masksToBounds = true
        }
        else{
            self.layer.cornerRadius  = width/2
            self.layer.masksToBounds = true
        }
    }
    
    func addShadow(radius: CGFloat){
        self.layer.cornerRadius = 10
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = radius
        self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        self.layer.masksToBounds = false
        
     
        
      
    }

}


//MARK: *************** Global Helper Method ***************
func setLanguage(_ lang:String){
    let path    =   Bundle.main.path(forResource: lang, ofType: "lproj")
    if path == nil{
        myBundle  = Bundle.main
    }
    else{
        myBundle = Bundle(path:path!)
        if (myBundle == nil) {
            myBundle = Bundle.main
        }
    }
}

func localized(_ key:String) -> String{
   return NSLocalizedString(key, tableName: "English", bundle: Bundle.main, value: "", comment: "")
}

func setUserDefault(_ key:String,value:String){
    print_debug(value)
    print_debug(key)
    let defaults = UserDefaults.standard
    defaults.setValue(value, forKey: key)
    defaults.synchronize()
}

func getUserDefault(_ key:String) ->String{
    print_debug(key)
    let defaults = UserDefaults.standard
    let val = defaults.value(forKey: key)
    if val != nil{
        return val! as! String 
    }
    else{
        return ""
    }
}

func removeUserDefault(_ key:String){
    let defaults = UserDefaults.standard
    defaults.removeObject(forKey: key)
}

func setDictUserDefault(_ detail:Dictionary<String,Any>){
    for (key,value) in detail{
        if(detail[key] != nil){
            print_debug("\(key)")
            setUserDefault(key, value: "\(value)")
        }
    }
}

func heightForLabel(_ text:String,width:CGFloat,font:UIFont) -> CGFloat{
    let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
    label.numberOfLines = 0
    label.lineBreakMode = NSLineBreakMode.byWordWrapping
    label.font = font
    label.text = text
    label.sizeToFit()
    return label.frame.height
}

func print_debug<T>(_ obj:T,file: String = #file, line: Int = #line, function: String = #function) {
    let fileName =  file.description.components(separatedBy: "/")//split(file.description) {$0 == " "}
    print("File:'\(fileName.last!)' Line:'\(line)' :: \(obj)")
}

func uniq<S: Sequence, E: Hashable>(_ source: S) -> [E] where E==S.Iterator.Element {
    var seen: [E:Bool] = [:]
    return source.filter { seen.updateValue(true, forKey: $0) == nil }
}

//MARK: *************** Helper Class ***************

class SGHelper: NSObject {
    static let viewLogo = UIView()
    
    struct Platform {
        static var isSimulator: Bool {
            return TARGET_OS_SIMULATOR != 0 // Use this line in Xcode 7 or newer
            //return TARGET_IPHONE_SIMULATOR != 0 // Use this line in Xcode 6
        }
    }
    class func isValidEmail(_ testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    class func getDictionary(_ detail: Any?) -> Dictionary<String, Any>
    {
        guard let dict = detail as? Dictionary<String, Any> else
        {
            return ["":"" as Any]
        }
        return dict
    }
    class func convertArrayToString(arr:Array<Any>)->String{
        var retStr = ""
        for str in arr{
            if retStr != ""{
                retStr = "\(retStr),\(str)"
            }
            else{
                retStr = "\(str)"
            }
        }
        
        return retStr
    }
    
    class func mergeImage(bottomImage: UIImage, topImage: UIImage) -> UIImage{
        let size = CGSize(width: 50, height: 50)
        UIGraphicsBeginImageContext(size)
        let topImg = topImage.resizeImage(targetSize: CGSize(width: 50, height: 50))
        let areaSize = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        bottomImage.draw(in: areaSize)
        print(topImg.size)
        topImg.draw(in: areaSize, blendMode: .normal, alpha: 1.0)

        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        print(newImage.size)
        return newImage
    }
    
    // Write text on Image
   
    
    class func startAnimateLogo() {
        let color = UIColor(red: 36.0/255, green: 27.0/255, blue: 27.0/255, alpha: 0.59)////UIColor.clear.withAlphaComponent(0.75)
        let bounds = UIScreen.main.bounds
        viewLogo.backgroundColor = color
        viewLogo.frame = bounds
        let animateRect = CGRect(x: bounds.size.width/2-50, y: bounds.size.height/2-50, width: 100, height: 100)
        //let viewRect = CGRect(x: bounds.size.width/2-50, y: bounds.size.height/2-50, width: 130, height: 130)
        let imgViewAnimating = UIImageView(frame: animateRect)
        //let uiview = UIView(frame: viewRect)
        //uiview.backgroundColor = UIColor(red: 83.0/255, green: 75.0/255, blue: 75.0/255, alpha: 0.59)////UIColor.black
        var images = [UIImage]()
        for index in 1...52 {
            print(index)
            images.append(UIImage(named: "BounceWhite-\(index) (dragged)")!)
        }
        imgViewAnimating.animationImages = images
        imgViewAnimating.animationDuration = 5
        imgViewAnimating.startAnimating()
        imgViewAnimating.backgroundColor = UIColor.black
        //viewLogo.addSubview(uiview)
        viewLogo.addSubview(imgViewAnimating)
        UIApplication.shared.keyWindow?.addSubview(viewLogo)
    }
    
    class func stopAnimateLogo() {
        viewLogo.removeFromSuperview()
        for view in viewLogo.subviews {
            view.removeFromSuperview()
        }
    }
    
    
    
}

//MARK: ************* Extension UIDatePicker *************

extension UIDatePicker {
    /// Returns the date that reflects the displayed date clamped to the `minuteInterval` of the picker.
    /// - note: Adapted from [ima747's](http://stackoverflow.com/users/463183/ima747) answer on [Stack Overflow](http://stackoverflow.com/questions/7504060/uidatepicker-with-15m-interval-but-always-exact-time-as-return-value/42263214#42263214})
    public var clampedDate: Date {
        let referenceTimeInterval = self.date.timeIntervalSinceReferenceDate
        let remainingSeconds = referenceTimeInterval.truncatingRemainder(dividingBy: TimeInterval(minuteInterval*60))
        let timeRoundedToInterval = referenceTimeInterval - remainingSeconds
        return Date(timeIntervalSinceReferenceDate: timeRoundedToInterval)
    }
}



