//
//  SGServiceController.swift
//  SG
//
//  Created by Salman Ghumsani on 7/1/16.
//  Copyright © 2016 Appsinvo All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON

class SGServiceController: NSObject {
    
    var BASE_URL = BASEURL  //"https://doxbay.com/test/"
    
    //MARK: ************* GET INSTANCE *************
    class func instance() -> SGServiceController {
        return SGServiceController()
    }
    
    //MARK: ************* GET IMAGE WITH URL *************
    func getImage(_ url:String,handler: @escaping (UIImage?)->Void) {
        print(url," :url")
        Alamofire.request(url, method: .get).responseImage { response in
            guard let image = response.result.value else {
                print(response)
                handler(nil)
                return
            }
            handler(image)
            print(image)
        }
    }

    
    //MARK: ============= HIT GET SERVICE =============
    func hitGetService(_ strURL:String,handler: @escaping((Dictionary<String,Any>?)-> Void),unReachable:(() -> Void)) {
        print_debug(strURL)
        if networkReachable() == false {
            unReachable()
        }
        let encodeURL   =   strURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        if encodeURL == nil {
            print_debug("We can't hit please check url")
            return
        }
        Alamofire.request(encodeURL!).responseJSON { response in
            //print_debug("Request: \(String(describing: response.request))")   // original url request
            //print_debug("Response: \(String(describing: response.response))") // http url response
            //print_debug("Result: \(response.result)")                         // response serialization result
            switch response.result {
            case .success:
                if let jsonDict = response.result.value as? Dictionary<String,Any> {
                    print_debug("Json Response: \(jsonDict)") // serialized json response
                    handler(jsonDict)
                }
                else{
                    handler(nil)
                }
                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                    print("Server Response: \(utf8Text)") // original server data as UTF8 string
                }
                break
            case .failure(let error):
                handler(nil)
                print_debug(error)
                break
            }
        }
    }
    
    //MARK: ************* HIT POST SERVICE JSON FORM*************
    func hitPostServiceJsonForm(_ params:Dictionary<String,Any>,unReachable:(() -> Void),handler:@escaping ((Dictionary<String,Any>?) -> Void)) {
        if networkReachable() == false {
            unReachable()
            SwiftLoader.hide()
            return
        }
        print_debug(params)
        BASE_URL = "\(BASE_URL)\(params[Constant.kAPI_NAME]!)"
        var request = URLRequest(url: URL(string: BASE_URL)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
        print_debug(BASE_URL)
        Alamofire.request(request).responseJSON { response in
            SwiftLoader.hide()
            print_debug("Request: \(String(describing: response.request))")   // original url request
            print_debug("Response: \(String(describing: response.response))") // http url response
            print_debug("Result: \(response.result)")                         // response serialization result
            switch response.result {
            case .success:
                if let jsonDict = response.result.value as? Dictionary<String,Any> {
                    print_debug("Json Response: \(jsonDict)") // serialized json response
                    handler(jsonDict)
                }
                else{
                    handler(nil)
                }
                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                    print("Server Response: \(utf8Text)") // original server data as UTF8 string
                }
                break
            case .failure(let error):
                handler(nil)
                print_debug(error)
                break
            }
        }
    }
    
    
    func requestPOSTURLWithFormData( params : [String : String]?,imageData: Data?, headers : [String : String]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        BASE_URL = "\(BASE_URL)\(params![Constant.kAPI_NAME]!)"
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in params! {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                
            }
            if let data = imageData{
                multipartFormData.append(data, withName: "image", fileName: "image.jpg", mimeType: "image/jpeg")
            }
            
        }, usingThreshold: UInt64.init(), to: BASE_URL, method: .post, headers: headers) { (result) in
            
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    
                    print("Succesfully uploaded")
                    print(response.result)
                    
                    if response.result.isSuccess {
                        let resJson = JSON(response.result.value!)
                        success(resJson)
                    }
                    if response.result.isFailure {
                        let error : Error = response.result.error!
                        failure(error)
                    }
                    
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                failure(error)
            }
        }
    }
    
    func requestPOSTURLWithFormDataAndImageArrayAndPDFArray(params : [String : String]?, headers : [String : String]?, imageArray : [Any]?, pdfArray : [Any]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        BASE_URL = "\(BASE_URL)\(params![Constant.kAPI_NAME]!)"
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in params! {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                
            }
            
            if(imageArray!.count > 0)
            {
                var count = 0
                for img in imageArray!
                {
                    count += 1
                    let data = UIImagePNGRepresentation(img as! UIImage) as Data?
                    if (data != nil)
                    {
                        let imageName = "image" + String(count)
                        multipartFormData.append(data!, withName: imageName, fileName: "image.png", mimeType: "image/png")
                    }
                }
            }            
        }, usingThreshold: UInt64.init(), to: BASE_URL, method: .post, headers: headers) { (result) in
            
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    
                    print("Succesfully uploaded")
                    print(response.result)
                    
                    if response.result.isSuccess {
                        let resJson = JSON(response.result.value!)
                        success(resJson)
                    }
                    if response.result.isFailure {
                        let error : Error = response.result.error!
                        failure(error)
                    }
                    
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                failure(error)
            }
        }
    }
    
    
    //MARK: *********** HIT POST SERVICE ***********
    func hitPostService(_ params:Dictionary<String,String>,unReachable:(() -> Void),handler:@escaping ((Dictionary<String,Any>?) -> Void)) {
        if networkReachable() == false {
            unReachable()
        }
        
        
        BASE_URL = "\(BASE_URL)\(params[Constant.kAPI_NAME]!)"
        print(BASE_URL)
        print(params)
        
        
    
        Alamofire.request(BASE_URL, method: .post, parameters: params, encoding: URLEncoding.default).responseJSON { response in
            print_debug("Result: \(response.result)")                         // response serialization result
            do {
                 let json = try JSON(data: response.data!)
                print(json)
            } catch {
                print(error)
            }
            
            
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                print("Server Response: \(utf8Text)") // original server data as UTF8 string
            }
            switch response.result {
            case .success:
                if let jsonDict = response.result.value as? Dictionary<String,Any> {
                    print_debug("Json Response: \(jsonDict)") // serialized json response
                    if jsonDict[Constant.kERROR_CODE] as? String == "2"{
                        SwiftLoader.hide()
                        let alert = UIAlertController(title: Constant.kAPPNAME, message: jsonDict[Constant.kERROR_MSG] as? String, preferredStyle: .alert)
                        let okAction =  UIAlertAction(title: "OK", style: .default) { (action) -> Void in
                        }
                        alert.addAction(okAction)
                      //  mainNavigation?.present(alert, animated: true, completion: nil)
                    }
                    else{
                        handler(jsonDict)
                    }
                }
                else{
                    handler(nil)
                }
                
                break
            case .failure(let error):
                handler(nil)
                print_debug(error)
                break
            }
        }
    }
    
    
    
    
    //MARK: ************* MULTIPART SERVICES *************
    func hitMultipartForImage(_ params:Dictionary<String,String>,image:UIImage?,imageParamName : String?,unReachable:(() -> Void),handler:@escaping ((Dictionary<String,Any>?) -> Void)) {
        print_debug("Params:  \(params)")
        BASE_URL = "\(BASE_URL)\(params[Constant.kAPI_NAME]!)"
        var imageParam = "image"
        if imageParamName ?? "" != ""{
            imageParam = imageParamName!
        }
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                for (key, value) in params {
                    if "appendURL" != key {
                        multipartFormData.append(value.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
                    }
                }
                if image != nil{
                    let imgData = UIImageJPEGRepresentation(image!, 0.5)
                    if imgData != nil {
                        print_debug("img data available")
                        multipartFormData.append(imgData!, withName: imageParam, fileName: "file.png", mimeType: "image/png")
                    }
                }
        },
            to: BASE_URL,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                   
                        switch response.result {
                        case .success:
                            if let jsonDict = response.result.value as? Dictionary<String,Any> {
                                print_debug("Json Response: \(jsonDict)") // serialized json response
                                handler(jsonDict)
                            }
                            else{
                                handler(nil)
                            }
                            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                                print("Server Response: \(utf8Text)") // original server data as UTF8 string
                            }
                            break
                        case .failure(let error):
                            handler(nil)
                            print_debug(error)
                            break
                        }
                    }
                case .failure(let encodingError):
                    print(encodingError)
                }
        }
        )
    }
    
    
    
    
    
    
    
    
    
    func hitMultipartOnlyParam(_ params:Dictionary<String,String>,unReachable:(() -> Void),handler:@escaping ((Dictionary<String,Any>?) -> Void))
    {
        BASE_URL = "\(BASE_URL)\(params[Constant.kAPI_NAME]!)"
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                for (key, value) in params {
                    if "method" != key {
                        //print_debug("\(key) == \(value)")
                        multipartFormData.append(value.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
                    }
                }
        },
            to: BASE_URL,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON  { response in
                        if let jsonDict = response.result.value as? Dictionary<String,Any> {
                            print_debug("Json Response: \(jsonDict)") // serialized json response
                            handler(jsonDict)
                        }
                        else{
                            handler(nil)
                        }
                        if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                            print("Server Response: \(utf8Text)") // original server data as UTF8 string
                        }
                    }
                    break
                case .failure(let encodingError):
                    handler(nil)
                    print_debug(encodingError)
                    break
                }
        }
        )
    }
    
    func hitMultipartForImageArray(_ params:Dictionary<String,String>,dictImages: [String:Any],unReachable:(() -> Void),handler:@escaping ((Dictionary<String,Any>?) -> Void)) {
        
        print_debug("Params:\(params)")
        BASE_URL = "\(BASE_URL)\(params[Constant.kAPI_NAME]!)"
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                for (key, value) in params {
                    if "appendURL" != key {
                        multipartFormData.append(value.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
                    }
                }
                for (_, dict) in dictImages.enumerated() {
                    for image in dict.value as! [UIImage] {
                        if let imgData = UIImageJPEGRepresentation(image, 0.5) {
                            multipartFormData.append(imgData, withName: "\(dict.key)[]", fileName: "file.png", mimeType: "image/png")
                        }
                    }
                }
                
        },
            to: BASE_URL,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        switch response.result {
                        case .success:
                            if let jsonDict = response.result.value as? Dictionary<String,Any> {
                                print_debug("Json Response: \(jsonDict)") // serialized json response
                                handler(jsonDict)
                            }
                            else{
                                handler(nil)
                            }
                            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                                print("Server Response: \(utf8Text)") // original server data as UTF8 string
                            }
                            break
                        case .failure(let error):
                            handler(nil)
                            print_debug(error)
                            break
                        }
                    }
                case .failure(let encodingError):
                    handler(nil)
                    print(encodingError)
                }
        }
        )
    }
    
}
//MARK: ************* CHECK NETWORK REACHABILITY *************
func networkReachable() -> Bool {
    return (NetworkReachabilityManager()?.isReachable)!
}





