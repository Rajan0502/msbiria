//
//  quizQuestoatalViewController.swift
//  Doxbay
//
//  Created by kavi yadav on 19/06/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit

class quizQuestoatalViewController: UIViewController {

    @IBOutlet weak var logoView: UIView!
    @IBOutlet weak var pointsLbl: UILabel!
    @IBOutlet weak var questLbl: UILabel!
    @IBOutlet weak var pointsView: UIView!
    @IBOutlet weak var questionView: UIView!
    var QuizId = String ()
    override func viewDidLoad() {
        super.viewDidLoad()
        print(QuizId)
self.setShadow()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBOutlet weak var tapONBack: UIButton!
    

    @IBAction func Back(_ sender: Any) {
        self.presentingViewController?.dismiss(animated: true, completion:nil)

    }
    func setShadow()  {
    self.questionView.layer.shadowOffset = CGSize(width: 0, height: 3)
    self.questionView.layer.shadowRadius = 3
    self.questionView.layer.shadowOpacity = 0.3
    self.questionView.layer.shadowColor = UIColor.black.cgColor
    self.questionView.layer.shadowPath = UIBezierPath(roundedRect: self.questionView.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 8, height: 8)).cgPath
    self.questionView.layer.shouldRasterize = true
    self.questionView.layer.rasterizationScale = UIScreen.main.scale
        
        self.pointsView.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.pointsView.layer.shadowRadius = 3
        self.pointsView.layer.shadowOpacity = 0.3
        self.pointsView.layer.shadowColor = UIColor.black.cgColor
        self.pointsView.layer.shadowPath = UIBezierPath(roundedRect: self.pointsView.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 8, height: 8)).cgPath
        self.pointsView.layer.shouldRasterize = true
        self.pointsView.layer.rasterizationScale = UIScreen.main.scale

        self.logoView.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.logoView.layer.shadowRadius = 3
        self.logoView.layer.shadowOpacity = 0.3
        self.logoView.layer.shadowColor = UIColor.black.cgColor
        self.logoView.layer.shadowPath = UIBezierPath(roundedRect: self.logoView.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 8, height: 8)).cgPath
        self.logoView.layer.shouldRasterize = true
        self.logoView.layer.rasterizationScale = UIScreen.main.scale


    }
    @IBAction func TapOnStartQuiz(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "QuestionListViewController") as! QuestionListViewController
        self.present(vc, animated:true, completion:nil)
//self.showAttDetails()
    }
//    func showAttDetails()
//    {
//        PopupController
//            .create(self)
//            .customize(
//                [
//                    .animation(.slideUp),
//                    .scrollable(false),
//                    .backgroundStyle(.blackFilter(alpha: 0.7))
//                ]
//            )
//            .didShowHandler { popup in
//                print("showed popup!")
//            }
//            .didCloseHandler { _ in
//                print("closed popup!")
//            }
//            .show(DemoPopupViewController2.instance())
//    }

}

class ShadowViewInQuiz: UIView {
    override var bounds: CGRect {
        didSet {
            setupShadow()
        }
    }
    
    private func setupShadow() {
        //self.layer.cornerRadius = 8
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowRadius = 3
        self.layer.shadowOpacity = 0.3
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 8, height: 8)).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
}
