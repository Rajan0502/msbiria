//
//  Counter.swift
//  Doxbay
//
//  Created by Luminous on 26/04/18.
//  Copyright © 2018 Yuji Hato. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import GoogleSignIn
import Alamofire
import SwiftyJSON

class UtilityWebAPI {
    

    var outPutResponse = "";

 
    
    func executeWebAPIAlamofire(methodName: String , parameters : [String:Any]) -> String {
       // let finalURL = "\(baseURLFinal)\(methodName)"
    
        let finalURL = BASEURL + getToken //"\(baseURLFinal)\(getTocket)"
        let parameters : [String:Any] = ["email":"techdev@gmail.com"]
        
        
        
        let headers: HTTPHeaders = [
         "Content-type": "multipart/form-data"
         ]
         
         Alamofire.upload(multipartFormData: { (multipartFormData) in
         for (key, value) in parameters {
         multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
         }
         }, usingThreshold: UInt64.init(), to: finalURL, method: .post, headers: headers) { (result) in
         switch result{
         case .success(let upload, _, _):
         upload.responseJSON { response in
         print("Succesfully uploaded")
         print(response.result)
         
         
         if response.error != nil
         {
            self.outPutResponse = "Error";
            return ;
         }
         else
         {
         let swiftyJsonVar = JSON(response.result.value!)
         let success = swiftyJsonVar["success"].stringValue
         let data = swiftyJsonVar["data"].dictionary!
         let token = data["token"]!.stringValue
         
         print("xxxxxxxx   ---- \(success)--")
         print("xxxxxxxx   ---- \( data)--")
         print("xxxxxxxx   ---- \( token)--")
            
           // self.outPutResponse = response.result.value! as!  ;
         }
         
         }
         case .failure(let error):
         print("Error in upload: \(error.localizedDescription)")
             self.outPutResponse = "error" ;
            return
         }
         }
        return outPutResponse;
    }
    
    
    
    
    
    
    
    
    
    
    
    func executeWebAPIWithSessionDataTask(emailid: String)
    {
       // let paramObjectData: [String: Any] = ["email":"techdev@gmail.com"]
        let finalURL = BASEURL + getToken
        
        let url = URL(string: finalURL)!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        let postString = emailid  // techdev@gmail.com
        request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                print("error=\(error ?? "Error" as! Error)")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
            }
            
            
            let swiftyJsonVar = JSON(data)
            let xxxxxxxx = swiftyJsonVar["success"].stringValue
            let xxxxxxxxdd = swiftyJsonVar["data"]["token"].stringValue
            print("xxxxxxxx PPP   ---- \(xxxxxxxx ) --")
            print("xxxxxxxxdd PP  ---- \(xxxxxxxxdd)  --")
        }
        task.resume()
    }

    
    
    
}

