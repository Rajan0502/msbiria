//
//  FontDirectory.swift
//  Doxbay
//
//  Created by Rajan Kumar Tiwari on 24/07/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import Foundation
import UIKit

extension UIFont
{
    class func SystemFont14Regular()-> UIFont
    {
        return UIFont.systemFont(ofSize: 14.0, weight: .regular)
    }
    
    class func SystemFont12Regular()-> UIFont
    {
        return UIFont.systemFont(ofSize: 12.0, weight: .regular)
    }
    
    class func SystemFont14Semibold()-> UIFont
    {
        return UIFont.systemFont(ofSize: 14.0, weight: .semibold)
    }
}
