
//
//  AppDelegate.swift
//  test11
//
//  Created by Yuji Hato on 4/20/15.
//  Copyright (c) 2015 Yuji Hato. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import FirebaseDatabase
import GoogleSignIn
//import IQKeyboardManagerSwift
import MBProgressHUD
import IQKeyboardManager

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {
    
    var window: UIWindow?
    var ref: DatabaseReference!
    
    
    fileprivate func createMenuView()
    {
        let islogin = UserDefaults.standard.bool(forKey: "islogin")
        let isRemindMe = UserDefaults.standard.bool(forKey: "isRemindMe")
        if(!islogin)
        {
            let storyboard = UIStoryboard(name: "LoginSearch", bundle: nil)
            
            let LoginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
            self.window?.rootViewController = LoginViewController
            self.window?.makeKeyAndVisible()
         }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            token = UserDefaults.standard.string(forKey: "tokenID")!;
            dox_id = UserDefaults.standard.string(forKey: "uid")!;
            for_uuid = UserDefaults.standard.string(forKey: "uid")!;
            email = UserDefaults.standard.string(forKey: "email")!;
            
            if(!isRemindMe)
            {
                let vc  = storyboard.instantiateViewController(withIdentifier: "ProfileViewTimeLine") as! ProfileViewTimeLine
                vc.isFromLogin = true
                vc.profileID = UserDefaults.standard.string(forKey: "uid")!
                isForEdit = true
                self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
                self.window?.rootViewController = vc
                self.window?.makeKeyAndVisible()
                
            }
            else
            {
                
                let mainViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
                let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
                let rightViewController = storyboard.instantiateViewController(withIdentifier: "RightViewController") as! RightViewController
                
                let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
                
                UINavigationBar.appearance().tintColor = UIColor(hex: "689F38")
                
                leftViewController.mainViewController = nvc
                
                
                
                let slideMenuController = ExSlideMenuController(mainViewController:nvc, leftMenuViewController: leftViewController, rightMenuViewController: rightViewController)
                
                slideMenuController.automaticallyAdjustsScrollViewInsets = true
                slideMenuController.delegate = mainViewController
                self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
                self.window?.rootViewController = slideMenuController
                self.window?.makeKeyAndVisible()
            }
        }

    }
    
    
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    
        // Init FireBase DB
        FirebaseApp.configure()
        
        //Init Google+ sign In
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
        IQKeyboardManager.shared().isEnabled = true
        ref = Database.database().reference().child("/discussions/people/")
        self.createMenuView()
        
        
        return true
    }
    
    
    
    
    
    func applicationWillResignActive(_ application: UIApplication) {
    }
    
    
    
    
    func applicationDidEnterBackground(_ application: UIApplication) {
    }
    
    
    
    
    func applicationWillEnterForeground(_ application: UIApplication) {
    }
    
    
    
    func applicationDidBecomeActive(_ application: UIApplication) {
    }
    
    
    
    
    func applicationWillTerminate(_ application: UIApplication) {
        self.saveContext()
    }
    
    
    
    
    
    
    
    
    
    
    
    // MARK: - Core Data stack
    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "dekatotoro.test11" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "test11", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        // The persistent store coordinator for the application. This implementatiisFromPageon creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("test11.sqlite")
        var error: NSError? = nil
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator!.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch var error1 as NSError {
            error = error1
            coordinator = nil
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            dict[NSUnderlyingErrorKey] = error
            error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(error?.debugDescription ?? ""), \(error?.userInfo.debugDescription ?? "")")
            abort()
        } catch {
            fatalError()
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext? = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        if coordinator == nil {
            return nil
        }
        var managedObjectContext = NSManagedObjectContext()
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    
    
    
    // MARK: - Core Data Saving support
    func saveContext () {
        if let moc = self.managedObjectContext {
            var error: NSError? = nil
            if moc.hasChanges {
                do {
                    try moc.save()
                } catch let error1 as NSError {
                    error = error1
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    NSLog("Unresolved error \(error?.debugDescription ?? ""), \(error?.userInfo.debugDescription ?? "")")
                    abort()
                }
            }
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    // Google Sign in Delegate Method
    @available(iOS 9.0, *)
    func application(_ application: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any])
        -> Bool {
            print("GIDSignIn-------UIApplicationOpenURLOptionsKey");
            return GIDSignIn.sharedInstance().handle(url,
                                                     sourceApplication:options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                     annotation: [:])
    }
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        print("GIDSignIn-------sourceApplication");
        return GIDSignIn.sharedInstance().handle(url,
                                                 sourceApplication: sourceApplication,
                                                 annotation: annotation)
    }
    
    
    
    
    // this delegate will call after successfully login / any error come in bitween login
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        if error != nil {
            return
        }
        
        
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken, accessToken: authentication.accessToken)

        
        Auth.auth().currentUser?.linkAndRetrieveData(with: credential) { (authResult, error) in
             if let error = error {
                    Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
                        if let error = error {
                            print("authResult-------authResult---Errorrrr");
                            return
                        }
                        else
                        {
                            print("authResult-------authResult---\(authResult?.user.uid)");
                            self.ref = Database.database().reference().child("/discussions/people/")
                            let userId = authResult?.user.uid
                            let idToken = user.authentication.idToken
                            let fullName = user.profile.name
                            let givenName = user.profile.givenName
                            let email = user.profile.email

                        
                            
                            var imageURL = ""
                            if user.profile.hasImage {
                                imageURL = user.profile.imageURL(withDimension: 100).absoluteString
                            }
                            
                            
                            print("Google---Details-->>\(user)")
                            print("Google---user-->>userId:- \(userId) @@  idToken:- \(idToken)  @@  fullName:- \(fullName)   @@  givenName:- \(givenName)   @@  email:- \(email)    @@  imageURL:- \(imageURL)")
                            self.postDataInRealTimeFireBaseDB(type: "G", emailID: email! , password: "" ,    uiID: userId! , name: fullName! , photoUrl: imageURL);
                            
                            
                        }
                    }
                }
        }
        
        
  
       
 
 
        
       
    }
    
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        print("GIDSignIn-------didDisconnectWith");
    }
    
    
    
    
    var fromType : String = "";
    func postDataInRealTimeFireBaseDB(type: String, emailID: String , password: String, uiID: String, name: String , photoUrl : String)
    {
        fromType = type;
        ref = Database.database().reference().child("/discussions/people/")
        let  deviceID = UIDevice.current.identifierForVendor!.uuidString
        
        //creating artist with the given values
        let userProfileData = ["city":"",
                               "deviceId": deviceID,
                               "deviceTypw": "IOS",
                               "displayName": name,
                               "dox_id":uiID,
                               "eduction_super_speciality":"",
                               "email": emailID,
                               "mobile": "",
                               "photoUrl": photoUrl,
                               "registration_number": ""
            
        ]
        
        print("ref/// -----\(ref)")
        print("uiID/// -----\(uiID)")
        print("userProfileData-----\(userProfileData)")
        
        //adding the artist inside the generated unique key
        ref.child(uiID).setValue(userProfileData)
        

            DispatchQueue.main.async(execute: {
                self.PostUserDeatils(emailid: emailID , uiID: uiID , deviceId: deviceID , name: name , photoUrl: photoUrl);
            });
       
        
    }
    
    
    func  PostUserDeatils(emailid: String , uiID : String , deviceId:String , name : String, photoUrl : String)
    {
        SwiftLoader.show(title: "Loading..", animated: true)
        let requestURL = BASEURL + postUserdd
       // let parametersx : [String:String] = ["email": emailid, "dox_id":uiID , "displayName":name , "photoUrl" : photoUrl]
         let parametersx : [String:String] = ["email": emailid, "dox_id":uiID , "displayName":name , "photoUrl" : photoUrl]
        print("Request URxxxxxxL- \(requestURL)  Paramxxxxxxxxxxxx --- \(parametersx)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parametersx, headers: nil, success: {(response) -> Void in
            print("------------response------\(response)")
            
            SwiftLoader.hide();
            let success = response["success"].boolValue
            let userID = response["data"]["id"].stringValue
            let tokenx = response["data"]["token"].stringValue
            profileData["email"] = response["data"]["email"].stringValue
            profileData["photoUrl"] = response["data"]["photoUrl"].stringValue
            profileData["mobile"] = response["data"]["mobile"].stringValue
            profileData["displayName"] = response["data"]["displayName"].stringValue
            is_valid_user = Int(response["data"]["is_valid_user"] as? String ?? "0")!

            if success
            {
                UserDefaults.standard.set(tokenx, forKey: "tokenID")
                UserDefaults.standard.set(uiID, forKey: "uid")
                UserDefaults.standard.set(emailid, forKey: "email")
                UserDefaults.standard.set(deviceId, forKey: "deviceId")
                UserDefaults.standard.set(true, forKey: "islogin")
                UserDefaults.standard.set(false, forKey: "isRemindMe")
                token = UserDefaults.standard.string(forKey: "tokenID")!;
                dox_id = UserDefaults.standard.string(forKey: "uid")!;
                for_uuid = UserDefaults.standard.string(forKey: "uid")!;
                email = UserDefaults.standard.string(forKey: "email")!;
                
                
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                if(is_valid_user == 1 )
                {
                    UserDefaults.standard.set(true, forKey: "isRemindMe")
                    self.createMenuView();
                }
                else
                {
                    let vc = storyBoard.instantiateViewController(withIdentifier: "ProfileViewTimeLine") as! ProfileViewTimeLine
                    vc.isFromLogin = true
                    vc.profileID = dox_id
                    isForEdit = true
                    
                    self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
                    self.window?.rootViewController = vc
                    self.window?.makeKeyAndVisible()
                }
               
            }
            else
            {
                ShowToast.showPositiveMessage(message: "Something went wrong. Please try again.");
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            SwiftLoader.hide()
           
        })
    }
    
    
}

