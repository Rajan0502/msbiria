//
//  XXXCellCollectionViewCell.swift
//  CollectionViewDataSourceBlog
//
//  Created by Luminous on 08/05/18.
//  Copyright © 2018 Erica Millado. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage


class ExploreCollectinCell: UICollectionViewCell {
    
    @IBOutlet var bookImagexp: UIImageView!
    @IBOutlet var bookLabelxp: UILabel!
    @IBOutlet var orgChnnelCount: UILabel!
    
    func displayContent( title: String, chnalCount : String , imgURl : String) {
            // let image : UIImage = UIImage(named:"tempAouthor")!
             //bookImagexp.image = image;
       // bookLabelxp.lineBreakMode = .byWordWrapping
      //  bookLabelxp.numberOfLines = 0
        bookLabelxp.lineBreakMode = .byTruncatingMiddle
        bookLabelxp.lineBreakMode = .byWordWrapping
        bookLabelxp.numberOfLines = 0;

             bookLabelxp.text = title
             orgChnnelCount.text = chnalCount + " Channels"
        
        let greet4Height = bookLabelxp.optimalHeight
        bookLabelxp.frame = CGRect(x: bookLabelxp.frame.origin.x, y: bookLabelxp.frame.origin.y, width: bookLabelxp.frame.width, height: greet4Height)
        
        let remoteImageURL = URL(string: imgURl)!
        Alamofire.request(remoteImageURL).responseData { (response) in
            if response.error == nil {
                print(response.result)
                
                if let data = response.data {
                    self.bookImagexp.image = UIImage(data: data)
                }
            }
        }
        }
    
    
}


extension UILabel {
    var optimalHeight : CGFloat {
        get
        {
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: CGFloat.greatestFiniteMagnitude))
            label.numberOfLines = 0
            label.lineBreakMode = NSLineBreakMode.byWordWrapping
            label.font = self.font
            label.text = self.text
            label.sizeToFit()
            return label.frame.height
        }
        
    }
}
