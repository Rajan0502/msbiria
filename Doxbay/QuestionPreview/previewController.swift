//
//  previewController.swift
//  Doxbay
//
//  Created by kavi yadav on 09/07/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit
import MBProgressHUD
class previewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var CollectionView: UICollectionView!

    @IBOutlet weak var BgView: UIView!
    @IBOutlet weak var lblAnswerOption1: UILabel!
    
    @IBOutlet weak var lblAnswerOption2: UILabel!
    
    @IBOutlet weak var lblAnswerOption3: UILabel!
    
    @IBOutlet weak var lblAnswerOption4: UILabel!
    
    @IBOutlet weak var LblExplanation: UILabel!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var btnRadio4: UIButton!
    @IBOutlet weak var btnRadio3: UIButton!
    @IBOutlet weak var btnRadio2: UIButton!
    @IBOutlet weak var btnRadio1: UIButton!
    var Questiondata = [[String:Any]]()
    var Arrayindex = Int()
    var data = [[String:Any]]()

    var QuizId = String ()
    var OptionArray = NSMutableArray ()
    var OptionList = [""]

    override func viewDidLoad() {
        super.viewDidLoad()
        CollectionView.delegate = self
        CollectionView.dataSource = self
        
        self.GetQuestionPreviewList()
        BgView.addShadow(radius: 1)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ColCell", for: indexPath as IndexPath) as! previewColCell
        if indexPath.row <= Arrayindex {
            cell.shadowView.backgroundColor = UIColor(red: 255/255, green: 212/255, blue: 121/255, alpha: 1)
            
        }
        else{
            cell.shadowView.backgroundColor = UIColor.white
        }

        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let padding: CGFloat = 10
        let collectionViewSize = collectionView.frame.size.width - padding
        return CGSize(width: collectionViewSize/5, height: 15)
        
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    func GetQuestionPreviewList() {
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Loading"
        //MBProgressHUD.hide(for: self.view, animated: false)
        
        let requestURL =  BASEURL + getQuizQuestionList;
        let parameters: [String:String] = ["token": token , "quiz_id":QuizId]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            MBProgressHUD.hide(for: self.view, animated: false)
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)-- \(response)")
            if success
            {
                
                let abc = response["data"].arrayObject;
                self.data = response["data"].arrayObject as! [[String : Any]]
                print(self.data)
                self.LoadarrayObectIndex()
                
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
        })
        
    }
    func LoadarrayObectIndex() {
        self.OptionList.removeAll()
        Questiondata = [data[Arrayindex]]
        let allData = Questiondata[0]
        let question = allData["question_text"] as? String
        let optionData = allData["options"] as! NSArray
        for options  in optionData {
            let AllOptions = options as! NSDictionary
            let optionAnswer = AllOptions.value(forKey: "quiz_option")
            let correctAnswer = AllOptions.value(forKey: "correct_answer")
            let quiz_answer = AllOptions.value(forKey: "quiz_answer")
            let optionId = AllOptions.value(forKey: "id")

            let dict = ["optionAnswer" : optionAnswer , "correctAnswer" : correctAnswer , "quiz_answer" : quiz_answer , "optionId" : optionId]
            OptionArray.add(dict)
            OptionList.append(optionAnswer as! String)
            
        }
        print(OptionArray)
        print(OptionList)

        
        let option1 = OptionList[0]
        let option2 = OptionList[1]
        let option3 = OptionList[2]
        let option4 = OptionList[3]
        let explanation = allData["explanation_text"] as? String
        let correctAnswer = allData[""] as? String
        self.lblQuestion.text = question
        self.lblAnswerOption1.text = option1
        self.lblAnswerOption2.text = option2
        self.lblAnswerOption3.text = option3
        self.lblAnswerOption4.text = option4
        self.LblExplanation.text = explanation





        self.CollectionView.reloadData()
        //self.QuestionTableview.reloadData()
    }

    @IBAction func tapOnOption(_ sender: UIButton) {
        
        
    }
    @IBAction func tapOnBack(_ sender: Any) {
        self.presentingViewController?.dismiss(animated: true, completion:nil)

        
    }
    
    @IBAction func tapOnPrevious(_ sender: UIButton) {
        if Arrayindex != 0 {
            Arrayindex -= 1
        if(Arrayindex <= data.count-1){
            Questiondata.removeAll()
            
            self.LoadarrayObectIndex()
        }
        }
    }

        

    
    
    @IBAction func tapOnNext(_ sender: UIButton) {
          Arrayindex += 1
        if(Arrayindex <= data.count-1){
            Questiondata.removeAll()
            
            self.LoadarrayObectIndex()
        }

    }
}
class ShadowViewInPreview: UIView {
    override var bounds: CGRect {
        didSet {
            setupShadow()
        }
    }
    
    private func setupShadow() {
        //self.layer.cornerRadius = 8
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowRadius = 3
        self.layer.shadowOpacity = 0.3
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 8, height: 8)).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
}

