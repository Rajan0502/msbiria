//
//  SubscribedChannel_VC.swift
//  Doxbay
//
//  Created by Luminous on 16/05/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit
import SwiftyJSON
import MBProgressHUD


class SubscribedChannel_VC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var expID : String = "";
    var ExploreChannelCatName = "";
    var expChData : [ExploreChannelListModal] = [];
    
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var headerTitle: UILabel!
    
    
    @IBAction func backClk(_ sender: Any)
    {
        // self.navigationController?.popViewController(animated: false)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnConnectedBranch(_ sender: Any) {
        
        /*let vc = self.storyboard?.instantiateViewController(withIdentifier: "ConnectedBranch_VC") as! ConnectedBranch_VC
         self.navigationController?.pushViewController(vc, animated: true)
         */
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ConnectedBranch_VC") as! ConnectedBranch_VC
        self.present(vc, animated:true, completion:nil)
    }
    
    //self.tableView.addSubview(self.refreshControl)
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.red
        return refreshControl
    }()
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        // self.getExploreListData();
        refreshControl.endRefreshing()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self;
        tableView.dataSource = self
        self.tableView.addSubview(self.refreshControl)
        
        // headerTitle.text = ExploreChannelCatName;
         self.getExploreListData();
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.parent?.view.frame = CGRect(x:0, y:64, width:self.view.frame.width, height: UIScreen.main.bounds.size.height-64)
                self.tableView.reloadData();
            //floatingActionButton.isHidden = true
            
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100;
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return expChData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExploreChannelListCellTableViewCell", for: indexPath) as! ExploreChannelListCellTableViewCell
        
        
//        cell.contentView.layer.borderWidth = 1.0
//        cell.contentView.layer.borderColor = UIColor.clear.cgColor
//        cell.contentView.layer.masksToBounds = true
//        cell.layer.shadowColor = UIColor.gray.cgColor
//        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
//        cell.layer.shadowRadius = 1.0
//        cell.layer.shadowOpacity = 1.0
//        cell.layer.masksToBounds = false
//        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
//
        let Explore : ExploreChannelListModal = expChData[indexPath.row];
        
        
        cell.chanelName.lineBreakMode = .byTruncatingMiddle
        cell.chanelName.lineBreakMode = .byWordWrapping
        cell.chanelName.numberOfLines = 0;
        cell.chanelName.text = Explore.name;
        let greet4Height = cell.chanelName.optimalHeight
        cell.chanelName.frame = CGRect(x: cell.chanelName.frame.origin.x, y: cell.chanelName.frame.origin.y, width: cell.chanelName.frame.width, height: greet4Height)
        
        
        
        cell.totalChannelPost.text = Explore.count_post + " Channel Post";
        if(Explore.photoUrl == nil)
        {
        }
        else
        {
            do {
                try   cell.channelImg.sd_setImage(with:URL(string: Explore.photoUrl), placeholderImage: UIImage(named: "vc_channel"))
            }catch {
                print("Unexpected error: \(error).")
            }
            
        }
        
        
        
        
        
        cell.channelImg.layer.cornerRadius =  (cell.channelImg.frame.size.width) / 2;
        cell.channelImg.layer.masksToBounds = true
        
        if(Explore.count_newpost == "0" || Explore.count_newpost == "")
        {
           
        }
        else
        {
            let label = UILabel(frame: CGRect(x:self.view.frame.width - 80  , y: 40, width: 40, height: 40))
            label.text =   Explore.count_newpost
            label.tag = indexPath.row
            label.layer.cornerRadius =  (label.frame.size.width) / 2;
            label.layer.masksToBounds = true
            label.backgroundColor = UIColor(red: 25.0/255.0, green: 28.0/255.0, blue: 118.0/255.0, alpha: 1.0)
            label.textColor = UIColor.white
            label.textAlignment = NSTextAlignment.center
            cell.contentView.addSubview(label)
        }
        return cell;
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let xyz : ExploreChannelListModal = expChData[(indexPath.row)]
        if(xyz.count_newpost.isEmpty   || xyz.count_newpost == "0" || xyz.count_newpost == "")
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ExploreChannelDetailsViewController") as! ExploreChannelDetailsViewController
            vc.chanalID = xyz.id;
            vc.expID = expID;
          //  self.navigationController?.pushViewController(signUp, animated: true)
            self.present(vc, animated:true, completion:nil)
        }
        else
        {
            let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
            progressHUD.mode = MBProgressHUDMode.indeterminate
            progressHUD.label.text = "Loading"
            
            let requestURL =  BASEURL + channelVisit;
            let parameters: [String:String] = ["token": token , "channel_id": xyz.id]
            print("Request URL- \(requestURL)  Param --- \(parameters)")
            
            UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
                
                MBProgressHUD.hide(for: self.view, animated: false)
                let success = response["success"].boolValue
                print("xxxxxxxx   ---- \(success)-- \(response)")
                if success
                {
                    self.expChData[(indexPath.row)].count_newpost = "0"
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "ChannelPostTM_VC") as! ChannelPostTM_VC
                    vc.expID = xyz.id;
                    vc.orgChannelName = xyz.name;
                    vc.orgChannelImgLogo = xyz.photoUrl
                    self.present(vc, animated:true, completion:nil)
                    
                }
            }, failure:  {
                (error) -> Void in
                print("error===\(error)")
                MBProgressHUD.hide(for: self.view, animated: false)
            })
        }
    }
    
    
    
    
    
    
    
    
    @objc func clickPos(_ sender: UITapGestureRecognizer)
    {
        let touch = sender.location(in: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: touch)
        let xyz : ExploreChannelListModal = expChData[(indexPath?.row)!]
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ExploreChannelDetailsViewController") as! ExploreChannelDetailsViewController
        vc.chanalID = xyz.id;
        vc.expID = expID;
        self.present(vc, animated:true, completion:nil)
       // self.navigationController?.pushViewController(signUp, animated: true)
    }
    
    
    
    
    
    
    func getExploreListData()
    {
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Loading"
        //MBProgressHUD.hide(for: self.view, animated: false)
        
        let requestURL =  BASEURL + getSubscribedChannels;
        let parameters: [String:String] = ["token": token]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            MBProgressHUD.hide(for: self.view, animated: false)
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)-- \(response)")
            if success
            {
                let abc = response["data"].arrayObject;
                self.parse(json: response)
                
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
        })
    }
    
    func parse(json: JSON)
    {
        self.expChData.removeAll();
        for result in json["data"].arrayValue {
            let id = result["id"].stringValue
            let name = result["name"].stringValue
            let photoUrl = result["photoUrl"].stringValue
            let count_post = String(result["count_post"].intValue)
            let count_newpost = String(result["count_newpost"].intValue)
            let catNAme1 : ExploreChannelListModal = ExploreChannelListModal(id: id , name: name,  photoUrl: photoUrl, count_post: count_post , count_newpost: count_newpost);
            expChData.append(catNAme1);
        }
        
        self.tableView.reloadData();
        MBProgressHUD.hide(for: self.view, animated: false)
    }
    
}
