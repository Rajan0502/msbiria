//
//  Directory_UserList.swift
//  Doxbay
//
//  Created by Luminous on 25/06/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit
import  SwiftyJSON;
import  AlamofireImage;
import MBProgressHUD

class Directory_UserList: UIViewController , UITableViewDelegate, UITableViewDataSource {
    
    var subArr : [channelSubscribeModal] = [];
    var chanalId : String = "";
    var FromPage : String = "Channel Subscriber"
    
    
    
    
    @IBOutlet var headerTitle: UILabel!
    @IBOutlet var tableView: UITableView!
    
    @IBAction func btnBack(_ sender: Any) {
        // self.navigationController?.popViewController(animated: false)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //self.tableView.addSubview(self.refreshControl)
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.red
        return refreshControl
    }()
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        //self.getExploreChannelsSubscriberList);
        refreshControl.endRefreshing()
    }
    
    
    
    
    
    @IBAction func searchChannelsSubscriber(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let  vc = storyBoard.instantiateViewController(withIdentifier: "SearchUser") as! SearchUser
        vc.isFromPage = FromPage
        vc.searchHeader = "Type here to search subscriber "
        vc.searchID = chanalId;
        self.present(vc, animated:true, completion:nil)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.addSubview(self.refreshControl)
        
        if (FromPage == "Member Search")
        {
            headerTitle.text = FromPage;
        }
        getExploreChannelsSubscriberList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subArr.count;
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(85.00);
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChannelSubcriberCell", for : indexPath) as! ChannelSubcriberCell
        
        let rData : channelSubscribeModal = subArr[indexPath.row];
        cell.labName.lineBreakMode = .byTruncatingMiddle
        cell.labName.lineBreakMode = .byWordWrapping
        cell.labName.numberOfLines = 0;
        cell.labName.text = rData.displayName;
        let greet4Height = cell.labName.optimalHeight
        cell.labName.frame = CGRect(x: cell.labName.frame.origin.x, y: cell.labName.frame.origin.y, width: cell.labName.frame.width, height: greet4Height)
        
        cell.labLoc.text =  rData.city;
        cell.profilePic.tag = indexPath.row
        if(!rData.photoUrl.isEmpty)
        {
            cell.profilePic.sd_setImage(with:URL(string: rData.photoUrl), placeholderImage: UIImage(named: "vc_channel"))
            
            cell.profilePic.layer.cornerRadius =  (cell.profilePic.frame.size.width) / 2;
            cell.profilePic.layer.masksToBounds = true
            
            let tap3 = UITapGestureRecognizer(target: self, action: #selector(ViewUserProfile))
            cell.profilePic.addGestureRecognizer(tap3)
            cell.profilePic.isUserInteractionEnabled = true
            
        }
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rData : channelSubscribeModal = subArr[indexPath.row];
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ProfileViewTimeLine") as! ProfileViewTimeLine
        vc.profileID = rData.dox_id
        self.present(vc, animated:true, completion:nil)
    }
    @objc func ViewUserProfile(_ sender: UITapGestureRecognizer)
    {
        let label = sender.view as! UIImageView
        let rData : channelSubscribeModal = subArr[label.tag];
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ProfileViewTimeLine") as! ProfileViewTimeLine
        vc.profileID = rData.dox_id
        self.present(vc, animated:true, completion:nil)
    }
    
    func getExploreChannelsSubscriberList()
    {
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Loading"
        //MBProgressHUD.hide(for: self.view, animated: false)
        
        let requestURL =  BASEURL + getSubscriber;
        let parameters: [String:String] = ["token": token , "channel_id": channel_id, "currentpage": String(self.pageNo)]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            MBProgressHUD.hide(for: self.view, animated: false)
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)-- \(response)")
            if success
            {
                let abc = response["data"].arrayObject;
                self.parse(json: response)
                
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
        })
    }
    
    func parse(json: JSON) {
        if(json["data"].arrayValue.count < 10)
        {
            isAvilableForLoad = false
        }
        for result in json["data"].arrayValue {
            let id = result["id"].stringValue
            let channel_id = result["channel_id"].stringValue
            let displayName = result["displayName"].stringValue
            let city = result["city"].stringValue
            let dox_id = result["dox_id"].stringValue
            let email = result["email"].stringValue
            let photoUrl = result["photoUrl"].stringValue
            
            let dData : channelSubscribeModal = channelSubscribeModal(id: id , channel_id: channel_id , displayName: displayName, city: city , dox_id: dox_id , email: email , photoUrl: photoUrl);
            self.subArr.append(dData);
        }
        
        self.tableView.reloadData();
        MBProgressHUD.hide(for: self.view, animated: false)
    }
    var pageNo : Int = 1;
    var loadingData = false;
    var isAvilableForLoad = true;
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = subArr.count - 3
        if !loadingData && indexPath.row == lastElement {
            loadingData = true
            loadMoreData()
        }
    }
    func loadMoreData() {
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                self.pageNo = self.pageNo + 1
                if(self.isAvilableForLoad)
                {
                    self.getExploreChannelsSubscriberList();
                }
                self.loadingData = false
            }
        }
    }
    
}
