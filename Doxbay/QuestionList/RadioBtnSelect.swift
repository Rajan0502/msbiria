//
//  RadioBtnSelect.swift
//  Doxbay
//
//  Created by kavi yadav on 20/06/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit

class RadioBtnSelect: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgRadio: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureCell(info: [String:Any], selected: Bool) {
        if selected {
            imgRadio.image = UIImage(named: "onRadio")
        } else {
            imgRadio.image = UIImage(named: "offRadio")
        }
        lblTitle.text = info["optionText"] as? String
    }
    

}
