//
//  QuestionListViewController.swift
//  Doxbay
//
//  Created by kavi yadav on 20/06/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit
import MBProgressHUD
import SDWebImage
import AVKit
import AVFoundation
import MobileCoreServices
import Alamofire
import SwiftyJSON

var InstanceQuestionVC: QuestionListViewController?
var signupQuizAnswerSheet: [[String: Any]]?
var FillAnswerSheet = [[String: Any]]()
var QuestionId = String ()
var UserAnswer = String ()

class QuestionListViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var CollectionView: UICollectionView!
    var data = [[String:Any]]()
    var UserAnsdata = [[String:Any]]()

    var Questiondata = [[String:Any]]()
    var Arrayindex = Int()
    @IBOutlet weak var QuestionTableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.GetQuestionList()
        self.CollectionView.delegate = self
        self.CollectionView.dataSource = self
        QuestionTableview.register(UINib(nibName: "QuestuionOptionCellTableViewCell", bundle: nil), forCellReuseIdentifier: "CellOptionTableQuestion")
       QuestionTableview.delegate = self
        // Do any additional setup after loading the view.
        Arrayindex = 0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellData = Questiondata[0]
//        let inputType = cellData["inputType"] as! String
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellOptionTableQuestion", for: indexPath)as! QuestuionOptionCellTableViewCell
            cell.configureCell(info: cellData, index: indexPath.row)
            return cell
    }
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int
    {
        return Questiondata.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat((4*40)+70)
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ColCell", for: indexPath as IndexPath) as! questionListCellCollectionViewCell
        cell.shadowView.addShadow(radius: 2)
        if indexPath.row <= Arrayindex {
            cell.shadowView.backgroundColor = UIColor(red: 255/255, green: 212/255, blue: 121/255, alpha: 1)

        }
        else{
            cell.shadowView.backgroundColor = UIColor.white
        }
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let padding: CGFloat = 10
        let collectionViewSize = collectionView.frame.size.width - padding
        return CGSize(width: collectionViewSize/5, height: 15)
        
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func GetQuestionList() {
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Loading"
        //MBProgressHUD.hide(for: self.view, animated: false)
        
        let requestURL =  BASEURL + getQuizQuestionList;
        let parameters: [String:String] = ["token": token , "quiz_id":"38"]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            MBProgressHUD.hide(for: self.view, animated: false)
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)-- \(response)")
            if success
            {

                let abc = response["data"].arrayObject;
                self.data = response["data"].arrayObject as! [[String : Any]]
                print(self.data)
                self.LoadarrayObectIndex()
                
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
        })

    }
    func SaveQuizUserAnser() {
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Loading"
        //MBProgressHUD.hide(for: self.view, animated: false)
        
        let requestURL =  BASEURL + SaveQuizUserAnswer;
        let parameters: [String:String] = ["token": token , "question_id":QuestionId , "user_answer":UserAnswer , "quiz_id":"38" , "elapsed_time":"0"]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            MBProgressHUD.hide(for: self.view, animated: false)
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)-- \(response)")
            if success
            {
                
                let abc = response["data"].arrayObject;
//                self.UserAnsdata = response["data"].arrayObject as! [[String : Any]]
//                print(self.data)
               // self.LoadarrayObectIndex()
                
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
        })
        
    }
    
    func LoadarrayObectIndex() {
        Questiondata = [data[Arrayindex]]
        fillAnswerSheet()
        self.CollectionView.reloadData()
        self.QuestionTableview.reloadData()
    }
    @IBAction func Back(_ sender: Any) {
        self.presentingViewController?.dismiss(animated: true, completion:nil)
    }
    
    
    @IBAction func CheckAnswer(_ sender: UIButton) {
       // self.ShowPopUp()
       // self.SaveQuizUserAnser()
        Arrayindex += 1
        if(Arrayindex <= data.count-1){
        Questiondata.removeAll()

        self.LoadarrayObectIndex()
        }
    }
    func fillAnswerSheet() {
        signupAnswerSheet = [[String: Any]]()
        for question in Questiondata {
            print(question)
            var dictAnswer = [String: Any]()
            let inputType = "radio"
            let OptionArray = question["options"] as! NSArray
            for option in OptionArray{
                print(option)
                let optionDict = option as! NSDictionary
                print(optionDict)
                 dictAnswer["id"] = optionDict["id"]
                dictAnswer["quiz_answer"] = optionDict["quiz_answer"]
                dictAnswer["quiz_option"] = optionDict["quiz_option"]
                dictAnswer["correct_answer"] = optionDict["correct_answer"]
                FillAnswerSheet.append(dictAnswer)
                 print(FillAnswerSheet)
            }
            print(FillAnswerSheet as Any)
        }
    }
    func ShowPopUp()
    {
        PopupController
            .create(self)
            .customize(
                [
                    .animation(.slideUp),
                    .scrollable(false),
                    .backgroundStyle(.blackFilter(alpha: 0.7))
                ]
            )
            .didShowHandler { popup in
                print("showed popup!")
            }
            .didCloseHandler { _ in
                print("closed popup!")
            }
            .show(popUp.instance())
    }

}

class ShadowInTap: UIView {
    override var bounds: CGRect {
        didSet {
            setupShadow()
        }
    }
    
    private func setupShadow() {
        self.layer.cornerRadius = 8
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowRadius = 3
        self.layer.shadowOpacity = 0.3
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 8, height: 8)).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
}
