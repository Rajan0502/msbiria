//
//  QuestuionOptionCellTableViewCell.swift
//  Doxbay
//
//  Created by kavi yadav on 20/06/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit

class QuestuionOptionCellTableViewCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var optionView: UIView!
    var arrImage = [[String: Any]]()
    var arrRadio = [[String: Any]]()
    var cellType = "radio"
    var radioCellSelectedIndex = IndexPath()
    var cellIndex = 0

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureCell(info: [String:Any],index: Int){
        //cellType = info["inputType"] as! String
        lblTitle.text = info["question_text"] as? String
        optionView.frame.size.height =  self.tableView.frame.size.height + self.lblTitle.frame.size.height 
        optionView.addShadow(radius: 5)
        cellIndex = index
        if cellType == "image" {
            tableView.register(UINib(nibName: "CellImage", bundle: nil), forCellReuseIdentifier: "CellImage")
            arrImage = info["options"] as! [[String: Any]]
            
        } else {
            tableView.register(UINib(nibName: "CellRadio", bundle: nil), forCellReuseIdentifier: "CellRadio")
            arrRadio = info["options"] as! [[String: Any]]
        }
        tableView.delegate = self as? UITableViewDelegate
        tableView.dataSource = self as? UITableViewDataSource
        tableView.reloadData()
    }

}
extension QuestuionOptionCellTableViewCell: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if cellType == "image" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellImage", for: indexPath) as!  CellImage
            cell.configureCell(info: arrImage[indexPath.row], cellIndex: indexPath.row, parentCellIndex: cellIndex)
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellRadio", for: indexPath) as! CellRadio
            if radioCellSelectedIndex == indexPath {
                cell.configureCell(info: arrRadio[indexPath.row], selected: true)
            }else {
                cell.configureCell(info: arrRadio[indexPath.row], selected: false)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if cellType == "image" {
            return arrImage.count
        }
        else {
            return arrRadio.count
        }
    }
}


extension QuestuionOptionCellTableViewCell: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if cellType == "image" {
            return 60
        }
        else {
            return 40
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        radioCellSelectedIndex = indexPath
        if cellType == "radio" {
                var data = FillAnswerSheet[indexPath.row]
                var answer = arrRadio[radioCellSelectedIndex.row]
            QuestionId  = data["id"] as! String
            UserAnswer  = data["quiz_option"] as! String
        }
        tableView.reloadData()
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }

}

