//
//  Setting_VC.swift
//  DoxbaySalman
//
//  Created by Luminous on 19/05/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit
import FirebaseAuth

class Setting_VC: UIViewController {

    
    /*
     
        Setting_VC  and View
        Admin_VC  and View
        LeftViewController
        MainViewController  -- callPages
        LeftViewController --  sectionArray ,  mainSection ,  yourAcoountArray , numberOfSections ,  func tableView , func tableView(_   , func tableView(_ tableView: UITableView, didSelectRowAt
     
    */
    
    @IBAction func backClk(_ sender: Any)
    {
         self.navigationController?.popViewController(animated: false)
        //self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnResetPasword(_ sender: Any) {
         let email = UserDefaults.standard.string(forKey: "email")
        Auth.auth().sendPasswordReset(withEmail: email!) { error in
            ShowToast.showPositiveMessage(message: "We have sent a link on your email id, please check mail ");
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
