//
//  Admin_VC.swift
//  DoxbaySalman
//
//  Created by Luminous on 19/05/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit
import MBProgressHUD

class Admin_VC: UIViewController, UIWebViewDelegate {
    
    var selectedName:String = webAdminURL
    @IBOutlet weak var web_view: UIWebView!
    
    
    @IBAction func backClk(_ sender: Any)
    {
         self.navigationController?.popViewController(animated: false)
        //self.dismiss(animated: true, completion: nil)
        
    }
    
    
    //@IBOutlet var web_view: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let url = NSURL(string:  selectedName)
        web_view.loadRequest(NSURLRequest(url: url! as URL) as URLRequest)
        web_view.scalesPageToFit = true
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    func webViewDidStartLoad(_ webView: UIWebView)
    {
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Loading"
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
       MBProgressHUD.hide(for: self.view, animated: false)
    }
    
    
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print(error.localizedDescription )
    }
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return true
    }
}
