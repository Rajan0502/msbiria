//
//  ExploreTableViewCell2.swift
//  Doxbay
//
//  Created by Unify on 26/04/18.
//  Copyright © 2018 Yuji Hato. All rights reserved.
//

import UIKit

class ExploreTableViewCell2: UITableViewCell {
    @IBOutlet weak var profileImg: UIImageView!
    
    @IBOutlet weak var pageBtn2: UIButton!
    @IBOutlet weak var pageBtn1: UIButton!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var chatBtn: UIButton!
    @IBOutlet weak var collVw: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
