//
//  ExpSlideCell.swift
//  Doxbay
//
//  Created by Unify on 27/04/18.
//  Copyright © 2018 Yuji Hato. All rights reserved.
//

import UIKit

class ExpSlideCell: UITableViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
