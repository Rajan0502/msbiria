//
//  ExploreViewController.swift
//  Doxbay
//
//  Created by Unify on 26/04/18.
//  Copyright © 2018 Yuji Hato. All rights reserved.
//

import UIKit
import SwiftyJSON
import MBProgressHUD

class ExploreViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{

    var viewSelectionArray:[Bool] = []
    @IBOutlet weak var tblVw: UITableView!
    @IBOutlet weak var slideTblVw: UITableView!
    @IBOutlet weak var slideBtn: UIButton!
    @IBAction func slideBtnClk(_ sender: Any)
    {
        self.openCloseSideMenu();
    }
    
    let kHeaderSectionTag: Int = 6900;
    var expandedSectionHeaderNumber: Int = -1
    var expandedSectionHeader: UITableViewHeaderFooterView!
    var sectionItems: Array<Any> = []
    var sectionNames: Array<Any> = []
    
    override func viewDidLoad()
    {
        
        super.viewDidLoad()
        
        var frame1 = self.slideBtn.frame
        frame1.origin.x = 0
        self.slideBtn.frame = frame1
    
        var frame2 = self.slideTblVw.frame
        frame2.size.width = self.view.frame.size.width-80
        frame2.origin.x = -self.view.frame.size.width-80
        self.slideTblVw.frame = frame2
        
        self.slideBtn.setImage(UIImage(named:"slideBtnClose"), for: .normal)
        self.slideBtn.setImage(UIImage(named:"slideBtnOpen"), for: .selected)
        self.slideBtn.isSelected = false

        for _ in 0...4
        {
         viewSelectionArray.append(false)
        }
        
        sectionNames = [ "Organization", "Career Information Channels", "MSBIRIA Official Channels" ];
        sectionItems = [["MMA Official", "MARD JJ Hospital MARD JJ Hospital", "MARD GSMC & KEM Hospital", "MARD GMC Nagpur", "JARD"],
                         ["MMA Official", "MARD JJ Hospital", "MARD GSMC KEM Hospital", "MARD GMC Nagpur", "JARD"],
                         ["MMA Official", "MARD JJ Hospital", "MARD GSMC & KEM Hospital", "MARD GMC Nagpur", "JARD"]
        ];
        self.slideTblVw!.tableFooterView = UIView()
      //  callapi()
        // Do any additional setup after loading the view.
    }
    func callapi()
    {
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Loading"
        
        let requestURL = BASEURL+"octabeans_search/get_channel_list"
        let parameters: [String:String] = ["category_id":"19","token":"QnEOzn2Gc0Gj447ybQmkJgrAK8MJkmeDccB-Ro3W4AQ","currentpage":""]
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--")

            if success
            {
                self.sectionNames = response["data"].array!
                let array = JSON(self.sectionNames)
                let dic = array[0].dictionary
                
                print("xxxxxxxx   ---- \( dic!["name"])--")
                
               // sectionNames = ["Organization", "Career Information Channels", "MSBIRIA Official Channels" ];
                //        sectionItems = [["MMA Official", "MARD JJ Hospital MARD JJ Hospital", "MARD GSMC & KEM Hospital", "MARD GMC Nagpur", "JARD"],
                //                         ["MMA Official", "MARD JJ Hospital", "MARD GSMC KEM Hospital", "MARD GMC Nagpur", "JARD"],
                //                         ["MMA Official", "MARD JJ Hospital", "MARD GSMC & KEM Hospital", "MARD GMC Nagpur", "JARD"]
                //        ];
                self.sectionItems = [[],
                                [],
                                []
                ];
                self.slideTblVw.reloadData()
                MBProgressHUD.hide(for: self.view, animated: false)
            }
        
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if tableView == slideTblVw
        {
            if sectionNames.count > 0 {
                tableView.backgroundView = nil
                return sectionNames.count
            }
            return 0
        }
        else
        {
            return 2;
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == slideTblVw
        {
            if (self.expandedSectionHeaderNumber == section) {
                
                let arrayOfItems = self.sectionItems[section] as! NSArray
                return arrayOfItems.count;
            } else {
                return 0;
            }
        }
        else
        {
            if section==0
            {
                return 1;
            }
            else
            {
                return 5
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == slideTblVw
        {
            let cellIdentifier = "ExpSlideCell"
            var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? ExpSlideCell
            if cell == nil {
                var topLevelObjects = Bundle.main.loadNibNamed("ExpSlideCell", owner: self, options: nil)
                cell = topLevelObjects?[0] as? ExpSlideCell
                cell?.selectionStyle = .none
            }
            
            let section = self.sectionItems[indexPath.section] as! NSArray
            let titleStr = section[indexPath.row] as? String
            let height = titleStr?.height(withConstrainedWidth: self.slideTblVw.frame.size.width-50, font: UIFont.boldSystemFont(ofSize: 18))
            var frame = cell?.titleLbl?.frame
            frame?.size.height = height!+10
            cell?.titleLbl?.frame = frame!
            
            
            cell?.titleLbl?.textColor = UIColor.colorWithHexString(hexStr: "#888da8")
            cell?.titleLbl?.font = UIFont.boldSystemFont(ofSize: 18)
            cell?.titleLbl?.text = section[indexPath.row] as? String
            cell?.titleLbl.lineBreakMode = .byWordWrapping
            cell?.titleLbl.numberOfLines=3
            return cell!
        }
        else
        {
        if indexPath.section==0
        {
            let cellIdentifier = "ExploreTableViewCell1"
            var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? ExploreTableViewCell1
            if cell == nil {
                var topLevelObjects = Bundle.main.loadNibNamed("ExploreTableViewCell1", owner: self, options: nil)
                cell = topLevelObjects?[0] as? ExploreTableViewCell1
                cell?.selectionStyle = .none
            }
            cell?.txtFld.layer.borderColor = UIColor.lightGray.cgColor
            cell?.txtFld.layer.borderWidth = 0.5
             cell?.txtFld.layer.cornerRadius = 3
            return cell!
            
        }
        else
        {
        
        let cellIdentifier = "ExploreTableViewCell2"
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? ExploreTableViewCell2
        if cell == nil
        {
            var topLevelObjects = Bundle.main.loadNibNamed("ExploreTableViewCell2", owner: self, options: nil)
            cell = topLevelObjects?[0] as? ExploreTableViewCell2
            cell?.selectionStyle = .none
        }
            cell?.profileImg.layer.cornerRadius =  (cell?.profileImg.frame.size.width)! / 2;
            cell?.profileImg.layer.masksToBounds = true
             cell?.profileImg.layer.borderWidth = 4.0
            cell?.profileImg.layer.borderColor = UIColor.white.cgColor
            
            cell?.chatBtn.layer.cornerRadius =  (cell?.chatBtn.frame.size.width)! / 2;
            cell?.chatBtn.layer.masksToBounds = true
            
            cell?.btn2.layer.cornerRadius =  (cell?.btn2.frame.size.width)! / 2;
            cell?.btn2.layer.masksToBounds = true
            
            cell?.view1.isHidden=true
            cell?.view2.isHidden=true
            
            cell?.pageBtn1.setImage(UIImage(named:"whiteDot"), for: .normal)
            cell?.pageBtn1.setImage(UIImage(named:"redDot"), for: .selected)
            
            cell?.pageBtn2.setImage(UIImage(named:"whiteDot"), for: .normal)
            cell?.pageBtn2.setImage(UIImage(named:"redDot"), for: .selected)
            
            cell?.pageBtn1.isSelected=false;
            cell?.pageBtn2.isSelected=false;
            
            if(viewSelectionArray[indexPath.row])
            {
                 cell?.view1.isHidden=true
                 cell?.view2.isHidden=false
                 cell?.pageBtn2.isSelected=true;
            }
            else
            {
                cell?.view1.isHidden=false
                cell?.view2.isHidden=true
                cell?.pageBtn1.isSelected=true;

            }
            cell?.pageBtn1.tag = indexPath.row
            cell?.pageBtn2.tag = indexPath.row
            cell?.pageBtn1.addTarget(self, action: #selector(pageControl), for: .touchUpInside)
            
            cell?.pageBtn2.addTarget(self, action: #selector(pageControl), for: .touchUpInside)

//            cell?.collVw.register(UINib(nibName: "ExpCell1", bundle: nil), forCellWithReuseIdentifier: "ExpCell1")
//               cell?.collVw.register(UINib(nibName: "ExpCell2", bundle: nil), forCellWithReuseIdentifier: "ExpCell2")
//            cell?.collVw.tag = indexPath.row
//            cell?.collVw.dataSource=self
//            cell?.collVw.delegate=self
//            cell?.collVw.reloadData()
            
        return cell!
        }
        
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == slideTblVw
        {
            let section = self.sectionItems[indexPath.section] as! NSArray
            let titleStr = section[indexPath.row] as? String
            let height = titleStr?.height(withConstrainedWidth: self.slideTblVw.frame.size.width-50, font: UIFont.boldSystemFont(ofSize: 18))
            
            return height!+30
        }
        else
        {
            if indexPath.section==0
            {
                return 162
            }
            else
            {
                return 475
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if tableView == slideTblVw
        {
            self.openCloseSideMenu();
        }
        else
        {
            
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        if (self.sectionNames.count != 0) {
//            return self.sectionNames[section] as? String
//        }
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat 
    {
        if tableView == slideTblVw
        {
            let sectionDic = self.sectionNames[section]
            
            let titleStr = ""
//            let titleStr = sectionDic["name"] as? String
            let arrayOfItems = self.sectionItems[section] as! NSArray
            if(arrayOfItems.count>0)
            {
                let height = titleStr.height(withConstrainedWidth: self.slideTblVw.frame.size.width-50, font: UIFont.boldSystemFont(ofSize: 24))
                return height+30
            }
            else
            {
                let height = titleStr.height(withConstrainedWidth: self.slideTblVw.frame.size.width-50, font: UIFont.boldSystemFont(ofSize: 14))
                return height+30;
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        return 0;
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
          let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        
        let headerLabel = UILabel(frame: CGRect(x: 15, y: 10, width:
            header.frame.size.width-50, height: header.frame.size.height-10))
        
//        let sectionDic = self.sectionNames[section] as! NSDictionary
//        let titleStr = sectionDic["name"] as? String
//
        headerLabel.text = self.sectionNames[section] as! String
        headerLabel.lineBreakMode = .byWordWrapping
        headerLabel.numberOfLines=2
        header.addSubview(headerLabel)
    
        if tableView == slideTblVw
        {
            let arrayOfItems = self.sectionItems[section] as! NSArray
            if(arrayOfItems.count>0)
            {
                headerLabel.textColor = UIColor.colorWithHexString(hexStr: "#515365")
                headerLabel.font = UIFont.boldSystemFont(ofSize: 24)

                if let viewWithTag = self.view.viewWithTag(kHeaderSectionTag + section) {
                viewWithTag.removeFromSuperview()
                }
                let headerFrame = self.slideTblVw.frame.size
                let theImageView = UIImageView(frame: CGRect(x: headerFrame.width - 32, y: 10, width: 18, height:  header.frame.size.height-10));
                theImageView.image = UIImage(named: "Chevron-Dn-Wht")
                theImageView.tag = kHeaderSectionTag + section
                theImageView.contentMode = .scaleAspectFit
                header.addSubview(theImageView)

                header.tag = section
                let headerTapGesture = UITapGestureRecognizer()
                headerTapGesture.addTarget(self, action: #selector(ExploreViewController.sectionHeaderWasTouched(_:)))
                header.addGestureRecognizer(headerTapGesture)
            }
            else
            {
                headerLabel.textColor = UIColor.colorWithHexString(hexStr: "#888da8")
                headerLabel.font = UIFont.boldSystemFont(ofSize: 14)
            }
        }
       
    }
    
    // MARK: - Expand / Collapse Methods
    
    @objc func sectionHeaderWasTouched(_ sender: UITapGestureRecognizer) {
        let headerView = sender.view as! UITableViewHeaderFooterView
        let section    = headerView.tag
        let eImageView = headerView.viewWithTag(kHeaderSectionTag + section) as? UIImageView
        
        if (self.expandedSectionHeaderNumber == -1) {
            self.expandedSectionHeaderNumber = section
            tableViewExpandSection(section, imageView: eImageView!)
        } else {
            if (self.expandedSectionHeaderNumber == section) {
                tableViewCollapeSection(section, imageView: eImageView!)
            } else {
                let cImageView = self.view.viewWithTag(kHeaderSectionTag + self.expandedSectionHeaderNumber) as? UIImageView
                tableViewCollapeSection(self.expandedSectionHeaderNumber, imageView: cImageView!)
                tableViewExpandSection(section, imageView: eImageView!)
            }
        }
    }
    
    func tableViewCollapeSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        self.expandedSectionHeaderNumber = -1;
        if (sectionData.count == 0) {
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.slideTblVw!.beginUpdates()
            self.slideTblVw!.deleteRows(at: indexesPath, with: UITableViewRowAnimation.fade)
            self.slideTblVw!.endUpdates()
        }
    }
    
    func tableViewExpandSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        if (sectionData.count == 0) {
            self.expandedSectionHeaderNumber = -1;
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (90.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.expandedSectionHeaderNumber = section
            self.slideTblVw!.beginUpdates()
            self.slideTblVw!.insertRows(at: indexesPath, with: UITableViewRowAnimation.fade)
            self.slideTblVw!.endUpdates()
        }
    }
    
    
    
    
    @objc func pageControl(sender:UIButton)
    {
        if (!sender.isSelected)
        {
            if viewSelectionArray[sender.tag]==true
            {
                viewSelectionArray[sender.tag]=false
            }
            else
            {
                viewSelectionArray[sender.tag]=true
            }
            let indexPath = IndexPath(item: sender.tag, section: 1)
            tblVw.reloadRows(at: [indexPath], with: .none)
        } 
    }
    
  
    
    func openCloseSideMenu()
    {
        if self.slideBtn.frame.origin.x == 0
        {
            UIView.animate(withDuration: 0.40, delay:0, options: [], animations:
                {
                    var frame2 = self.slideTblVw.frame
                    frame2.origin.x = 0
                    self.slideTblVw.frame = frame2
                    var frame1 = self.slideBtn.frame
                    frame1.origin.x = self.slideTblVw.frame.size.width
                    self.slideBtn.frame = frame1
                    self.slideBtn.isSelected = true
            }, completion: { (finished: Bool) in
            })
        }
        else
        {
            UIView.animate(withDuration: 0.40, delay: 0.0, options: [], animations: {
                var frame2 = self.slideTblVw.frame
                frame2.origin.x = -self.slideTblVw.frame.size.width
                self.slideTblVw.frame = frame2
                var frame1 = self.slideBtn.frame
                frame1.origin.x = 0
                self.slideBtn.frame = frame1
                self.slideBtn.isSelected = false
            }, completion: { (finished: Bool) in
            })
        }
    }
    
}

