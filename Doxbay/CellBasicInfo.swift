//
//  CellBasicInfo.swift
//  DoxbaySalman
//
//  Created by AppsInvo  on 19/05/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit

class CellBasicInfo: UITableViewCell {
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtDOB: UITextField!
    @IBOutlet weak var txtSpeciality: UITextField!
    @IBOutlet weak var txtDesignation: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtCode: UITextField!
    @IBOutlet weak var txtQualification: UITextField!

    
    func configureView(info: [String: Any]) {
        txtName.text = info["displayName"] as? String
        txtDOB.text = info["doxbay_dob"] as? String
        txtSpeciality.text = info["doxbay_speciality"] as? String
        txtDesignation.text = info["doxbay_designation"] as? String
        txtAddress.text = info["doxbay_address"] as? String
        txtCode.text = info["doxbay_pincode"] as? String
        txtQualification.text = info["doxbay_qualification"] as? String
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(pickerChanged(_:)), for: .valueChanged)
        txtDOB.inputView = datePicker
        
        if !isForEdit {
            txtName.isUserInteractionEnabled = false
            txtDOB.isUserInteractionEnabled = false
            txtDesignation.isUserInteractionEnabled = false
            txtCode.isUserInteractionEnabled = false
            txtQualification.isUserInteractionEnabled = false
            txtAddress.isUserInteractionEnabled = false
            txtSpeciality.isUserInteractionEnabled = false
        } else {
            txtName.isUserInteractionEnabled = true
            txtDOB.isUserInteractionEnabled = true
            txtDesignation.isUserInteractionEnabled = true
            txtCode.isUserInteractionEnabled = true
            txtQualification.isUserInteractionEnabled = true
            txtAddress.isUserInteractionEnabled = true
            txtSpeciality.isUserInteractionEnabled = true
        }
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    @objc func pickerChanged(_ picker: UIDatePicker) {
        let date = picker.date
        profileData["doxbay_dob"] = date.convertToString("dd EEE yyyy")
        txtDOB.text = date.convertToString("dd MMM yyyy")
    }
    
    @IBAction func didChanged(_ sender: UITextField) {
        if sender == txtName {
            profileData["displayName"] = txtName.text!
        }
        if sender == txtSpeciality {
            profileData["doxbay_speciality"] = txtSpeciality.text!
        }
        if sender == txtDesignation {
            profileData["doxbay_designation"] = txtDesignation.text!
        }
        if sender == txtAddress {
            profileData["doxbay_address"] = txtAddress.text!
        }
        if sender == txtCode {
            profileData["doxbay_pincode"] = txtCode.text!
        }
        if sender == txtQualification {
            profileData["doxbay_qualification"] = txtQualification.text!
        }
    }
}













