//
//  CellPersonalInfo.swift
//  DoxbaySalman
//
//  Created by AppsInvo  on 21/05/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CellPersonalInfo: UITableViewCell {

    @IBOutlet weak var txtLandline: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var txtRegNum: UITextField!
    @IBOutlet weak var btnVerify: UIButton!
    
    
    

    @IBAction func tapVerify(_ sender: Any)
    {
        // https://2factor.in/API/V1/{api_key}/SMS/{phone_number}/AUTOGEN
        let todoEndpoint: String = generateSMSOtp  + profileData["mobile"]! + "/AUTOGEN";
        print("Snd OTP Request API \(todoEndpoint)")
        Alamofire.request(todoEndpoint)
            .responseJSON { response in
                // handle JSON
            }
            .responseString { response in
                if let error = response.result.error {
                    print(error)
                }
                if let value = response.result.value {
                    let swiftyJsonVar = JSON(response.data)
                     print("swiftyJsonVar -- \(swiftyJsonVar)")
                    let Status = swiftyJsonVar["Status"].stringValue
                     let Details = swiftyJsonVar["Details"].stringValue
                    print("Status --- \(Status) ---Details -----\(Details)")
                    
                    print(value)
                    ShowToast.showPositiveMessage(message: Details)
                    if(isFromLoginPage)
                    {
                        let vc = profileInstance.storyboard?.instantiateViewController(withIdentifier: "VerifyOTPVC") as! VerifyOTPVC
                        vc.moileNo = profileData["mobile"]!
                        vc.Details = Details
                        //profileInstance.navigationController?.pushViewController(vc, animated: true)
                        profileInstance.present(vc, animated:true, completion:nil)
                    }
                    else if (isForEdit)
                    {
                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                        let vc = storyBoard.instantiateViewController(withIdentifier: "VerifyOTPVC") as! VerifyOTPVC
                        vc.moileNo = profileData["mobile"]!
                        vc.Details = Details
                        profileInstance.present(vc, animated:true, completion:nil)
                    }
                    else
                    {
                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                        let vc = storyBoard.instantiateViewController(withIdentifier: "VerifyOTPVC") as! VerifyOTPVC
                        vc.moileNo = profileData["mobile"]!
                         vc.Details = Details
                        profileInstance.present(vc, animated:true, completion:nil)
                    }
                }
        }
    }
    
    
    func configureView(info: [String: Any])
    {
        txtEmail.text =  info["email"] as? String
        txtMobile.text = info["mobile"] as? String
        txtLandline.text = info["doxbay_landline"] as? String

        
        
        btnVerify.isHidden = !isForEdit
        txtMobile.isUserInteractionEnabled = isForEdit
        txtEmail.isUserInteractionEnabled = false
        txtLandline.isUserInteractionEnabled = isForEdit
        txtRegNum.isUserInteractionEnabled = isForEdit
        
        if(is_valid_user != 1 )
        {
            btnVerify.setTitle("Verify",for: .normal)
            
        }
        else
        {
            btnVerify.setTitle("Verified",for: .normal)
            btnVerify.isUserInteractionEnabled = false
            txtMobile.isUserInteractionEnabled = false
        }
      
        
       
    }
    
    @IBAction func didChanged(_ sender: UITextField) {
        
        if sender == txtLandline {
            profileData["doxbay_landline"] = txtLandline.text!
        }
        if sender == txtEmail {
            profileData["email"] = txtEmail.text!
        }
        if sender == txtMobile {
            profileData["mobile"] = txtMobile.text!
        }
        if sender == txtRegNum {
            profileData["registration_number"] = txtRegNum.text!
        }        
    }
    
    
    
}



















