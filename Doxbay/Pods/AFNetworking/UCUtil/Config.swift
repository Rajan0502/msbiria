//
//  Config.swift
//  Doxbay
//
//  Created by Unify on 26/04/18.
//  Copyright © 2018 Yuji Hato. All rights reserved.
//

import UIKit

var token = ""; //QnEOzn2Gc0Gj447ybQmkJgrAK8MJkmeDccB-Ro3W4AQ";
var channel_id = "629"  //608";
var dox_id = "" //ZAbpSJQOvLe0ioHIQ7EKgmBZM7h1";
var for_uuid = ""// ZAbpSJQOvLe0ioHIQ7EKgmBZM7h1";
var email = "";

var isFromMenu:String = ""

// let BASEURL = "https://doxbay.com/test/";
let BASEURL = "https://doxbay.com/workstation/";



// user data API
let getUser = "doxbay_user/get_user";
let postUser = "Doxbay_user/post_user"
let postUserdd = "ddlogic_login/post_user"
let getToken = "Doxbay_user/get_token";

let addUserJob = "Octabeans_api/add_user_job"
let addUserEducation = "Octabeans_api/add_user_education"
let addUserSpeciality = "Octabeans_api/add_user_speciality"
let deleteUserJob = "Octabeans_api/delete_user_job";
let deleteUserEducation = "Octabeans_api/delete__user_education"
let deleteUspeciality = "Octabeans_api/delete_uspeciality";
let createUseFollow = "Octabeans_notification/create_user_follow"
let getUserPost = "octabeans_user/get_user_post";
let deleteUserPost = "octabeans_user/delete_user_post";
let userImageUpdate = "Doxbay_user/user_image_update";
let blockUnblockUser = "Blocked_user/block_unblock_user";


let getOrganizationTimeline = "octabeans_organization_timeline/get_organization_timeline";
let getAllOrganizationTimeline = "Octabeans_all_organization_timeline/get_all_organization_timeline";
let addVideo = "Octabeans_upload/add_video";
let createUserPost = "Octabeans_notification/create_user_post";
let addNewPostChannel = "Octabeans_notification/add_newpost"
let getChannelPost = "octabeans_channel_post/get_channel_post"

let createUserEvent = "Octabeans_notification_event/create_user_event";
let getUserEvent = "octabeans_user/get_user_event";
let deleteUserEvent = "octabeans_user/delete_user_event"

let searchOrganizationTimeline = "octabeans_organization_timeline/search_organization_timeline"
let allOrganizationTimelineSearch = "Octabeans_all_organization_timeline/all_organization_timeline_search"
let searchPost = "octabeans_search/search_post";


// call this method if = "content_type": "user_post"
let createUserPostComment = "octabeans_user/create_user_post_comment";
let createUserPostLike = "octabeans_likes/create_user_post_like";
let getUserPostComment = "octabeans_user/get_user_post_comment";

// call this method if = "content_type": "user_event"
let createEventComment = "octabeans_user/create_event_comment";
let createEventLike = "octabeans_likes/create_event_like";
let getEventComment = "octabeans_user/get_event_comment";

// call this method if = "content_type": "channel_post"
let postComment = "doxbay_comment/post_comment";
let postLike = "octabeans_likes/post_like";
let getComment = "doxbay_comment/get_comment";

let subsribeUnsubscribeChannel = "Channel_subscriber/subsribe_unsubscribe_channel";


let deleteComment = "doxbay_comment/delete_comment"
let deleteEventComment = "octabeans_user/delete_event_comment"
let deleteUserPostComment = "octabeans_user/delete_user_post_comment"

// get caterory list of explore
let getChannelCategory = "Getchannel/get_channel_category"
// get channel list of category
let getExpChannel = "octabeans_channel_post/get_channel"


//get channel details
let getChannelList = "octabeans_search/get_channel_list"
let searchChannel = "octabeans_search/search_channel"
let searchConnectedChannel = "Connected_channel/search_connected_channel";
let searchConnectedChannelExtra = "Connected_channel/search_connected_channel_extra";



// conneted channels
let getConnectedChannels = "Connected_channel/get_connected_channel";
let getConnectedChannelExtra = "Connected_channel/get_connected_channel_extra";




// get organization details on behal of post type
let getChannelCategoryPost = "octabeans_channel_category_post/get_channel_category_post"



// add remove fevorite post
let postFavorite = "doxbay_comment/post_favorite";

// contact us
let channelContact = "Octabeans_about/channel_contact";
// about us
let channelAbout = "Octabeans_about/channel_about";

// get office channel details
let channelOffice = "Octabeans_about/channel_office";

// get channel subscriber user list for channel
let getSubscriber = "pagination/get_subscriber";
let searchUser = "pagination/search_user";




// get subscribed channel list
let getSubscribedChannels = "Channel_subscriber/get_subscribed_channels";
let channelVisit = "Channel_subscriber/channel_visit"


let getUserFollowerList = "follower/get_user_follower_list"
let getUserFollowingList = "follower/get_user_following_list";
let searchUserFollowerList = "follower/search_user_follower_list"
let searchUserFollowingList = "follower/search_user_following_list"

// get fevorites list
let getFavoritePost = "octabeans_channel_post/get_favorite_post";





// user OTP validate
let user_validate = "/doxbay_user/user_validate"


// Form page 
let questionList = "question/question_list"
let formTitlePagination = "/question/form_title_pagination"
let saveQuestionResponse = "question/saveQuestionResponse"
let addAnswerImage = "Octabeans_upload/add_answer_image"

// get Question list for channel
let getQuizQuestionList = "quiz/question_list";
let SaveQuizUserAnswer = "quiz/save_user_answer";
let getQuizList = "quiz/quiz_list";
let leaderBoard = "quiz/leaderboard"


let getUserReportReasonList = "report_user/get_user_report_reason_list"
let reportUserPost = "report_post/report_user_post";
let reportChannelPost = "report_post/report_channel_post";
let reportUserEvent = "report_post/report_user_event"
let userReported = "report_user/user_reported";



// webAdmin URL
let webAdminURL = "https://doxbay.com/workstation"


// OTP
let generateSMSOtp = "https://2factor.in/API/V1/dc854b63-541a-11e8-a895-0200cd936042/SMS/"
let VerityOtp = "https://2factor.in/API/V1/dc854b63-541a-11e8-a895-0200cd936042/SMS/VERIFY/"
class Config: NSObject
{
}
