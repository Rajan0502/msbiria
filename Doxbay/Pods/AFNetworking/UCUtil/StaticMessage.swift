//
//  StaticMessage.swift
//  Doxbay
//
//  Created by Luminous on 18/07/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import Foundation


let  AppNameText = "Maharashtra IRIA Connect"
let  LoginText = "Login"
let  LogOutText = "Logout"
let  SomeThingWentWorng = "Something went wrong please try again later"
let  DoYouWantToLogOut = "Dow you want to logout from App"
let  YesText = "Yes"
let  NoThankText = "No, Thanks"
let  CommingSoonText = "Comming soon......"
let  SuccessfullyUpdateText = "successfully updated"
let  ReportText = "Report"
let  BlockText = "BLOCK"
let  UnBlockText = "UNBLOCK"
let  SaveText = "Save"
let  UpdateText = "Update"
let  SubmitText = "Submit"
let  SubscribeMessgaeText = "Your subscription request send to admin, Please wait for admin confirmation. Thanks"
let  SubscribeNowText = "Subscribe"
let  UnSubscribe = "UnSubscribe"
let  ReportReson = "Please select user report reason"



