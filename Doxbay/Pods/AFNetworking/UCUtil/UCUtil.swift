//
//  UCUtil.swift
//  Doxbay
//
//  Created by Unify on 24/04/18.
//  Copyright © 2018 Yuji Hato. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class UCUtil: NSObject {
    
    
    
   class func colorWithHexString (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substring(from: 1)
        }
        
        if (cString.characters.count != 6) {
            return UIColor.gray
        }
        
        let rString = (cString as NSString).substring(to: 2)
        let gString = ((cString as NSString).substring(from: 2) as NSString).substring(to: 2)
        let bString = ((cString as NSString).substring(from: 4) as NSString).substring(to: 2)
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string: gString).scanHexInt32(&g)
        Scanner(string: bString).scanHexInt32(&b)
        
        
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
    }

    
    
    
    
    class func requestGETURL(_ strURL: String, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void) {
        Alamofire.request(strURL).responseJSON { (responseObject) -> Void in
            
            print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    
    
    
    
    class func requestPOSTURL(_ strURL : String, params : [String : AnyObject]?, headers : [String : String]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        
        Alamofire.request(strURL, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (responseObject) -> Void in
            
            print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    
    
    
    class func requestPOSTURLWithFormData(_ strURL : String, params : [String : String]?, headers : [String : String]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
 
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]

        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in params! {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                
            }
            //        if let data = imageData{
            
            //        multipartFormData.append(data, withName: "image", fileName: "image.png", mimeType: "image/png")
            
            //        }
     
        }, usingThreshold: UInt64.init(), to: strURL, method: .post, headers: headers) { (result) in
            
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    
                    print("Succesfully uploaded")
                    print(response.result)
                    
                    if response.result.isSuccess {
                        let resJson = JSON(response.result.value!)
                        success(resJson)
                    }
                    if response.result.isFailure {
                        let error : Error = response.result.error!
                        failure(error)
                    }
                    
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                failure(error)
            }
        }
    }
    
    
    
    
    
    class func requestPOSTURLWithFormDataAndImageArrayAndPDFArray(_ strURL : String, params : [String : String]?, headers : [String : String]?, imageArray : [Any]?, pdfArray : [Any]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in params! {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if(imageArray!.count > 0)
            {
                var count = 0
                for img in imageArray!
                {
                    count += 1
                    let data =  UIImageJPEGRepresentation(img as! UIImage, 0.5) as? Data
                    if (data != nil)
                    {
                        let imageName = "image" + String(count)
                        multipartFormData.append(data!, withName: imageName, fileName: "image" + String(count) + ".png", mimeType: "image/png")
                    }
                }
            }


            if(pdfArray!.count > 0)
            {
                for pdfurl in pdfArray!
                {
                        do {
                            let data = try Data(contentsOf: pdfurl as! URL)
                            multipartFormData.append(data, withName: "pdf_name",  fileName: "pdf.pdf", mimeType: "application/pdf")
                        } catch {
                            print(error)
                        }
                }
            }
            
        }, usingThreshold: UInt64.init(), to: strURL, method: .post, headers: headers) { (result) in
            
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    
                    print("Succesfully uploaded")
                    print(response.result)
                    
                    if response.result.isSuccess {
                        let resJson = JSON(response.result.value!)
                        success(resJson)
                    }
                    if response.result.isFailure {
                        let error : Error = response.result.error!
                        failure(error)
                    }
                    
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                failure(error)
            }
        }
    }
    
    
    
    
    
    class func requestPOSTvideoWithUrl(_ strURL : String, params : [String : String]?, headers : [String : String]?, videoArray : [Any]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in params! {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                
            }
            
            if(videoArray!.count > 0)
            {
                for videoUrl in videoArray!
                {
                    do {
                        let data = try Data(contentsOf:videoUrl as! URL)
                        multipartFormData.append(data, withName: "video", fileName: "videoiOS.mp4", mimeType: "video/mp4")
                    } catch {
                        print(error)
                    }
                }
            }
            
        }, usingThreshold: UInt64.init(), to: strURL, method: .post, headers: headers) { (result) in
            
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    
                    print("Succesfully uploaded")
                    print(response.result)
                    
                    if response.result.isSuccess {
                        let resJson = JSON(response.result.value!)
                        success(resJson)
                    }
                    if response.result.isFailure {
                        let error : Error = response.result.error!
                        failure(error)
                    }
                    
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                failure(error)
            }
        }
    }
    
    class func getImagData(photoUrl: URL, success:@escaping (UIImage) -> Void, failure:@escaping (Error) -> Void)
    {
        Alamofire.request(photoUrl).responseData { (response) in
        if response.error == nil {
            print(response.result)
                if let data = response.data {
                    success(UIImage(data: data)!)
                }
                else
                {
                    failure(response.error!)
                }
            }
            else
            {
                failure(response.error!)
            }
        }
    }
    
    
    
    
}
