//
//  ListSignupTypeVC.swift
//  Doxbay
//
//  Created by AppsInvo  on 05/05/18.
//  Copyright © 2018 Salman. All rights reserved.
//

import UIKit

class FormList_VC : UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var listInfo = [[String:Any]]()
    @IBOutlet weak var tblView: UITableView!
    
    @IBAction func backClk(_ sender: Any)
    {
        // self.navigationController?.popViewController(animated: false)
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getList()
        self.hideKeyboardOnTap(#selector(self.dismissKeyboard))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    
    
    func getList()
    {
        let params = ["channelId": channel_id,
                      "token": token,
                      "currentpage": String(FormPageNo),
                      Constant.kAPI_NAME: formTitlePagination]
        SwiftLoader.show(title: "Loading...", animated: true)
        SGServiceController.instance().requestPOSTURLWithFormData(params: params, imageData: nil, headers: nil, success: { (response) in
            print(response)
            SwiftLoader.hide()
            if let data = response.dictionaryObject, let arrdata = data["data"] as? [[String: Any]] {
                self.listInfo.append(contentsOf: arrdata)
                print(data)
                self.tblView.reloadData()
                if(arrdata.count < 10)
                {
                    self.FormIsAvilableForLoad = false
                }
            }
        }) { (error) in
            SwiftLoader.hide()
            print(error)
        }
    }
    
    
    
    
    var FormPageNo : Int = 0;
    var ForLmoadingData = false;
    var FormIsAvilableForLoad = true;
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = listInfo.count - 3
        if !ForLmoadingData && indexPath.row == lastElement {
            ForLmoadingData = true
            self.loadMoreData()
        }
    }
    func loadMoreData() {
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                self.FormPageNo = self.FormPageNo + 1
                if(self.FormIsAvilableForLoad)
                {
                    self.getList();
                }
            }
        }
    }
    
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listInfo.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var nfo: [String: Any] = listInfo[indexPath.row];
        var fTitleH =  Utility.heightForView(text: (nfo["titleText"] as? String)!);
        if(fTitleH > 30)
        {
            fTitleH = fTitleH + 10;
        }
        var fDescriptionH =  Utility.heightForView(text: (nfo["description"] as? String)!);
        if(fDescriptionH > 60)
        {
            fDescriptionH = fDescriptionH + 30;
        }
        return  fTitleH + fDescriptionH + 140
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellListSignup", for: indexPath) as! CellListSignup
        cell.configureView(info: listInfo[indexPath.row])
        
        cell.btnSubmitForm.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        cell.btnSubmitForm.tag = indexPath.row
        
        return cell
    }
    
    @objc func buttonAction(sender: UIButton!)
    {
        let btnsendtag: UIButton = sender
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "FormSubmit_VC") as! FormSubmit_VC
        vc.formData = listInfo[btnsendtag.tag]
        self.present(vc, animated:true, completion:nil)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "FormSubmit_VC") as! FormSubmit_VC
        vc.formData = listInfo[indexPath.row]
        self.present(vc, animated:true, completion:nil)
    }
}






