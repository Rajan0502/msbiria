//  DynamicSignupVC.swift
//  Doxbay
//  Created by AppsInvo  on 05/05/18.
//  Copyright © 2018 Salman. All rights reserved.


import UIKit
var signupAnswerSheet: [[String: Any]]?
var isFromChannelID : String = ""

class FormSubmit_VC: UIViewController {
    
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var formData = [String: Any]()
    var data = [[String:Any]]()
    
    
    
    @IBAction func backClk(_ sender: Any)
    {
        // self.navigationController?.popViewController(animated: false)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "CellOptionCollection", bundle: nil), forCellReuseIdentifier: "CellOptionCollection")
        tableView.register(UINib(nibName: "CellOptionTable", bundle: nil), forCellReuseIdentifier: "CellOptionTable")
        tableView.register(UINib(nibName: "CellTextFiled", bundle: nil), forCellReuseIdentifier: "CellTextFiled")
        instanceVC = self
        btnSubmit.makeRoundCorner(5)
        self.hideKeyboardOnTap(#selector(self.dismissKeyboard))
        
        getDynamicForm()
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func tapBack(_ sender: Any) {
        // self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    func fillAnswerSheet() {
        signupAnswerSheet = [[String: Any]]()
        for question in data {
            var dictAnswer = [String: Any]()
            let inputType = question["inputType"] as! String
            dictAnswer["id"] = question["id"]
            dictAnswer["questionText"] = question["questionText"]
            dictAnswer["titleId"] = question["titleId"]
            dictAnswer["channelId"] = isFromChannelID
            dictAnswer["uuid"] =  for_uuid
            dictAnswer["inputType"] = question["inputType"] as! String
            
            switch inputType {
            case "text":
                if let arrOption = question["options"] as? [[String: Any]], arrOption.count > 0 {
                    var options = arrOption[0]
                    options["optionAnswer"] = ""
                    dictAnswer["options"] = [options]
                }
                
                signupAnswerSheet?.append(dictAnswer)
            case "radio":
                if let arrOption = question["options"] as? [[String: Any]], arrOption.count > 0 {
                    var options = arrOption[0]
                    options["optionAnswer"] = ""
                    dictAnswer["options"] = [options]
                }
                signupAnswerSheet?.append(dictAnswer)
            case "image":
                if let arrOption = question["options"] as? [[String: Any]], arrOption.count > 0 {
                    var arrMutableOption = [[String: Any]]()
                    for (_,option) in arrOption.enumerated() {
                        var mutableOption = option
                        mutableOption["optionAnswer"] = ""
                        mutableOption.removeValue(forKey: "")
                        arrMutableOption.append(mutableOption)
                    }
                    dictAnswer["options"] = arrMutableOption
                }
                signupAnswerSheet?.append(dictAnswer)
            case "checkbox":
                if let arrOption = question["options"] as? [[String: Any]], arrOption.count > 0 {
                    var arrMutableOption = [[String: Any]]()
                    for (_,option) in arrOption.enumerated() {
                        var mutableOption = option
                        mutableOption["optionAnswer"] = ""
                        arrMutableOption.append(mutableOption)
                    }
                    dictAnswer["options"] = arrMutableOption
                }
                signupAnswerSheet?.append(dictAnswer)
            default:
                break
            }
        }
    }
    
    func getDynamicForm(){
        SwiftLoader.show(title: "Loading...", animated: true)
        let params = ["titleId":formData["id"] as! String,
                      "token": token,
                      Constant.kAPI_NAME: questionList]
        
        SGServiceController.instance().requestPOSTURLWithFormData(params: params, imageData: nil, headers: nil, success: { (response) in
            print(response)
            if let data = response.dictionaryObject, let dictList = data["data"] as? [[String: Any]] {
                self.data = dictList
                self.fillAnswerSheet()
                self.tableView.reloadData()
                SwiftLoader.hide()
            }
            else
            {
                SwiftLoader.hide()
            }
        }) { (error) in
            print(error)
            SwiftLoader.hide()
        }
    }
    
    
    @IBAction func tapSubmit(_ sender: Any)
    {
        SwiftLoader.show(title: "Submitting..", animated: true)
        
        let params = ["questionResponse":signupAnswerSheet!,
                      "titleId":formData["id"] as! String,
                      "channelId": isFromChannelID,
                      "token": token,
                      ] as [String : Any]
        
     //   xxx  question---["options": [["id": 526, "optionText": both paper ballot & evoting, "optionAnswer": both paper ballot & evoting]], "id": 274, "inputType": "radio", "titleId": 88, "questionText": do we need paper ballot ?, "uuid": "101307028376544188199", "channelId": "629"]
        

        for question in signupAnswerSheet! {
             print("xxx  question---\(question)")
            
             let questionText = question["questionText"] as! String
             print("xxx  questionText---\(questionText)")
          
             let dictAnswerx : [String: Any] = question
             print("xxx  dictAnswerxdictAnswerxdictAnswerx ---\(dictAnswerx)")

         
            
            
            for (key, value) in dictAnswerx {
                print("\(key) -> \(value)")
                if key == "options"
                {
                    
                    
                    // print("dictAnswerxpppdictAnswerxpppdictAnswerxppp -> \(dictAnswerxppp)")
                    let xxxx = dictAnswerx["optionAnswer"] as? String
                    if xxxx == ""
                    {
                        ShowToast.showNegativeMessage(message: "Plesae answer to Question. " + xxxx! )
                        SwiftLoader.hide()
                        return
                    }
                }
            }
        }
        
        let data = ["data":signupAnswerSheet!,
                    Constant.kAPI_NAME: saveQuestionResponse] as [String : Any]
        
        SGServiceController.instance().hitPostServiceJsonForm(data, unReachable: {
            print("Not reachable")
            SwiftLoader.hide()
        }) { (response) in
            print(response)
            SwiftLoader.hide()
            if response != nil {
                 self.showOkAlert("Sucessfully submitted")
                 self.dismiss(animated: true, completion: nil)
            }
        }
        
    }
    
    
}


extension FormSubmit_VC : UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellData = data[indexPath.row]
        let inputType = cellData["inputType"] as! String
        switch inputType {
        case "text":
            return 90
        case "radio":
            let arrRadio = cellData["options"] as! [[String: Any]]
            return CGFloat((arrRadio.count*40)+70)
        case "image":
            let arrImage = cellData["options"] as! [[String: Any]]
            return CGFloat((arrImage.count*60)+70)
        case "checkbox":
            let arrImage = cellData["options"] as! [[String: Any]]
            if arrImage.count%2 == 0 {
                return CGFloat((arrImage.count*25)+50)
            }else{
                return CGFloat((arrImage.count*25)+75)
            }
        default:
            break
        }
        return 80
    }
}



extension FormSubmit_VC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellData = data[indexPath.row]
        let inputType = cellData["inputType"] as! String
        
        switch inputType {
        case "text":
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellTextFiled", for: indexPath)as! CellTextFiled
            cell.configureCell(info: cellData)
            return cell
        case "radio":
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellOptionTable", for: indexPath)as! CellOptionTable
            cell.configureCell(info: cellData, index: indexPath.row)
            return cell
        case "image":
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellOptionTable", for: indexPath)as! CellOptionTable
            cell.configureCell(info: cellData, index: indexPath.row)
            return cell
        case "checkbox":
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellOptionCollection", for: indexPath)as! CellOptionCollection
            cell.configureView(info: cellData)
            return cell
        default:
            break
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellTextFiled", for: indexPath)as! CellTextFiled
        cell.configureCell(info: cellData)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
}






