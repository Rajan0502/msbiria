//
//  popUp.swift
//  Doxbay
//
//  Created by kavi yadav on 10/07/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit

class popUp: PopupController,PopupContentViewController {

    @IBOutlet weak var contentView: UIView!
    var contentFrame = CGFloat()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.view.frame.size = CGSize(width: 300,height: 300)
        
        contentFrame = contentView.frame.size.height
        self.view.frame.size.height = contentFrame
        self.view.layer.cornerRadius = 10
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    class func instance() -> popUp {
        let storyboard = UIStoryboard(name: "popUp", bundle: nil)
        return (storyboard.instantiateInitialViewController() as? popUp)!
    }
    
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: 320,height: contentFrame + 80)
    }

}
