
//  ViewController.swift
//  Parallex Scrolling
//  Created by bibek on 8/25/16.
//  Copyright © 2016 bibek. All rights reserved.

let screenFrameX = UIScreen.main.bounds
let floatingFrameX = CGRect(x: screenFrame.size.width - 80 , y: screenFrame.size.height - 70 , width: 60, height: 60)
let floatingActionButtonX = LiquidFloatingActionButton(frame: floatingFrameX)



import UIKit
import MBProgressHUD
import SDWebImage
import AVKit
import AVFoundation
import MobileCoreServices
import Alamofire
import SwiftyJSON


var profileInstance = ProfileViewTimeLine()
var profileData = [String: String]()
var allDetail = [String: Any]()
var isForEdit = false
var isFromLoginPage = false
var is_valid_user : Int = 0;


class ProfileViewTimeLine : UIViewController {


   let btnBlock :UIButton = UIButton(frame: CGRect(x: 0, y: screenHeight - 50, width: screenWidth / 2 - 1, height: 50))
   let btnReport :UIButton = UIButton(frame: CGRect(x: screenWidth / 2 + 2, y: screenHeight - 50, width: screenWidth / 2 - 1, height: 50))
  let color = UIColor(red: 25.0/255.0, green: 28.0/255.0, blue: 118.0/255.0, alpha: 1.0)
    
    
    var profileID : String = ""
    var segmentIndex : Int = 0
    var isFromLogin : Bool = false
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var btnSaveProfile: UIButton!
    
    @IBOutlet weak var profileBGImg: UIImageView!
    @IBOutlet weak var profileImg: UIImageView!
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userQulification: UILabel!
    @IBOutlet weak var followerCount: UILabel!
    @IBOutlet weak var followingCount: UILabel!
    @IBOutlet weak var postCount: UILabel!
    
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var btnRefrace: UIImageView!
    @IBOutlet weak var btntakePic: UIImageView!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    
    @IBOutlet var labFollowers: UILabel!
    @IBOutlet var labFollowing: UILabel!
    @IBOutlet var labPost: UILabel!
    
    @IBOutlet weak var followersUIView: UIView!
    @IBOutlet weak var followingUIView: UIView!
    @IBOutlet weak var postUIView: UIView!
    
    
    
    
    var arrSpecialities = [[String: Any]]()
    var arrQualification = [[String: Any]]()
    var arrjobDetail = [[String: Any]]()
    
    
    var isFollowUnfollow : String = "0"
    var isBlockUnBlock : String = "0"

    var photoUrl : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let screensize: CGRect = UIScreen.main.bounds
        let screenWidth = screensize.width - 0
        let screenHeight = screensize.height
        
        tableView.frame = CGRect(x: 5 , y: 10, width: screenWidth - 10, height: screenHeight - 50)
        viewHeader.alpha = 0
        tableView.delegate = self
        tableView.dataSource = self
        
        isFromLoginPage = false;
        if(profileID == for_uuid)
        {
            isForEdit = true
        }
        if (isFromLogin)
        {
            isFromLoginPage = true
            isForEdit = true
            segmentedControl.isHidden = true
            userName.isHidden = true
            userQulification.isHidden = true
            followerCount.isHidden = true
            followingCount.isHidden = true
            postCount.isHidden = true
            btnFollow.isHidden = true
           // btnBlock.isHidden = true

            btnRefrace.isHidden = true
            labFollowers.isHidden = true
            labFollowing.isHidden = true
            labPost.isHidden = true
          //  btntakePic.isHidden = true
            
        }
        
       // btnSaveProfile.isHidden = !isForEdit
        btntakePic.isHidden = !isForEdit
        btnFollow.isHidden = isForEdit
       // btnBlock.isHidden = isForEdit

        
        
        if(isFromLogin)
        {
            tableView.register(UINib(nibName: "CellBasicInfo", bundle: nil), forCellReuseIdentifier: "CellBasicInfo")
             tableView.register(UINib(nibName: "CellPersonalInfo", bundle: nil), forCellReuseIdentifier: "CellPersonalInfo")
        }
        else{
            tableView.register(UINib(nibName: "CellBasicViewUpdate", bundle: nil), forCellReuseIdentifier: "CellBasicViewUpdate")
        }
        
        tableView.register(UINib(nibName: "CellQualification", bundle: nil), forCellReuseIdentifier: "CellQualification")
        tableView.register(UINib(nibName: "CellSpecialities", bundle: nil), forCellReuseIdentifier: "CellSpecialities")
        
        
      

         tableView.register(UINib(nibName: "CellExperience", bundle: nil), forCellReuseIdentifier: "CellExperience")
        
        
           segmentedControl.addTarget(self, action: #selector(self.segmentedValueChanged(_:)), for: .valueChanged)
        
        
        
        
        let refpage = UITapGestureRecognizer(target: self, action: #selector(self.refRacePage(sender:)))
        btnRefrace.addGestureRecognizer(refpage)
        btnRefrace.isUserInteractionEnabled = true
    
        

        getProfile()
        profileInstance = self
        
        
        if(isForEdit)
        {
            let tapImg = UITapGestureRecognizer(target: self, action: #selector(self.chooseImg(sender:)))
            profileImg.addGestureRecognizer(tapImg)
            profileImg.isUserInteractionEnabled = true
            
            btntakePic.addGestureRecognizer(tapImg)
            btntakePic.isUserInteractionEnabled = true
            
            if(!isFromLogin)
            {
                cellFactory()
                initFloatingButton();
            }
        }
        else
        {
            addFloatingButton()
           
            self.btnSaveProfile.isHidden = true
            
            btnBlock.backgroundColor = .red
            btnBlock.setTitle("Button", for: .normal)
            btnBlock.addTarget(self, action:#selector(self.btnBlockUnblock), for: .touchUpInside)
            self.view.addSubview(btnBlock)
            if(self.isBlockUnBlock == "1")
            {
                self.btnBlock.setTitle("UnBlock",for: .normal)
            }
            else
            {
                self.btnBlock.setTitle("Block",for: .normal)
            }
            
           
            self.btnReport.backgroundColor = color
            btnReport.setTitle("Report Now", for: .normal)
            btnReport.addTarget(self, action:#selector(self.btnReportNow), for: .touchUpInside)
            self.view.addSubview(btnReport)
            
           
        }
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(getFollowersList))
        followersUIView.addGestureRecognizer(tap)
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(getFollowingList))
        followingUIView.addGestureRecognizer(tap2)
        
        
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(getUserPostList))
        postUIView.addGestureRecognizer(tap3)
        
        
        
        
        
    }
    
    @objc func btnBlockUnblock() {
        self.UserBlockUnblock();
    }
    
    
    var reportList : [ReportResaonList] = []
    @objc func btnReportNow() {
        SwiftLoader.show(title: "Loading..", animated: true)
        let requestURL = BASEURL +  getUserReportReasonList
        let parameters: [String:String] = ["token": token ]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in

            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--response --\(response)")
            SwiftLoader.hide()
            if success
            {
                self.showReportResion(response: response);
            }
        }, failure:  {
            (error) -> Void in
            SwiftLoader.hide()
            print("error===\(error)")
        })
    }
    
    var actions : [(String, UIAlertActionStyle)] = []
    func showReportResion(response : JSON)
    {
        self.actions  = []
        for result in response["data"].arrayValue
        {
            let id = result["id"].stringValue
            let nameVal = result["name"].stringValue
            let re : ReportResaonList = ReportResaonList(id: id , name: nameVal);
            reportList.append(re);
            self.actions.append((nameVal , UIAlertActionStyle.destructive))
        }
        actions.append(("Dismiss", UIAlertActionStyle.cancel))
        
        
        Alerts.showActionsheet(viewController: self, title: "DoxBay MSBIRIA", message: "Please select user report reason", actions: actions) { (index) in
            print("call action \(index)")
            if(index < self.reportList.count)
            {
                let reasonID = self.reportList[index].id
                self.reportUserNow(id: reasonID );
            }
        }
    }
    func reportUserNow(id : String)
    {
        SwiftLoader.show(title: "Loading..", animated: true)
        let requestURL = BASEURL +  userReported
        let parameters: [String:String] = ["token": token , "report_reason_id" : id , "reported_user_id":   profileID]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--response --\(response)")
            SwiftLoader.hide()
            if success
            {
               ShowToast.showPositiveMessage(message: "Successfully reported")
            }
            else
            {
                ShowToast.showPositiveMessage(message: "Somthing went worng, Please try again")
            }
        }, failure:  {
            (error) -> Void in
            SwiftLoader.hide()
            print("error===\(error)")
            ShowToast.showPositiveMessage(message: "Somthing went worng, Please try again")
        })
    }
    
    
    
    
    @objc func getFollowersList()
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ChannelSubscriberList_VC") as! ChannelSubscriberList_VC
        vc.chanalId = profileID;
        vc.FromPage = "Followers"
        self.present(vc, animated:true, completion:nil)
    }
    @objc func getFollowingList()
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ChannelSubscriberList_VC") as! ChannelSubscriberList_VC
        vc.chanalId = profileID;
        vc.FromPage = "Following"
        self.present(vc, animated:true, completion:nil)
    }
    @objc func getUserPostList()
    {
         print("111 33");
    }
    
   
    
    var cellsX = [LiquidFloatingCell]()
   func  initFloatingButton()
    {
        DispatchQueue.main.async {
        floatingActionButtonX.color = UIColor(red: 25.0/255.0, green: 28.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        }
        
        floatingActionButtonX.dataSource = self
        floatingActionButtonX.delegate = self
        DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
        self.view.addSubview(floatingActionButtonX)
        self.view.bringSubview(toFront: floatingActionButtonX)
        }
        
        
        floatingActionButtonX.isHidden = true
    }
    
    func cellFactory() {
        cellsX.append(CustomLiquidFloatingCell(icon: UIImage(named: "vc_event")!, name: "Publish a event"))
        cellsX.append(CustomLiquidFloatingCell(icon: UIImage(named: "vc_post_pdf")!, name: "Post a Thought"))
        
    }
    
    
    var roundButton = UIButton()
    func addFloatingButton()
    {
        self.roundButton = UIButton(type: .custom)
        self.roundButton.setTitleColor(UIColor.white, for: .normal)
        self.roundButton.addTarget(self, action: #selector(ButtonClick(_:)), for: UIControlEvents.touchUpInside)
        self.view.addSubview(roundButton)
    }
    
    
    
    
    
    override func viewWillLayoutSubviews() {
         if(!isForEdit)
         {
            
            
        roundButton.layer.cornerRadius = roundButton.layer.frame.size.width/2
         roundButton.backgroundColor =  color
        roundButton.clipsToBounds = true
        roundButton.setImage(UIImage(named:"vc_chat"), for: .normal)
        //roundButton.setTitle("New",for: .normal)
        roundButton.titleColor(for: .normal)
        roundButton.setTitleColor(UIColor.white, for: .normal)
        roundButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([ roundButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20),roundButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -50),roundButton.widthAnchor.constraint(equalToConstant: 50),roundButton.heightAnchor.constraint(equalToConstant: 50)])
        }
    }
    
    @IBAction func ButtonClick(_ sender: UIButton){
        ShowToast.showNegativeMessage(message: "Comming sonn")
    }
    
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if(isForEdit)
        {
            tableView.reloadSections(NSIndexSet(index: 4) as IndexSet, with: .none)
        }
    }
    
    
    @objc func refRacePage(sender: UITapGestureRecognizer!)
    {
        if(segmentIndex == 0){
            floatingActionButtonX.isHidden = true
            DispatchQueue.main.async{
                self.getProfile()
            }
        }
        else if(segmentIndex == 1)
        {
            floatingActionButtonX.isHidden = false
             DispatchQueue.main.async{
                self.getExploreListData();
            }
        }
        else if(segmentIndex == 2)
        {
            floatingActionButtonX.isHidden = false
            DispatchQueue.main.async{
                self.getExploreListData();
            }
        }
    }
    
    @objc func segmentedValueChanged(_ sender:UISegmentedControl!)
    {
        segmentIndex = sender.selectedSegmentIndex;
        if(segmentIndex == 0){
            self.btnSaveProfile.isHidden = !isForEdit
            DispatchQueue.main.async{
                floatingActionButtonX.isHidden = true
                self.tableView.frame = CGRect(x: 0 , y: 0, width: screenWidth, height: screenHeight - 50)
                self.tableView.reloadData();
                
            }
        }
        else if(segmentIndex == 1)
        {
            DispatchQueue.main.async{
                floatingActionButtonX.isHidden = false
                
                self.tableView.frame = CGRect(x: 0 , y: 0, width: screenWidth , height: screenHeight)
                if(self.postTimeLine.count > 0)
                {
                    self.timeLimeData = self.postTimeLine;
                    self.tableView.reloadData();
                }
                else{
                    self.getExploreListData();
                }
                self.btnSaveProfile.isHidden = true
            }
        }
        else if(segmentIndex == 2)
        {
            DispatchQueue.main.async{
                floatingActionButtonX.isHidden = false
                self.tableView.frame = CGRect(x: 0 , y: 0, width: screenWidth, height: screenHeight)
                if(self.eventTimeLime.count > 0)
                {
                    self.timeLimeData = self.eventTimeLime;
                    self.tableView.reloadData();
                }
                else{
                    self.getExploreListData();
                }

              self.btnSaveProfile.isHidden = true
            }
        }
        
        print("Selected Segment Index is : \(sender.selectedSegmentIndex)")
    }

    
    
 
//-------------------------------Post Profile------------------------------
//=======================***********************============================
//==========================*************************======================
    @IBAction func btnFollowUnFollow(_ sender: Any) {
        var isFollow  = "";
        if(self.isFollowUnfollow == "0")
        {
            isFollow = "1" ;
        }
        else
        {
            isFollow = "0"
        }
        
        SwiftLoader.show(title: "Loading..", animated: true)
        let requestURL = BASEURL + createUseFollow
        let parameters = ["token":token, "uuid":profileID, "is_followed":isFollow]
        
        print("requestURL \(requestURL) --- parameters  - \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
             print("xxxxxxxx   ---- \(response)--")
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--")
            SwiftLoader.hide()
            if success
            {
                if(self.isFollowUnfollow == "0")
                {
                    self.btnFollow.setTitle("UnFollow",for: .normal)
                    self.isFollowUnfollow = "1"
                }
                else
                {
                    self.btnFollow.setTitle("Follow",for: .normal)
                    self.isFollowUnfollow = "0"
                }
            }
        }, failure:  {
            (error) -> Void in
            SwiftLoader.hide()
            print("error===\(error)")
        })
    }
    
    
    
    func UserBlockUnblock()
    {
        var isBlock  = "";
        if(self.isBlockUnBlock == "0")
        {
            isBlock = "1" ;
        }
        else
        {
            isBlock = "0"
        }
        
        SwiftLoader.show(title: "Loading..", animated: true)
        let requestURL = BASEURL + blockUnblockUser
        let parameters = ["token":token, "blocked_user_id":profileID, "is_blocked":isBlock]
        
        print("requestURL \(requestURL) --- parameters  - \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            print("xxxxxxxx   ---- \(response)--")
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--")
            SwiftLoader.hide()
            if success
            {
                if(self.isBlockUnBlock == "0")
                {
                    self.btnBlock.setTitle("UnBlock",for: .normal)
                    self.isBlockUnBlock = "1"
                }
                else
                {
                    self.btnBlock.setTitle("Block",for: .normal)
                    self.isBlockUnBlock = "0"
                }
            }
        }, failure:  {
            (error) -> Void in
            SwiftLoader.hide()
            print("error===\(error)")
        })
    }
    
    @IBAction func isBlock_User(_ sender: UIButton) {
        
        
    }
    
   
    
    @IBAction func saveProfileData(_ sender: Any)
    {
        if(isForEdit)
        {
            if(profileData["displayName"] == "")
            {
               ShowToast.showPositiveMessage(message: "Please enter your full Name.")
            }
            else if(profileData["doxbay_dob"] == "")
            {
                ShowToast.showPositiveMessage(message:"Please enter your Date of Birth")
            }
            else if(profileData["doxbay_qualification"] == "")
            {
                ShowToast.showPositiveMessage(message:"Please enter your Qulification")
            }
            else if(profileData["doxbay_speciality"] == "")
            {
                ShowToast.showPositiveMessage(message:"Please enter your Speciality")
            }
            else if(profileData["doxbay_designation"] == "")
            {
                ShowToast.showPositiveMessage(message:"Please enter your designation")
            }
            else if(profileData["doxbay_address"] == "")
            {
                ShowToast.showPositiveMessage(message:"Please enter your Address")
            }
            else if(profileData["doxbay_pincode"] == "")
            {
                ShowToast.showPositiveMessage(message:"Please enter your Pin Code")
            }
            else if(profileData["email"] == "")
            {
                ShowToast.showPositiveMessage(message:"Please enter your Email ID")
            }
            else if(profileData["doxbay_landline"] == "")
            {
               ShowToast.showPositiveMessage(message:"Please enter your valid landline number")
            }
            else if(profileData["mobile"] == "")
            {
                ShowToast.showPositiveMessage(message:"Please enter your Mobile No.")
            }
    //        else if(profileData["registration_number"] == "")
    //        {
    //            ShowToast.showPositiveMessage(message:"Please enter your Registraction No.")
    //        }
            else if(photoUrl == "")
            {
                ShowToast.showPositiveMessage(message:"Please upload your profile pic")
            }
            else{
            
            SwiftLoader.show(title: "Loading..", animated: true)
            profileData[Constant.kAPI_NAME] = postUser
            profileData["photoUrl"] = photoUrl
           
            
            SGServiceController.instance().hitPostService(profileData, unReachable: {
                print("No internet")
                SwiftLoader.hide()
            }, handler: { (response) in
                SwiftLoader.hide()
                if response == nil {
                    return
                }
                else
                {
                    ShowToast.showPositiveMessage(message: "Profile save sussefully save.")
                    self.getProfile()
                    if(!self.isFromLogin)
                    {
                       // self.getProfile()
                    }
                   else if( is_valid_user != 1)
                    {
                       ShowToast.showPositiveMessage(message: "Please verify your mobile no.")
                    }
                    else
                    {
                        OperationQueue.main.addOperation
                            {
                                UserDefaults.standard.set(true, forKey: "isRemindMe")
                                let storyboard: UIStoryboard =
                                    UIStoryboard.init(name: "LoginSearch",bundle: nil);
                                let loginC:
                                    LoginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController;
                                loginC.lunchHomePage();
                        }
                    }
                }
            })
            }
        }
        else
        {
            self.UserBlockUnblock();
        }
    }
  
    
    
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    func getProfile() {
        SwiftLoader.show(title: "Loading..", animated: true)
        let params = ["token":token, "for_uuid":profileID, Constant.kAPI_NAME: getUser]
        
        SGServiceController.instance().hitPostService(params, unReachable: {
            print("No internet")
        }) { (response) in
            if response == nil {
                SwiftLoader.hide()
                return
            }
            SwiftLoader.hide()
            
            let data = response?["data"] as? [String: Any]
            allDetail = data!
            if let arr = data?["specialities"] as? [[String: Any]] {
                self.arrSpecialities = arr
                print("specialities ",arr)
            }
            
            if let arr = data?["qualification_details"] as? [[String: Any]] {
                self.arrQualification = arr
                print("qualification_details", arr)
            }
            
            if let arr = data?["jobdetail"] as? [[String: Any]] {
                self.arrjobDetail = arr
                print("jobdetail ",arr)
            }
            self.setData(info: allDetail)
            self.tableView.reloadData()

        }
    }
    
    func setData(info: [String: Any])
    {
        is_valid_user = Int(info["is_valid_user"] as? String ?? "0")!
        isBlockUnBlock = info["is_blocked"] as? String ?? "0"
        userName.text = info["displayName"] as? String ?? ""
        userQulification.text = info["doxbay_qualification"] as? String ?? ""
        followerCount.text = String(info["count_follower"] as? Int ?? 0)
        followingCount.text = String(info["count_following"]  as? Int ?? 0)
        postCount.text = String(info["count_post"] as? Int ?? 0)
        
        
        self.isFollowUnfollow = info["is_followed"] as? String ?? "0"
        
        if(self.isFollowUnfollow == "0")
        {
            self.btnFollow.setTitle("Follow",for: .normal)
        }
        else
        {
            self.btnFollow.setTitle("UnFollow",for: .normal)
        }
        
        if(!isForEdit)
        {
            if(self.isBlockUnBlock == "1")
            {
                self.btnBlock.setTitle("UnBlock",for: .normal)
            }
            else
            {
                self.btnBlock.setTitle("Block",for: .normal)
            }
        }
        
        
        profileImg.layer.cornerRadius =  (profileImg.frame.size.width) / 2;
        profileImg.layer.masksToBounds = true
        
        let pImg = info["photoUrl"] as? String ?? "" ;
        self.photoUrl = pImg;
        profileBGImg.sd_setImage(with: URL(string: pImg), placeholderImage: UIImage(named: "vc_user"))
        
        profileImg.sd_setImage(with: URL(string: pImg), placeholderImage: UIImage(named: "vc_user"))
        
        
        
        profileData["email"] = info["email"] as? String ?? ""
        profileData["mobile"] = info["mobile"] as? String ?? ""
        profileData["doxbay_landline"] = info["doxbay_landline"] as? String ?? ""
        profileData["registration_number"] = info["registration_number"] as? String ?? ""
        
        
        profileData["doxbay_qualification"] = info["doxbay_qualification"] as? String ?? ""
        profileData["displayName"] = info["displayName"] as? String ?? ""
        profileData["doxbay_dob"] = info["doxbay_dob"] as? String ?? ""
        profileData["doxbay_speciality"] = info["doxbay_speciality"] as? String ?? ""
        profileData["doxbay_designation"] = info["doxbay_designation"] as? String ?? ""
        profileData["doxbay_address"] = info["doxbay_address"] as? String ?? ""
        profileData["doxbay_pincode"] = info["doxbay_pincode"] as? String ?? ""
        profileData["dox_id"] = info["dox_id"] as? String ?? ""
        
        
        
        if(isFromLogin && is_valid_user == 1)
        {
            OperationQueue.main.addOperation
                {
                    UserDefaults.standard.set(true, forKey: "isRemindMe")
                    let storyboard: UIStoryboard = UIStoryboard.init(name: "LoginSearch",bundle: nil);
                    let loginC:LoginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController;
                    loginC.lunchHomePage();
            }
        }
    }
    
    
    
    @IBAction func btnBack(_ sender: Any) {
        if(!isFromLogin)
        {
            if(isForEdit)
            {
                self.navigationController?.popViewController(animated: true)
            }
            else
            {
                self.dismiss(animated: true, completion: nil)
            }
        }
        isForEdit = false
        isFromLoginPage = false
        is_valid_user  = 0;
    }
    
    
    @objc func tapHeader(sender: UIButton) {
        var addView = UIView()
        if sender.tag == 1 {
            addView = AddQualificationView.fromNib(xibName: "AddQualificationView") as! AddQualificationView
        }
        if sender.tag == 2 {
            addView = AddSpecilityView.fromNib(xibName: "AddSpecilityView") as! AddSpecilityView
        }
        if sender.tag == 3 {
            addView = AddExperienceView.fromNib(xibName: "AddExperienceView") as! AddExperienceView
        }
        
        
        addView.frame = self.view.bounds
        self.view.addSubview(addView)
        addView.alpha = 0.0
        UIView.animate(withDuration: 0.5) {
            addView.alpha = 1.0
        }
        let btnsendtag: UIButton = sender
        print(btnsendtag.tag)
        
    }

    
//-------------------------------Post Profile------------------------------
//=======================***********************============================
    
    
    
    
    
    
    
//---------------------User Post section start from heare------------------
//=======================***********************============================
    var expID : String = "";
    var orgChannelName = "";
    var orgChannelImgLogo = "";
    
    
    
    // 1
    // var expandedLabel: UILabel!
    var indexOfCellToExpand: Int?
    
    //3
    var isShow : Bool = false;
    var currentExpendRow : Int = -1;
    
    var expandedRows = Set<Int>()
    var isForReloadIndex : Int  = -1
    
    
    //var TimeLineDataArry : [OTLModal] = []
    var timeLimeData : [OTLModal] = []
    var postTimeLine : [OTLModal] = []
    var eventTimeLime : [OTLModal] = []
    
    
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(OrganizationTimeLine_VC.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.red
        return refreshControl
    }()
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        getExploreListData();
        refreshControl.endRefreshing()
    }
    

    
    @objc func expandCell(sender: UITapGestureRecognizer)
    {
        let label = sender.view as! UILabel
        
        let rowData : OTLModal = timeLimeData[label.tag]
        let titleStr = rowData.title
        
        if((titleStr.count)<350)
        {
            return
        }
        
        let isVideoImgPdf = checkForImgVideoPdf(index : label.tag)
        if (isVideoImgPdf)
        {
            let cell : Profile_Cell_IMG = tableView.cellForRow(at: IndexPath(row: label.tag, section: 0)) as!Profile_Cell_IMG
            
            cell.postDescription.sizeToFit()
            indexOfCellToExpand = label.tag
            cell.postDescription.attributedText =  Utility.getHTMLToString(titleStr: titleStr)
        }
        else
        {
            let cell : ProfileCell_Normal = tableView.cellForRow(at: IndexPath(row: label.tag, section: 0)) as! ProfileCell_Normal
            
            cell.postDescription.sizeToFit()
            indexOfCellToExpand = label.tag
            cell.postDescription.attributedText =  Utility.getHTMLToString(titleStr: titleStr)
        }
        
        if(isShow && label.tag == currentExpendRow)
        {
            indexOfCellToExpand = -1
            currentExpendRow =  -1
            isShow = false
        }
        else
        {
            isShow = true
            currentExpendRow = label.tag;
        }
        
        tableView.reloadRows(at: [IndexPath(row: label.tag, section: 0)], with: .fade)
        tableView.scrollToRow(at: IndexPath(row: label.tag, section: 0), at: .top, animated: true)
    }
    
    
    
    @objc func viewUserProfile(sender: UITapGestureRecognizer!)
    {
        let touch = sender.location(in: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: touch)
        {
            
            //  let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewTimeLine") as! ProfileViewTimeLine
            // self.navigationController?.pushViewController(vc, animated: true)
            
            let indx = indexPath.row;
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ProfileViewTimeLine") as! ProfileViewTimeLine
            isForEdit = false;
            vc.profileID = self.timeLimeData[indx].userID;
            self.present(vc, animated:true, completion:nil)
            
            
        }
    }
    
    
    @objc func viewImage(_ sender: UITapGestureRecognizer)
    {
        let touch = sender.location(in: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: touch)
        {
           // let dic = self.timeLineArray1[indexPath.row] as! Dictionary<String,Any>
            let imageStr =  self.timeLimeData[indexPath.row].image; //dic["image"] as? String
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "ImagePagerViewControllerPager") as! ImagePagerViewControllerPager
            newViewController.imageURL = imageStr
            newViewController.requestType = "image"
            self.present(newViewController, animated: true, completion: nil)
        }
    }
    
    
    @objc func viewPDFFile(_ sender: UITapGestureRecognizer)
    {
        let touch = sender.location(in: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: touch)
        {
          //  let dic = self.timeLineArray1[indexPath.row] as! Dictionary<String,Any>
            let imageStr = self.timeLimeData[indexPath.row].pdf; // dic["pdf"] as? String
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "ImagePagerViewControllerPager") as! ImagePagerViewControllerPager
            newViewController.imageURL = imageStr
            newViewController.requestType = "pdf"
            self.present(newViewController, animated: true, completion: nil)
        }
    }
    
    
    
    
    @objc func plsyPostVideo(_ sender: UITapGestureRecognizer)
    {
        let touch = sender.location(in: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: touch)
        {
          //  let dic = self.timeLineArray1[indexPath.row] as! Dictionary<String,Any>
            let video_url = NSURL(string:self.timeLimeData[indexPath.row].video_url); //NSURL(string: String(describing: dic["video_url"]!))
            play(url: video_url!)
        }
    }
    func play(url:NSURL) {
        print("playing \(url)")
        let player = AVPlayer(url: (url as URL))
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    
    func getThumbnailImage(forUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(1, 60) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
        }
        return nil
    }
    
    
    
    // like API
    @objc func btnLikePost(sender: UITapGestureRecognizer!)
    {

        let label = sender.view as! UIImageView
        let tagIndex : Int = label.tag
        
        // let dic = self.timeLineArray1[tagIndex] as! Dictionary<String,Any>
        let id = timeLimeData[tagIndex].id
        var is_liked = timeLimeData[tagIndex].is_liked
        
        if is_liked == "0"
        {
            is_liked = "1"
            timeLimeData[tagIndex].is_liked = "1"
            
            let aa : Int = Int(timeLimeData[tagIndex].count_like)! + 1
            timeLimeData[tagIndex].count_like = String(aa)
            
            let indexPath = IndexPath(item: tagIndex, section: 0)
            if(checkForImgVideoPdf(index : tagIndex))
            {
                let cell = self.tableView.cellForRow(at: indexPath) as! Profile_Cell_IMG
                cell.imgIsPostLike.image =  UIImage(named:
                    "vc_like_true")
                cell.likeCount.text = timeLimeData[tagIndex].count_like + " Likes"
            }
            else
            {
                let cell = self.tableView.cellForRow(at: indexPath) as! ProfileCell_Normal
                cell.imgIsPostLike.image =  UIImage(named: "vc_like_true")
                cell.likeCount.text = timeLimeData[tagIndex].count_like  + " Likes"
            }
        }
        else
        {
            is_liked = "0"
            timeLimeData[tagIndex].is_liked = "0"
            let aa : Int = Int(timeLimeData[tagIndex].count_like)! - 1
            timeLimeData[tagIndex].count_like = String(aa)
            
            let indexPath = IndexPath(item: tagIndex, section: 0)
            if(checkForImgVideoPdf(index : tagIndex))
            {
                let cell = self.tableView.cellForRow(at: indexPath) as! Profile_Cell_IMG
                cell.imgIsPostLike.image =  UIImage(named: "vc_like_false")
                cell.likeCount.text = timeLimeData[tagIndex].count_like  + " Likes"
            }
            else
            {
                let cell = self.tableView.cellForRow(at: indexPath) as! ProfileCell_Normal
                cell.imgIsPostLike.image =  UIImage(named: "vc_like_false")
                cell.likeCount.text = timeLimeData[tagIndex].count_like  + " Likes"
            }
        }
        
        
        let contentType =  timeLimeData[tagIndex].category //String(describing: dic["content_type"]!)
        var requestURL = "";
        if(contentType == "user_post")
        {
            requestURL = BASEURL + createUserPostLike
        }
        else if(contentType == "user_event")
        {
            requestURL = BASEURL + createEventLike
        }
        else if(contentType == "channel_post" || contentType == "Post")
        {
            requestURL = BASEURL + postLike
        }
        
        let parameters: [String:String] = ["id":id,"channel_id":channel_id ,"token": token ,"is_liked":is_liked]
        
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--")
            
            if success
            {
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
        })
    }
    
    
    
    // View comment history
    @objc func btnPostViewComment(sender: UITapGestureRecognizer!)
    {
        let imgv  = sender.view as! UIImageView
        let indx : Int = imgv.tag
        self.isForReloadIndex = indx
        let vc = CommentsViewController(nibName: "CommentsViewController", bundle: nil)
        vc.id =  timeLimeData[indx].id
        vc.contentType = timeLimeData[indx].category
        self.present(vc, animated:true, completion:nil)
    }
    
    
    
    @objc func deleteTimeLinePost(sender: UITapGestureRecognizer!)
    {
        let touch = sender.location(in: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: touch)
        let indx : Int = (indexPath?.row)!
        
        let alert = UIAlertController(title: "MSBIRIA App", message: "Do you want to delete this post?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            
          
            
            var requestURL =  ""
            var parameters: [String:String] = ["":""]
            
            if(self.timeLimeData[indx].content_type == "user_event")
            {
                requestURL = BASEURL + deleteUserEvent
                parameters = ["event_id": self.timeLimeData[indx].id,"token": token ]
            }
            else
            {
                requestURL = BASEURL + deleteUserPost
                parameters = ["post_id": self.timeLimeData[indx].id,"token": token ]
            }
           
            print("Request URL- \(requestURL)  Param --- \(parameters)")
            
            UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
                let success = response["success"].boolValue
                print("xxxxxxxx   ---- \(success)--")
                
                if success
                {
                    if(self.timeLimeData[indx].content_type == "user_event")
                    {
                        self.eventTimeLime.remove(at: indx)
                    }
                    else
                    {
                        self.postTimeLine.remove(at: indx)
                    }
                    self.timeLimeData.remove(at: indx)
                    self.tableView.deleteRows(at: [indexPath!], with: .fade)
                   
                }
            }, failure:  {
                (error) -> Void in
                print("error===\(error)")
            })
            
        }))
        alert.addAction(UIAlertAction(title: "No, Thank", style: .default, handler: { action in
            print("Yay! You brought your towel!")
        }))
        self.present(alert, animated: true)
    }
    
    @objc func clickisFav(sender: UITapGestureRecognizer!)
    {
        let touch = sender.location(in: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: touch)
        let indx : Int = (indexPath?.row)!
        var isFavarity =  timeLimeData[indx].is_favorite
        
        if(isFavarity == "1")
        {
            isFavarity = "0";
            timeLimeData[indx].is_favorite = "0"
            
            let indexPath = IndexPath(item: indx, section: 0)
            if(checkForImgVideoPdf(index : indx))
            {
                let cell = self.tableView.cellForRow(at: indexPath) as! Profile_Cell_IMG
                cell.imgIsFavorites.image =  UIImage(named:
                    "vc_favorite_normal")
            }
            else
            {
                let cell = self.tableView.cellForRow(at: indexPath) as! ProfileCell_Normal
                cell.imgIsFavorites.image =  UIImage(named: "vc_favorite_normal")
            }
        }
        else
        {
            isFavarity = "1";
            timeLimeData[indx].is_favorite = "1"
            let indexPath = IndexPath(item: indx, section: 0)
            if(checkForImgVideoPdf(index : indx))
            {
                let cell = self.tableView.cellForRow(at: indexPath) as! Profile_Cell_IMG
                cell.imgIsFavorites.image =  UIImage(named:
                    "vc_favorite")
            }
            else
            {
                let cell = self.tableView.cellForRow(at: indexPath) as! ProfileCell_Normal
                cell.imgIsFavorites.image =  UIImage(named: "vc_favorite")
            }
        }
        
        
        let requestURL = BASEURL + postFavorite
        let parameters: [String:String] = ["id":timeLimeData[indx].id,"token": token ,"is_favorite": isFavarity]
        
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--")
            
            if success
            {
                
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
        })
    }
    
    
    
    
    
    
    
    
    
    
  //============== User Time line
    func getExploreListData()
    {
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Loading"
        
        var requestURL : String = ""
        let parameters  : [String:String] = ["token": token , "for_uuid": profileID]
        if (segmentIndex == 1)
        {
            requestURL =  BASEURL + getUserPost;
        }
        else
        {
            requestURL =  BASEURL + getUserEvent;
        }
        
        
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            MBProgressHUD.hide(for: self.view, animated: false)
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)-- \(response)")
            if success
            {
                //self.timeLineArray1 = response["data"].arrayObject!
                self.parseJSONData(json: response)
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
        })
    }
    
    
    
    
    func parseJSONData(json: JSON)
    {
//         if (segmentIndex == 1)
//         {
//            postTimeLine = timeLimeData;
//        }
//         else{
//            eventTimeLime = timeLimeData;
//        }
        
        
        timeLimeData.removeAll();
        for result in json["data"].arrayValue
        {

            let id = result["id"].stringValue
            var title = result["title"].stringValue
            let time = result["time"].stringValue
            let count_like = result["count_like"].stringValue
            let count_comment = result["count_comment"].stringValue
            let is_liked = result["is_liked"].stringValue
            let userID = result["user"]["id"].stringValue
            var userName = result["user"]["name"].stringValue
            let userImage = result["user"]["image"].stringValue
            let channel_name = result["user"]["channel_name"].stringValue
            let image = result["image"].stringValue
            
            
            
            var description = ""
            var category = ""
            var pdf = ""
            var pdf_name = ""
            var video = ""
            var video_thumb = ""
            var video_url = ""
            var video_url_oe = ""
            var content_type = ""
            var is_favorite = ""
            var channel_id = ""
            
            if (segmentIndex == 1)
            {
                 description = result["description"].stringValue
                 category = result["category"].stringValue
                 pdf = result["pdf"].stringValue
                 pdf_name = result["pdf_name"].stringValue
                 video = result["video"].stringValue
                 video_thumb = result["video_thumb"].stringValue
                 video_url = result["video_url"].stringValue
                 video_url_oe = result["video_url_oe"].stringValue
                 content_type = result["content_type"].stringValue
                 is_favorite = result["is_favorite"].stringValue
                 channel_id = result["user"]["channel_id"].stringValue
            }
            else
            {
                description = result["comment"].stringValue
                title = result["comment"].stringValue
                let evnttitle = " at " + result["title"].stringValue
                let event = " " + result["event"].stringValue
                let as_event  = " as " + result["as_event"].stringValue
                let  at_event =  ", " + result["at_event"].stringValue
                let  on_event = "  on " + result["on_event"].stringValue
                
                
                userName = userName + " " + event  + as_event + evnttitle + at_event + on_event
                
                category = "Event"
                content_type = "user_event"
                pdf = ""
                video = ""
                video_thumb = ""
                video_url = ""
                video_url_oe = ""
                is_favorite = ""
                channel_id = ""
            }
            
           
            
            
            let mData : OTLModal = OTLModal(id: id , title: title , description: description  , category: category , pdf: pdf , pdf_name: pdf_name , image: image , video: video , video_thumb: video_thumb , video_url: video_url , video_url_oe: video_url_oe , time: time , count_like: count_like , count_comment: count_comment , is_favorite: is_favorite , is_liked: is_liked , content_type: content_type , userID: userID , userName: userName , userImage: userImage , channel_name: channel_name , channel_id: channel_id)
            timeLimeData.append(mData)
            
            if (segmentIndex == 1)
            {
                postTimeLine.append(mData)
            }
            else{
                eventTimeLime.append(mData)
            }
            
        }
        DispatchQueue.main.async{
            self.tableView.reloadData();
        }
    }
    
    
    
    func checkForImgVideoPdf(index : Int) -> Bool
    {
        let rowData : OTLModal = timeLimeData[index]
        if (rowData.image.split(separator: ",").count > 0)
        {
            return true
        }
        else if (rowData.video_url.split(separator: ",").count > 0)
        {
            return true
        }
        else if (rowData.pdf.split(separator: ",").count > 0)
        {
            return true
        }
        else
        {
            return false
        }
    }

// -----------------------end user post ----------------------------------
//=======================***************====================================

 var imagePickerViewController = UIImagePickerController()
  //  func  chooseImg () {
    @objc func chooseImg(sender: UITapGestureRecognizer!){
        self.showAlertWithActions(msg: "", titles: ["Camera","Photo Library","Cancel"]) { (selectOption) in
            if selectOption == 1 {//Camera
                self.imagePickerViewController.sourceType = .camera
                self.present(self.imagePickerViewController, animated: true, completion: nil)
                
            }else if selectOption == 2 {//Photo Library
                self.imagePickerViewController.sourceType = .photoLibrary
                self.present(self.imagePickerViewController, animated: true, completion: nil)
            }
            self.imagePickerViewController.delegate = self
            print(selectOption)
        }
    }
    
    
  
    func uploadImage(image: UIImage) {
        SwiftLoader.show(title: "Uploading", animated: true)
        let param = [Constant.kAPI_NAME: userImageUpdate,
                     "token": token]
        
        SGServiceController.instance().hitMultipartForImage(param, image: image, imageParamName: "photoUrl", unReachable: {
            print("Not reachable")
            SwiftLoader.hide()
        }) { (response) in
           
            SwiftLoader.hide()
            if response != nil {
                print("Image uploaded")
               
                
                let json = JSON(response as Any)
                if let pImg = json["data"]["photoUrl"].string {
                    print("Image uploaded xxx \(pImg)")
                    self.photoUrl = pImg;
                    self.profileBGImg.sd_setImage(with: URL(string: pImg), placeholderImage: UIImage(named: "vc_user"))
                    
                    self.profileImg.sd_setImage(with: URL(string: pImg), placeholderImage: UIImage(named: "vc_user"))
                    
                } else {
                    print(json[999].error!) // "Array[999] is out of bounds"
                }
                
              
                
               
            }
        }
    }
    
    
    
   
    
    
}


extension ProfileViewTimeLine: UINavigationControllerDelegate,  UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage]as? UIImage {
            print(image)
            //   imgView.image = image
            self.uploadImage(image: image)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}









extension ProfileViewTimeLine : UITableViewDataSource {
   
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if(segmentIndex == 0)
        {
            if(isFromLogin)
            {
                return 5
            }
            else{
                return 4
            }
        }
        else
        {
            return  1
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(segmentIndex == 0)
        {
            if(section == 0) {
                return  1
            }
            else if(section == 1) { //qualification
                return arrQualification.count
            }
            else if( section == 2) { //Specialites
                return arrSpecialities.count
            }
            else if(section == 3) {//Experience
                return arrjobDetail.count
            }
            else if(section == 4) {
                return  1
            }
            return 1
        }
        else{
            return timeLimeData.count ;
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(segmentIndex == 0)
        {
            return  CGFloat(40)
        }
        else{
            return 0;
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
         if(segmentIndex == 0)
         {
            
             let  headerCell = CustomeHeader.fromNib(xibName: "CustomeHeader") as! CustomeHeader
            headerCell.headerAddButton.isHidden = !isForEdit
            
            if(section == 0)   {
                headerCell.headerName.text = "MSBIRIA Digital Directory"
                let image = UIImage(named: "vc_edit") as UIImage?
                headerCell.headerAddButton.setImage(image, for: .normal)
                headerCell.headerAddButton.isHidden = true
            }
            else  if(section == 1) {
                headerCell.headerName.text = "Qulification"
            }
            else  if(section == 2) {
                headerCell.headerName.text = "Specialites"
            }
            else  if(section == 3) {
                headerCell.headerName.text = "Experience"
            }
                
            else  if(section == 4) {
                headerCell.headerName.text = "Personal info"
                headerCell.tag = section
                headerCell.headerAddButton.isHidden = true
                return headerCell
            }
            
            
            
            
            if(!isForEdit)
            {
                if(section == 1 && self.arrQualification.count<=0)
                {
                    headerCell.headerName.isHidden = true
                }
                if(section == 2 && self.arrSpecialities.count<=0)
                {
                    headerCell.headerName.isHidden = true
                }
                if(section == 3 && self.arrjobDetail.count<=0)
                {
                    headerCell.headerName.isHidden = true
                }
            }
            
            
            headerCell.tag = section
            headerCell.headerAddButton.tag = section
            headerCell.headerAddButton.addTarget(self, action: #selector(tapHeader), for: .touchUpInside)
            return headerCell
        }
        else
         {
            let  headerCell = CustomeHeader.fromNib(xibName: "CustomeHeader") as! CustomeHeader
            headerCell.headerAddButton.isHidden = !isForEdit
            return headerCell
        }
    }
    
   
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(segmentIndex == 0)
        {
            var height = 0
            if(indexPath.section == 0) {
                if isFromLoginPage
                {
                   height = 560
                }
                else if !isForEdit {
                    height = 560
                }
                else
                {
                    height = 700
                }
            }
            else if(indexPath.section == 1){
                height = 100
            }
            else if (indexPath.section == 2) {
                height = 60
            }
            else if(indexPath.section == 3) {
                height = 75
            }
            else if(indexPath.section == 4) {
                height = 350
            }
            return CGFloat(height)
        }
        else
        {
            let rowData : OTLModal = timeLimeData[indexPath.row]
            let isVideoImgPdf = checkForImgVideoPdf(index : indexPath.row)
            
            if (isVideoImgPdf)
            {
                if indexPath.row == indexOfCellToExpand
                {
                    let xxx =  Utility.heightForView(text: rowData.title) + 190 ;
                    return   xxx
                }
                else
                {
                    return  440;
                }
            }
            else
            {
                if indexPath.row == indexOfCellToExpand
                {
                    let xxx =  Utility.heightForView(text: rowData.title);
                    return   xxx
                }
                else
                {
                    return 230
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(segmentIndex == 0)
        {
            if indexPath.section == 0 {
                if(isFromLogin)
                {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CellBasicInfo", for: indexPath) as! CellBasicInfo
                cell.configureView(info: profileData)
                return cell
                }
                else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "CellBasicViewUpdate", for: indexPath) as! CellBasicViewUpdate
                    cell.configureView(info: profileData)
                    return cell
                    
                }
            }
            if indexPath.section == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CellQualification", for: indexPath) as! CellQualification
                cell.configureView(info: arrQualification[indexPath.row])
                return cell
            }
            if indexPath.section == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CellSpecialities", for: indexPath) as! CellSpecialities
                cell.configureView(info: arrSpecialities[indexPath.row])
                return cell
            }
            
            if indexPath.section == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CellExperience", for: indexPath) as! CellExperience
                cell.configureView(info: arrjobDetail[indexPath.row])
                return cell
            }
            
            //section 4
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellPersonalInfo", for: indexPath) as! CellPersonalInfo
             cell.configureView(info: profileData)
            return cell
        }
        else{
            let rowData : OTLModal = timeLimeData[indexPath.row]
            let isVideoImgPdf = checkForImgVideoPdf(index : indexPath.row)
            
            if (isVideoImgPdf)
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Profile_Cell_IMG") as! Profile_Cell_IMG
                var titleStr = rowData.title
                titleStr = titleStr.trimmingCharacters(in: .whitespacesAndNewlines)
                if indexPath.row == indexOfCellToExpand
                {
                    cell.postDescription.attributedText = Utility.getHTMLToString(titleStr: titleStr);
                }
                else
                {
                    if((titleStr.count)>350)
                    {
                        titleStr = (titleStr.substring(with: 0..<350)) + "...<strong><font color=red>Read More</font></strong>"
                    }
                    cell.postDescription.attributedText =  Utility.getHTMLToString(titleStr: titleStr);
                }
                cell.postDescription.tag = indexPath.row
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.expandCell(sender:)))
                cell.postDescription.addGestureRecognizer(tap)
                cell.postDescription.isUserInteractionEnabled = true
                
                
                
                let miliSecondsTime:Double? = Double(rowData.time)
                let date = Date(timeIntervalSince1970: (miliSecondsTime! / 1000.0))
                
                cell.eventHeader.lineBreakMode = .byTruncatingMiddle
                cell.eventHeader.lineBreakMode = .byWordWrapping
                cell.eventHeader.numberOfLines = 0;
                 cell.eventHeader.attributedText =  Utility.getHTMLToString(titleStr: "<strong><font color=black>" + rowData.userName + "</font></strong> </br><strong><font color=red>" + rowData.category + " </font></strong>" + date.getElapsedInterval() + " ago")
                
                let greet4Height = cell.eventHeader.optimalHeight
                cell.eventHeader.frame = CGRect(x: cell.eventHeader.frame.origin.x, y: cell.eventHeader.frame.origin.y, width: cell.eventHeader.frame.width, height: greet4Height)
                
                
                
                cell.likeCount.text = rowData.count_like + " Likes"
                cell.commentCount.text = rowData.count_comment + " Comments"
                if(rowData.is_liked == "0")
                {
                    cell.imgIsPostLike.image =  UIImage(named: "vc_like_false")
                }
                else
                {
                    cell.imgIsPostLike.image =  UIImage(named: "vc_like_true")
                }
                if(!isForEdit)
                {
                    if(rowData.is_favorite == "0")
                    {
                        cell.imgIsFavorites.image =  UIImage(named: "vc_favorite_normal")
                    }
                    else
                    {
                        cell.imgIsFavorites.image =  UIImage(named: "vc_favorite")
                    }
                    let tapfavv = UITapGestureRecognizer(target: self, action: #selector(self.clickisFav(sender:)))
                    cell.imgIsFavorites.addGestureRecognizer(tapfavv)
                    cell.imgIsFavorites.isUserInteractionEnabled = true
                    cell.imgIsFavorites.tag = indexPath.row
                }
                else
                {
                    cell.imgIsFavorites.image =  UIImage(named: "ic_delete")
                    let tapfavv = UITapGestureRecognizer(target: self, action: #selector(self.deleteTimeLinePost(sender:)))
                    cell.imgIsFavorites.addGestureRecognizer(tapfavv)
                    cell.imgIsFavorites.isUserInteractionEnabled = true
                    cell.imgIsFavorites.tag = indexPath.row
                }
                
                
                
                cell.userProfilePic.layer.cornerRadius =  (cell.userProfilePic.frame.size.width) / 2;
                cell.userProfilePic.layer.masksToBounds = true
                cell.userProfilePic.sd_setImage(with: URL(string: String(rowData.userImage)), placeholderImage: UIImage(named: "vc_user"))
                let postimg = rowData.image.split(separator: ",")
                if (postimg.count > 0)
                {
                    cell.vc_play.isHidden = true
                    cell.postImage.sd_setImage(with: URL(string: String(postimg[0])), placeholderImage: UIImage(named: "vc_ops_gallery"))
                    let tapImg = UITapGestureRecognizer(target: self, action: #selector(viewImage))
                    cell.postImage.addGestureRecognizer(tapImg)
                    cell.postImage.isUserInteractionEnabled = true
                    cell.postImage.tag = indexPath.row
                }
                
                if (rowData.video_thumb.count > 0)
                {
                    cell.vc_play.isHidden = false
                    let video_thumb = rowData.video_thumb.split(separator: ",")
                    if (video_thumb.count > 0)
                    {
                        cell.postImage.sd_setImage(with: URL(string: String(video_thumb[0])), placeholderImage: UIImage(named: "vc_play_circle_trans"))
                        let tapImg = UITapGestureRecognizer(target: self, action: #selector(plsyPostVideo))
                        cell.postImage.addGestureRecognizer(tapImg)
                        cell.postImage.isUserInteractionEnabled = true
                        cell.postImage.tag = indexPath.row
                    }
                    else
                    {
                        cell.postImage.image = UIImage(named: "vc_play_circle_trans");
                        let tapImg = UITapGestureRecognizer(target: self, action: #selector(plsyPostVideo))
                        cell.postImage.addGestureRecognizer(tapImg)
                        cell.postImage.isUserInteractionEnabled = true
                        cell.postImage.tag = indexPath.row
                    }
                }
                
                let postPdf = rowData.pdf.split(separator: ",")
                if (postPdf.count > 0)
                {
                    cell.vc_play.isHidden = true
                    cell.postImage.image = UIImage(named: "ic_type_pdf");
                    let tapImg = UITapGestureRecognizer(target: self, action: #selector(viewPDFFile(_:)))
                    cell.postImage.addGestureRecognizer(tapImg)
                    cell.postImage.isUserInteractionEnabled = true
                    cell.postImage.tag = indexPath.row
                }

                
                
                let tapLike = UITapGestureRecognizer(target: self, action: #selector(self.btnLikePost(sender:)))
                cell.imgIsPostLike.addGestureRecognizer(tapLike)
                cell.imgIsPostLike.isUserInteractionEnabled = true
                cell.imgIsPostLike.tag = indexPath.row
                
                
                
                
                let tapComment = UITapGestureRecognizer(target: self, action: #selector(self.btnPostViewComment(sender:)))
                cell.imgPostComment.addGestureRecognizer(tapComment)
                cell.imgPostComment.isUserInteractionEnabled = true
                cell.imgPostComment.tag = indexPath.row
                
                
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell_Normal") as! ProfileCell_Normal
                var titleStr = rowData.title
                titleStr = titleStr.trimmingCharacters(in: .whitespacesAndNewlines)
                if indexPath.row == indexOfCellToExpand
                {
                    cell.postDescription.attributedText = Utility.getHTMLToString(titleStr: titleStr);
                }
                else
                {
                    if((titleStr.count)>350)
                    {
                        titleStr = (titleStr.substring(with: 0..<350)) + "...<strong><font color=red>Read More</font></strong>"
                    }
                    cell.postDescription.attributedText =  Utility.getHTMLToString(titleStr: titleStr);
                }
                cell.postDescription.tag = indexPath.row
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.expandCell(sender:)))
                cell.postDescription.addGestureRecognizer(tap)
                cell.postDescription.isUserInteractionEnabled = true
                
                
                
                
                let miliSecondsTime:Double? = Double(rowData.time)
                let date = Date(timeIntervalSince1970: (miliSecondsTime! / 1000.0))
                
                cell.eventHeader.lineBreakMode = .byTruncatingMiddle
                cell.eventHeader.lineBreakMode = .byWordWrapping
                cell.eventHeader.numberOfLines = 0;
                cell.eventHeader.attributedText =  Utility.getHTMLToString(titleStr: "<strong><font color=black>" + rowData.userName + "</font></strong> </br><strong><font color=red>" + rowData.category + " </font></strong>" + date.getElapsedInterval() + " ago")
                
                let greet4Height = cell.eventHeader.optimalHeight
                cell.eventHeader.frame = CGRect(x: cell.eventHeader.frame.origin.x, y: cell.eventHeader.frame.origin.y, width: cell.eventHeader.frame.width, height: greet4Height)
                
                
                cell.eventHeader.lineBreakMode = .byTruncatingMiddle
                cell.eventHeader.lineBreakMode = .byWordWrapping
                cell.eventHeader.numberOfLines = 0;
                cell.eventHeader.attributedText =  Utility.getHTMLToString(titleStr: "<strong><font color=black>" + rowData.userName + "</font></strong> </br><strong><font color=red>" + rowData.category + " </font></strong>" + date.getElapsedInterval() + " ago")
                
                let greet4Heightx = cell.eventHeader.optimalHeight
                cell.eventHeader.frame = CGRect(x: cell.eventHeader.frame.origin.x, y: cell.eventHeader.frame.origin.y, width: cell.eventHeader.frame.width, height: greet4Heightx)
                

                
                
                
                
                cell.likeCount.text = rowData.count_like + " Likes"
                cell.commentCount.text = rowData.count_comment + " Comments"
                if(rowData.is_liked == "0")
                {
                    cell.imgIsPostLike.image =  UIImage(named: "vc_like_false")
                }
                else
                {
                    cell.imgIsPostLike.image =  UIImage(named: "vc_like_true")
                }
               

                if(!isForEdit)
                {
                    if(rowData.is_favorite == "0")
                    {
                        cell.imgIsFavorites.image =  UIImage(named: "vc_favorite_normal")
                    }
                    else
                    {
                        cell.imgIsFavorites.image =  UIImage(named: "vc_favorite")
                    }
                    let tapfavv = UITapGestureRecognizer(target: self, action: #selector(self.clickisFav(sender:)))
                    cell.imgIsFavorites.addGestureRecognizer(tapfavv)
                    cell.imgIsFavorites.isUserInteractionEnabled = true
                    cell.imgIsFavorites.tag = indexPath.row
                }
                else
                {
                    cell.imgIsFavorites.image =  UIImage(named: "ic_delete")
                    let tapfavv = UITapGestureRecognizer(target: self, action: #selector(self.deleteTimeLinePost(sender:)))
                    cell.imgIsFavorites.addGestureRecognizer(tapfavv)
                    cell.imgIsFavorites.isUserInteractionEnabled = true
                    cell.imgIsFavorites.tag = indexPath.row
                }
                
                
                
                
                cell.userProfilePic.layer.cornerRadius =  (cell.userProfilePic.frame.size.width) / 2;
                cell.userProfilePic.layer.masksToBounds = true
                
                cell.userProfilePic.sd_setImage(with: URL(string: String(rowData.userImage)), placeholderImage: UIImage(named: "vc_user"))
                
      
                
                
                
                let tapLike = UITapGestureRecognizer(target: self, action: #selector(self.btnLikePost(sender:)))
                cell.imgIsPostLike.addGestureRecognizer(tapLike)
                cell.imgIsPostLike.isUserInteractionEnabled = true
                cell.imgIsPostLike.tag = indexPath.row
                
                let tapComment = UITapGestureRecognizer(target: self, action: #selector(self.btnPostViewComment(sender:)))
                cell.imgPostComment.addGestureRecognizer(tapComment)
                cell.imgPostComment.isUserInteractionEnabled = true
                cell.imgPostComment.tag = indexPath.row
                
                
                return cell
            }
        }
    }
    
    
    
    
    

    
}






extension ProfileViewTimeLine: UITableViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < 220 {
            let diff = 220-scrollView.contentOffset.y
            let percentage = (diff/240)
            UIView.animate(withDuration: 1.0, animations: {
                self.viewHeader.alpha = 1.0-percentage
            })
        }
        if scrollView.contentOffset.y > 220  {
            UIView.animate(withDuration: 1.0, animations: {
                self.viewHeader.alpha = 1.0
            })
        }
       
    }
}





extension ProfileViewTimeLine: LiquidFloatingActionButtonDelegate {
    func liquidFloatingActionButton(_ liquidFloatingActionButton: LiquidFloatingActionButton, didSelectItemAtIndex index: Int) {
        liquidFloatingActionButton.close()
         if index == 0
        {
           let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
           // let vc = storyBoard.instantiateViewController(withIdentifier: "EventListViewController") as! EventListViewController
            let vc = storyBoard.instantiateViewController(withIdentifier: "EventListViewController") as! EventListViewController
            vc.isFromPage = "profile"
            self.present(vc, animated: false, completion: nil)
           // self.navigationController?.pushViewController(vc, animated: true)
            //   self.navigationController?.pushViewController(vc, animated: true)
        }
        else if index == 1 {
             let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
           // let vc = storyBoard.instantiateViewController(withIdentifier: "PostAThoughtVC") as! PostAThoughtVC
             let vc = storyBoard.instantiateViewController(withIdentifier: "PostAThoughtVC") as! PostAThoughtVC
            vc.isFromPage = "Profile"
            vc.isFromPageType = "UserPost"
           self.present(vc, animated: true, completion: nil)
          //   self.navigationController?.pushViewController(vc, animated: true)
           // self.navigationController?.pushViewController(vc, animated: true)

        }
    }
    
    
}




extension ProfileViewTimeLine: LiquidFloatingActionButtonDataSource {
    func numberOfCells(_ liquidFloatingActionButton: LiquidFloatingActionButton) -> Int {
        return cellsX.count
    }

    func cellForIndex(_ index: Int) -> LiquidFloatingCell {
        return cellsX[index]
    }
}

class Alerts {
    static func showActionsheet(viewController: UIViewController, title: String, message: String, actions: [(String, UIAlertActionStyle)], completion: @escaping (_ index: Int) -> Void) {
        let alertViewController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        for (index, (title, style)) in actions.enumerated() {
            let alertAction = UIAlertAction(title: title, style: style) { (_) in
                completion(index)
            }
            alertViewController.addAction(alertAction)
        }
        viewController.present(alertViewController, animated: true, completion: nil)
    }
}


