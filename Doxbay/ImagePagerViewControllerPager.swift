//
//  ViewController.swift
//  SwiftSlideShow
//
//  Created by Malek Trabelsi on 11/29/17.
//  Copyright © 2017 Malek Trabelsi. All rights reserved.
//

import UIKit
import Alamofire

class ImagePagerViewControllerPager : UIViewController, UIScrollViewDelegate , UIWebViewDelegate {

    
    var imageURL : String = "";
    var requestType : String = "";
    var imageArray : [String] = [];
    var imgStrTemp  : String = "";
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
 
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var webVew: UIWebView!
    
    
    @IBAction func btnCoseView(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    @objc func btnCoseViewPage(_ sender:UIButton!)
    {
        print("Button tapped")
        self.dismiss(animated: true, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        navigationItem.hidesBackButton = true;
        
        
        let button:UIButton = UIButton(frame: CGRect(x: 7, y: 7, width: 70, height: 70))
        button.backgroundColor = .black
       // button.setTitle("Button", for: .normal)  //
        button.setImage(UIImage(named: "close_page"), for: .normal)
        button.addTarget(self, action:#selector(self.btnCoseViewPage), for: .touchUpInside)
        self.view.addSubview(button)
        
        
        let imgArr = self.imageURL.split(separator: ",")
        imgStrTemp = imgArr[0].trimmingCharacters(in: .whitespacesAndNewlines)
        self.pageControl.numberOfPages = imgArr.count ;
       
        if(requestType == "image")
        {
             self.webVew.isHidden = true;
             self.imgView.isHidden = true;
             self.textView.isHidden = true
        }
        else
        {
            self.webVew.isHidden = false;
            self.imgView.isHidden = true;
            self.textView.isHidden = true

            let website = imgStrTemp;
            let url = NSURL(string:  website)
            self.webVew.isUserInteractionEnabled = true
            self.webVew.loadRequest(NSURLRequest(url: url! as URL) as URLRequest)
        }
        
        
        
        
        
        self.scrollView.frame = CGRect(x:0, y:100, width:self.view.frame.width, height:self.view.frame.height - 100)
        let scrollViewWidth:CGFloat = self.scrollView.frame.width
        let scrollViewHeight:CGFloat = 300 //self.scrollView.frame.height
      
        
        
        
        if(requestType == "image")
        {
            for var i in 0..<imgArr.count
            {
                 let imgFour = UIImageView(frame: CGRect(x:0, y:100,width:scrollViewWidth, height:scrollViewHeight))
                let imgTwo = UIImageView(frame: CGRect(x: scrollViewWidth*CGFloat(i) , y:100,width:scrollViewWidth, height:scrollViewHeight ))
                
                let imgStrTempxp = imgArr[Int(i)].trimmingCharacters(in: .whitespacesAndNewlines)
                if(i == 0)
                {
                    imgFour.sd_setImage(with: URL(string: String(imgStrTempxp)), placeholderImage: UIImage(named: "Exbg"))
                    self.scrollView.addSubview(imgFour)
                }
                else
                {
                    imgTwo.sd_setImage(with: URL(string: String(imgStrTempxp)), placeholderImage: UIImage(named: "Exbg"))
                    self.scrollView.addSubview(imgTwo)
                }
            }
        }
        
        self.scrollView.contentSize = CGSize(width:self.scrollView.frame.width * CGFloat(imgArr.count), height:self.scrollView.frame.height)
        
        self.scrollView.delegate = self
        self.pageControl.currentPage = 0
        
        
        //2
        textView.textAlignment = .center
        textView.text = "ist Time photo loadd"
        textView.textColor = UIColor.black
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}



private typealias ScrollView = ImagePagerViewControllerPager
extension ScrollView
{
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        // Test the offset and calculate the current page after scrolling ends
        let pageWidth:CGFloat = scrollView.frame.width
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        // Change the indicator
        self.pageControl.currentPage = Int(currentPage);
        
        
        
        let imgArr = self.imageURL.split(separator: ",")
        if Int(currentPage) == 0
        {
            self.textView.text = "Sweettutos.com is your blog of choice for Mobile tutorials"
        }
        if(self.requestType == "image")
        {
            let imgArrx = self.imageURL.split(separator: ",")
            var imgStrTempx = imgArrx[Int(currentPage)].trimmingCharacters(in: .whitespacesAndNewlines)
        }
    
    }
}
