    //
    //  RegisterViewController.swift
    //  Doxbay
    //
    //  Created by Admin on 22/04/18.
    //  Copyright © 2018 Yuji Hato. All rights reserved.
    //
    
    var isRememberLogin : Bool = false
    
    import UIKit
    import Firebase
    import FirebaseAuth
    import FirebaseDatabase
    import GoogleSignIn
    import Alamofire
    import SwiftyJSON
    import MBProgressHUD
    
    
    class RegisterViewController_OLD: UIViewController, GIDSignInUIDelegate {
        
        
        var ref: DatabaseReference!
       
        @IBOutlet weak var firstName: UITextField!
        @IBOutlet weak var textFieldLastName: UITextField!
        @IBOutlet weak var textFieldEmailID: UITextField!
        @IBOutlet weak var textFieldPassword: UITextField!
        @IBOutlet weak var textFieldConfirmPassword: UITextField!
        
        
        
    
        @IBAction func btnRegisterNow(_ sender: Any) {
            registerWithEmailPass();
        }
        
        
      
        
        var isAcceptTermAndCondition : Bool = false;
        @IBAction func btnAcceptTermCondition(_ sender: Any) {
            if(isAcceptTermAndCondition)
            {
                isAcceptTermAndCondition = false
            }
            else
            {
                isAcceptTermAndCondition = true;
            }
        }
        
        
    
        @IBAction func btnLoginNow(_ sender: Any) {
            self.dismiss(animated: false, completion: nil)
        }
        
        
        @IBAction func btnGoogleLogin(_ sender: Any) {
            GIDSignIn.sharedInstance().signIn();
        }
        
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            // Google Sign In Instance
            GIDSignIn.sharedInstance().uiDelegate = self
            ref = Database.database().reference().child("/discussions/people/")
            
            self.hideKeyboardOnTap(#selector(self.dismissKeyboard))
        }
        
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
        }
        

        
        
        
        
        
        func registerWithEmailPass()
        {
            let userEmailID = textFieldEmailID.text!;
            let userPassword = textFieldPassword.text!;
            let displayName = ("\(firstName.text!)  \(textFieldLastName.text!)");
            let checkemail = Utility();
            
            if(firstName.text!.count < 1)
            {
                ShowToast.showPositiveMessage(message: "Please enter first name");
            }
            else if(textFieldLastName.text!.count < 1)
            {
                ShowToast.showPositiveMessage(message: "Please enter last name");
            }
            else if(userEmailID.count < 8)
            {
                ShowToast.showPositiveMessage(message: "Not a valid email id");
            }
            else if(!checkemail.validateEmail(enteredEmail: userEmailID))
            {
                ShowToast.showPositiveMessage(message: "Not a valid email id");
            }
            else if(userPassword.count < 5)
            {
                ShowToast.showPositiveMessage(message: "Password grater then 5 chrector");
            }
            else if(textFieldConfirmPassword.text!.count < 5)
            {
                ShowToast.showPositiveMessage(message: "Confirm password grater then 5 chrector");
            }
            else if(userPassword != textFieldConfirmPassword.text!)
            {
                ShowToast.showPositiveMessage(message: "Password and Confirm shoud be same");
            }
            else if(!isAcceptTermAndCondition)
            {
                ShowToast.showPositiveMessage(message: "Please accept DoxBay term and condition");
            }
            else
            {
                let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
                progressHUD.mode = MBProgressHUDMode.indeterminate
                progressHUD.label.text = "Please wait.."
                Auth.auth().createUser(withEmail: userEmailID , password: userPassword) { (user, error) in
                    if error == nil
                    {
                        MBProgressHUD.hide(for: self.view, animated: false)
                        let uid = user?.user.uid
                        let email = user?.user.email

                        self.postDataInRealTimeFireBaseDB(type: "F", emailID: email! , password: userPassword ,  uiID: uid!, name: displayName , photoUrl: "");
                    }
                    else
                    {
                        MBProgressHUD.hide(for: self.view, animated: false)
                        ShowToast.showPositiveMessage(message: "Something went wrong. Please try again.");
                    }
                }
            }
        }
        
        
        
        
        
        func postDataInRealTimeFireBaseDB(type: String, emailID: String , password: String, uiID: String, name: String , photoUrl : String)
        {
            let  deviceID = UIDevice.current.identifierForVendor!.uuidString
            
            //creating artist with the given values
            let userProfileData = ["city":"",
                                   "deviceId": deviceID,
                                   "deviceTypw": "IOS",
                                   "displayName": name,
                                   "dox_id":uiID,
                                   "eduction_super_speciality":"",
                                   "email": emailID,
                                   "mobile": "",
                                   "photoUrl": photoUrl,
                                   "registration_number": ""
                
            ]
           
            //adding the artist inside the generated unique key
            ref.child(uiID).setValue(userProfileData)
            
           
            
            self.PostUserDeatils(emailid: emailID , uiID: uiID , deviceId: deviceID , name: name , photoUrl: photoUrl);
        }
        
        
        
        func PostUserDeatils(emailid: String , uiID : String , deviceId:String , name : String, photoUrl : String)
        {
            let finalURL =  BASEURL + postUser
            let url = URL(string: finalURL)!
            var request = URLRequest(url: url)
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            let postString = "email=\(emailid)&dox_id=\(uiID)&displayName=\(name)&photoUrl=\(photoUrl)";
            request.httpBody = postString.data(using: .utf8)
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data, error == nil else {
                    print("error=\(error ?? "Error" as! Error)")
                    return
                }
                
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                }
                
                
                let swiftyJsonVar = JSON(data)
                let status = swiftyJsonVar["success"].intValue
                let userID = swiftyJsonVar["data"]["id"].stringValue
                print("==========userID===== \(userID)")
                if(status == 1)
                {
                        self.generateTockenIDForUser(emailid: emailid , uiID: uiID , deviceId: deviceId);
                }
                else
                {
                    ShowToast.showPositiveMessage(message: "Something went wrong. Please try again.");
                }
            }
            task.resume()
        }
        
        
        
        func generateTockenIDForUser(emailid: String , uiID : String , deviceId:String )
        {
            let finalURL =  BASEURL + getToken
            let url = URL(string: finalURL)!
            var request = URLRequest(url: url)
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            let postString = "email=\(emailid)"
            request.httpBody = postString.data(using: .utf8)
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data, error == nil else {
                    print("error=\(error ?? "Error" as! Error)")
                    return
                }
                
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                }
                
                
                let swiftyJsonVar = JSON(data)
                let status = swiftyJsonVar["success"].intValue
                let tokenID = swiftyJsonVar["data"]["token"].stringValue
                print("==========tokenID===== \(tokenID)")
               
                
                if(tokenID != "")
                {
                    UserDefaults.standard.set(tokenID, forKey: "tokenID")
                    UserDefaults.standard.set(uiID, forKey: "uid")
                    UserDefaults.standard.set(emailid, forKey: "email")
                    UserDefaults.standard.set(deviceId, forKey: "deviceId")
                    UserDefaults.standard.set(true, forKey: "islogin")
                    token = UserDefaults.standard.string(forKey: "tokenID")!;
                    dox_id = UserDefaults.standard.string(forKey: "uid")!;
                    for_uuid = UserDefaults.standard.string(forKey: "uid")!;
                    email = UserDefaults.standard.string(forKey: "email")!;
                    
                
                    
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "ProfileViewTimeLine") as! ProfileViewTimeLine
                    vc.isFromLogin = true
                    vc.profileID = dox_id
                    isForEdit = true
                    self.present(vc, animated: true, completion: nil)
                    
                    /*
                    OperationQueue.main.addOperation
                        {
                        let storyboard: UIStoryboard =
                            UIStoryboard.init(name: "Main",bundle: nil);
                        let loginC:
                            LoginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController;
                        loginC.lunchHomePage();
                    }
                */
                }
                else
                {
                    ShowToast.showPositiveMessage(message: "Sorry!. Your email is not registed with DoxBay App. Please register first then login");
                }
            }
            task.resume()
        }
        
      
        @objc func dismissKeyboard() {
            view.endEditing(true)
            // do aditional stuff
        }
    }
    
    
    
    
 
    
