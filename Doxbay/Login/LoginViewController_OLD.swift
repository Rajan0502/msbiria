//
//  LoginViewController.swift
//  Doxbay
//
//  Created by Admin on 22/04/18.
//  Copyright © 2018 Yuji Hato. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import GoogleSignIn
import SwiftyJSON
import Alamofire
import MBProgressHUD

class LoginViewController_OLD : UIViewController, GIDSignInUIDelegate, UITextFieldDelegate {
    @IBOutlet var btnLogin: UIButton!
    
    
    
    
    var ref: DatabaseReference!
    @IBOutlet weak var textFieldEmailID: UITextField!
    @IBOutlet weak var TextFieldPassword: UITextField!
    
    
    
    @IBAction func btnRegisterNow(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        self.present(newViewController, animated: true, completion: nil)
    }
    
    
    
    
    @IBAction func btnLoginNow(_ sender: Any) {
        loginWithEmailPass();
    }
    
    
    @IBAction func btnLoginGoogle(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn();
    }
    
    
    var isRemindMe = false;
    @IBAction func btnCheckRemindMe(_ sender: Any) {
        if(isRemindMe)
        {
            isRemindMe = false
        }
        else
        {
            isRemindMe = true;
        }
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Google Sign In Instance
        GIDSignIn.sharedInstance().uiDelegate = self
        
        let islogin = UserDefaults.standard.bool(forKey: "islogin")
        let isRemindMe = UserDefaults.standard.bool(forKey: "isRemindMe")
        if(islogin && isRemindMe)
        {
            OperationQueue.main.addOperation {
                self.lunchHomePage();
            }
        }
        
        self.hideKeyboardOnTap(#selector(self.dismissKeyboard))
        //subCribeChhanel();
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    func lunchHomePage()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
        let rightViewController = storyboard.instantiateViewController(withIdentifier: "RightViewController") as! RightViewController
        
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        
        UINavigationBar.appearance().tintColor = UIColor(hex: "689F38")
        
        leftViewController.mainViewController = nvc
        
        let slideMenuController = ExSlideMenuController(mainViewController:nvc, leftMenuViewController: leftViewController, rightMenuViewController: rightViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = mainViewController as! SlideMenuControllerDelegate
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window!.rootViewController = slideMenuController
        appDelegate.window!.makeKeyAndVisible()
    }
    
    func initializeRoot()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
        let rightViewController = storyboard.instantiateViewController(withIdentifier: "RightViewController") as! RightViewController
        
        var tabItems = [Any]() /* capacity: 4 */
        let Home = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        var nav = UINavigationController(rootViewController: Home)
        nav.tabBarItem.title = "Home"
        nav.tabBarItem.image = UIImage(named: "home")
        tabItems.append(nav)
        
        let dvc = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        nav = UINavigationController(rootViewController: dvc)
        nav.tabBarItem.title = "Friend Request"
        nav.tabBarItem.image = UIImage(named: "tab1")
        tabItems.append(nav)
        let cvc = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        nav = UINavigationController(rootViewController: cvc)
        nav.tabBarItem.title = "Chat"
        nav.tabBarItem.image = UIImage(named: "tab2")
        tabItems.append(nav)
        let rvc = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        nav = UINavigationController(rootViewController: rvc)
        nav.tabBarItem.title = "Notification"
        nav.tabBarItem.image = UIImage(named: "tab3")
        tabItems.append(nav)
        let dvc2 = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        // dvc2.isFromTab = truea
        nav = UINavigationController(rootViewController: dvc2)
        nav.tabBarItem.title = "Search"
        nav.tabBarItem.image = UIImage(named: "tab4")
        tabItems.append(nav)
        
        let tbc = UITabBarController()
        tbc.viewControllers = tabItems as? [UIViewController] ?? [UIViewController]()
        tbc.selectedIndex = 0
        
        leftViewController.mainViewController = tbc
        
        let slideMenuController = ExSlideMenuController(mainViewController:tbc, leftMenuViewController: leftViewController, rightMenuViewController: rightViewController)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window!.rootViewController = slideMenuController
        appDelegate.window!.makeKeyAndVisible()
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    func loginWithEmailPass()
    {
        let xx = CheckBox();
        let userEmailID = textFieldEmailID.text!;
        let userPassword = TextFieldPassword.text!;
        let checkemail = Utility();
        if(userEmailID.count < 8)
        {
            ShowToast.showPositiveMessage(message: "Not a valid email id");
        }
        else if(!checkemail.validateEmail(enteredEmail: userEmailID))
        {
            ShowToast.showPositiveMessage(message: "Not a valid email id");
        }
        else if(userPassword.count < 5)
        {
            ShowToast.showPositiveMessage(message: "Password grater then 5 chrector");
        }
        else
        {
            let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
            progressHUD.mode = MBProgressHUDMode.indeterminate
            progressHUD.label.text = "Please wait.."
            
            Auth.auth().signIn(withEmail: userEmailID , password: userPassword) { (user, error) in
                if error == nil
                {
                    let uid = user?.user.uid
                    let email = user?.user.email
                    let  deviceID = UIDevice.current.identifierForVendor!.uuidString
                    
                   self.getUserToken(emailid: email! , uiID: uid! , deviceId: deviceID)
                     MBProgressHUD.hide(for: self.view, animated: false)
                }
                else
                {
                    MBProgressHUD.hide(for: self.view, animated: false)
                    ShowToast.showPositiveMessage(message: "Something went wrong. Please try again.");
                    print("Error ----  \(error ?? "Not error data but in error method " as! Error)")
                }
            }
        }
    }
    
    
    
    
    
    
    
    
    
    
    func getUserToken(emailid: String , uiID : String , deviceId:String)
    {
        let requestURL = BASEURL + getToken
        let parameters: [String:String] = ["email": emailid]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            token = response["data"]["token"].stringValue
            
            print("xxxxxxxx   ---- \(success)--  \(response)")
            print("xxxxxxxx token   ---- \(token)--  \(response)")
            
            if success
            {
                if(token != "")
                {
                    isRememberLogin = self.isRemindMe
                    UserDefaults.standard.set(token, forKey: "tokenID")
                    UserDefaults.standard.set(uiID, forKey: "uid")
                    UserDefaults.standard.set(emailid, forKey: "email")
                    UserDefaults.standard.set(deviceId, forKey: "deviceId")
                    UserDefaults.standard.set(true, forKey: "islogin")
                   // UserDefaults.standard.set(isRememberLogin, forKey: "isRemindMe")
                    
                    token = UserDefaults.standard.string(forKey: "tokenID")!;
                    dox_id = UserDefaults.standard.string(forKey: "uid")!;
                    for_uuid = UserDefaults.standard.string(forKey: "uid")!;
                    email = UserDefaults.standard.string(forKey: "email")!;
                    
                    
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "ProfileViewTimeLine") as! ProfileViewTimeLine
                    vc.isFromLogin = true
                    vc.profileID = dox_id
                    isForEdit = true
                    self.present(vc, animated: true, completion: nil)
                    /*
                    OperationQueue.main.addOperation {
                        self.lunchHomePage();
                    }
                 */
                }
                else
                {
                    ShowToast.showPositiveMessage(message: "Sorry!. Your email is not registed with DoxBay App. Please register first then login");
                }
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
        })
    }
    
    
    
    
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    
    
    
    
    
    
}





