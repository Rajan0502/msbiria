//
//  CheckBox.swift
//  DoxBayNew
//
//  Created by Luminous on 22/04/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

var isAcceptTermCondition: Bool = false
import UIKit

class CheckBox: UIButton {

    // Images
    let checkedImage = UIImage(named: "if_checked")! as UIImage
    let uncheckedImage = UIImage(named: "if_not_checked")! as UIImage
    
    // Bool property
    var isChecked: Bool = false {
        didSet{
            if isChecked == true {
                self.setImage(checkedImage, for: UIControlState.normal)
            } else {
                self.setImage(uncheckedImage, for: UIControlState.normal)
            }
        }
    }
    
    override func awakeFromNib() {
        self.addTarget(self, action:#selector(buttonClicked(sender:)), for: UIControlEvents.touchUpInside)
        self.isChecked = false
        isAcceptTermCondition = false
    }
    
    @objc func buttonClicked(sender: UIButton) {
        if sender == self {
            isChecked = !isChecked
            isAcceptTermCondition = isChecked;
             print("----isChecked----\(isChecked)")
        }
    }

}
