//
//  Login_Abhi.swift
//  DoxbayAbhishek
//
//  Created by Luminous on 08/06/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import GoogleSignIn
import SwiftyJSON
import Alamofire
import MBProgressHUD

class LoginViewController: UIViewController , GIDSignInUIDelegate, UITextFieldDelegate{

    
    
    @IBOutlet var uiviewww: UIView!
    @IBOutlet var imgAppLogo: UIImageView!
    
    
    
    var ref: DatabaseReference!
    
    
    @IBOutlet var btnLoginOutlet: UIButton!
    @IBOutlet var btnGoogleLoginOutLet: UIButton!
    
    @IBOutlet var emailText: UITextField!
    @IBOutlet var passwordText: UITextField!
    
    
    @IBOutlet weak var viewTermCondition: UILabel!
    
    
    
    @IBAction func btnLoginAction(_ sender: Any) {
        loginWithEmailPass();
    }
    
    @IBAction func btnGoogleLoginAction(_ sender: Any) {
        if (isAcceptTermCondition)
        {
            GIDSignIn.sharedInstance().signIn();
        }
        else{
            ShowToast.showPositiveMessage(message: "Please accept terms and conditions")
        }
    }
    
    
    
    @IBAction func btnForgotPassword(_ sender: Any) {
        self.showGetUserName();
    }
    
    
    
    
    func showGetUserName() {
         let checkemail = Utility();
        let alertController = UIAlertController(title:  AppNameText , message: "Reset password", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Reset", style: .default, handler: {
            alert -> Void in
            let fNameField = alertController.textFields![0] as UITextField
          //  let lNameField = alertController.textFields![1] as UITextField
            
            let emaildID = fNameField.text!;
            if(emaildID.count < 8)
            {
                ShowToast.showPositiveMessage(message: "Please enter the valid Email ID.");
            }
//            else if(!checkemail.checkemail.validateEmail(enteredEmail: emaildID))
//            {
//                ShowToast.showPositiveMessage(message: "Please enter the valid Email ID.");
//            }
            else
            {
                        Auth.auth().sendPasswordReset(withEmail: emaildID) { error in
                            if error != nil
                            {
                                ShowToast.showPositiveMessage(message: "Your reset password  link successfully sent to your emaild id");
                            }
                            else
                            {
                                ShowToast.showPositiveMessage(message:  SomeThingWentWorng);
                            }
                    }
            }
            
           
        }))
        
        alertController.addTextField(configurationHandler: { (textField) -> Void in
            textField.placeholder = "Enter your email id"
            textField.textAlignment = .center
        })
        
//        alertController.addTextField(configurationHandler: { (textField) -> Void in
//            textField.placeholder = "Last Name"
//            textField.textAlignment = .center
//        })
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
//        let screensize: CGRect = UIScreen.main.bounds
//        let screenWidth = screensize.width - 20
//        let screenHeight = screensize.height
//        var imgXY : Int = 0
//        let imgHW : Int = 120
//        if(Int(screenWidth) % 2 == 0)
//        {
//            imgXY =  Int(screenWidth + 1)
//        }
//        else
//        {
//            imgXY =  Int(screenWidth)
//        }
//        imgXY = imgXY - imgHW ;
//        imgXY = imgXY / 2
//        imgAppLogo.frame = CGRect(x: imgXY-5 , y: 10, width: imgHW, height: imgHW)
//
//
//
//
//        var scrollView: UIScrollView!
//        scrollView = UIScrollView(frame: CGRect(x: 0, y: 10, width: screenWidth, height: screenHeight))
//        scrollView.contentSize = CGSize(width: screenWidth, height: 600)
//        scrollView.addSubview(uiviewww)
//        scrollView.showsVerticalScrollIndicator = false
//        view.addSubview(scrollView)
//

        
        // Google Sign In Instance
        GIDSignIn.sharedInstance().uiDelegate = self
        self.hideKeyboardOnTap(#selector(self.dismissKeyboard))
        
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.viewTemCondition(sender:)))
        viewTermCondition.addGestureRecognizer(tap)
        viewTermCondition.isUserInteractionEnabled = true
        
      //  self.checkSign();
    }
    
    
    func  checkSign()
    {
        Auth.auth().fetchProviders(forEmail: "it.lptdelhi@gmail.com", completion: {
        (providers, error) in
    
        if let error = error {
       print("xxxxxx-xxx-xx---\(error.localizedDescription)")
        } else if let providers = providers {
        print("xxxxxx-xxx-xx---\(providers)")
        }
        })
        
        
        
        Auth.auth().fetchSignInMethods(forEmail: "it.lptdelhi@gmail.com", completion: {
            (providers, error) in
            
            if let error = error {
                print("xxxxxx-xxx-xx- 111--\(error.localizedDescription)")
            } else if let providers = providers {
                print("xxxxxx-xxx-xx- 11--\(providers)")
            }
        })
    }
    
    
    @objc func viewTemCondition(sender : UILabel)
    {
        let storyBoard: UIStoryboard = UIStoryboard(name: "LoginSearch", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "TermCond_PrivacyPoli_VC") as! TermCond_PrivacyPoli_VC
        vc.isFromPage = "Register"
        vc.PageType = "Term_Condition"
        self.present(vc, animated: true, completion: nil)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    
    
    
    
    func loginWithEmailPass()
    {
        let userEmailID = emailText.text!;
        let userPassword = passwordText.text!;
        let checkemail = Utility();
        if(userEmailID.count < 8)
        {
            ShowToast.showPositiveMessage(message: "Please enter the valid Email ID.");
        }
        else if(!checkemail.validateEmail(enteredEmail: userEmailID))
        {
            ShowToast.showPositiveMessage(message: "Please enter the valid Email ID.");
        }
        else if(userPassword.count < 5)
        {
            ShowToast.showPositiveMessage(message: "Password grater then 5 chrector");
        }
        else if (!isAcceptTermCondition)
        {
            ShowToast.showPositiveMessage(message: "Please accept terms and conditions")
        }
        else
        {
            let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
            progressHUD.mode = MBProgressHUDMode.indeterminate
            progressHUD.label.text = "Please wait.."
            
            Auth.auth().signIn(withEmail: userEmailID , password: userPassword) { (user, error) in
                if error == nil
                {
                    
                    /*
                    let credential = EmailAuthProvider.credential(withEmail: userEmailID, password: userPassword)
                    Auth.auth().currentUser?.linkAndRetrieveData(with: credential) { (authResult, error) in
                        if let error = error {
                            Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
                                if let error = error {
                                    print("authResult-------authResult---Errorrrr");
                                    return
                                }
                                else
                                {
                                    print("authResult-------authResult---\(authResult)");
                                }
                            }
                        }
                    }
                    */
                    let uid = user?.user.uid
                    let email = user?.user.email
                    let  deviceID = UIDevice.current.identifierForVendor!.uuidString
                    
                    self.getUserToken(emailid: email! , uiID: uid! , deviceId: deviceID)
                    MBProgressHUD.hide(for: self.view, animated: false)
                }
                else
                {
                    
                    let credential = EmailAuthProvider.credential(withEmail: userEmailID, password: userPassword)
                    Auth.auth().currentUser?.linkAndRetrieveData(with: credential) { (authResult, error) in
                        if let error = error {
                            Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
                                if let error = error {
                                    print("authResult-------authResult---Errorrrr");
                                    return
                                }
                                else
                                {
                                    print("authResult-------authResult---\(authResult)");
                                }
                            }
                        }
                    }
                    
                    MBProgressHUD.hide(for: self.view, animated: false)
                    ShowToast.showPositiveMessage(message: "Something went wrong. Please try again.");
                    print("Error ----  \(error ?? "Not error data but in error method " as! Error)")
                }
            }
        }
    }
    
    
    
    
    
    
    
    
    
    
    func getUserToken(emailid: String , uiID : String , deviceId:String)
    {
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Please wait.."
        
        let requestURL = BASEURL + getToken
        let parameters: [String:String] = ["email": emailid]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            MBProgressHUD.hide(for: self.view, animated: false)
            
            let success = response["success"].boolValue
            token = response["data"]["token"].stringValue

            if success
            {
                if(token != "")
                {
                  //  isRememberLogin =  true
                    UserDefaults.standard.set(token, forKey: "tokenID")
                    UserDefaults.standard.set(uiID, forKey: "uid")
                    UserDefaults.standard.set(emailid, forKey: "email")
                    UserDefaults.standard.set(deviceId, forKey: "deviceId")
                    UserDefaults.standard.set(true, forKey: "islogin")
                    UserDefaults.standard.set(false, forKey: "isRemindMe")
                    
                    token = UserDefaults.standard.string(forKey: "tokenID")!;
                    dox_id = UserDefaults.standard.string(forKey: "uid")!;
                    for_uuid = UserDefaults.standard.string(forKey: "uid")!;
                    email = UserDefaults.standard.string(forKey: "email")!;
                    
                    
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "ProfileViewTimeLine") as! ProfileViewTimeLine
                    vc.isFromLogin = true
                    vc.profileID = UserDefaults.standard.string(forKey: "uid")!
                    isForEdit = true
                    self.present(vc, animated: true, completion: nil)
                    /*
                     OperationQueue.main.addOperation {
                     self.lunchHomePage();
                     }
                     */
                }
                else
                {
                    ShowToast.showPositiveMessage(message: "Sorry!. Your email is not registed with MSBIRIA App. Please register first then login");
                }
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
        })
    }
    
    
    
    
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    
    
    
    
    
    
    
    
    

    func lunchHomePage()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
        let rightViewController = storyboard.instantiateViewController(withIdentifier: "RightViewController") as! RightViewController
        
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        
        UINavigationBar.appearance().tintColor = UIColor(hex: "689F38")
        
        leftViewController.mainViewController = nvc
        
        let slideMenuController = ExSlideMenuController(mainViewController:nvc, leftMenuViewController: leftViewController, rightMenuViewController: rightViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = mainViewController as! SlideMenuControllerDelegate
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window!.rootViewController = slideMenuController
        appDelegate.window!.makeKeyAndVisible()
    }
    
    func initializeRoot()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
        let rightViewController = storyboard.instantiateViewController(withIdentifier: "RightViewController") as! RightViewController
        
        var tabItems = [Any]() /* capacity: 4 */
        let Home = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        var nav = UINavigationController(rootViewController: Home)
        nav.tabBarItem.title = "Home"
        nav.tabBarItem.image = UIImage(named: "home")
        tabItems.append(nav)
        
        let dvc = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        nav = UINavigationController(rootViewController: dvc)
        nav.tabBarItem.title = "Friend Request"
        nav.tabBarItem.image = UIImage(named: "tab1")
        tabItems.append(nav)
        let cvc = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        nav = UINavigationController(rootViewController: cvc)
        nav.tabBarItem.title = "Chat"
        nav.tabBarItem.image = UIImage(named: "tab2")
        tabItems.append(nav)
        let rvc = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        nav = UINavigationController(rootViewController: rvc)
        nav.tabBarItem.title = "Notification"
        nav.tabBarItem.image = UIImage(named: "tab3")
        tabItems.append(nav)
        let dvc2 = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        // dvc2.isFromTab = truea
        nav = UINavigationController(rootViewController: dvc2)
        nav.tabBarItem.title = "Search"
        nav.tabBarItem.image = UIImage(named: "tab4")
        tabItems.append(nav)
        
        let tbc = UITabBarController()
        tbc.viewControllers = tabItems as? [UIViewController] ?? [UIViewController]()
        tbc.selectedIndex = 0
        
        leftViewController.mainViewController = tbc
        
        let slideMenuController = ExSlideMenuController(mainViewController:tbc, leftMenuViewController: leftViewController, rightMenuViewController: rightViewController)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window!.rootViewController = slideMenuController
        appDelegate.window!.makeKeyAndVisible()
    }
    
}
