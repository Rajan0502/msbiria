//
//  TermCond_PrivacyPoli_VC.swift
//  Doxbay
//
//  Created by Luminous on 13/07/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit

class TermCond_PrivacyPoli_VC: UIViewController , UIWebViewDelegate {

    var isFromPage = ""
    var PageType = ""
    let TNC = "http://web.doxbay.com/pages/tnc/";
    let PVC = "http://web.doxbay.com/pages/privacy-policy/";
    let HC = "http://web.doxbay.com/pages/doxbay-help-center/";
    
    var URL = ""; // Terms and Conditions
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var headerTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        webView.delegate = self
        if(PageType  == "Term_Condition")
        {
            URL = TNC;
            headerTitle.text = "Terms and Conditions"
        }
        else if (PageType == "Privacy_Policy"){
           URL = PVC;
            headerTitle.text = "Privacy Policy"
        }
        else if (PageType == "Help_Center"){
            URL = HC;
            headerTitle.text = "Help Center"
        }
        webView.loadRequest(NSURLRequest(url: NSURL(string: URL)! as URL) as URLRequest);
    }

    func webViewDidStartLoad(_ webView: UIWebView) {
         SwiftLoader.show(title: "Loading...", animated: true)
         print("Strat Loading")
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        print("Finish Loading")
        SwiftLoader.hide()
    }
    func webView(webView: UIWebView, didFailLoadWithError error: Error?) {
        print(error?.localizedDescription)
        SwiftLoader.hide()
    }
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func btnBackAction(_ sender: Any) {
        // self.navigationController?.popViewController(animated: false)
        self.dismiss(animated: true, completion: nil)
    }
    
}
