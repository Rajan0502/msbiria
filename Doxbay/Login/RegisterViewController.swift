//
//  Registraction_Abhi.swift
//  DoxbayAbhishek
//
//  Created by Luminous on 08/06/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import GoogleSignIn
import Alamofire
import SwiftyJSON
import MBProgressHUD

class RegisterViewController: UIViewController, GIDSignInUIDelegate {

    var ref: DatabaseReference!
    
    @IBOutlet var uiviewww: UIView!
    @IBOutlet var imgAppLogo: UIImageView!
    
    
    @IBOutlet var fullaNameText: UITextField!
    @IBOutlet var emailIDText: UITextField!
        @IBOutlet var paswordText: UITextField!
    @IBOutlet var confirmPasswordText: UITextField!
    
    @IBOutlet var btnSignupOutlet: UIButton!
    @IBOutlet var btnRegisterGoogle: UIButton!
    
    @IBOutlet weak var viewTermCondtion: UILabel!
    
    
    @IBAction func btnRegisterAction(_ sender: Any) {
        registerWithEmailPass();
    }
    
    @IBAction func btnRegisterGoogle(_ sender: Any) {
        if (isAcceptTermCondition)
        {
            GIDSignIn.sharedInstance().signIn();
        }
        else{
            ShowToast.showPositiveMessage(message: "Please accept terms and conditions")
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let screensize: CGRect = UIScreen.main.bounds
//        let screenWidth = screensize.width - 20
//        let screenHeight = screensize.height
//        var imgXY : Int = 0
//        let imgHW : Int = 120
//        if(Int(screenWidth) % 2 == 0)
//        {
//            imgXY =  Int(screenWidth + 1)
//        }
//        else
//        {
//            imgXY =  Int(screenWidth)
//        }
//        imgXY = imgXY - imgHW ;
//        imgXY = imgXY / 2
//        imgAppLogo.frame = CGRect(x: imgXY-5 , y: 10, width: imgHW, height: imgHW)
//
//
//
//
//        var scrollView: UIScrollView!
//        scrollView = UIScrollView(frame: CGRect(x: 0, y: 10, width: screenWidth, height: screenHeight))
//        scrollView.contentSize = CGSize(width: screenWidth, height: 600)
//        scrollView.addSubview(uiviewww)
//        scrollView.showsVerticalScrollIndicator = false
//        view.addSubview(scrollView)
//

        
        
        // Google Sign In Instance
        GIDSignIn.sharedInstance().uiDelegate = self
        ref = Database.database().reference().child("/discussions/people/")
        self.hideKeyboardOnTap(#selector(self.dismissKeyboard))
      
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.viewTemCondition(sender:)))
        viewTermCondtion.addGestureRecognizer(tap)
        viewTermCondtion.isUserInteractionEnabled = true
        
    }
    @objc func viewTemCondition(sender : UILabel)
    {
        let storyBoard: UIStoryboard = UIStoryboard(name: "LoginSearch", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "TermCond_PrivacyPoli_VC") as! TermCond_PrivacyPoli_VC
        vc.isFromPage = "Register"
        vc.PageType = "Term_Condition"
        self.present(vc, animated: true, completion: nil)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    
    
    
    
    func registerWithEmailPass()
    {
        let userEmailID = emailIDText.text!;
        let userPassword = paswordText.text!;
        let displayName = fullaNameText.text!
        let confirmPassword = confirmPasswordText.text!
        
        let checkemail = Utility();
        
        if(displayName.count < 1)
        {
            ShowToast.showPositiveMessage(message: "Please enter full name name");
        }
        else if(userEmailID.count < 8)
        {
            ShowToast.showPositiveMessage(message: "Please enter a valid Email ID");
        }
        else if(!checkemail.validateEmail(enteredEmail: userEmailID))
        {
            ShowToast.showPositiveMessage(message: "Please enter a valid Email ID");
        }
        else if(userPassword.count < 5)
        {
            ShowToast.showPositiveMessage(message: "Your password shoud be grater then or equal to 5 chrector");
        }
        else if(confirmPassword.count < 5)
        {
            ShowToast.showPositiveMessage(message: "Confirm password shoud be grater then or equal to 5 chrector");
        }
        else if(userPassword != confirmPassword)
        {
            ShowToast.showPositiveMessage(message: "Password and Confirm shoud be same");
        }
        else if (!isAcceptTermCondition)
        {
                ShowToast.showPositiveMessage(message: "Please accept terms and conditions")
        }
        else
        {
            
            
            /*
            let credential = EmailAuthProvider.credential(withEmail: userEmailID, password: userPassword)
           // let prevUser = Auth.auth().currentUser
            Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
                if let error = error {
                    print("authResult-------authResult---Errorrrr");
                    return
                }
                else
                {
                    print("authResult-------authResult---\(authResult)");
                }
            }
            */
            
            
            let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
            progressHUD.mode = MBProgressHUDMode.indeterminate
            progressHUD.label.text = "Please wait.."
           
            Auth.auth().createUser(withEmail: userEmailID , password: userPassword) { (user, error) in
                print("errorerrorerror --\(error)")
                if error == nil
                {
                    
                    
                    let credential = EmailAuthProvider.credential(withEmail: userEmailID, password: userPassword)
                    Auth.auth().currentUser?.linkAndRetrieveData(with: credential) { (authResult, error) in
                        if let error = error {
                            Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
                                if let error = error {
                                    print("authResult-------authResult---Errorrrr");
                                    MBProgressHUD.hide(for: self.view, animated: false)
                                    ShowToast.showPositiveMessage(message: "Something went wrong. Please try again.");
                                    return
                                }
                                else
                                {
                                    print("authResult-------authResult---\(authResult)");
                                    
                                    MBProgressHUD.hide(for: self.view, animated: false)
                                    let uid = authResult?.user.uid
                                    let email = authResult?.user.email
                                    
                                    print("Ema il Password---Details-->>\(user)")
                                    print("Google---user-->>userId:- \(uid)  @@  fullName:- \(displayName)    @@  email:- \(email)  ")
                                    
                                    self.postDataInRealTimeFireBaseDB(type: "F", emailID: email! , password: userPassword ,  uiID: uid!, name: displayName , photoUrl: "");
                                }
                            }
                        }
                    }
                    
                    
                  
                   
                }
                else
                {
                    let credential = EmailAuthProvider.credential(withEmail: userEmailID, password: userPassword)
                    Auth.auth().currentUser?.linkAndRetrieveData(with: credential) { (authResult, error) in
                        if let error = error {
                            Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
                                if let error = error {
                                    print("authResult-------authResult---Errorrrr");
                                    MBProgressHUD.hide(for: self.view, animated: false)
                                    ShowToast.showPositiveMessage(message: "Something went wrong. Please try again.");
                                    return
                                }
                                else
                                {
                                    print("authResult-------authResult---\(authResult)");
                                    let uid = authResult?.user.uid
                                    let email = authResult?.user.email
                                    
                                    print("Ema il Password---Details-->>\(user)")
                                    print("Google---user-->>userId:- \(uid)  @@  fullName:- \(displayName)    @@  email:- \(email)  ")
                                    
                                    self.postDataInRealTimeFireBaseDB(type: "F", emailID: email! , password: userPassword ,  uiID: uid!, name: displayName , photoUrl: "");
                                }
                            }
                        }
                    }
                    
                   
                }
            }
            
            
           
        
        }
    }
    
    
    
    
    var fromType : String = "";
    func postDataInRealTimeFireBaseDB(type: String, emailID: String , password: String, uiID: String, name: String , photoUrl : String)
    {
        fromType = type;
        ref = Database.database().reference().child("/discussions/people/")
        let  deviceID = UIDevice.current.identifierForVendor!.uuidString
        
        //creating artist with the given values
        let userProfileData = ["city":"",
                               "deviceId": deviceID,
                               "deviceTypw": "IOS",
                               "displayName": name,
                               "dox_id":uiID,
                               "eduction_super_speciality":"",
                               "email": emailID,
                               "mobile": "",
                               "photoUrl": photoUrl,
                               "registration_number": ""
            
        ]

        print("ref/// -----\(ref)")
        print("uiID/// -----\(uiID)")
        print("userProfileData-----\(userProfileData)")
        
        //adding the artist inside the generated unique key
        ref.child(uiID).setValue(userProfileData)
        
        

             self.PostUserDeatils(emailid: emailID , uiID: uiID , deviceId: deviceID , name: name , photoUrl: photoUrl);
        
       
    }
    
    func  PostUserDeatils(emailid: String , uiID : String , deviceId:String , name : String, photoUrl : String)
    {
        SwiftLoader.show(title: "Loading..", animated: true)
        let requestURL = BASEURL + postUserdd
        // let parametersx : [String:String] = ["email": emailid, "dox_id":uiID , "displayName":name , "photoUrl" : photoUrl]
        let parametersx : [String:String] = ["email": emailid, "dox_id":uiID , "displayName":name , "photoUrl" : photoUrl]
        print("Request URxxxxxxL- \(requestURL)  Paramxxxxxxxxxxxx --- \(parametersx)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parametersx, headers: nil, success: {(response) -> Void in
            print("------------response------\(response)")
            
            SwiftLoader.hide();
            let success = response["success"].boolValue
            let userID = response["data"]["id"].stringValue
            let tokenx = response["data"]["token"].stringValue
           
            
            if success
            {
                if(tokenx != "")
                {
                    UserDefaults.standard.set(tokenx, forKey: "tokenID")
                    UserDefaults.standard.set(uiID, forKey: "uid")
                    UserDefaults.standard.set(emailid, forKey: "email")
                    UserDefaults.standard.set(deviceId, forKey: "deviceId")
                    UserDefaults.standard.set(true, forKey: "islogin")
                    UserDefaults.standard.set(false, forKey: "isRemindMe")
                    token = UserDefaults.standard.string(forKey: "tokenID")!;
                    dox_id = UserDefaults.standard.string(forKey: "uid")!;
                    for_uuid = UserDefaults.standard.string(forKey: "uid")!;
                    email = UserDefaults.standard.string(forKey: "email")!;
                    
                    
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "ProfileViewTimeLine") as! ProfileViewTimeLine
                    vc.isFromLogin = true
                    vc.profileID = dox_id
                    isForEdit = true
                    self.present(vc, animated: true, completion: nil)
                }
                else
                {
                    ShowToast.showPositiveMessage(message: "Sorry!. Your email is not registed with MSBIRIA App. Please register first then login");
                }
            }
            else
            {
                ShowToast.showPositiveMessage(message: "Something went wrong. Please try again.");
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            SwiftLoader.hide()
            
        })
    }
    
    
    /*
    func  PostUserDeatils(emailid: String , uiID : String , deviceId:String , name : String, photoUrl : String)
    {
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Please wait.."
        
        let requestURL = BASEURL + postUser
        //"email=\(emailid)&dox_id=\(uiID)&displayName=\(name)&photoUrl=\(photoUrl)";
        let parameters: [String:String] = ["email": emailid, "dox_id":uiID , "displayName":name , "photoUrl" : photoUrl]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
             MBProgressHUD.hide(for: self.view, animated: false)
            
            let success = response["success"].boolValue
            let userID = response["data"]["id"].stringValue
            
            print("xxxxxxxx   ---- \(success)--  \(response)")
            print("xxxxxxxx userID   ---- \(token)--  \(response)")
            
           
                if success
                {
                    if(self.fromType == "G")
                    {
                        DispatchQueue.main.async(execute: {
                            self.generateTockenIDForUser(emailid: emailid , uiID: uiID , deviceId: deviceId);
                        });
                    }
                    else{
                        self.generateTockenIDForUser(emailid: emailid , uiID: uiID , deviceId: deviceId);
                    }
                }
                else
                {
                    ShowToast.showPositiveMessage(message: "Something went wrong. Please try again.");
                }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
             MBProgressHUD.hide(for: self.view, animated: false)
        })
    }
   
    
    
    func generateTockenIDForUser(emailid: String , uiID : String , deviceId:String)
    {
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Please wait.."
        
        let requestURL = BASEURL + getToken
        let parameters: [String:String] = ["email": emailid]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
             MBProgressHUD.hide(for: self.view, animated: false)
            
            let success = response["success"].boolValue
            token = response["data"]["token"].stringValue
            
            print("xxxxxxxx   ---- \(success)--  \(response)")
            print("xxxxxxxx token   ---- \(token)--  \(response)")
            
            if success
            {
                if(token != "")
                {
                    UserDefaults.standard.set(token, forKey: "tokenID")
                    UserDefaults.standard.set(uiID, forKey: "uid")
                    UserDefaults.standard.set(emailid, forKey: "email")
                    UserDefaults.standard.set(deviceId, forKey: "deviceId")
                    UserDefaults.standard.set(true, forKey: "islogin")
                    UserDefaults.standard.set(false, forKey: "isRemindMe")
                    token = UserDefaults.standard.string(forKey: "tokenID")!;
                    dox_id = UserDefaults.standard.string(forKey: "uid")!;
                    for_uuid = UserDefaults.standard.string(forKey: "uid")!;
                    email = UserDefaults.standard.string(forKey: "email")!;
                    
                    
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "ProfileViewTimeLine") as! ProfileViewTimeLine
                    vc.isFromLogin = true
                    vc.profileID = dox_id
                    isForEdit = true
                    self.present(vc, animated: true, completion: nil)
                }
                else
                {
                    ShowToast.showPositiveMessage(message: "Sorry!. Your email is not registed with DoxBay App. Please register first then login");
                }
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
        })
    }
    
    */
    
    
    
    

    @objc func dismissKeyboard() {
        view.endEditing(true)
        // do aditional stuff
    }
    
    
}





extension UIViewController {
    func hideKeyboardOnTap(_ selector: Selector) {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: selector)
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
}

