//
//  CellImage.swift
//  Doxbay
//
//  Created by AppsInvo  on 05/05/18.
//  Copyright © 2018 Salman. All rights reserved.
//

import UIKit

var instanceVC: FormSubmit_VC?

class CellImage: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnChoosable: UIButton!
    var imagePickerViewController = UIImagePickerController()
    var cellIndex = 0
    var parentCellIndex = 0
    
    func configureCell(info: [String:Any], cellIndex: Int, parentCellIndex: Int) {
        self.cellIndex = cellIndex
        self.parentCellIndex = parentCellIndex
        btnChoosable.makeBorder(1, color: UIColor.lightGray)
        btnChoosable.makeRoundCorner(5)
        imgView.makeRoundCorner(5)
        DispatchQueue.main.async {
            //self.btnChoosable.setTitle("\(info["optionText"]!)" , for: .normal)
        }
       
    }
    
    @IBAction func tapChoosable(_ sender: Any) {
        instanceVC?.showAlertWithActions(msg: "", titles: ["Camera","Photo Library","Cancel"]) { (selectOption) in
            if selectOption == 1 {//Camera
                self.imagePickerViewController.sourceType = .camera
                instanceVC?.present(self.imagePickerViewController, animated: true, completion: nil)
                
            }else if selectOption == 2 {//Photo Library
                self.imagePickerViewController.sourceType = .photoLibrary
                instanceVC?.present(self.imagePickerViewController, animated: true, completion: nil)
            }
            self.imagePickerViewController.delegate = self
            print(selectOption)
        }
    }
    
    
    func uploadImage(image: UIImage) {
        SwiftLoader.show(title: "Uploading", animated: true)
        let param = [Constant.kAPI_NAME: addAnswerImage,
                     "token": token]
        
        SGServiceController.instance().hitMultipartForImage(param, image: image, imageParamName: "image", unReachable: {
            print("Not reachable")
            SwiftLoader.hide()
        }) { (response) in
            print(response)
            SwiftLoader.hide()
            if response != nil {
                print("Image uploaded")
                if let img = response?["image"] as? String {
                    var question = signupAnswerSheet![self.parentCellIndex]
                    print(self.parentCellIndex,question)
                    if let arrOption = question["options"] as? [[String: Any]], arrOption.count > 0 {
                        var arrMutableOption = arrOption
                        var option = arrMutableOption[self.cellIndex]
                        option["optionAnswer"] = img
                        arrMutableOption[self.cellIndex] = option
                        question["options"] = arrMutableOption
                        signupAnswerSheet![self.parentCellIndex] = question
                    }
                }
            }
        }
    }
    
}


extension CellImage: UINavigationControllerDelegate,UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage]as? UIImage {
            print(image)
            imgView.image = image
            uploadImage(image: image)
        }
        instanceVC?.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}















