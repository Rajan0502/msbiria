//
//  CellRadio.swift
//  Doxbay
//
//  Created by AppsInvo  on 05/05/18.
//  Copyright © 2018 Salman. All rights reserved.
//

import UIKit

class CellRadio: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgRadio: UIImageView!
    
    func configureCell(info: [String:Any], selected: Bool) {
        if selected {
            imgRadio.image = UIImage(named: "onRadio")
        } else {
            imgRadio.image = UIImage(named: "offRadio")
        }
        lblTitle.text = info["quiz_option"] as? String
    }
    
    
}
