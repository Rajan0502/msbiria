//
//  CellOptionCollection.swift
//  Doxbay
//
//  Created by AppsInvo  on 05/05/18.
//  Copyright © 2018 Salman. All rights reserved.
//

import UIKit

class CellOptionCollection: UITableViewCell {
    @IBOutlet weak var collectionViewCheck: UICollectionView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var containerView: UIView!
    var arrOption = [[String:Any]]()
    var selectedIndex = [IndexPath]()
    

    
    func configureView(info: [String: Any]) {
        lblTitle.text = info["questionText"] as? String
        collectionViewCheck.register(UINib(nibName: "CellCheckBox", bundle: nil), forCellWithReuseIdentifier: "CellCheckBox")
        arrOption = info["options"] as! [[String: Any]]
        collectionViewCheck.delegate = self
        collectionViewCheck.dataSource = self
        collectionViewCheck.reloadData()
        containerView.addShadow(radius: 5)
    }
}

extension CellOptionCollection: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellCheckBox", for: indexPath) as! CellCheckBox
        if selectedIndex.contains(indexPath) {
            cell.configureView(info: arrOption[indexPath.row], selected: true)
        }
        else {
            cell.configureView(info: arrOption[indexPath.row], selected: false)
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrOption.count
    }
}

extension CellOptionCollection: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let index = selectedIndex.index(where: {$0 == indexPath}) {
            selectedIndex.remove(at: index)
        } else {
            selectedIndex.append(indexPath)
        }
        
        if let parentIndex = instanceVC?.tableView.indexPath(for: self) {
            var question = signupAnswerSheet![parentIndex.row]
            if let arrOption = question["options"] as? [[String: Any]], arrOption.count > 0 {
                var arrMutableOption = arrOption
                for (index,option) in arrOption.enumerated() {
                    if let _ = selectedIndex.index(where: {$0.row == index}) {
                        var mutableOption = option
                        mutableOption["optionAnswer"] = "1"
                        arrMutableOption[index] = mutableOption
                    }
                    else {
                        var mutableOption = option
                        mutableOption["optionAnswer"] = "0"
                        arrMutableOption[index] = mutableOption
                    }
                }
                question["options"] = arrMutableOption
                signupAnswerSheet![parentIndex.row] = question
            }
        }
        
        
        
        collectionView.reloadData()
    }
}

extension CellOptionCollection: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width/2, height: 50)
    }
}








