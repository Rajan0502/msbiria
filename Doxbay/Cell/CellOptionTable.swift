//
//  CellOption.swift
//  Doxbay
//
//  Created by AppsInvo  on 05/05/18.
//  Copyright © 2018 Salman. All rights reserved.
//

import UIKit

class CellOptionTable: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewContainer: UIView!
    
    var arrImage = [[String: Any]]()
    var arrRadio = [[String: Any]]()
    var cellType = ""
    var radioCellSelectedIndex = IndexPath()
    var cellIndex = 0
    
    func configureCell(info: [String:Any],index: Int){
        cellType = info["inputType"] as! String
        lblTitle.text = info["questionText"] as? String
        viewContainer.addShadow(radius: 5)
        cellIndex = index
        if cellType == "image" {
            tableView.register(UINib(nibName: "CellImage", bundle: nil), forCellReuseIdentifier: "CellImage")
            arrImage = info["options"] as! [[String: Any]]
           
        } else {
            tableView.register(UINib(nibName: "CellRadio", bundle: nil), forCellReuseIdentifier: "CellRadio")
            arrRadio = info["options"] as! [[String: Any]]
        }
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
}


extension CellOptionTable: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if cellType == "image" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellImage", for: indexPath) as!  CellImage
            cell.configureCell(info: arrImage[indexPath.row], cellIndex: indexPath.row, parentCellIndex: cellIndex)
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellRadio", for: indexPath) as! CellRadio
            if radioCellSelectedIndex == indexPath {
                cell.configureCell(info: arrRadio[indexPath.row], selected: true)
            }else {
                cell.configureCell(info: arrRadio[indexPath.row], selected: false)
            }
            return cell
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if cellType == "image" {
            return arrImage.count
        }
        else {
            return arrRadio.count
        }
    }
}


extension CellOptionTable: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if cellType == "image" {
            return 60
        }
        else {
            return 40
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         radioCellSelectedIndex = indexPath
        if cellType == "radio" {
            if let index = instanceVC?.tableView.indexPath(for: self) {
                var data = signupAnswerSheet![index.row]
                var answer = arrRadio[radioCellSelectedIndex.row]
                answer["optionAnswer"]  = answer["optionText"]
                data["options"] = [answer]
                signupAnswerSheet![index.row] = data
            }
        }
        tableView.reloadData()
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
}








