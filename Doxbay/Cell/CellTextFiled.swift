//
//  CellTextFiled.swift
//  Doxbay
//
//  Created by AppsInvo  on 05/05/18.
//  Copyright © 2018 Salman. All rights reserved.
//

import UIKit

class CellTextFiled: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtAnswer: UITextField!
    @IBOutlet weak var viewContainer: UIView!
    func configureCell(info : [String: Any]) {
        lblTitle.text = info["questionText"] as? String
        viewContainer.addShadow(radius: 5)
        txtAnswer.addTarget(self, action: #selector(didChangeText(_:)), for: UIControlEvents.editingChanged)
    }
    
    @objc func didChangeText(_ textField: UITextField) {
        print(textField.text)
        if let index = instanceVC?.tableView.indexPath(for: self) {
            var data = signupAnswerSheet![index.row]
            var options = data["options"] as! [[String:Any]]
            options[0]["optionAnswer"] = txtAnswer.text
            data["options"] = options
            signupAnswerSheet![index.row] = data
        }
    }
    
}


extension CellTextFiled: UITextFieldDelegate {
    
}
