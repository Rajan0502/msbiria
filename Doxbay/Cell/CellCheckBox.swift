//
//  CellCheckBox.swift
//  Doxbay
//
//  Created by AppsInvo  on 05/05/18.
//  Copyright © 2018 Salman. All rights reserved.
//

import UIKit

class CellCheckBox: UICollectionViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    var cellIndex = 0
    var parentCellIndex = 0
    
    
    func configureView(info: [String: Any], selected: Bool) {
        if selected {
            imgView.image = UIImage(named:"check")
        } else {
            imgView.image = UIImage(named:"emptyCheck")
        }
        lblTitle.text = info["optionText"] as? String
    }
}
