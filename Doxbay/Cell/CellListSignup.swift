//
//  ListCell.swift
//  DocsBay
//
//  Created by Apple on 5/5/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class CellListSignup: UITableViewCell {
    
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnSubmitForm: UIButton!
    
    @IBOutlet var labDescription: UILabel!
    
    
    func configureView(info: [String: Any]) {
        //  lblTitle.text = info["description"] as? String
        lblTime.text =  info["postedAt"] as? String
        
        
        lblTitle.lineBreakMode = .byTruncatingMiddle
        lblTitle.lineBreakMode = .byWordWrapping
        lblTitle.numberOfLines = 0;
        
        lblTitle.text = info["titleText"] as? String
        let greet4Height = lblTitle.optimalHeight
        lblTitle.frame = CGRect(x: lblTitle.frame.origin.x, y: lblTitle.frame.origin.y, width: lblTitle.frame.width, height: greet4Height)
        
        
        
        labDescription.lineBreakMode = .byTruncatingMiddle
        labDescription.lineBreakMode = .byWordWrapping
        labDescription.numberOfLines = 0;
        labDescription.text = info["description"] as? String
        let greet4Heighte = labDescription.optimalHeight
        labDescription.frame = CGRect(x: labDescription.frame.origin.x, y: labDescription.frame.origin.y, width: labDescription.frame.width, height: greet4Heighte)
        
        
        
        
        btnSubmitForm.makeRoundCorner(2)
        viewShadow.addShadow(radius: 5)
    }
    
}
