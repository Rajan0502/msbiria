//
//  QuizLIstViewController.swift
//  Doxbay
//
//  Created by kavi yadav on 18/06/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit
import MBProgressHUD
import SDWebImage
import AVKit
import AVFoundation
import MobileCoreServices
import Alamofire
import SwiftyJSON

class QuizLIstViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var channelName = "";
    var channelIDd = "429";
    
    @IBOutlet weak var QuizImg: UIImageView!
    @IBOutlet weak var NavTitle: UILabel!
    @IBOutlet weak var quizListTable: UITableView!
    var timeLimeData : [OTLModal] = []

    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.quizListTable.delegate = self
        self.quizListTable.dataSource = self
        QuizImg.layer.cornerRadius = QuizImg.frame.size.width/2
        QuizImg.clipsToBounds = true
        self.GetQuizList()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return timeLimeData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! QuizCell
        
        let quizName = timeLimeData[indexPath.row].title
        let PostDate = timeLimeData[indexPath.row].time
        cell.QuizName.text = quizName
        cell.QuizPOstTime.text = PostDate

        if(timeLimeData[indexPath.row].count_comment == "1")
        {
            cell.btnPreview.isHidden = false
            cell.btnPreview.tag  =  indexPath.row
            cell.btnPreview.addTarget(self, action: #selector(tapOnPreview), for: .touchUpInside)
            
            cell.StartQuiz.setTitle("SHOW LEADERBOARD",for: .normal)
            cell.StartQuiz.tag  =  indexPath.row
            cell.StartQuiz.addTarget(self, action: #selector(tapLeaderBoard), for: .touchUpInside)
        }
        else
        {
            cell.btnPreview.isHidden = true
            cell.StartQuiz.setTitle("START QUIZ",for: .normal)
            cell.StartQuiz.tag = indexPath.row
            cell.StartQuiz.addTarget(self, action: #selector(TapOnStartQuiz), for: .touchUpInside)
        }
        
        
       
        return cell
        
    }
    
    

    @IBAction func TapOnBack(_ sender: Any) {
        self.presentingViewController?.dismiss(animated: true, completion:nil)
    }
    
    
    
    
    
    @objc func TapOnStartQuiz(_ sender: UIButton) {
        let index = sender.tag
        let quizId = timeLimeData[index].id

        if(timeLimeData[index].count_comment == "1")
        {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "QuizLeaderBoard_VC") as! QuizLeaderBoard_VC
            vc.QuizID = quizId
            vc.FromQuiz = timeLimeData[index].title
            vc.chanalId = channelIDd
            self.present(vc, animated:true, completion:nil)
        }
        else
        {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "quizQuestoatalViewController") as! quizQuestoatalViewController
            vc.QuizId = quizId
            self.present(vc, animated:true, completion:nil)
            
        }
    }
    
    
    
    @objc func tapOnPreview(_ sender: UIButton) {
        let index = sender.tag
        let quizId = timeLimeData[index].id
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "previewController") as! previewController
        vc.QuizId = quizId
        self.present(vc, animated:true, completion:nil)
    }
    @objc func tapLeaderBoard(_ sender: UIButton) {
        let index = sender.tag
        let quizId = timeLimeData[index].id
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "QuizLeaderBoard_VC") as! QuizLeaderBoard_VC
        vc.QuizID = quizId
        vc.FromQuiz = timeLimeData[index].title
        vc.chanalId = channelIDd
        self.present(vc, animated:true, completion:nil)
    }
    
    
    func GetQuizList() {
            let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
            progressHUD.mode = MBProgressHUDMode.indeterminate
            progressHUD.label.text = "Loading"
            //MBProgressHUD.hide(for: self.view, animated: false)
            
            let requestURL =  BASEURL + getQuizList;
            let parameters: [String:String] = ["token": token , "channel_id": channelIDd ,"currentpage": String(self.pageNo)]
            print("Request URL- \(requestURL)  Param --- \(parameters)")
            
            UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
                
                MBProgressHUD.hide(for: self.view, animated: false)
                let success = response["success"].boolValue
                print("xxxxxxxx   ---- \(success)-- \(response)")
                if success
                {
                    let abc = response["data"].arrayObject;
                    self.parseJSONData(json: response)
                    
                }
            }, failure:  {
                (error) -> Void in
                print("error===\(error)")
                MBProgressHUD.hide(for: self.view, animated: false)
            })
        
    }
    
    func parseJSONData(json: JSON)
    {
        // timeLimeData.removeAll();
        if(json["data"].arrayValue.count < 10)
        {
            isAvilableForLoad = false
        }
        for result in json["data"].arrayValue
        {
            let id = result["id"].stringValue
            let title = result["title"].stringValue
            let description = result["description"].stringValue
            let category = result["category"].stringValue
            let pdf = result["pdf"].stringValue
            let pdf_name = result["pdf_name"].stringValue
            let image = result["image"].stringValue
            let video = result["video"].stringValue
            let video_thumb = result["video_thumb"].stringValue
            let video_url = result["video_url"].stringValue
            let video_url_oe = result["video_url_oe"].stringValue
            let time = result["postedAt"].stringValue
            let count_like = result["status"].stringValue
            let is__final_submited = result["is__final_submited"].stringValue
            let is_favorite = result["point_per_right_answer"].stringValue
            let is_liked = result["total_elapsed_time"].stringValue
            let content_type = result["content_type"].stringValue
            let point_per_wrong_answer = result["point_per_wrong_answer"].stringValue
            let userName = result["user"]["name"].stringValue
            let userImage = result["user"]["image"].stringValue
            let channel_name = result["user"]["channel_name"].stringValue
            let channel_id = result["user"]["channel_id"].stringValue
            
            let mData : OTLModal = OTLModal(id: id , title: title , description: description  , category: category , pdf: pdf , pdf_name: pdf_name , image: image , video: video , video_thumb: video_thumb , video_url: video_url , video_url_oe: video_url_oe , time: time , count_like: count_like , count_comment: is__final_submited , is_favorite: is_favorite , is_liked: is_liked , content_type: content_type , userID: point_per_wrong_answer , userName: userName , userImage: userImage , channel_name: channel_name , channel_id: channel_id)
          //  print(mData)
            timeLimeData.append(mData)
        }
        DispatchQueue.main.async{
            self.quizListTable.reloadData();
        }
    }
    
    var pageNo : Int = 0;
    var loadingData = false;
    var isAvilableForLoad = true;
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = timeLimeData.count - 3
        if !loadingData && indexPath.row == lastElement {
            loadingData = true
            loadMoreData()
        }
    }
    func loadMoreData() {
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                self.pageNo = self.pageNo + 1
                if(self.isAvilableForLoad)
                {
                    self.GetQuizList();
                }
                self.loadingData = false
            }
        }
    }
    
    
    
    
}
class ShadowViewOnquestion: UIView {
    override var bounds: CGRect {
        didSet {
            setupShadow()
        }
    }

    private func setupShadow() {
        //self.layer.cornerRadius = 8
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowRadius = 3
        self.layer.shadowOpacity = 0.3
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 8, height: 8)).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
}
