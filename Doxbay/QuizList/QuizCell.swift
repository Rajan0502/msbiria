//
//  QuizCell.swift
//  Doxbay
//
//  Created by kavi yadav on 18/06/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit

class QuizCell: UITableViewCell {

    
    @IBOutlet weak var BngView: UIView!
    @IBOutlet weak var QuizPOstTime: UILabel!
    @IBOutlet weak var QuizName: UILabel!
    @IBOutlet weak var StartQuiz: UIButton!
    @IBOutlet weak var btnPreview: UIButton!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
