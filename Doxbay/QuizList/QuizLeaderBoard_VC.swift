//
//  QuizLeaderBoard_VC.swift
//  Doxbay
//
//  Created by Luminous on 11/07/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit
import SwiftyJSON
import MBProgressHUD

class QuizLeaderBoard_VC: UIViewController , UITableViewDelegate, UITableViewDataSource {
    
    
    
    
    var subArr : [channelSubscribeModal] = [];
    var QuizID : String = "43";
    var chanalId : String = "";
    var FromQuiz : String = ""
    
    
    
    
    @IBOutlet var headerTitle: UILabel!
    @IBOutlet var tableView: UITableView!
    
    @IBAction func btnBack(_ sender: Any) {
        // self.navigationController?.popViewController(animated: false)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //self.tableView.addSubview(self.refreshControl)
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.red
        return refreshControl
    }()
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        //self.getExploreChannelsSubscriberList);
        refreshControl.endRefreshing()
    }
    
    
    
    
    
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.addSubview(self.refreshControl)
        
        getExploreChannelsSubscriberList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subArr.count;
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return CGFloat(85.00);
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChannelSubcriberCell", for : indexPath) as! ChannelSubcriberCell
        
        let rData : channelSubscribeModal = subArr[indexPath.row];
        cell.labName.lineBreakMode = .byTruncatingMiddle
        cell.labName.lineBreakMode = .byWordWrapping
        cell.labName.numberOfLines = 0;
        cell.labName.text = rData.displayName;
        let greet4Height = cell.labName.optimalHeight
        cell.labName.frame = CGRect(x: cell.labName.frame.origin.x, y: cell.labName.frame.origin.y, width: cell.labName.frame.width, height: greet4Height)
        
     
        cell.profilePic.tag = indexPath.row
        if(!rData.photoUrl.isEmpty)
        {
            cell.profilePic.sd_setImage(with:URL(string: rData.photoUrl), placeholderImage: UIImage(named: "vc_channel"))
            
            cell.profilePic.layer.cornerRadius =  (cell.profilePic.frame.size.width) / 2;
            cell.profilePic.layer.masksToBounds = true
            
            let tap3 = UITapGestureRecognizer(target: self, action: #selector(ViewUserProfile))
            cell.profilePic.addGestureRecognizer(tap3)
            cell.profilePic.isUserInteractionEnabled = true
            
        }
        
        
        cell.labLoc.text =  rData.city;
        
        let label = UILabel(frame: CGRect(x:self.view.frame.width - 70  , y: 40, width: 40, height: 40))
        label.text =   rData.dox_id
        label.tag = indexPath.row
        label.layer.cornerRadius =  (label.frame.size.width) / 2;
        label.layer.masksToBounds = true
        label.backgroundColor = UIColor(red: 25.0/255.0, green: 28.0/255.0, blue: 118.0/255.0, alpha: 1.0)
        label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.center
        cell.contentView.addSubview(label)
        
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rData : channelSubscribeModal = subArr[indexPath.row];
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ProfileViewTimeLine") as! ProfileViewTimeLine
        vc.profileID = rData.dox_id
        self.present(vc, animated:true, completion:nil)
    }
    @objc func ViewUserProfile(_ sender: UITapGestureRecognizer)
    {
        let label = sender.view as! UIImageView
        let rData : channelSubscribeModal = subArr[label.tag];
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ProfileViewTimeLine") as! ProfileViewTimeLine
        vc.profileID = rData.dox_id
        self.present(vc, animated:true, completion:nil)
    }
    
    func getExploreChannelsSubscriberList()
    {
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Loading"
        //MBProgressHUD.hide(for: self.view, animated: false)
        
        let requestURL =  BASEURL + leaderBoard;
        let parameters: [String:String] = ["token": token , "quiz_id": QuizID, "currentpage": String(self.pageNo)]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            MBProgressHUD.hide(for: self.view, animated: false)
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)-- \(response)")
            if success
            {
                let abc = response["data"].arrayObject;
                self.parse(json: response)
                
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
        })
    }
    
    func parse(json: JSON) {
        if(json["data"].arrayValue.count < 10)
        {
            isAvilableForLoad = false
        }
        for result in json["data"].arrayValue {
            let id = result["id"].stringValue
            let channel_id = ""
            let displayName = result["name"].stringValue
            let email = result["email"].stringValue
            let photoUrl = result["photo"].stringValue
            
            let miliSecondsTime:Double? = Double(result["total_elapsed_time"].stringValue)
            let date = Date(timeIntervalSince1970: (miliSecondsTime! / 1000.0))
            
            
            
            let city = "Point : " + result["point"].stringValue +  "  Time : " + date.getElapsedInterval()
            let dox_id = result["rank"].stringValue
            
            
            let dData : channelSubscribeModal = channelSubscribeModal(id: id , channel_id: channel_id , displayName: displayName, city: city , dox_id: dox_id , email: email , photoUrl: photoUrl);
            self.subArr.append(dData);
        }
        
        self.tableView.reloadData();
        MBProgressHUD.hide(for: self.view, animated: false)
    }
    
    
    var pageNo : Int = 1;
    var loadingData = false;
    var isAvilableForLoad = true;
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = subArr.count - 3
        if !loadingData && indexPath.row == lastElement {
            loadingData = true
            loadMoreData()
        }
    }
    func loadMoreData() {
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                self.pageNo = self.pageNo + 1
                if(self.isAvilableForLoad)
                {
                    self.getExploreChannelsSubscriberList();
                }
                self.loadingData = false
            }
        }
    }

}
