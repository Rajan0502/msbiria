//
//  CellQualification.swift
//  DoxbaySalman
//
//  Created by AppsInvo  on 20/05/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit

class CellQualification: UITableViewCell {

    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var lblSpeciality: UILabel!
    @IBOutlet weak var lblDegreee: UILabel!
    @IBOutlet weak var lblCollegeName: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    var id = ""
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configureView(info: [String: Any]) {
        id = info["id"] as? String ?? ""
        btnDelete.isHidden = !isForEdit
        
        if let isCurrent = info["is_current"] as? String, isCurrent == "Currently Study Heare" {
            lblYear.text = "\(info["start_year"]!) to Pressent"
        }
        else {
            lblYear.text = "\(info["start_year"]!) to \(info["end_year"]!)"
        }
        let firstCharacter = info["college_name"] as! String
        lblSpeciality.text = info["speciality_name"] as? String
        lblDegreee.text = info["degree_name"] as? String
        lblCollegeName.text = info["college_name"] as? String
        if(info["college_name"] as? String != "")
        {
            lblTitle.text = "\(firstCharacter.characters.first!)"
        }
    }
    
    
    @IBAction func tapDelete(_ sender: Any) {
        profileInstance.showOkCancelAlertWithAction("Are you sure delete?") { (isOk) in
            if isOk {
                let params = ["id":self.id, "token": token, Constant.kAPI_NAME: deleteUserEducation]
                SGServiceController.instance().hitPostService(params, unReachable: {
                    print("Not reachable")
                }) { (response) in
                    print(response)
                    if let index = profileInstance.tableView.indexPath(for: self) {
                        print(index.section)
                        print(index.row)
                        profileInstance.tableView.beginUpdates()
                        profileInstance.arrQualification.remove(at: index.row)
                        profileInstance.tableView.deleteRows(at: [index], with: .none)
                        profileInstance.tableView.endUpdates()
                        
                    }
                }
            }
        }
    }
    
    
    
}
