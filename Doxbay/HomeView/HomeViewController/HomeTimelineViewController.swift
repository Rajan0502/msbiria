//
//  HomeTimelineViewController.swift
//  Doxbay
//
//  Created by Rajan Kumar Tiwari on 23/07/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import Foundation
import MBProgressHUD
import SwiftyJSON
import CCBottomRefreshControl
import AVKit
import AVFoundation
import MobileCoreServices

/*
 a. pagination, increasing the page index
 b. adding html label
 c. increasing height dynamically
 d. adding view at the place of home view controller.
 
 //
 // pdf open view - fixed.
 // Report alignment -  6s-10, iPhone 8 Plus
 // Footer down
 // Image zooming
 
 
 
 */

class HomeTimelineViewController: UIViewController
{
    
    @IBOutlet weak var timelineTableView: UITableView!
    @IBOutlet weak var subcriptionRequestInProgressLabel: UILabel!
    
    // It will hold data source
    var timelineDataSource:[OTLModal] = [OTLModal]()
    var isTimelineDataAvailable:Bool = false
    var pageIndex:NSInteger = 0
    
    var refreshControl = UIRefreshControl()
    
    var bottomRefreshControl = UIRefreshControl()
    var isNetworkRequestInProgress = false
    
    var postContentType = String ()
    var post_id = String ()
    var User_UUid = String ()
    var channelID = String ()
    
    var actions : [(String, UIAlertActionStyle)] = []
    var reportList : [ReportResaonList] = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.isTimelineDataAvailable = true
        self.setupUI()
        self.fetchSubCribeChanelStatusFromServer();
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.parent?.view.frame = CGRect(x:0, y:64, width:self.view.frame.width, height: UIScreen.main.bounds.size.height-68)
        }
    }
    
    //MARK:- Private Methods
    func setupUI()
    {
        self.regiesterTableView()
        self.addRefreshControlToTableView()
        self.addBottomRefreshControlToTableView()
        self.timelineTableView.tableFooterView = UIView(frame: .zero)
    }
 
    func addRefreshControlToTableView()
    {
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull down to refresh")
        self.refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.timelineTableView .addSubview(self.refreshControl)
    }
    
    func addBottomRefreshControlToTableView()
    {
        self.bottomRefreshControl.attributedTitle = NSAttributedString(string: "Pull up to load more")
        self.bottomRefreshControl.addTarget(self, action: #selector(bottomRefresh), for: .valueChanged)
        self.timelineTableView.bottomRefreshControl = self.bottomRefreshControl
    }
    
    func regiesterTableView()
    {
        self.timelineTableView.register(UINib(nibName:CustomCell.cellNibName , bundle: nil) , forCellReuseIdentifier: CustomCell.cellIdentifier)
    }
    
    //MARK:- Helper Methods
    class  func GetHomeTimelineViewController() -> HomeTimelineViewController
    {
        let sb =  UIStoryboard(name: "HomeTimeLineScene", bundle: nil)
        let homeTimelineVC = sb.instantiateViewController(withIdentifier: "HomeTimelineViewController") as! HomeTimelineViewController
        return homeTimelineVC
    }
    
    @objc func bottomRefresh(sender:UIRefreshControl)
    {
        self.bottomRefreshControl.endRefreshing()
        if  self.isNetworkRequestInProgress == true { return }
        
        if self.isTimelineDataAvailable == true
        {
            self.fetchTimelineDataFromServer()
        }
    }
    
    @objc func refresh(sender:UIRefreshControl)
    {
        refreshControl.endRefreshing()
        if  self.isNetworkRequestInProgress == true { return}
        
        self.pageIndex = 0
        self.isTimelineDataAvailable = true
        self.bottomRefreshControl.attributedTitle = NSAttributedString(string: "Pull up to load more")
        self.fetchTimelineDataFromServer()
    }
    
//    func loadImageViewControllerForTimelineData(_ timelineData:OTLModal?)
//    {
//        guard let timelinedata = timelineData  else {
//            return
//        }
//        let imageStrURLs =  timelinedata.image
//        //let imageStrURL = imageStrURLs.split(separator: ",")
//        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let newViewController = storyBoard.instantiateViewController(withIdentifier: "ImagePagerViewControllerPager") as! ImagePagerViewControllerPager
//        //        newViewController.imageURL = String(imageStrURL[0])
//        newViewController.imageURL = imageStrURLs
//        newViewController.requestType = "image"
//        self.present(newViewController, animated: true, completion: nil)
//    }
    
    func loadImageViewControllerForTimelineData(_ timelineData:OTLModal?, forCell cell:CustomCell?)
    {
        guard let timelinedata = timelineData  else {
            return
        }
        
        let imageStrURLs =  timelinedata.image
        let fullScreenImageVC = FullScreenImageViewController.GetFullScreenImageViewController()
        fullScreenImageVC.imageURLSting = imageStrURLs
        fullScreenImageVC.image = cell?.thumbnailImageView.image
        self.present(fullScreenImageVC, animated: true, completion: nil)
    }
    
    //MARK:- Data Formation Methods
    func fetchSubCribeChanelStatusFromServer()
    {
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Loading" //Checking subscription status
        let requestURL = BASEURL + getExpChannel;
        let parameters: [String:String] = ["id": channel_id ,"token": token]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            MBProgressHUD.hide(for: self.view, animated: false)
            let success = response["success"].boolValue
            
            print("get Subscribe channel Status   ---- \(success)-- \(response)")
            if success
            {
                let subcriptionStatus = response["data"]["is_subscribed"].stringValue
                UserDefaults.standard.set(subcriptionStatus, forKey: "is_subscribe")
                
                if(subcriptionStatus == "2") //Timeline data available
                {
                    self.fetchTimelineDataFromServer();
                }
                else
                {
                    self.postRequestToServerForChannelSubscription()
                    
                    self.timelineTableView.isHidden = true
                    self.subcriptionRequestInProgressLabel.isHidden = false
                }
            }
            else
            {
                
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
        })
    }
    
    func fetchTimelineDataFromServer()
    {
        self.isNetworkRequestInProgress = true
        
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Loading" //Getting time line data
        
        let requestURL = BASEURL + getOrganizationTimeline
        let parameters: [String:String] = ["channel_id": channel_id ,"token": token,"currentpage":  String(self.pageIndex)]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            self.isNetworkRequestInProgress = false
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--  \(response)")
            
            if success
            {
                self.parsex(json: response, withCompletionHandler: { (dataSource:[OTLModal]) in
                    
                    if self.pageIndex == 0 // first call
                    {
                        self.timelineDataSource = dataSource
                    }
                    else // paginated data
                    {
                        self.timelineDataSource.append(contentsOf: dataSource) // add the fetched to data source
                    }
                    
                    self.subcriptionRequestInProgressLabel.isHidden = true
                    self.timelineTableView.isHidden = false
                    self.timelineTableView.reloadData()
                    MBProgressHUD.hide(for: self.view, animated: false)
                    //Adding page index  value for next time call
                    self.pageIndex = self.pageIndex + 1
                })
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
        })
    }
    
    func postRequestToServerForChannelSubscription()
    {
        let requestURL = BASEURL + subsribeUnsubscribeChannel;
        let parameters: [String:String] = ["channel_id": channel_id ,"token": token,"is_subscribe": "1"]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            print("subsribeUnsubscribeChannel Request   ---- \(response)--")
            
            if success
            {
                UserDefaults.standard.set("1", forKey: "is_subscribe")
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
        })
    }
    
    func parsex(json: JSON, withCompletionHandler completion:@escaping (_ data:[OTLModal]) -> Void)
    {
        //TODO: check the value is working
        if(json["data"].arrayValue.count < 10)
        {
            self.isTimelineDataAvailable = false
            self.bottomRefreshControl.attributedTitle = NSAttributedString(string: "Timeline Data Not Available")
            print(" isTimelineDataAvailable no")
        }
        else
        {
            print(" isTimelineDataAvailable yes")
        }
        
        var tempDataSource = [OTLModal]()
        for result in json["data"].arrayValue
        {
            let id = result["id"].stringValue
            let title = result["title"].stringValue
            let description = result["description"].stringValue
            let category = result["category"].stringValue
            let pdf = result["pdf"].stringValue
            let pdf_name = result["pdf_name"].stringValue
            let image = result["image"].stringValue
            let video = result["video"].stringValue
            let video_thumb = result["video_thumb"].stringValue
            let video_url = result["video_url"].stringValue
            let video_url_oe = result["video_url_oe"].stringValue
            let time = result["time"].stringValue
            let count_like = result["count_like"].stringValue
            let count_comment = result["count_comment"].stringValue
            let is_favorite = result["is_favorite"].stringValue
            let is_liked = result["is_liked"].stringValue
            let content_type = result["content_type"].stringValue
            let userID = result["user"]["id"].stringValue
            let userName = result["user"]["name"].stringValue
            let userImage = result["user"]["image"].stringValue
            let channel_name = result["channel_name"].stringValue
            let channel_id = result["channel_id"].stringValue
            
            let mData : OTLModal = OTLModal(id: id , title: title , description: description  , category: category , pdf: pdf , pdf_name: pdf_name , image: image , video: video , video_thumb: video_thumb , video_url: video_url , video_url_oe: video_url_oe , time: time , count_like: count_like , count_comment: count_comment , is_favorite: is_favorite , is_liked: is_liked , content_type: content_type , userID: userID , userName: userName , userImage: userImage , channel_name: channel_name , channel_id: channel_id)
            tempDataSource.append(mData)
        }
        completion(tempDataSource)
    }
    
    func showReportReason(response : JSON)
    {
        self.actions  = []
        for result in response["data"].arrayValue
        {
            let id = result["id"].stringValue
            let nameVal = result["name"].stringValue
            let re : ReportResaonList = ReportResaonList(id: id , name: nameVal);
            self.reportList.append(re);
            self.actions.append((nameVal , UIAlertActionStyle.destructive))
        }

        self.actions.append(("Dismiss", UIAlertActionStyle.cancel))
        
        
        Alerts.showActionsheet(viewController: self, title: "DoxBay MSBIRIA", message: "Please select user report reason", actions: actions) { (index) in
            
            print("call action \(index)")
            if(index < self.reportList.count)
            {
                let reasonID = self.reportList[index].id
                self.reportUserNow(ReasonId: reasonID );
            }
        }
    }
    
    func reportUserNow(ReasonId : String)
    {
        SwiftLoader.show(title: "Loading..", animated: true)
        var requestURL = ""
        var parameters = [String:String]()
        parameters = ["token": token ,"post_id": post_id , "report_reason_id" : ReasonId , "for_uuid" : User_UUid ,"channel_id":channel_id]
        
        if postContentType == "user_post" {
            requestURL = BASEURL + reportUserPost
        }
        else if postContentType == "channel_post" {
            requestURL = BASEURL + reportChannelPost
        }
        else if(postContentType == "user_event")
        {
            requestURL = BASEURL + reportUserEvent
        }
        
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--response --\(response)")
            SwiftLoader.hide()
            if success
            {
                ShowToast.showPositiveMessage(message: "Successfully reported")
            }
            else
            {
                ShowToast.showPositiveMessage(message: "Somthing went worng, Please try again")
            }
        }, failure:  {
            (error) -> Void in
            SwiftLoader.hide()
            print("error===\(error)")
            ShowToast.showPositiveMessage(message: "Somthing went worng, Please try again")
        })
    }
}

extension HomeTimelineViewController:UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.timelineDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CustomCell.cellIdentifier) as? CustomCell else {
            return UITableViewCell()
        }
        if timelineDataSource.isEmpty == false, timelineDataSource.count >= indexPath.row
        {
            cell.prepareCellWithTimeline(item: timelineDataSource[indexPath.row])
            cell.cellDelegate = self
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let customCell = tableView.dequeueReusableCell(withIdentifier: CustomCell.cellIdentifier) as! CustomCell
        if self.timelineDataSource.isEmpty == false, self.timelineDataSource.count >= indexPath.row
        {
            let timelinedata:OTLModal = self.timelineDataSource[indexPath.row]
            let height =  customCell.getHeightOfCellFor(item: timelinedata)
            print("Height heightForRowAt -> \(height) OfRow \(indexPath.row)")
            return height
        }
        return 0
    }
}
extension HomeTimelineViewController:UITableViewDelegate
{
    
}

extension HomeTimelineViewController:CustomCellDelegate
{
    
    
    func cell(_ cell: CustomCell, didTapOpenPDFButton pdfButton: UIButton)
    {
        guard let timelinedata = cell.timelineItem  else {
            return
        }
        
        let imageStr =   timelinedata.pdf // dic["pdf"] as? String
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "ImagePagerViewControllerPager") as! ImagePagerViewControllerPager
        newViewController.imageURL = imageStr
        newViewController.requestType = "pdf"
        self.present(newViewController, animated: true, completion: nil)
    }

    func cell(_ cell: CustomCell, didTapFollowButton followButton: UIButton)
    {
        
    }
    
    func cell(_ cell: CustomCell, didTapReportButton reportButton: UIButton)
    {
        guard let timelinedata = cell.timelineItem  else {
            return
        }
        
        postContentType = timelinedata.content_type
        post_id = timelinedata.id
        User_UUid = timelinedata.userID
        channelID = timelinedata.channel_id
        
        let requestURL = BASEURL +  getUserReportReasonList
        let parameters: [String:String] = ["token": token ]
        
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--")
            
            if success
            {
                // self.ReportListDataArray = response["data"].arrayObject!
                // print(self.ReportListDataArray)
                //                self.reportView.frame = CGRect(x: 0, y: self.view.frame.origin.y-35, width:self.view.frame.size.width, height: self.view.frame.size.height)
                //                self.view.addSubview(self.reportView)
                //                self.reportTable.reloadData()
                self.showReportReason(response: response);
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
        })
        
    }
    
    func cell(_ cell: CustomCell, didTapCommentButton commentButton: UIButton)
    {
        guard let timelinedata = cell.timelineItem  else {
            return
        }
        
        let sb = UIStoryboard.init(name:"Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "Comments_VC") as! Comments_VC

        vc.id =  timelinedata.id
        vc.contentType = timelinedata.content_type
        self.present(vc, animated:true, completion:nil)
    }
    
    func cell(_ cell: CustomCell, didTapLikeButton likeButton: UIButton)
    {
        guard var timelinedata = cell.timelineItem  else {
            return
        }
        let id = timelinedata.id
        var is_liked = timelinedata.is_liked
        if is_liked == "0"
        {
            is_liked = "1"
            timelinedata.is_liked = "1"
            
            let aa : Int = Int(timelinedata.count_like)! + 1
            timelinedata.count_like = String(aa)
            
            cell.likeLabel.text = timelinedata.count_like + " Likes"
            
            cell.makeLikeButtonSelected(true)
        }
        else
        {
            is_liked = "0"
            timelinedata.is_liked = "0"
            
            let aa : Int = Int(timelinedata.count_like)! - 1
            timelinedata.count_like = String(aa)
            
            cell.likeLabel.text = timelinedata.count_like + " Likes"
            
            cell.makeLikeButtonSelected(false)
        }
        
        cell.timelineItem = timelinedata
        let indexPath = self.timelineTableView.indexPath(for: cell)
        self.timelineDataSource[(indexPath?.row)!] = timelinedata
        
        let contentType = timelinedata.content_type // String(describing: dic["content_type"]!)
        var requestURL = "";
        if(contentType == "user_post")
        {
            requestURL = BASEURL + createUserPostLike
        }
        else if(contentType == "user_event")
        {
            requestURL = BASEURL + createEventLike
        }
        else if(contentType == "channel_post")
        {
            requestURL = BASEURL + postLike
        }
        
        let parameters: [String:String] = ["id":id,"channel_id":channel_id ,"token": token ,"is_liked":is_liked]
        
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--")
            
            if success
            {
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
        })
        
    }
    
    func cell(_ cell: CustomCell, didTapFavoriteButton favoriteButton: UIButton)
    {
        guard var timelinedata = cell.timelineItem  else {
            return
        }
    
        var isFavarity =  timelinedata.is_favorite
        if(isFavarity == "1")
        {
            isFavarity = "0";
            cell.makeFavoriteButtonSelected(false)
        }
        else
        {
            isFavarity = "1";
            cell.makeFavoriteButtonSelected(true)
        }
        timelinedata.is_favorite = isFavarity
        //This we need to do because we are working value type data source, not with reference type.
        cell.timelineItem = timelinedata
        let indexPath = self.timelineTableView.indexPath(for: cell)
        self.timelineDataSource[(indexPath?.row)!] = timelinedata
        
        
        let requestURL = BASEURL + postFavorite
        let parameters: [String:String] = ["id":timelinedata.id,"token": token ,"is_favorite": isFavarity]
        
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--")
            
            if success
            {
                
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
        })
    }
    
    func cell(_ cell: CustomCell, didTapPlayVideoButton playVideoButton: UIButton)
    {
        guard let timelinedata = cell.timelineItem else {
            return
        }

        let videoURL = URL(string:timelinedata.video_url)
        let player  = AVPlayer(url:videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    func cell(_ cell: CustomCell, didTapViewImageButton viewImagesButton: UIButton)
    {
//       self.loadImageViewControllerForTimelineData(cell.timelineItem)
        self.loadImageViewControllerForTimelineData(cell.timelineItem, forCell: cell)
    }
    
    func cell(_ cell: CustomCell, didTapDescriptionButton descriptionButton: UIButton)
    {
        
    }
    
    func cell(_ cell: CustomCell, didTapThumbnailImageButton thumbnailImageButton: UIButton)
    {
        self.loadImageViewControllerForTimelineData(cell.timelineItem, forCell: nil)
    }
    
}

