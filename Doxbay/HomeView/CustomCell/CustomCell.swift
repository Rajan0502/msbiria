//
//  CustomCell.swift
//  Doxbay
//
//  Created by Rajan Kumar Tiwari on 23/07/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit

protocol CustomCellDelegate
{
    func cell(_ cell:CustomCell, didTapOpenPDFButton pdfButton:UIButton)
    func cell(_ cell:CustomCell, didTapPlayVideoButton playVideoButton:UIButton)
    func cell(_ cell:CustomCell, didTapThumbnailImageButton thumbnailImageButton:UIButton)
    func cell(_ cell:CustomCell, didTapViewImageButton viewImagesButton:UIButton)
    func cell(_ cell:CustomCell, didTapFollowButton followButton:UIButton)
    func cell(_ cell:CustomCell, didTapFavoriteButton favoriteButton:UIButton)
    func cell(_ cell:CustomCell, didTapLikeButton likeButton:UIButton)
    func cell(_ cell:CustomCell, didTapCommentButton commentButton:UIButton)
    func cell(_ cell:CustomCell, didTapReportButton reportButton:UIButton)
    func cell(_ cell:CustomCell, didTapDescriptionButton descriptionButton:UIButton) //openPDFButtonAction
}

class CustomCell: UITableViewCell
{
    static let cellIdentifier:String = "CustomCell"
    static let cellNibName:String    = "CustomCell"
 
    @IBOutlet weak var pdfContainerView: UIView!
    @IBOutlet weak var viewImagesButton: UIButton!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var thumbnailContainerView: UIView!
    @IBOutlet weak var videoImageView: UIImageView!
    @IBOutlet weak var videoContainerView: UIView!
    @IBOutlet weak var videoPlayerImageContainerView: UIView!
    @IBOutlet weak var descriptionContainerView: UIView!
    @IBOutlet weak var itemContainerView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userPostTitleLabel: UILabel!
    @IBOutlet weak var userPostDateTimeLabel: UILabel!
    @IBOutlet weak var likeLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var reportLabel: UILabel! //TODO: report label is not being set as of now. data is not coming from server
    @IBOutlet weak var pdfNameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel! //temp addition
    
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var favouriteButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var commentsButton: UIButton!
    
    @IBOutlet weak var descriptionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionContainerTopLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewImagesTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var pdfContainerViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var footerViewTopConstraint: NSLayoutConstraint!
    
    var timelineItem:OTLModal?
    var cellDelegate:CustomCellDelegate?
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.timelineItem = nil
        self.setupUI()
        self.backgroundColor = UIColor.cusomCellBackgroundColor
    }
    
    override func prepareForReuse()
    {
        super.prepareForReuse()
        self.timelineItem = nil
    }
    
    //MARK:- Helper Methods
    func setupUI()
    {
        //setup font
        self.userNameLabel.font          = UIFont.SystemFont14Regular()
        self.userPostTitleLabel.font     = UIFont.SystemFont12Regular()
        self.userPostDateTimeLabel.font  = UIFont.SystemFont12Regular()
        self.likeLabel.font              = UIFont.SystemFont12Regular()
        self.commentLabel.font           = UIFont.SystemFont12Regular()
        self.reportLabel.font            = UIFont.SystemFont12Regular()
        self.pdfNameLabel.font           = UIFont.SystemFont12Regular()
        self.descriptionLabel.font       = UIFont.SystemFont14Semibold()
        
        //setup text
        self.userNameLabel.textColor          = UIColor.black
        self.userPostTitleLabel.textColor     = UIColor.black
        self.userPostDateTimeLabel.textColor  = UIColor.black
        self.likeLabel.textColor              = UIColor.black
        self.commentLabel.textColor           = UIColor.black
        self.reportLabel.textColor            = UIColor.black
        self.pdfNameLabel.textColor           = UIColor.white
        self.descriptionLabel.textColor       = UIColor.gray
        
        self.prepareCircleProfileImage()
        self.createCardViewAroundCell()
        self.createCardViewAroundViewImagesButton()
        self.prepareCircleAroundVideoPlayerImageContainerView()
    }
    
    func prepareCircleProfileImage()
    {
        self.profileImageView.layer.cornerRadius =  (self.profileImageView.frame.size.width) / 2;
        self.profileImageView.layer.borderColor  =  UIColor.gray.cgColor
        self.profileImageView.layer.borderWidth  = 0.5
        self.profileImageView.layer.masksToBounds = true
    }
    
    func prepareCircleAroundVideoPlayerImageContainerView()
    {
        self.videoPlayerImageContainerView.layer.cornerRadius =
            (self.videoPlayerImageContainerView.frame.size.width) / 2;
        self.videoPlayerImageContainerView.layer.masksToBounds = true
    }
    
    func createCardViewAroundCell()
    {
        self.itemContainerView.layer.cornerRadius = 5.0
        self.itemContainerView.layer.borderColor  = UIColor.lightGray.cgColor
        self.itemContainerView.layer.borderWidth  = 0.5
        self.itemContainerView.layer.masksToBounds = true
    }
    
    func createCardViewAroundViewImagesButton()
    {
        self.viewImagesButton.layer.cornerRadius = 5.0
        self.viewImagesButton.layer.borderColor  = UIColor.lightGray.cgColor
        self.viewImagesButton.layer.borderWidth  = 0.5
        self.viewImagesButton.layer.masksToBounds = true
    }
    
    func  prepareCellWithTimeline(item timelineItem:OTLModal)
    {
        self.timelineItem = timelineItem
        
        self.profileImageView.sd_setImage(with: URL(string: String(timelineItem.userImage)),
                                          placeholderImage: UIImage(named: "vc_user"))
        
        self.userNameLabel.text      = timelineItem.userName
        self.userPostTitleLabel.text = timelineItem.channel_name
        
        let miliSecondsTime:Double?  = Double(timelineItem.time)
        let date = Date(timeIntervalSince1970: (miliSecondsTime! / 1000.0))
        self.userPostDateTimeLabel.attributedText =  Utility.getHTMLToString(titleStr: "<strong><font color=red>" + timelineItem.category + " </font></strong>" + date.getElapsedInterval() + " ago")
        
        
        //if thumbnail image and video player image is present, priority will go to video thumbnail.
        //And video thumbnail will be shown.
        if (timelineItem.video_thumb.isEmpty == false)
        {
            self.videoContainerView.isHidden     = false
            self.thumbnailContainerView.isHidden = true
            
            let video_thumb = timelineItem.video_thumb.split(separator: ",")
            if (video_thumb.count > 0)
            {
                self.videoImageView.sd_setImage(with: URL(string: String(video_thumb[0])), placeholderImage: UIImage(named: "vc_play_circle_trans"))
            }
            else
            {
                self.videoImageView.image = UIImage(named: "vc_play_circle_trans");
            }
        }
        else if (timelineItem.image.isEmpty == false)
        {
            self.thumbnailContainerView.isHidden = false
            self.videoContainerView.isHidden     = true
            
            let postimg = timelineItem.image.split(separator: ",")
            self.thumbnailImageView.sd_setImage(with: URL(string: String(postimg[0])), placeholderImage: UIImage(named: "vc_ops_gallery"))
        }
        else // there is no image or video thumbnail present
        {
            self.videoContainerView.isHidden     = true
            self.thumbnailContainerView.isHidden = true
        }
        
        var titleStr = timelineItem.title
        if titleStr.isEmpty == false
        {
            self.descriptionContainerView.isHidden = false;
            if((titleStr.count)>350)
            {
                // Read more required 350 can end up in between of line.
                titleStr = (titleStr.substring(with: 0..<350)) + "...<strong><font color=red>Read More</font></strong>"
            }
            self.descriptionLabel.attributedText = Utility.getHTMLToString(titleStr: titleStr)
        }
        else
        {
            self.descriptionContainerView.isHidden = true;
        }
        
        if self.shouldShowViewImagesButton(item: timelineItem) == true
        {
            self.viewImagesButton.isHidden = false
        }
        else
        {
            self.viewImagesButton.isHidden = true
        }
        
        let postPdf = timelineItem.pdf.split(separator: ",")
        if (postPdf.count > 0)
        {
            self.pdfContainerView.isHidden = false
            self.pdfNameLabel.text         = timelineItem.pdf_name
        }
        else
        {
            self.pdfContainerView.isHidden = true
        }
        
        self.likeLabel.text = timelineItem.count_like + " Likes"
        if(timelineItem.is_liked == "0")
        {
            self.makeLikeButtonSelected(false)
        }
        else
        {
            self.makeLikeButtonSelected(true)
        }
        
        self.commentLabel.text = timelineItem.count_comment + " Comments"
        if(timelineItem.is_favorite == "0")
        {
            self.makeFavoriteButtonSelected(false)
        }
        else
        {
            self.makeFavoriteButtonSelected(true)
        }
        
        
        //CHANGING FRAME
        var top:CGFloat = 78.0;
        
        if (self.shouldShowVideoOrThumbnailForTimeline(item: timelineItem) == true)
        {
            top = top + self.videoContainerView.frame.height + 2 // 2 Margin
        }
        var  height:CGFloat = (self.descriptionLabel.attributedText?.height(withConstrainedWidth: self.descriptionLabel.frame.width))!

        if (timelineItem.title.isEmpty == false)
        {
            
            if height > CGFloat(68.0) // Minimum height
            {
                height = 68 + (height - 68) + 6 // 6 Margin
            }
            else
            {
                height = 68 + 6 // 6 Margin
            }
            
            self.descriptionContainerView.frame.origin.y    = top
            self.descriptionContainerView.frame.size.height = height
        }
        else
        {
            height = 5.0 // margin
        }
        
        top = top + height
        
        self.viewImagesButton.frame.origin.y = top + 2
        if (self.shouldShowViewImagesButton(item: timelineItem) == true)
        {
            top = top + self.viewImagesButton.frame.height + 2 // margin
        }
        
        //showing pdf container
        self.pdfContainerView.frame.origin.y = top + 2 // 2 margin
        if timelineItem.pdf.isEmpty == false
        {
            top = top + self.pdfContainerView.frame.height + 2
        }
        self.footerView.frame.origin.y = top;
        self.itemContainerView.frame.size.height = top + self.footerView.frame.height + 2 //2 margin
    }
    
    func getHeightOfCellFor(item timelineItem:OTLModal) -> CGFloat
    {
        //CHANGING FRAME
        var top:CGFloat = 78.0;
        
        if (self.shouldShowVideoOrThumbnailForTimeline(item: timelineItem) == true)
        {
            top = top + self.videoContainerView.frame.height + 2 // 2 Margin
        }
        else
        {
            //top = top - self.videoContainerView.frame.height
        }
        
       
        var height:CGFloat = 0.0
        if timelineItem.title.isEmpty == false
        {
            var titleStr = timelineItem.title
            if((titleStr.count)>350)
            {
                titleStr = (titleStr.substring(with: 0..<350)) + "...<strong><font color=red>Read More</font></strong>"
            }
            
            self.descriptionLabel.attributedText = Utility.getHTMLToString(titleStr: titleStr)
            height = (self.descriptionLabel.attributedText?.height(withConstrainedWidth: self.descriptionLabel.frame.width))!
            if height > CGFloat(68.0) // Minimum height
            {
                height = 68 + (height - 68) + 6 // 6 Margin
            }
            else
            {
                height = 68 + 6 // 6 Margin
            }
        }
        else
        {
            height = 5 // margin
        }
    
        top = top + height
        
        if (self.shouldShowViewImagesButton(item: timelineItem) == true)
        {
            top = top + self.viewImagesButton.frame.height + 2 // margin
        }
    
        if timelineItem.pdf.isEmpty == false
        {
            top = top + self.pdfContainerView.frame.height + 2
        }
        
        return top + self.footerView.frame.height + 2 + 8 //bottom 4, top 4 margin
    }
    
    //MARK:- Action Methods
    @IBAction func likeButtonAction(_ sender: UIButton)
    {
        print("sender -> didTapLikeButton")
        self.cellDelegate?.cell(self, didTapLikeButton: sender)
    }
    
    @IBAction func commentButtonAction(_ sender: UIButton)
    {
        print("sender -> didTapCommentButton")
        self.cellDelegate?.cell(self, didTapCommentButton: sender)
    }
    
    @IBAction func reportButtonAction(_ sender: UIButton)
    {
        print("sender -> didTapReportButton")
        self.cellDelegate?.cell(self, didTapReportButton: sender)
    }

    @IBAction func followButtonAction(_ sender: UIButton)
    {
        print("sender -> didTapFollowButton")
        self.cellDelegate?.cell(self, didTapFollowButton: sender)
    }
    
    @IBAction func favoriteButtonAction(_ sender: UIButton)
    {
        print("sender -> didTapFavoriteButton")
        self.cellDelegate?.cell(self, didTapFavoriteButton: sender)
    }
    @IBAction func viewImageButtonAction(_ sender: UIButton)
    {
        print("sender -> didTapViewImageButton")
        self.cellDelegate?.cell(self, didTapViewImageButton: sender)
    }
    
    @IBAction func thumbnailImageButtonAction(_ sender: UIButton)
    {
        print("sender -> didTapThumbnailImageButton")
        self.cellDelegate?.cell(self, didTapThumbnailImageButton: sender)
    }
    
    @IBAction func playVideoButtonAction(_ sender: UIButton)
    {
        print("sender -> didTapPlayVideoButton")
        self.cellDelegate?.cell(self, didTapPlayVideoButton: sender)
    }
    
    @IBAction func openPDFButtonAction(_ sender: UIButton)
    {
        print("sender -> didTapOpenPDFButton")
        self.cellDelegate?.cell(self, didTapOpenPDFButton: sender)
    }
    
    @IBAction func seeFullTimelineDescriptionButtonAction(_ sender: UIButton)
    {
        print("sender -> didTapDescriptionButton")
        if self.timelineItem?.title.isEmpty == false
        {
            //if description is less than 350 character no detail need to be shown.
            if (self.timelineItem?.title)!.length <= 350 { return }
            print("sender -> didTapDescriptionButton -> Read More")
            self.cellDelegate?.cell(self, didTapDescriptionButton: sender)
        }
    }
    
    //MARK:- Helper and private methods
    private func shouldShowVideoOrThumbnailForTimeline(item:OTLModal) -> Bool
    {
        var shouldShow = false
        if (item.video_thumb.isEmpty == false)
        {
           shouldShow = true
        }
        else if (item.image.isEmpty == false)
        {
            shouldShow = true
        }
        else // there is no image or video thumbnail present
        {
            shouldShow = false
        }
        return shouldShow
    }
    
    private func shouldShowViewImagesButton(item:OTLModal) -> Bool
    {
        //if TIMELINE data has video url as well image, then viewImage button will be shown
        var shouldShow = false
        if item.video_thumb.isEmpty == false, item.image.isEmpty == false
        {
            shouldShow = true
        }
        return shouldShow
    }
    
    func makeFavoriteButtonSelected(_ selected:Bool)
    {
        if selected == true
        {
            self.favouriteButton.setImage(UIImage(named:"vc_favorite"), for: .normal)
        }
        else
        {
            self.favouriteButton.setImage(UIImage(named:"vc_favorite_normal"), for: .normal)
        }
    }
    
    func makeLikeButtonSelected(_ selected:Bool)
    {
        if selected == true
        {
            self.likeButton.setImage( UIImage(named: "vc_like_true"), for: .normal)
        }
        else
        {
            self.likeButton.setImage( UIImage(named: "vc_like_false"), for: .normal)
        }
    }
}

extension String
{
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect,
                                            options: .usesLineFragmentOrigin,
                                            attributes: [.font : font], context: nil)

        return ceil(boundingBox.height)
    }

    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect,
                                            options: .usesLineFragmentOrigin,
                                            attributes: [.font : font], context: nil)

        return ceil(boundingBox.width)
    }
}

extension NSAttributedString
{
    func height(withConstrainedWidth width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = boundingRect(with: constraintRect,
                                       options: .usesLineFragmentOrigin,
                                       context: nil)

        return ceil(boundingBox.height)
    }

    func width(withConstrainedHeight height: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = boundingRect(with: constraintRect,
                                       options: .usesLineFragmentOrigin,
                                       context: nil)

        return ceil(boundingBox.width)
    }
}

