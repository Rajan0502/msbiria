//
//  SpecificationXIB.swift
//  Secure
//
//  Created by Apple on 5/20/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class AddQualificationView: UIView {

    @IBOutlet weak var textCollegeName: UITextField!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var textSpeciality: UITextField!
    @IBOutlet weak var textCourseName: UITextField!
    @IBOutlet weak var viewEndDate: UIView!
    @IBOutlet weak var txtStartFrom: UITextField!
    @IBOutlet weak var txtDateEnd: UITextField!
    
    
    var is_current : String = "";
    @IBAction func tapCheck(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        viewEndDate.isHidden = sender.isSelected
        if(sender.isSelected)
        {
            is_current = "Currently Study Heare"
        }
        else
        {
            is_current = "";
        }
    }
    
    @IBAction func tapSave(_ sender: Any) {
        
          let params = ["college_name": textCollegeName.text!, "degree_name":textCourseName.text!, "token": token, Constant.kAPI_NAME: addUserEducation, "start_year":txtStartFrom.text!, "end_year":txtDateEnd.text!,"speciality_name": textSpeciality.text! , "is_current":is_current]
        
        if(params["college_name"] == "")
        {
            ShowToast.showPositiveMessage(message: "Please enter your college name.")
        }
        else if(params["degree_name"] == "")
        {
            ShowToast.showPositiveMessage(message: "Please enter your course")
        }
        else if(params["speciality_name"] == "")
        {
            ShowToast.showPositiveMessage(message: "Please enter your Speciality")
        }
        else if(params["start_year"] == "")
        {
            ShowToast.showPositiveMessage(message: "Please enter from date")
        }
        else if(is_current != "Currently Study Heare")
        {
            if(params["end_year"] == "")
            {
                ShowToast.showPositiveMessage(message: "Please enter end date")
            }
        }
        else{
        
         
            
            SwiftLoader.show(title: "Loading...", animated: true)
            SGServiceController.instance().hitPostService(params, unReachable: {
                print("no internet")
                SwiftLoader.hide()
            }) { (response) in
                SwiftLoader.hide()
                if response == nil {
                    return
                }
                profileInstance.getProfile()
                print(response)
                UIView.animate(withDuration: 0.5, animations: {
                    self.alpha = 0.0
                }) { (complete) in
                    self.removeFromSuperview()
                }
            }
        }
    }
    
    @IBAction func tapCancel(_ sender: Any) {
        UIView.animate(withDuration: 0.5, animations: {
            self.alpha = 0.0
        }) { (complete) in
            self.removeFromSuperview()
        }
    }
    
}
