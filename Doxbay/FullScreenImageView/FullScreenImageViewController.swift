//
//  FullScreenImageViewController.swift
//  Doxbay
//
//  Created by Rajan Tiwari on 28/07/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit

class FullScreenImageViewController: UIViewController {

    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var fullScreenImageView:UIImageView!
    @IBOutlet weak var wrapperScrollView:UIScrollView!
    
    let MAXIMUM_ZOOM_SCALE:CGFloat = 3.0
    let DEFAULT_ZOOM_SCALE:CGFloat = 1.0
    
    var image:UIImage?
    var imageURLSting:String?
    var doublTapGesture:UITapGestureRecognizer?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.setupImages()
    }
    
    func addDoubleTapGestureToFullScreenImage()
    {
        if self.doublTapGesture == nil
        {
            self.doublTapGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.handleDoubleTapGestureAction(_:)))
            self.doublTapGesture?.numberOfTapsRequired = 2
            self.wrapperScrollView.addGestureRecognizer(self.doublTapGesture!)
//            self.fullScreenImageView.addGestureRecognizer(self.doublTapGesture!)

        }
    }
    func setupUI()
    {
        self.wrapperScrollView.isScrollEnabled        = true;
        self.wrapperScrollView.zoomScale              = self.DEFAULT_ZOOM_SCALE;
        self.wrapperScrollView.maximumZoomScale       = self.MAXIMUM_ZOOM_SCALE;
        self.fullScreenImageView.image = self.image
        self.setupImages()
        self.addDoubleTapGestureToFullScreenImage()
    }
    
    func setupImages()
    {
        if let imageURLSting =  self.imageURLSting
        {
            let imageURLs = imageURLSting.split(separator: ",")
            if imageURLs.isEmpty == false, let imageURL = imageURLs.first, let url = URL(string:String(imageURL))
            {
                self.fullScreenImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "vc_ops_gallery"))
            }
        }
    }
    
    @IBAction func cancelButtonAction(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    class func GetFullScreenImageViewController() -> FullScreenImageViewController
    {
        let sb = UIStoryboard(name: "FullScreenImageViewScene", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "FullScreenImageViewController") as? FullScreenImageViewController
        return vc!
    }
    
    func zoomRectForScale(_ scale:CGFloat, withCenter center:CGPoint) -> CGRect
    {
        var zoomRect:CGRect = .zero
        zoomRect.size.height = self.wrapperScrollView.frame.size.height / scale
        zoomRect.size.width  = self.wrapperScrollView.frame.size.width  / scale
        
        //choose an origin so as to get the right center.
        zoomRect.origin.x =  center.x - (zoomRect.size.width / 2.0)
        zoomRect.origin.y =  center.y - (zoomRect.size.height / 2.0)
        
        return zoomRect
    }
    
    @objc func handleDoubleTapGestureAction(_ sender:UITapGestureRecognizer)
    {
        if self.wrapperScrollView.zoomScale == DEFAULT_ZOOM_SCALE
        {
            let newScale =  self.wrapperScrollView.zoomScale * MAXIMUM_ZOOM_SCALE
            let touchedPoint = sender .location(ofTouch: 0, in: sender.view)
            let zoomRect   = self.zoomRectForScale(newScale, withCenter: touchedPoint)
            self.wrapperScrollView.zoom(to: zoomRect, animated: true)
        }
        else
        {
            UIView.animate(withDuration: 0.33, animations: {
                self.wrapperScrollView.zoomScale = self.DEFAULT_ZOOM_SCALE
            })
        }
    }
    
}

extension FullScreenImageViewController:UIScrollViewDelegate
{
    func viewForZooming(in scrollView: UIScrollView) -> UIView?
    {
        return self.fullScreenImageView
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat)
    {
        var zoomScale:CGFloat = DEFAULT_ZOOM_SCALE
        if (scale > MAXIMUM_ZOOM_SCALE)
        {
            zoomScale = MAXIMUM_ZOOM_SCALE
        }
        else if (scale < DEFAULT_ZOOM_SCALE)
        {
            zoomScale = DEFAULT_ZOOM_SCALE
        }
        else
        {
            zoomScale = scale
        }
        
        UIView.animate(withDuration: 0.33) {
            self.wrapperScrollView.zoomScale = zoomScale
        }
    }
}


