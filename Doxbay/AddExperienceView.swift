//
//  AddExperienceView.swift
//  DoxbaySalman
//
//  Created by AppsInvo  on 20/05/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit

class AddExperienceView: UIView {

    @IBOutlet weak var txtDateEnd: UITextField!
    @IBOutlet weak var txtDateForm: UIView!
    @IBOutlet weak var txtDesignation: UITextField!
    @IBOutlet weak var txtOrgName: UITextField!
    @IBOutlet weak var viewEndDate: UIView!
    @IBOutlet weak var txtStartFrom: UITextField!
    @IBOutlet weak var textSpeciality: UITextField!
    
    
    @IBAction func tapSave(_ sender: Any)
    {
        
        let params = ["hospital_name": txtOrgName.text!, "designation_name":txtDesignation.text!, "token": token, Constant.kAPI_NAME: addUserJob, "start_year":txtStartFrom.text!, "end_year":txtDateEnd.text!,"speciality_name": textSpeciality.text! , "is_current":isWorking]
        
        if(params["hospital_name"] == "")
        {
            ShowToast.showPositiveMessage(message: "Please enter organization name")
        }
        else if(params["designation_name"] == "")
        {
            ShowToast.showPositiveMessage(message: "Please enter your desigantion")
        }
        else if(params["speciality_name"] == "")
        {
            ShowToast.showPositiveMessage(message: "Please enter specialites")
        }
        else if(params["start_year"] == "")
        {
            ShowToast.showPositiveMessage(message: "Please enter start date")
        }
        else if(isWorking == "Not Working")
        {
            if(params["end_year"] == "")
            {
                ShowToast.showPositiveMessage(message: "Please enter end date")
            }
        }
        else{
            
            
            print("Add Experiencee :--- \(params)")
            
            SwiftLoader.show(title: "Loading...", animated: true)
            SGServiceController.instance().hitPostService(params, unReachable: {
                print("no internet")
                SwiftLoader.hide()
            }) { (response) in
                SwiftLoader.hide()
                if response == nil {
                    return
                }
                profileInstance.getProfile()
                UIView.animate(withDuration: 0.5, animations: {
                    self.alpha = 0.0
                }) { (complete) in
                    self.removeFromSuperview()
                }
            }
        }
    }
    
    @IBAction func tapCancel(_ sender: Any) {
        UIView.animate(withDuration: 0.5, animations: {
            self.alpha = 0.0
        }) { (complete) in
            self.removeFromSuperview()
        }
    }

    
    var isWorking : String = "Not Working"
    @IBAction func tapCheck(_ sender: UIButton)
    {
        sender.isSelected = !sender.isSelected
        viewEndDate.isHidden = sender.isSelected
        if(sender.isSelected)
        {
            isWorking = "Working"
        }
        else
        {
            isWorking = "Not Working"
        }
        
    }
}
