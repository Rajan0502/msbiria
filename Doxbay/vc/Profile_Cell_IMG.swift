//
//  Profile_CellIMGTableViewCell.swift
//  Doxbay
//
//  Created by Luminous on 21/06/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit

class Profile_Cell_IMG : UITableViewCell {

  
    @IBOutlet weak var userProfilePic: UIImageView!
    
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var userPTitle: UILabel!
    
    @IBOutlet weak var userPostTypeDateTime: UILabel!
    
    @IBOutlet weak var postDescription: UILabel!
    
    @IBOutlet weak var imgIsFavorites: UIImageView!
    
    @IBOutlet weak var imgIsPostLike: UIImageView!
    
    @IBOutlet weak var likeCount: UILabel!
    
    @IBOutlet weak var imgPostComment: UIImageView!
    
    @IBOutlet weak var lblReport: UILabel!
    @IBOutlet weak var commentCount: UILabel!
    
    
    @IBOutlet weak var imgReport: UIImageView!
    
    @IBOutlet weak var postImage: UIImageView!
    
    
    @IBOutlet var eventHeader: UILabel!
    
    
    @IBOutlet var vc_play: UIImageView!
    
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
