//
//  VerifyOTPVC.swift
//  Secure
//
//  Created by Apple on 5/20/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class VerifyOTPVC: UIViewController {

    var moileNo : String!
    var  Details : String!
    
    @IBOutlet weak var sholast4Number: UILabel!
    @IBOutlet weak var txtOTP: UITextField!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.hideKeyboardOnTap(#selector(self.dismissKeyboard))
        
       sholast4Number.text = "xxxxxx" + moileNo.substring(from:moileNo.index(moileNo.endIndex, offsetBy: -4))
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    
    @IBAction func tapBack(_ sender: Any) {
         if(isFromLoginPage)
         {
            self.dismiss(animated: true, completion: nil)
        }
        if(isForEdit)
        {
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    @IBAction func tapVerify(_ sender: Any) {
        let todoEndpoint: String = VerityOtp + self.Details + "/" + txtOTP.text!;
        print("Verify OTP API \(todoEndpoint)")
        SwiftLoader.show(title: "Resending OTP Request..", animated: true)
        Alamofire.request(todoEndpoint)
            .responseJSON { response in
                // handle JSON
            }
            .responseString { response in
                if let error = response.result.error {
                    print(error)
                    SwiftLoader.hide()
                }
                if let value = response.result.value {
                    SwiftLoader.hide()
                    print(value)
                    let swiftyJsonVar = JSON(response.data)
                    let Status = swiftyJsonVar["Status"].stringValue
                    if(Status == "Success")
                    {
                        self.Details = swiftyJsonVar["Details"].stringValue
                        self.userValidate();
                    }
                    else
                    {
                        ShowToast.showPositiveMessage(message: "Wrong OTP")
                    }
                }
        }
    }
    
    @IBAction func tapResend(_ sender: Any) {
        // https://2factor.in/API/V1/{api_key}/SMS/{phone_number}/AUTOGEN
        let todoEndpoint: String = generateSMSOtp  + moileNo! + "/AUTOGEN";
        print("Snd OTP Request API \(todoEndpoint)")
        SwiftLoader.show(title: "Verifying..", animated: true)
        Alamofire.request(todoEndpoint)
            .responseJSON { response in
                // handle JSON
            }
            .responseString { response in
                if let error = response.result.error {
                    print(error)
                    SwiftLoader.hide()
                }
                if let value = response.result.value {
                    SwiftLoader.hide()
                    print(value)
                    let swiftyJsonVar = JSON(response.data)
                    let Status = swiftyJsonVar["Status"].stringValue
                    self.Details = swiftyJsonVar["Details"].stringValue
                    print("Status --- \(Status) ---Details -----\(self.Details)")
                    ShowToast.showPositiveMessage(message: "OTP successfully created and sent to your mobile no. ")
                }
        }
    }
    
    
    
    
  
    func userValidate()
    {
        SwiftLoader.show(title: "Please wait..", animated: true)
        let requestURL = BASEURL + user_validate
        let parameters: [String:String] = ["is_valid_user": "1" ,"token": token,"mobile": self.moileNo] //self.moileNo
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            let message = response["data"]["message"].stringValue
            print("xxxxxxxx   ---- \(success)--  \(response)")
            ShowToast.showPositiveMessage(message: message)
            if success
            {
                 is_valid_user = 1
            }
            else{
            }
            
            if(isFromLoginPage)
            {
                self.dismiss(animated: true, completion: nil)
            }
            if(isForEdit)
            {
                self.navigationController?.popViewController(animated: true)
            }
            else
            {
                self.dismiss(animated: true, completion: nil)
            }
            
            
            
            
        }, failure:  {
            (error) -> Void in
            SwiftLoader.hide()
            print("error===\(error)")
        })
    }
}
