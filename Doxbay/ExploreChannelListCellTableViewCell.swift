//
//  ExploreChannelListCellTableViewCell.swift
//  Doxbay
//
//  Created by Luminous on 09/05/18.
//  Copyright © 2018 Yuji Hato. All rights reserved.
//

import UIKit

class ExploreChannelListCellTableViewCell: UITableViewCell {

    
    @IBOutlet var channelImg: UIImageView!
    @IBOutlet var chanelName: UILabel!
    @IBOutlet var totalChannelPost: UILabel!
    @IBOutlet var labNewPostCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
