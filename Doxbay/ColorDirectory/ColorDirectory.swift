//
//  ColorDirectory.swift
//  Doxbay
//
//  Created by Rajan Kumar Tiwari on 24/07/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import Foundation
import UIKit

extension UIColor
{
    //MARK: HEX to UIColor
    convenience init(red: Int, green: Int, blue: Int)
    {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(hexCode:Int)
    {
        self.init(red:(hexCode >> 16) & 0xff, green:(hexCode >> 8) & 0xff, blue:hexCode & 0xff)
    }
    
    class var ISPLineGray: UIColor
    {
        return UIColor(hexCode: 0xebebeb)
    }
    
    class var cusomCellBackgroundColor:UIColor
    {
        return UIColor.init(red: 235, green: 235, blue: 241)
    }
}
