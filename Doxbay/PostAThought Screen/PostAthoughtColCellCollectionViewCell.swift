//
//  PostAthoughtColCellCollectionViewCell.swift
//  Doxbay
//
//  Created by kavi yadav on 11/06/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit

class PostAthoughtColCellCollectionViewCell: UICollectionViewCell {
   
    @IBOutlet weak var thoughtImgView: UIImageView!
    
}
