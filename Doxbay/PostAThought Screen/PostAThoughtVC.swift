//
//  PostAThoughtVC.swift
//  Doxbay
//
//  Created by AppsInvo  on 15/05/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import AVKit
import AVFoundation
import MobileCoreServices
import SwiftyJSON
import DKImagePickerController


class PostAThoughtVC: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIDocumentPickerDelegate,UIDocumentMenuDelegate,UITextViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource {
    
    
    var isFromPage : String = "";
    var isFromPageType : String = "";
    
    
    
    
    @IBOutlet weak var PostCollectionView: UICollectionView!
    
    var imagePicker = UIImagePickerController()
    let pickerController = DKImagePickerController()
    
    
    var thoughtSelected:Bool = true
    var eventSelected:Bool = false
    
    
    var PostVideoBool:Bool = false
    var PostFileBool:Bool = false
    var pdfPickerArray = [URL]()
    var imageNameUploadArray = [Any]()
    var videoURLArray = [String]()
    
    var timeLineArray1 = [Any]()
    var imagePickerArray = [Any]()
    
    
    
    @IBOutlet weak var tblVw: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func tapBack(_ sender: Any) {
        let aa = "sdhjgdsjhgds"
          print("xxxxxxxx \(aa)")
    //    navigationController?.popViewController(animated: true)
        dismiss(animated: false, completion: nil)
        print("xxxxxxxx")
//        if isFromPage == "Profile"
//        {
//          self.dismiss(animated: false, completion: nil)
//        }
//        else
//        {
//          self.navigationController?.popViewController(animated: true)
//        }
    }
    
    
    
    
    
    
    @objc func thoughtBtnClk(sender: UIButton!)
    {
        if(!sender.isSelected)
        {
            let indexPath = IndexPath(row: sender.tag, section: 0)
            let cell = self.tblVw.cellForRow(at: indexPath) as! PostStatusTableViewCell
            
            cell.eventLbl.textColor = UIColor.colorWithHexString(hexStr: "#888da8")
            cell.thoughtLbl.textColor = UIColor.black
            
            
            thoughtSelected = true
            eventSelected = false
            
            
            cell.thoughtImgVw.image =   cell.thoughtImgVw.image!.withRenderingMode(.alwaysTemplate)
            cell.thoughtImgVw.tintColor = UIColor.red
            
            cell.eventImgVw.image =   cell.eventImgVw.image!.withRenderingMode(.alwaysTemplate)
            cell.eventImgVw.tintColor = UIColor.colorWithHexString(hexStr: "#888da8")
            
            
            cell.btn1.isSelected = false
            cell.btn2.isSelected = false
            cell.btn3.isSelected = false
            
        }
        
    }
    @objc func eventBtnClk(sender: UIButton!)
    {
        if(!sender.isSelected)
        {
            let indexPath = IndexPath(row: sender.tag, section: 0)
            let cell = self.tblVw.cellForRow(at: indexPath) as! PostStatusTableViewCell
            cell.thoughtLbl.textColor = UIColor.colorWithHexString(hexStr: "#888da8")
            cell.eventLbl.textColor = UIColor.black
            
            cell.eventImgVw.image =   cell.eventImgVw.image!.withRenderingMode(.alwaysTemplate)
            cell.eventImgVw.tintColor = UIColor.red
            
            cell.thoughtImgVw.image =   cell.thoughtImgVw.image!.withRenderingMode(.alwaysTemplate)
            cell.thoughtImgVw.tintColor = UIColor.colorWithHexString(hexStr: "#888da8")
            
            eventSelected = true
            thoughtSelected = false
            
            cell.btn1.isSelected = true
            cell.btn2.isSelected = true
            cell.btn3.isSelected = true
        }
    }
    
    
    
    
    
    // for PDF file
    @objc func btn3Clk(sender: UIButton!)
    {
        self.PostFileBool = true
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Please select", message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            print("Cancel")
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: "File", style: .default)
        { _ in
            
            let types: NSArray = NSArray(object: kUTTypePDF as NSString)
            let documentPicker = UIDocumentPickerViewController(documentTypes: types as! [String], in: .import)
            documentPicker.delegate = self
            documentPicker.modalPresentationStyle = .formSheet
            self.present(documentPicker, animated: true, completion: nil)
        }
        actionSheetControllerIOS8.addAction(saveActionButton)
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    
    
    // for video
    @objc func btn2Clk(sender: UIButton!)
    {
        self.PostVideoBool = true
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Please select", message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            print("Cancel")
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: "Video", style: .default)
        { _ in
            
            
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.delegate = self
            self.imagePicker.mediaTypes = ["public.movie"]
            
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        actionSheetControllerIOS8.addAction(saveActionButton)
        
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    
    
    
    // for photo
    @objc func btn1Clk(sender: UIButton!)
    {
        self.PostVideoBool = false
        self.imagePicker.delegate = self
        self.imagePicker.mediaTypes = [kUTTypeImage as String]
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Please select", message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            print("Cancel")
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: "Camera", style: .default)
        { _ in
            self.imagePicker.allowsEditing = false
            
            self.imagePicker.sourceType = .camera
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        
        actionSheetControllerIOS8.addAction(saveActionButton)
        let deleteActionButton = UIAlertAction(title: "Gallery", style: .default)
        { _ in
            
            self.pickerController.sourceType = .photo
            self.pickerController.assetType = .allPhotos
            self.pickerController.didSelectAssets = { (assets: [DKAsset]) in
                print("didSelectAssets")
                for asset in assets {
                    asset.fetchOriginalImage(true, completeBlock: { (pickedImage, info) in
                        print(pickedImage)
                        self.imagePickerArray.append(pickedImage)
                        let imageDic: [String:Any] = ["name":"image.png","image":pickedImage]
                        self.PostCollectionView.isHidden = false
                        
                        self.imageNameUploadArray.append(imageDic)
                        self.PostCollectionView.reloadData()
                        // self.tblVw.reloadData()
                    })
                }
            }
            self.present(self.pickerController, animated: true) {}
        }
        actionSheetControllerIOS8.addAction(deleteActionButton)
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    
    @objc func cancelBtnClk(sender: UIButton!)
    {
        let indexPath = IndexPath(row: sender.tag, section: 1)
        let cell = self.tblVw.cellForRow(at: indexPath) as! MainHomeTableViewCell
        // cell.postTxtVw.text = ""
        
    }
    @objc func cameraBtnClk(sender: UIButton!)
    {
        
    }
    
    
    
    
    // post thought API
    @objc func postUserBtnClk(sender: UIButton!)
    {
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let cell = self.tblVw.cellForRow(at: indexPath) as! PostStatusTableViewCell
        let postTxt = cell.txtVwUserPost.text!
        
        if (postTxt == "Share what you are thinking here..")
        {
            ShowToast.showNegativeMessage(message: "Enter Text");
            return;
        }
        
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Loading"
        
        let data = (postTxt).data(using: String.Encoding.utf8)
        let base64 = data!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        
        
        
        var requestURL = ""
        var dataParam: [String:String] = ["":""]
        
        if(isFromPageType == "UserPost")
        {
            requestURL = BASEURL + createUserPost
            dataParam = ["dox_id": dox_id ,"for_uuid": for_uuid , "title": base64 ,"category":"Post"]
        }
        else
        {
            requestURL = BASEURL + addNewPostChannel
            dataParam = ["dox_id": dox_id ,"channel_id": channel_id , "title": base64 ,"category":"Post"]
        }
        
        
        
        
        if(videoURLArray.count>0)
        {
            let videoURLS = videoURLArray.joined(separator: ",")
            dataParam["video_url"] = videoURLS
            
        }
        var dataStr = ""
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dataParam, options: .prettyPrinted)
            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: [])
            if let dictFromJSON = decoded as? [String:String] {
                print(dictFromJSON)
                dataStr = String(describing: dictFromJSON)
            }
        } catch {
            print(error.localizedDescription)
        }
        
        dataStr=dataStr.replacingOccurrences(of: "[", with: "{")
        dataStr=dataStr.replacingOccurrences(of: "]", with: "}")
        
        
        let parameters: [String:String] = ["data":dataStr,"token": token]
        
        
        
        print("-----------POST THOUGHT START-----------------")
        print(" Request URL- \(requestURL)  Param --- \(parameters) ----")
        print(" Image Array========== -----\(imagePickerArray) ---")
        print(" Video Array========== \(dataParam["video_url"])-----")
        print(" Image PDF Array========== -----\(self.pdfPickerArray) -----")
        
        
        UCUtil.requestPOSTURLWithFormDataAndImageArrayAndPDFArray(requestURL, params: parameters, headers: nil,imageArray:imagePickerArray,pdfArray:pdfPickerArray, success: {(response) -> Void in
            
            
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--")
            
            if success
            {
                // self.callapi()
                cell.txtVwUserPost.text = "Share what you are thinking here.."
                
                
            }
            
            self.imagePickerArray.removeAll()
            self.videoURLArray.removeAll()
            self.imageNameUploadArray.removeAll()
            self.pdfPickerArray.removeAll()
            let indexPath = IndexPath(row: 0, section: 0)
            self.PostCollectionView.isHidden = true
            self.PostCollectionView.reloadData()
            self.tblVw.reloadRows(at: [indexPath], with: .none)
            
            MBProgressHUD.hide(for: self.view, animated: false)
            //ShowToast.showPositiveMessage(message: "Successfully post.");
            if self.isFromPage == "Profile"
            {
                self.dismiss(animated: true, completion: nil)
            }
            else
            {
                self.navigationController?.popViewController(animated: true)
            }
            
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
        })
    }
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        do {
            
            if self.PostVideoBool
            {
                self.PostCollectionView.isHidden = true
                
                let videoURL = info["UIImagePickerControllerMediaURL"] as? NSURL
                
                let data = try! Data(contentsOf:videoURL! as URL)
                
                let bcf = ByteCountFormatter()
                bcf.allowedUnits = [.useMB]  //optional: restricts the units to MB only
                bcf.countStyle = .file
                let string = bcf.string(fromByteCount: Int64(data.count))
                if let mb = Double(string) {
                    if mb > 50.0 {
                        self.showOkAlert("Maximum video size 50 mb")
                        return
                    }
                }
                uploadVideo(videoURL: videoURL!)
                
                let videoName = videoURL?.lastPathComponent
                let thumbnailImage = self.getThumbnailImage(forUrl: URL(string: String(describing: videoURL))!)
                
                let imageDic: [String:Any] = ["name":videoName ?? "","image":thumbnailImage ?? UIImage(named:"")]
                
                imageNameUploadArray.append(imageDic)
                let indexPath = IndexPath(row: 0, section: 0)
                self.tblVw.reloadRows(at: [indexPath], with: .none)
                
                DispatchQueue.global(qos: .background).async {
                    
                    let videoName = videoURL?.lastPathComponent
                    let videoNamePost = "video.mp4"
                    
                    let thumbnailImage = self.getThumbnailImage(forUrl: videoURL as! URL)
                    
                    DispatchQueue.main.sync
                        {
                            let imageDic: [String:Any] = ["name":videoNamePost ?? "","image":thumbnailImage ?? UIImage(named:"")]
                            
                            self.imageNameUploadArray.append(imageDic)
                            let indexPath = IndexPath(row: 0, section: 0)
                            self.tblVw.reloadRows(at: [indexPath], with: .none)
                            
                    }
                }
            }
            else
            {
                if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
                {
                    imagePickerArray.append(pickedImage)
                    let imageDic: [String:Any] = ["name":"image.png","image":pickedImage]
                    imageNameUploadArray.append(imageDic)
                    self.tblVw.reloadData()
                }
            }
            
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    
    
    //CollectionView Delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imageNameUploadArray.count
    }
    
    
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ColCell", for: indexPath as IndexPath) as! PostAthoughtColCellCollectionViewCell
        
        let selectedImage = self.imageNameUploadArray[indexPath.item]as! NSDictionary
        let uploadedimage = selectedImage.value(forKey: "image") as? UIImage
        var image = UIImage()
        if uploadedimage != nil {
            image = uploadedimage!
            
            cell.thoughtImgView.image = image
        }
        return cell
    }
    
    
    
    
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: documentPicker
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        
        let myURL = url as URL
        
        print("import result : \(myURL)")
        pdfPickerArray.append(myURL)
        
        let imageDic: [String:Any] = ["name":"pdfFile.pdf","image":""]
        imageNameUploadArray.append(imageDic)
        let indexPath = IndexPath(row: 0, section: 0)
        self.tblVw.reloadRows(at: [indexPath], with: .none)
        
        //optional, case PDF -> render
        
        //displayPDFweb.loadRequest(NSURLRequest(url: cico) as URLRequest)
    }
    
    
    
    
    @available(iOS 8.0, *)
    public func documentMenu(_ documentMenu:     UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
    
    func getThumbnailImage(forUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(1, 60) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
        }
        return nil
    }
    
    
    
    
    
    
    
    
    
    // Upload Video API
    func uploadVideo(videoURL:NSURL)
    {
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Uploading Video"
        
        var videoPathArray = [Any]()
        videoPathArray.append(videoURL)
        let requestURL = BASEURL + addVideo
        let parameters: [String:String] = ["token": token]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTvideoWithUrl(requestURL, params: parameters, headers: nil,videoArray:videoPathArray, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--")
            
            if success
            {
                let uplodedVideoUrl = response["url"].string
                self.videoURLArray.append(uplodedVideoUrl!)
            }
            MBProgressHUD.hide(for: self.view, animated: false)
            
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
            
        })
    }
    
    
    
    
    
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if textView.text == "Share what you are thinking here.." {
            textView.text = ""
        }
    }
}




extension PostAThoughtVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cellIdentifier = "PostStatusTableViewCell"
        
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? PostStatusTableViewCell
        if cell == nil {
            var topLevelObjects = Bundle.main.loadNibNamed("PostStatusTableViewCell", owner: self, options: nil)
            cell = topLevelObjects?[0] as? PostStatusTableViewCell
            cell?.selectionStyle = .none
        }
        cell?.imgVw.layer.cornerRadius =  (cell?.imgVw.frame.size.width)! / 2;
        cell?.imgVw.layer.masksToBounds = true
        
        cell?.postBtn.tag = indexPath.row
        cell?.postBtn.addTarget(self, action: #selector(postUserBtnClk), for: .touchUpInside)
        
        cell?.thoughtBtn.tag = indexPath.row
        cell?.thoughtBtn.addTarget(self, action: #selector(thoughtBtnClk), for: .touchUpInside)
        
        cell?.eventBtn.tag = indexPath.row
        cell?.eventBtn.addTarget(self, action: #selector(eventBtnClk), for: .touchUpInside)
        
        cell?.btn1.tag = indexPath.row
        cell?.btn1.addTarget(self, action: #selector(btn1Clk), for: .touchUpInside)
        cell?.btn2.tag = indexPath.row
        cell?.btn2.addTarget(self, action: #selector(btn2Clk), for: .touchUpInside)
        cell?.btn3.tag = indexPath.row
        cell?.btn3.addTarget(self, action: #selector(btn3Clk), for: .touchUpInside)
        
        cell?.btn1.setImage(UIImage(named:"camera"), for: .normal)
        cell?.btn1.setImage(UIImage(named:"camera_plane"), for: .selected)
        
        cell?.btn2.setImage(UIImage(named:"video"), for: .normal)
        cell?.btn2.setImage(UIImage(named:"nationalTv"), for: .selected)
        
        cell?.btn3.setImage(UIImage(named:"photo"), for: .normal)
        cell?.btn3.setImage(UIImage(named:"location"), for: .selected)
        
        cell?.txtVwUserPost.layer.borderColor = UIColor.colorWithHexString(hexStr: "#888da8").cgColor
        cell?.txtVwUserPost.layer.borderWidth = 0.5
        cell?.txtVwUserPost.layer.cornerRadius = 3
        cell?.txtVwUserPost.delegate = self
        
        //   cell?.postImgVw.sd_setImage(with: URL(string: String(userPhotoUrl)), placeholderImage: UIImage(named: "tempAouthor"))
        if(userPhotoUrl != "")
        {
            let remoteImageURL = URL(string: userPhotoUrl)!
            Alamofire.request(remoteImageURL).responseData { (response) in
                if response.error == nil {
                    print(response.result)
                    
                    if let data = response.data {
                        cell?.imgVw.image = UIImage(data: data)
                    }
                }
            }
        }
        
        
        if(thoughtSelected)
        {
            cell?.eventLbl.textColor = UIColor.colorWithHexString(hexStr: "#888da8")
            cell?.thoughtLbl.textColor = UIColor.black
            cell?.thoughtImgVw.image =   cell?.thoughtImgVw.image!.withRenderingMode(.alwaysTemplate)
            cell?.thoughtImgVw.tintColor = UIColor.red
            
            cell?.eventImgVw.image =   cell?.eventImgVw.image!.withRenderingMode(.alwaysTemplate)
            cell?.eventImgVw.tintColor = UIColor.colorWithHexString(hexStr: "#888da8")
            
            cell?.btn1.isSelected = false
            cell?.btn2.isSelected = false
            cell?.btn3.isSelected = false
        }
        if(eventSelected)
        {
            cell?.thoughtLbl.textColor = UIColor.colorWithHexString(hexStr: "#888da8")
            cell?.eventLbl.textColor = UIColor.black
            
            cell?.eventImgVw.image =   cell?.eventImgVw.image!.withRenderingMode(.alwaysTemplate)
            cell?.eventImgVw.tintColor = UIColor.red
            
            cell?.thoughtImgVw.image =   cell?.thoughtImgVw.image!.withRenderingMode(.alwaysTemplate)
            cell?.thoughtImgVw.tintColor = UIColor.colorWithHexString(hexStr: "#888da8")
            cell?.btn1.isSelected = true
            cell?.btn2.isSelected = true
            cell?.btn3.isSelected = true
        }
        
        
        if(imageNameUploadArray.count > 0)
        {
            var yAxis = (cell?.postContVw.frame.origin.y)!+(cell?.postContVw.frame.size.height)!+1
            
            for var i in 0..<imageNameUploadArray.count
            {
                let dic = imageNameUploadArray[i] as! Dictionary<String,Any>
                
                let myView = UIView(frame: CGRect(x: 0, y: Int(yAxis), width: Int((cell!.frame.size.width+22)), height: 80))
                
                let label = UILabel(frame: CGRect(x: 60, y: 0, width: myView.frame.size.width-60, height: 80))
                myView.backgroundColor = UIColor.white
                label.textAlignment = NSTextAlignment.left
                label.text = dic["name"] as? String
                myView.addSubview(label)
                
                let imageView = UIImageView()
                imageView.frame = CGRect(x: 0, y: 15, width: 50, height: 50)
                imageView.image = dic["image"] as? UIImage;
                imageView.contentMode = .scaleAspectFill
                imageView.layer.cornerRadius =  25;
                imageView.clipsToBounds = true
                imageView.layer.masksToBounds = true
                myView.addSubview(imageView)
                cell?.contVw.addSubview(myView)
                
                yAxis = yAxis+81
            }
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
}


extension PostAThoughtVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var yAxis = 0
        if(imageNameUploadArray.count > 0)
        {
            for var i in 0..<imageNameUploadArray.count
            {
                yAxis = yAxis + 80
            }
        }
        return CGFloat(440 + yAxis)
    }
    
}
















