//
//  SearchChannelUser.swift
//  Doxbay
//
//  Created by Luminous on 18/06/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit
import MBProgressHUD
import SDWebImage
import AVKit
import AVFoundation
import MobileCoreServices
import Alamofire
import SwiftyJSON

class SearchChannelUser: UIViewController , UITableViewDelegate, UITableViewDataSource , UITextFieldDelegate{
    
   
    var  isFromPage : String = ""
    var  searchHeader : String = ""
    var SearchID : String = ""
    
    
    var expChData : [ExploreChannelListModal] = [];
    
    
    @IBOutlet var searchBox: UITextField!
    
    
    
    @IBOutlet var tableView: UITableView!
    @IBAction func backClk(_ sender: Any)
    {
        if(isFromPage == "ConnectedBranchChannles" || isFromPage == "ContectedOfficeChannels" || isFromPage == "ExploreChannelsList")
        {
          self.dismiss(animated: true, completion: nil)
        }
        else
        {
             self.navigationController?.popViewController(animated: false)
        }
    }
    
    
    //self.tableView.addSubview(self.refreshControl)
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.red
        return refreshControl
    }()
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        //self.getExploreChannels();
        refreshControl.endRefreshing()
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBox.delegate = self
        tableView.delegate = self;
        tableView.dataSource = self
        self.tableView.addSubview(self.refreshControl)
        searchBox.placeholder = searchHeader
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 95;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return expChData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExploreChannelListCellTableViewCell", for: indexPath) as! ExploreChannelListCellTableViewCell
        
//
//        cell.contentView.layer.borderWidth = 1.0
//        cell.contentView.layer.borderColor = UIColor.clear.cgColor
//        cell.contentView.layer.masksToBounds = true
//        cell.layer.shadowColor = UIColor.gray.cgColor
//        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
//        cell.layer.shadowRadius = 1.0
//        cell.layer.shadowOpacity = 1.0
//        cell.layer.masksToBounds = false
//        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
//
        
        let Explore : ExploreChannelListModal = expChData[indexPath.row];
        cell.chanelName.lineBreakMode = .byTruncatingMiddle
        cell.chanelName.lineBreakMode = .byWordWrapping
        cell.chanelName.numberOfLines = 0;
        cell.chanelName.text = Explore.name;
        let greet4Height = cell.chanelName.optimalHeight
        cell.chanelName.frame = CGRect(x: cell.chanelName.frame.origin.x, y: cell.chanelName.frame.origin.y, width: cell.chanelName.frame.width, height: greet4Height)
        
        
        cell.totalChannelPost.text = Explore.count_post + " Channel Post";
        if(Explore.photoUrl == nil)
        {
        }
        else
        {
            do {
                try  cell.channelImg.sd_setImage(with:URL(string: Explore.photoUrl), placeholderImage: UIImage(named: "vc_channel"))
            }catch {
                print("Unexpected error: \(error).")
            }
        }
        let tap = UITapGestureRecognizer(target: self, action: #selector(clickPos))
        cell.channelImg.addGestureRecognizer(tap)
        cell.channelImg.isUserInteractionEnabled = true
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let xyz : ExploreChannelListModal = expChData[(indexPath.row)]
        

        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ExploreChannelDetailsViewController") as! ExploreChannelDetailsViewController
        nextViewController.chanalID = xyz.id;
        nextViewController.expID = xyz.id;
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    
    @objc func clickPos(_ sender: UITapGestureRecognizer)
    {
        let touch = sender.location(in: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: touch)
        let xyz : ExploreChannelListModal = expChData[(indexPath?.row)!]
        

        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ExploreChannelDetailsViewController") as! ExploreChannelDetailsViewController
        nextViewController.chanalID = xyz.id;
        nextViewController.expID = xyz.id;
       self.present(nextViewController, animated:true, completion:nil)
    }
    
    
    
    
    
    
    func getExploreChannels()
    {
        if  pageNo > totalpages
        {
            return
        }
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Loading"

        var requestURL =  ""
        var parameters: [String:String] = ["":""]
        
        if(isFromPage == "OrganizationChannels")
        {
            requestURL =  BASEURL + searchChannel;
            parameters = ["parentPath": "19" ,"token": token , "currentpage": String(self.pageNo) , "search_query" : searchText]
        }
        else if(isFromPage == "ExploreChannels")
        {
            requestURL =  BASEURL + searchChannel;
            parameters = ["parentPath": "" ,"token": token , "currentpage": String(self.pageNo) , "search_query" : searchText]
        }
        else if(isFromPage == "SubscribeChannels")
        {
            requestURL =  BASEURL + searchChannel;
            parameters = ["parentPath": "","token": token , "currentpage": String(self.pageNo) , "search_query" : searchText]
        }
        else if(isFromPage == "ConnectedBranchChannles")
        {
            requestURL =  BASEURL + searchConnectedChannel;
            parameters = ["channel_id":  channel_id,"token": token , "currentpage": String(self.pageNo) , "search_query" : searchText]
        }
        else if(isFromPage == "ContectedOfficeChannels")
        {
            requestURL =  BASEURL + searchConnectedChannelExtra;
            parameters = ["channel_id":  channel_id,"token": token , "currentpage": String(self.pageNo) , "search_query" : searchText]
        }
        else if(isFromPage == "ExploreChannelsList")
        {
            requestURL =  BASEURL + searchChannel;
            parameters = ["parentPath":  SearchID ,"token": token , "currentpage": String(self.pageNo) , "search_query" : searchText]
        }
        
       
        
        print("Request URL- \(requestURL)  Param --- \(parameters)")

        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in

            MBProgressHUD.hide(for: self.view, animated: false)
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)-- \(response)")
            if success
            {
                let abc = response["data"].arrayObject;
                self.parse(json: response)

            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
        })
    }
    
    
    
    var totalpages : Int = 1;
    func parse(json: JSON) {
        totalpages = json["totalpages"].intValue
        if(json["data"].arrayValue.count < 10)
        {
            isAvilableForLoad = false
        }
        for result in json["data"].arrayValue {
            let id = result["id"].stringValue
            let name = result["name"].stringValue
            let photoUrl = result["photoUrl"].stringValue
            let count_post = String(result["count_post"].intValue)
            let catNAme1 : ExploreChannelListModal = ExploreChannelListModal(id: id , name: name,  photoUrl: photoUrl, count_post: count_post , count_newpost: "");
            expChData.append(catNAme1);
        }
        
        self.tableView.reloadData();
        MBProgressHUD.hide(for: self.view, animated: false)
    }
    
    
    
    
    var pageNo : Int = 1;
    var loadingData = false;
    var isAvilableForLoad = true;
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = expChData.count - 3
        if !loadingData && indexPath.row == lastElement {
            loadingData = true
            loadMoreData()
        }
    }
    func loadMoreData() {
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                self.pageNo = self.pageNo + 1
                if(self.isAvilableForLoad)
                {
                    self.getExploreChannels();
                }
                self.loadingData = false
            }
        }
    }
    
    
    var  searchText : String = "";
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("TextField did begin editing method called")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("TextField did end editing method called\(textField.text!)")
        searchText = textField.text!
        if(!searchText.isEmpty)
        {
            self.expChData.removeAll();
            pageNo  = 1;
            totalpages = 1;
            loadingData = false;
            isAvilableForLoad = true;
            self.getExploreChannels();
        }
        else{
            ShowToast.showPositiveMessage(message: "Please enter your text for search")
        }
        
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("TextField should end editing method called")
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
}
