//
//  SearchUser.swift
//  Doxbay
//
//  Created by Luminous on 19/06/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MBProgressHUD


class SearchUser: UIViewController, UITableViewDataSource, UITableViewDelegate , UITextFieldDelegate {

    
    
    var isFromPage : String = ""
    var searchHeader : String = ""
    var searchID : String = ""
    
    var subArr : [channelSubscribeModal] = [];
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var searchBox: UITextField!
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    
    
    //self.tableView.addSubview(self.refreshControl)
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.red
        return refreshControl
    }()
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        //self.getExploreChannelsSubscriberList);
        refreshControl.endRefreshing()
    }
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.addSubview(self.refreshControl)
        searchBox.delegate = self
        self.searchBox.placeholder = searchHeader
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subArr.count;
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(100.00);
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChannelSubcriberCell", for : indexPath) as! ChannelSubcriberCell
        
        let rData : channelSubscribeModal = subArr[indexPath.row];
       // cell.labName.text = rData.displayName;
        cell.labName.lineBreakMode = .byTruncatingMiddle
        cell.labName.lineBreakMode = .byWordWrapping
        cell.labName.numberOfLines = 0;
        cell.labName.text = rData.displayName;
        let greet4Height = cell.labName.optimalHeight
        cell.labName.frame = CGRect(x: cell.labName.frame.origin.x, y: cell.labName.frame.origin.y, width: cell.labName.frame.width, height: greet4Height)
        
        cell.labLoc.text =  rData.city;
        cell.profilePic.tag = indexPath.row
        if(!rData.photoUrl.isEmpty)
        {
            cell.profilePic.sd_setImage(with:URL(string: rData.photoUrl), placeholderImage: UIImage(named: "vc_channel"))
            
            cell.profilePic.layer.cornerRadius =  (cell.profilePic.frame.size.width) / 2;
            cell.profilePic.layer.masksToBounds = true
            
            let tap3 = UITapGestureRecognizer(target: self, action: #selector(ViewUserProfile))
            cell.profilePic.addGestureRecognizer(tap3)
            cell.profilePic.isUserInteractionEnabled = true
            
        }
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rData : channelSubscribeModal = subArr[indexPath.row];
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ProfileViewTimeLine") as! ProfileViewTimeLine
        vc.profileID = rData.dox_id
        self.present(vc, animated:true, completion:nil)
    }
    @objc func ViewUserProfile(_ sender: UITapGestureRecognizer)
    {
        let label = sender.view as! UIImageView
        let rData : channelSubscribeModal = subArr[label.tag];
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ProfileViewTimeLine") as! ProfileViewTimeLine
        vc.profileID = rData.dox_id
        self.present(vc, animated:true, completion:nil)
    }
    
    func getExploreChannelsSubscriberList()
    {
        if  pageNo > totalpages
        {
            return
        }
        
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Loading"
        
        var requestURL =  ""
        var parameters: [String:String] = ["":""]
        
        if(isFromPage == "Followers")
        {
            requestURL =  BASEURL + searchUserFollowerList;
            parameters = ["for_uuid": searchID ,"token": token , "search_query": searchText, "currentpage": String(self.pageNo)]
        }
        else if(isFromPage == "Following")
        {
            requestURL =  BASEURL + searchUserFollowingList;
            parameters = ["for_uuid": searchID ,"token": token , "search_query": searchText, "currentpage": String(self.pageNo)]
        }
        else{
            requestURL =  BASEURL + searchUser;
            parameters = ["token": token , "search_query": searchText, "currentpage": String(self.pageNo)]
        }
        
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        
        
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            MBProgressHUD.hide(for: self.view, animated: false)
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)-- \(response)")
            if success
            {
                self.parse(json: response)
                
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
        })
    }
    
    
    var totalpages : Int = 1;
    func parse(json: JSON) {
        totalpages = json["totalpages"].intValue
        if(json["data"].arrayValue.count < 10)
        {
            isAvilableForLoad = false
        }
        for result in json["data"].arrayValue {
            let id = result["id"].stringValue
            let channel_id = ""; //result["channel_id"].stringValue
            let displayName = result["displayName"].stringValue
            let city = result["city"].stringValue
            let dox_id = result["dox_id"].stringValue
            let email = result["email"].stringValue
            let photoUrl = result["photoUrl"].stringValue
            
            let dData : channelSubscribeModal = channelSubscribeModal(id: id , channel_id: channel_id , displayName: displayName, city: city , dox_id: dox_id , email: email , photoUrl: photoUrl);
            self.subArr.append(dData);
        }
        
        self.tableView.reloadData();
        MBProgressHUD.hide(for: self.view, animated: false)
    }
    
    
    var pageNo : Int = 0;
    var loadingData = false;
    var isAvilableForLoad = true;
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = subArr.count - 3
        if !loadingData && indexPath.row == lastElement {
            loadingData = true
            loadMoreData()
        }
    }
    func loadMoreData() {
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                self.pageNo = self.pageNo + 1
                if(self.isAvilableForLoad)
                {
                    self.getExploreChannelsSubscriberList();
                }
                self.loadingData = false
            }
        }
    }
    
    
    
    
    
    
    //=================== TextField Delegate ================
    // UITextField Delegates
    var  searchText : String = "";
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("TextField did begin editing method called")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("TextField did end editing method called\(textField.text!)")
        searchText = textField.text!
        if(!searchText.isEmpty)
        {
            self.subArr.removeAll();
            pageNo  = 1;
            totalpages = 1;
            pageNo  = 0;
            loadingData = false;
            isAvilableForLoad = true;
            self.getExploreChannelsSubscriberList();
        }
        else{
            ShowToast.showPositiveMessage(message: "Please enter your text for search")
        }
        
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("TextField should end editing method called")
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
}
