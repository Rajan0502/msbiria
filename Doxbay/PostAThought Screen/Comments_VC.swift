//
//  ViewController.swift
//  CommentScreen
//
//  Created by kavi yadav on 08/06/18.
//  Copyright © 2018 kavi yadav. All rights reserved.
//

import UIKit
import MBProgressHUD
import SwiftyJSON


class Comments_VC: UIViewController,UITableViewDataSource,UITableViewDelegate {
  
    var id : String = ""
    var contentType : String = ""
    
    
    @IBOutlet weak var commentListTableView: UITableView!

    
  //  var timeLineArray1 = [Any]()
    @IBAction func backClk(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet var commentsBox: UITextField!
    

    @IBAction func btnPostComments(_ sender: Any) {
        self.commenttext = commentsBox.text!
        if (self.commenttext == "")
        {
            ShowToast.showNegativeMessage(message:"Enter Text");
            return;
        }
        else
        {
            self.postCommentt()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commentListTableView.delegate = self
        commentListTableView.dataSource = self
    
        self.getCommentsHistory();
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        commentListTableView.transform = CGAffineTransform (scaleX: 1,y: -1);
    }

    
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.CommentHistoryData.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let CPosData : CommentHistoryList = CommentHistoryData[indexPath.row];
        
        if  dox_id == CPosData.userID
        {
            let cell = commentListTableView.dequeueReusableCell(withIdentifier: "MyComments_Cell", for: indexPath) as! MyComments_Cell
            
            cell.contentView.transform = CGAffineTransform (scaleX: 1,y: -1);
            cell.senderMsg.text = CPosData.comment
            cell.senderName.text = "You"
            cell.senderImg.layer.cornerRadius = cell.senderImg.frame.size.width/2
            cell.senderImg.clipsToBounds = true
            cell.senderImg.sd_setImage(with: URL(string: CPosData.userImage) , placeholderImage: UIImage(named: "vc_user"));
            let miliSecondsTime:Double? = Double((CPosData.time))
            if(miliSecondsTime != nil)
            {
                let date = Date(timeIntervalSince1970: (miliSecondsTime! / 1000.0))
                cell.senderTime.text = date.getElapsedInterval() + " ago"
            }
            else
            {
                cell.senderTime.text = CPosData.time;
            }
            
            cell.senderMsg.numberOfLines = 10
            let maxSize = CGSize(width: 150, height: 300)
            let size = cell.senderMsg.sizeThatFits(maxSize)
            cell.senderMsg.frame = CGRect(origin: CGPoint(x: 100, y: 100), size: size)
            cell.btnDeleteComments.tag = indexPath.row
            cell.btnDeleteComments.addTarget(self, action:#selector(self.btnDeleteComment), for: .touchUpInside)
            
            return cell
        }
        else {
            let cell = commentListTableView.dequeueReusableCell(withIdentifier: "OthersComments_Cell", for: indexPath) as! OthersComments_Cell
            cell.contentView.transform = CGAffineTransform (scaleX: 1,y: -1);
            cell.receiverImg.layer.cornerRadius = cell.receiverImg.frame.size.width/2
            cell.receiverImg.clipsToBounds = true
            cell.receiverImg.sd_setImage(with: URL(string: CPosData.userImage) , placeholderImage: UIImage(named: "vc_user"));
            cell.receiverName.text = CPosData.userName
            cell.receiverMsg.text =  CPosData.comment;
            let miliSecondsTime:Double? = Double((CPosData.time))
            if(miliSecondsTime != nil)
            {
                let date = Date(timeIntervalSince1970: (miliSecondsTime! / 1000.0))
                cell.recevertime.text = date.getElapsedInterval() + " ago"
            }
            else
            {
                cell.recevertime.text = CPosData.time;
            }
            return cell
        }
    }
    
   @objc func btnDeleteComment(sender : UIButton!)
    {
        let btnsendtag: UIButton = sender
        let inx : Int = btnsendtag.tag
        let id = self.CommentHistoryData[inx].id
        var requestURL = "";
        if(contentType == "user_post" )
        {
            requestURL = BASEURL + deleteUserPostComment
        }
        else if(contentType == "user_event")
        {
            requestURL = BASEURL + deleteEventComment
        }
        else
        {
            requestURL = BASEURL + deleteComment
        }
        
        
        let parameters: [String:String] = ["id":id,"token": token]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)-- ----\(response)")
            MBProgressHUD.hide(for: self.view, animated: false)
            if success
            {
                 self.CommentHistoryData.remove(at: inx)
                self.commentListTableView.reloadData()
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
        })
        
    }

    
    
    
    func getCommentsHistory()
    {
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Loading"
        var requestURL = "";
        if(contentType == "user_post" )
        {
            requestURL = BASEURL + getUserPostComment
        }
        else if(contentType == "user_event")
        {
            requestURL = BASEURL + getEventComment
        }
        else
        {
            requestURL = BASEURL + getComment
        }
        
        
        let parameters: [String:String] = ["id":id,"token": token]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)-- ----\(response)")
            MBProgressHUD.hide(for: self.view, animated: false)
            if success
            {
                self.parsex(json: response);
                self.commentListTableView.reloadData()
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
        })
    }
   
    

   
    
    var CommentHistoryData : [CommentHistoryList] = [];
    func parsex(json: JSON) {
        for result in json["data"].arrayValue {
            let id = result["id"].stringValue
            let comment = result["comment"].stringValue
            let time = result["time"].stringValue
            let uid = result["user"]["id"].stringValue
            let uname = result["user"]["name"].stringValue
            let image = result["user"]["image"].stringValue
            let cData : CommentHistoryList = CommentHistoryList(id: id, comment: comment, time: time  , userID: uid , userName: uname , userImage: image);
            CommentHistoryData.append(cData);
        }
    }

    
    
    
    
    var commenttext : String = "";
    func postCommentt()
    {
        let postTxt =  commenttext
        if (postTxt == "")
        {
            ShowToast.showNegativeMessage(message:"Enter Text");
            return;
        }
        var requestURL = "";
        if(contentType == "user_post")
        {
            requestURL = BASEURL + createUserPostComment
        }
        else if(contentType == "user_event")
        {
            requestURL = BASEURL + createEventComment
        }
        else
        {
            requestURL = BASEURL +  postComment
        }
        
        let cData : CommentHistoryList = CommentHistoryList(id: id, comment: postTxt, time: "Just Now!"  , userID: dox_id , userName: "You" , userImage: "");
        CommentHistoryData.append(cData);
        self.commentListTableView.reloadData();
        
        
        let parameters: [String:String] = ["id":id, "channel_id":channel_id , "token":token,"comment":postTxt]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--")
            
            if success
            {
                self.commentsBox.text = "";
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
        })
    }

    
    
  
}


