//
//  FirstCellTableViewCell.swift
//  CommentScreen
//
//  Created by kavi yadav on 08/06/18.
//  Copyright © 2018 kavi yadav. All rights reserved.
//

import UIKit

class MyComments_Cell: UITableViewCell {

    @IBOutlet weak var senderTime: UILabel!
    @IBOutlet weak var senderName: UILabel!
    @IBOutlet weak var senderMsg: UILabel!
    @IBOutlet weak var senderImg: UIImageView!
    @IBOutlet var commentBoxView: UIView!
    
  
    @IBOutlet var btnDeleteComments: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
