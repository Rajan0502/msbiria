//
//  SearchTimeLine_VC.swift
//  Doxbay
//
//  Created by Luminous on 18/06/18.
//  Copyright © 2018 Luminous. All rights reserved.
//

import UIKit
import MBProgressHUD
import SDWebImage
import AVKit
import AVFoundation
import MobileCoreServices
import Alamofire
import SwiftyJSON


class SearchTimeLine_VC: UIViewController ,  UITextViewDelegate , UITextFieldDelegate , UITableViewDataSource,UITableViewDelegate,UINavigationControllerDelegate{
    
    
    var  isFromPage : String = ""
    var  searchHeader : String = ""
    var  searchID : String = "";
    
    
    
    var timeLimeData : [OTLModal] = []
    
    var refreshControl = UIRefreshControl()
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet var searchBox: UITextField!
    
    
    
    // 1
    // var expandedLabel: UILabel!
    var indexOfCellToExpand: Int!
    
    //3
    var isShow : Bool = false;
    var currentExpendRow : Int = -1;
    
    var expandedRows = Set<Int>()
    var isForReloadIndex : Int  = -1
    
    
    @IBAction func btnBackAction(_ sender: Any) {
        if(isFromPage == "ChannelPostTM")
        {
            self.dismiss(animated: true, completion: nil)
        }
        else
        {
            self.navigationController?.popViewController(animated: false)
        }
        
        
    }
    
    
    override func viewDidLoad() {
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
        super.viewDidLoad()
        searchBox.delegate = self
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tag=1;
        self.tableView.reloadData()
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        self.hideKeyboardOnTap(#selector(self.dismissKeyboard))
        searchBox.placeholder = searchHeader;
        
        
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
        // do aditional stuff
    }
    
    @objc func refresh(sender:AnyObject)
    {
        refreshControl.endRefreshing()
        // getTimeLineData()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            if(self.isForReloadIndex > 0)
            {
                self.tableView.scrollToRow(at: IndexPath(row: self.isForReloadIndex, section: 0), at: .top, animated: true)
                self.isForReloadIndex = -1
            }
            
        }
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
    
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return timeLimeData.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let rowData : OTLModal = timeLimeData[indexPath.row]
        let isVideoImgPdf = checkForImgVideoPdf(index : indexPath.row)
        
        if (isVideoImgPdf)
        {
            if indexPath.row == indexOfCellToExpand
            {
                let xxx =  Utility.heightForView(text: rowData.title) + 190 ;
                return   xxx
            }
            else
            {
                return  440;
            }
        }
        else
        {
            if indexPath.row == indexOfCellToExpand
            {
                let xxx =  Utility.heightForView(text: rowData.title);
                return   xxx
            }
            else
            {
                return 230
            }
        }
    }
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        
        let rowData : OTLModal = timeLimeData[indexPath.row]
        let isVideoImgPdf = checkForImgVideoPdf(index : indexPath.row)
        
        if (isVideoImgPdf)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TM_Image_Cell") as! TM_Image_Cell
            var titleStr = rowData.title
            titleStr = titleStr.trimmingCharacters(in: .whitespacesAndNewlines)
            if indexPath.row == indexOfCellToExpand
            {
                cell.postDescription.attributedText = Utility.getHTMLToString(titleStr: titleStr);
            }
            else
            {
                if((titleStr.count)>350)
                {
                    titleStr = (titleStr.substring(with: 0..<350)) + "...<strong><font color=red>Read More</font></strong>"
                }
                cell.postDescription.attributedText =  Utility.getHTMLToString(titleStr: titleStr);
            }
            
            
            cell.postDescription.tag = indexPath.row
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.expandCell(sender:)))
            cell.postDescription.addGestureRecognizer(tap)
            cell.postDescription.isUserInteractionEnabled = true
            
            
            cell.userName.text = rowData.userName
            cell.userPTitle.text = rowData.channel_name
            let miliSecondsTime:Double? = Double(rowData.time)
            let date = Date(timeIntervalSince1970: (miliSecondsTime! / 1000.0))
            
            cell.userPostTypeDateTime.attributedText =  Utility.getHTMLToString(titleStr: "<strong><font color=red>" + rowData.category + " </font></strong>" + date.getElapsedInterval() + " ago")
            
            
            cell.likeCount.text = rowData.count_like + " Likes"
            cell.commentCount.text = rowData.count_comment + " Comments"
            if(rowData.is_liked == "0")
            {
                cell.imgIsPostLike.image =  UIImage(named: "vc_like_false")
            }
            else
            {
                cell.imgIsPostLike.image =  UIImage(named: "vc_like_true")
            }
            if(rowData.is_favorite == "0")
            {
                cell.imgIsFavorites.image =  UIImage(named: "vc_favorite_normal")
            }
            else
            {
                cell.imgIsFavorites.image =  UIImage(named: "vc_favorite")
            }
            
            
            
            cell.userProfilePic.layer.cornerRadius =  (cell.userProfilePic.frame.size.width) / 2;
            cell.userProfilePic.layer.masksToBounds = true
            
            cell.userProfilePic.sd_setImage(with: URL(string: String(rowData.userImage)), placeholderImage: UIImage(named: "vc_user"))
            let tapProfile = UITapGestureRecognizer(target: self, action: #selector(self.viewUserProfile(sender:)))
            cell.userProfilePic.addGestureRecognizer(tapProfile)
            cell.userProfilePic.isUserInteractionEnabled = true
            cell.userProfilePic.tag = indexPath.row
            
            
            
            let postimg = rowData.image.split(separator: ",")
            if (postimg.count > 0)
            {
                cell.postImage.sd_setImage(with: URL(string: String(postimg[0])), placeholderImage: UIImage(named: "vc_ops_gallery"))
                let tapImg = UITapGestureRecognizer(target: self, action: #selector(viewImage))
                cell.postImage.addGestureRecognizer(tapImg)
                cell.postImage.isUserInteractionEnabled = true
                cell.postImage.tag = indexPath.row
            }
            
            if (rowData.video_url.count > 0)
            {
                let video_thumb = rowData.video_url.split(separator: ",")
                if (video_thumb.count > 0)
                {
                    
                    DispatchQueue.global(qos: .background).async {
                        let thumbnailImage = self.getThumbnailImage(forUrl: URL(string: String(video_thumb[0]))!)
                        DispatchQueue.main.async
                            {
                                cell.postImage.image = thumbnailImage
                        }
                    }
                    
                    
                    let tapImg = UITapGestureRecognizer(target: self, action: #selector(plsyPostVideo))
                    cell.postImage.addGestureRecognizer(tapImg)
                    cell.postImage.isUserInteractionEnabled = true
                    cell.postImage.tag = indexPath.row
                }
                else
                {
                    cell.postImage.image = UIImage(named: "vc_play_circle_trans");
                    let tapImg = UITapGestureRecognizer(target: self, action: #selector(plsyPostVideo))
                    cell.postImage.addGestureRecognizer(tapImg)
                    cell.postImage.isUserInteractionEnabled = true
                    cell.postImage.tag = indexPath.row
                }
            }
            
            let postPdf = rowData.pdf.split(separator: ",")
            if (postPdf.count > 0)
            {
                cell.postImage.image = UIImage(named: "ic_type_pdf");
                let tapImg = UITapGestureRecognizer(target: self, action: #selector(viewPDFFile(_:)))
                cell.postImage.addGestureRecognizer(tapImg)
                cell.postImage.isUserInteractionEnabled = true
                cell.postImage.tag = indexPath.row
            }
            
            
            
            
            
            let tapLike = UITapGestureRecognizer(target: self, action: #selector(self.btnLikePost(sender:)))
            cell.imgIsPostLike.addGestureRecognizer(tapLike)
            cell.imgIsPostLike.isUserInteractionEnabled = true
            cell.imgIsPostLike.tag = indexPath.row
            
            
            
            let tapComment = UITapGestureRecognizer(target: self, action: #selector(self.btnPostViewComment(sender:)))
            cell.imgPostComment.addGestureRecognizer(tapComment)
            cell.imgPostComment.isUserInteractionEnabled = true
            cell.imgPostComment.tag = indexPath.row
            
            cell.commentCount.tag = indexPath.row
            let tapCom = UITapGestureRecognizer(target: self, action: #selector(self.ViewComments(sender:)))
            cell.commentCount.addGestureRecognizer(tapCom)
            cell.commentCount.isUserInteractionEnabled = true
            
            
            let tapfavv = UITapGestureRecognizer(target: self, action: #selector(self.clickisFav(sender:)))
            cell.imgIsFavorites.addGestureRecognizer(tapfavv)
            cell.imgIsFavorites.isUserInteractionEnabled = true
            cell.imgIsFavorites.tag = indexPath.row
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TLN_Cell") as! TLN_Cell
            var titleStr = rowData.title
            titleStr = titleStr.trimmingCharacters(in: .whitespacesAndNewlines)
            if indexPath.row == indexOfCellToExpand
            {
                cell.postDescription.attributedText = Utility.getHTMLToString(titleStr: titleStr);
            }
            else
            {
                if((titleStr.count)>350)
                {
                    titleStr = (titleStr.substring(with: 0..<350)) + "...<strong><font color=red>Read More</font></strong>"
                }
                cell.postDescription.attributedText =  Utility.getHTMLToString(titleStr: titleStr);
            }
            
            cell.postDescription.tag = indexPath.row
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.expandCell(sender:)))
            cell.postDescription.addGestureRecognizer(tap)
            cell.postDescription.isUserInteractionEnabled = true
            
            
            cell.userName.text = rowData.userName
            cell.userPTitle.text = rowData.channel_name
            let miliSecondsTime:Double? = Double(rowData.time)
            let date = Date(timeIntervalSince1970: (miliSecondsTime! / 1000.0))
            
            cell.userPostTypeDateTime.attributedText =  Utility.getHTMLToString(titleStr: "<strong><font color=red>" + rowData.category + " </font></strong>" + date.getElapsedInterval() + " ago")
            
            
            cell.likeCount.text = rowData.count_like + " Likes"
            cell.commentCount.text = rowData.count_comment + " Comments"
            if(rowData.is_liked == "0")
            {
                cell.imgIsPostLike.image =  UIImage(named: "vc_like_false")
            }
            else
            {
                cell.imgIsPostLike.image =  UIImage(named: "vc_like_true")
            }
            if(rowData.is_favorite == "0")
            {
                cell.imgIsFavorites.image =  UIImage(named: "vc_favorite_normal")
            }
            else
            {
                cell.imgIsFavorites.image =  UIImage(named: "vc_favorite")
            }
            
            
            
            cell.userProfilePic.layer.cornerRadius =  (cell.userProfilePic.frame.size.width) / 2;
            cell.userProfilePic.layer.masksToBounds = true
            
            cell.userProfilePic.sd_setImage(with: URL(string: String(rowData.userImage)), placeholderImage: UIImage(named: "vc_user"))
            
            let tapProfile = UITapGestureRecognizer(target: self, action: #selector(self.viewUserProfile(sender:)))
            cell.userProfilePic.addGestureRecognizer(tapProfile)
            cell.userProfilePic.isUserInteractionEnabled = true
            cell.userProfilePic.tag = indexPath.row
            
            
            
            
            
            
            
            let tapLike = UITapGestureRecognizer(target: self, action: #selector(self.btnLikePost(sender:)))
            cell.imgIsPostLike.addGestureRecognizer(tapLike)
            cell.imgIsPostLike.isUserInteractionEnabled = true
            cell.imgIsPostLike.tag = indexPath.row
            
            let tapComment = UITapGestureRecognizer(target: self, action: #selector(self.btnPostViewComment(sender:)))
            cell.imgPostComment.addGestureRecognizer(tapComment)
            cell.imgPostComment.isUserInteractionEnabled = true
            cell.imgPostComment.tag = indexPath.row
            
            cell.commentCount.tag = indexPath.row
            let tapCom = UITapGestureRecognizer(target: self, action: #selector(self.ViewComments(sender:)))
            cell.commentCount.addGestureRecognizer(tapCom)
            cell.commentCount.isUserInteractionEnabled = true
            
            let tapfavv = UITapGestureRecognizer(target: self, action: #selector(self.clickisFav(sender:)))
            cell.imgIsFavorites.addGestureRecognizer(tapfavv)
            cell.imgIsFavorites.isUserInteractionEnabled = true
            cell.imgIsFavorites.tag = indexPath.row
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    }
    
    
    
    
    
    
    @objc func expandCell(sender: UITapGestureRecognizer)
    {
        let label = sender.view as! UILabel
        
        let rowData : OTLModal = timeLimeData[label.tag]
        let titleStr = rowData.title
        
        if((titleStr.count)<350)
        {
            return
        }
        
        let isVideoImgPdf = checkForImgVideoPdf(index : label.tag)
        if (isVideoImgPdf)
        {
            let cell : TM_Image_Cell = tableView.cellForRow(at: IndexPath(row: label.tag, section: 0)) as!TM_Image_Cell
            
            cell.postDescription.sizeToFit()
            indexOfCellToExpand = label.tag
            cell.postDescription.attributedText =  Utility.getHTMLToString(titleStr: titleStr)
        }
        else
        {
            let cell : TLN_Cell = tableView.cellForRow(at: IndexPath(row: label.tag, section: 0)) as! TLN_Cell
            
            cell.postDescription.sizeToFit()
            indexOfCellToExpand = label.tag
            cell.postDescription.attributedText =  Utility.getHTMLToString(titleStr: titleStr)
        }
        
        if(isShow && label.tag == currentExpendRow)
        {
            indexOfCellToExpand = -1
            currentExpendRow =  -1
            isShow = false
        }
        else
        {
            isShow = true
            currentExpendRow = label.tag;
        }
        
        tableView.reloadRows(at: [IndexPath(row: label.tag, section: 0)], with: .fade)
        tableView.scrollToRow(at: IndexPath(row: label.tag, section: 0), at: .top, animated: true)
    }
    
    
    
    
    
    
    
    
    
    
    
    @objc func viewUserProfile(sender: UITapGestureRecognizer!)
    {
        let touch = sender.location(in: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: touch)
        {
            let indx = indexPath.row;
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ProfileViewTimeLine") as! ProfileViewTimeLine
            isForEdit = false;
            vc.profileID = self.timeLimeData[indx].userID;
            self.present(vc, animated:true, completion:nil)
        }
    }
    
    
    @objc func viewImage(_ sender: UITapGestureRecognizer)
    {
        let touch = sender.location(in: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: touch)
        {
            //  let dic = self.timeLineArray1[indexPath.row] as! Dictionary<String,Any>
            let imageStr = self.timeLimeData[indexPath.row].image // dic["image"] as? String
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "ImagePagerViewControllerPager") as! ImagePagerViewControllerPager
            newViewController.imageURL = imageStr
            newViewController.requestType = "image"
            self.present(newViewController, animated: true, completion: nil)
        }
    }
    
    
    @objc func viewPDFFile(_ sender: UITapGestureRecognizer)
    {
        let touch = sender.location(in: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: touch)
        {
            // let dic = self.timeLineArray1[indexPath.row] as! Dictionary<String,Any>
            let imageStr = self.timeLimeData[indexPath.row].pdf // dic["pdf"] as? String
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "ImagePagerViewControllerPager") as! ImagePagerViewControllerPager
            newViewController.imageURL = imageStr
            newViewController.requestType = "pdf"
            self.present(newViewController, animated: true, completion: nil)
        }
    }
    
    
    
    
    @objc func plsyPostVideo(_ sender: UITapGestureRecognizer)
    {
        let touch = sender.location(in: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: touch)
        {
            // let dic = self.timeLineArray1[indexPath.row] as! Dictionary<String,Any>
            let video_url = NSURL(string: String(describing: self.timeLimeData[indexPath.row].video_url))
            play(url: video_url!)
        }
    }
    func play(url:NSURL) {
        print("playing \(url)")
        let player = AVPlayer(url: (url as URL))
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    func getThumbnailImage(forUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(1, 60) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
        }
        return nil
    }
    
    
    
    
    
    
    // like API
    @objc func btnLikePost(sender: UITapGestureRecognizer!)
    {
        
        let label = sender.view as! UIImageView
        
        let tagIndex : Int = label.tag
        
        //  let dic = self.timeLineArray1[tagIndex] as! Dictionary<String,Any>
        let id = timeLimeData[tagIndex].id
        var is_liked = timeLimeData[tagIndex].is_liked
        
        if is_liked == "0"
        {
            is_liked = "1"
            timeLimeData[tagIndex].is_liked = "1"
            
            let aa : Int = Int(timeLimeData[tagIndex].count_like)! + 1
            timeLimeData[tagIndex].count_like = String(aa)
            
            let indexPath = IndexPath(item: tagIndex, section: 0)
            if(checkForImgVideoPdf(index : tagIndex))
            {
                let cell = self.tableView.cellForRow(at: indexPath) as! TM_Image_Cell
                cell.imgIsPostLike.image =  UIImage(named:
                    "vc_like_true")
                cell.likeCount.text = timeLimeData[tagIndex].count_like + " Likes"
            }
            else
            {
                let cell = self.tableView.cellForRow(at: indexPath) as! TLN_Cell
                cell.imgIsPostLike.image =  UIImage(named: "vc_like_true")
                cell.likeCount.text = timeLimeData[tagIndex].count_like  + " Likes"
            }
        }
        else
        {
            is_liked = "0"
            timeLimeData[tagIndex].is_liked = "0"
            let aa : Int = Int(timeLimeData[tagIndex].count_like)! - 1
            timeLimeData[tagIndex].count_like = String(aa)
            
            let indexPath = IndexPath(item: tagIndex, section: 0)
            if(checkForImgVideoPdf(index : tagIndex))
            {
                let cell = self.tableView.cellForRow(at: indexPath) as! TM_Image_Cell
                cell.imgIsPostLike.image =  UIImage(named: "vc_like_false")
                cell.likeCount.text = timeLimeData[tagIndex].count_like  + " Likes"
            }
            else
            {
                let cell = self.tableView.cellForRow(at: indexPath) as! TLN_Cell
                cell.imgIsPostLike.image =  UIImage(named: "vc_like_false")
                cell.likeCount.text = timeLimeData[tagIndex].count_like  + " Likes"
            }
        }
        
        
        let contentType = self.timeLimeData[tagIndex].content_type //String(describing: dic["content_type"]!)
        var requestURL = "";
        if(contentType == "user_post")
        {
            requestURL = BASEURL + createUserPostLike
        }
        else if(contentType == "user_event")
        {
            requestURL = BASEURL + createEventLike
        }
        else if(contentType == "channel_post")
        {
            requestURL = BASEURL + postLike
        }
        
        let parameters: [String:String] = ["id":id,"channel_id":channel_id ,"token": token ,"is_liked":is_liked]
        
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--")
            
            if success
            {
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
        })
    }
    
    
    
    // View comment history
    @objc func btnPostViewComment(sender: UITapGestureRecognizer!)
    {
        let imgv  = sender.view as! UIImageView
        let indx : Int = imgv.tag
        isForReloadIndex = indx
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Comments_VC") as! Comments_VC
        vc.id =  timeLimeData[indx].id
        vc.contentType = timeLimeData[indx].content_type
        self.present(vc, animated:true, completion:nil)
    }
    @objc func ViewComments(sender: UITapGestureRecognizer)
    {
        let label = sender.view as! UILabel
        let indx : Int = label.tag
        isForReloadIndex = indx
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Comments_VC") as! Comments_VC
        vc.id =  timeLimeData[indx].id
        vc.contentType = timeLimeData[indx].content_type
        self.present(vc, animated:true, completion:nil)
    }
    
    
    
    @objc func clickisFav(sender: UITapGestureRecognizer!)
    {
        let touch = sender.location(in: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: touch)
        let indx : Int = (indexPath?.row)!
        var isFavarity =  timeLimeData[indx].is_favorite
        
        if(isFavarity == "1")
        {
            isFavarity = "0";
            timeLimeData[indx].is_favorite = "0"
            
            let indexPath = IndexPath(item: indx, section: 0)
            if(checkForImgVideoPdf(index : indx))
            {
                let cell = self.tableView.cellForRow(at: indexPath) as! TM_Image_Cell
                cell.imgIsFavorites.image =  UIImage(named:
                    "vc_favorite_normal")
            }
            else
            {
                let cell = self.tableView.cellForRow(at: indexPath) as! TLN_Cell
                cell.imgIsFavorites.image =  UIImage(named: "vc_favorite_normal")
            }
        }
        else
        {
            isFavarity = "1";
            timeLimeData[indx].is_favorite = "1"
            let indexPath = IndexPath(item: indx, section: 0)
            if(checkForImgVideoPdf(index : indx))
            {
                let cell = self.tableView.cellForRow(at: indexPath) as! TM_Image_Cell
                cell.imgIsFavorites.image =  UIImage(named:
                    "vc_favorite")
            }
            else
            {
                let cell = self.tableView.cellForRow(at: indexPath) as! TLN_Cell
                cell.imgIsFavorites.image =  UIImage(named: "vc_favorite")
            }
        }
        
        
        let requestURL = BASEURL + postFavorite
        let parameters: [String:String] = ["id":timeLimeData[indx].id,"token": token ,"is_favorite": isFavarity]
        
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--")
            
            if success
            {
                
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
        })
    }
    
    
    
    
    
    
    
    
    
    func checkForImgVideoPdf(index : Int) -> Bool
    {
        let rowData : OTLModal = timeLimeData[index]
        if (rowData.image.split(separator: ",").count > 0)
        {
            return true
        }
        else if (rowData.video_url.split(separator: ",").count > 0)
        {
            return true
        }
        else if (rowData.pdf.split(separator: ",").count > 0)
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    
    
    
    func getTimeLineData()
    {
        if  pageNo > totalpages
        {
            return
        }
        
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Loading"
        
        var requestURL = ""
        var parameters: [String:String] = ["":""]
        
        if(isFromPage == "OrganizationTimeLine")
        {
            requestURL = BASEURL + searchOrganizationTimeline
            parameters = ["channel_id": channel_id ,"token": token, "currentpage": String(pageNo) ,  "search_query": searchText]
        }
        else if(isFromPage == "AllOrganizationTimeLine")
        {
            requestURL = BASEURL + allOrganizationTimelineSearch
            parameters = ["channel_id": channel_id ,"token": token,"currentpage": String(pageNo) , "search_query" : searchText]
        }
        else if(isFromPage == "ChannelPostTM")
        {
            requestURL = BASEURL + searchPost
            parameters = ["channel_id": searchID ,"token": token,"currentpage": String(pageNo) , "search_query" : searchText]
        }
        
        
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--  \(response)")
            
            if success
            {
                self.parsex(json: response)
                MBProgressHUD.hide(for: self.view, animated: false)
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
        })
    }
    
    var totalpages : Int = 1
    func parsex(json: JSON) {
        totalpages = json["totalpages"].intValue
        if(json["data"].arrayValue.count < 10)
        {
            isAvilableForLoad = false
        }
        for result in json["data"].arrayValue {
            let id = result["id"].stringValue
            let title = result["title"].stringValue
            let description = result["description"].stringValue
            let category = result["category"].stringValue
            let pdf = result["count_channpdfel"].stringValue
            let pdf_name = result["pdf_name"].stringValue
            let image = result["image"].stringValue
            let video = result["video"].stringValue
            let video_thumb = result["video_thumb"].stringValue
            let video_url = result["video_url"].stringValue
            let video_url_oe = result["video_url_oe"].stringValue
            let time = result["time"].stringValue
            let count_like = result["count_like"].stringValue
            let count_comment = result["count_comment"].stringValue
            let is_favorite = result["is_favorite"].stringValue
            let is_liked = result["is_liked"].stringValue
            let content_type = result["content_type"].stringValue
            let userID = result["user"]["id"].stringValue
            let userName = result["user"]["name"].stringValue
            let userImage = result["user"]["image"].stringValue
            let channel_name = result["channel_name"].stringValue
            let channel_id = result["channel_id"].stringValue
            
            let mData : OTLModal = OTLModal(id: id , title: title , description: description  , category: category , pdf: pdf , pdf_name: pdf_name , image: image , video: video , video_thumb: video_thumb , video_url: video_url , video_url_oe: video_url_oe , time: time , count_like: count_like , count_comment: count_comment , is_favorite: is_favorite , is_liked: is_liked , content_type: content_type , userID: userID , userName: userName , userImage: userImage , channel_name: channel_name , channel_id: channel_id)
            timeLimeData.append(mData)
        }
        self.tableView.reloadData()
    }
    
    
    
    
    
    var pageNo : Int = 1;
    var loadingData = false;
    var isAvilableForLoad = true;
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = timeLimeData.count - 3
        if !loadingData && indexPath.row == lastElement {
            loadingData = true
            loadMoreData()
        }
    }
    func loadMoreData() {
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                self.pageNo = self.pageNo + 1
                if(self.isAvilableForLoad)
                {
                    self.getTimeLineData();
                }
                self.loadingData = false
            }
        }
    }
    
    
    
    
    
    
    
    //=================== TextField Delegate ================
    // UITextField Delegates
    var  searchText : String = "";
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("TextField did begin editing method called")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("TextField did end editing method called\(textField.text!)")
        searchText = textField.text!
        if(!searchText.isEmpty)
        {
             timeLimeData.removeAll()
             pageNo  = 1;
             loadingData = false;
             isAvilableForLoad = true;
             self.getTimeLineData();
        }
        else{
            ShowToast.showPositiveMessage(message: "Please enter your text for search")
        }
        
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("TextField should end editing method called")
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    
}
