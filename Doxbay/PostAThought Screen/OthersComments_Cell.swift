//
//  SecondCell.swift
//  CommentScreen
//
//  Created by kavi yadav on 08/06/18.
//  Copyright © 2018 kavi yadav. All rights reserved.
//

import UIKit

class OthersComments_Cell: UITableViewCell {

    
    @IBOutlet weak var receiverMsg: UILabel!
    @IBOutlet weak var receiverImg: UIImageView!
    @IBOutlet weak var receiverName: UILabel!
    @IBOutlet weak var recevertime: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

 
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
