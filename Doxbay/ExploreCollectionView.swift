//
//  XXXXXViewController.swift
//  CollectionViewDataSourceBlog
//
//  Created by Luminous on 08/05/18.
//  Copyright © 2018 Erica Millado. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import MBProgressHUD


class ExploreCollectionView : UIViewController , UICollectionViewDataSource{
    
    
    var catListArr : [Organization_CollModal] = [];
    
    @IBOutlet var collectionView: UICollectionView!
    @IBAction func backClk(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func btnConnectedBranch(_ sender: Any) {
        /*
         let vc = self.storyboard?.instantiateViewController(withIdentifier: "ConnectedBranch_VC") as! ConnectedBranch_VC
         self.navigationController?.pushViewController(vc, animated: true)
         */
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ConnectedBranch_VC") as! ConnectedBranch_VC
        self.present(nextViewController, animated:true, completion:nil)
        
        
    }
    
    //self.tableView.addSubview(self.refreshControl)
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.red
        return refreshControl
    }()
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.getExploreListData();
        refreshControl.endRefreshing()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "backButton")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "backButton")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "TEST", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        
        
        
        
        
        print("screenTyscreenTypeWithHeightpe:", UIDevice.current.screenTypeWithHeight.rawValue)
        print("screenWidth:", screenWidth)
        print("screenHeight:", screenHeight)
        
        var sWidth : Int = Int(screenWidth);
        var xxx : Double = 0.0
        if sWidth % 2 == 0 {
            sWidth = sWidth - 18
            xxx =  Double(sWidth / 2)
            print("Sell widht  odd \(xxx)")
            print("Sell widht odd \(sWidth)")
        } else {
            sWidth = sWidth - 19
            sWidth =  sWidth / 2
            xxx =  Double(sWidth / 2)
            print("Sell widht  odd \(xxx)")
            print("Sell widht  odd \(sWidth)")
        }
        
        var cellSize = CGSize(width: 100  , height:115)
        if(UIDevice.current.screenTypeWithHeight.rawValue == "1136")
        {
            cellSize = CGSize(width: 150  , height:180)
        }
        else
        {
            cellSize = CGSize(width: 175  , height:190)
        }
        
        //  let cellSize = CGSize(width: sWidth  , height:190)
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical //.horizontal
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
        layout.minimumLineSpacing = 2.0
        layout.minimumInteritemSpacing = 2.0
        collectionView.setCollectionViewLayout(layout, animated: true)
        
        
        
        self.collectionView.addSubview(self.refreshControl)
        self.getExploreListData();
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.parent?.view.frame = CGRect(x:0, y:64, width:self.view.frame.width, height: UIScreen.main.bounds.size.height-64)
            //  floatingActionButton.isHidden = true
            
        }
    }
    
    
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  self.catListArr.count
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExploreCollectinCell", for: indexPath) as! ExploreCollectinCell
        
        
        cell.contentView.layer.borderWidth = 1.0
        cell.contentView.layer.borderColor = UIColor.clear.cgColor
        cell.contentView.layer.masksToBounds = true
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cell.layer.shadowRadius = 2.0
        cell.layer.shadowOpacity = 1.0
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
        
        
        
        let Organization_CollModal = catListArr[indexPath.row]
        
        cell.displayContent(title: Organization_CollModal.CatName , chnalCount: Organization_CollModal.totalChannel , imgURl: Organization_CollModal.catImg)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(clickPos))
        cell.bookImagexp.addGestureRecognizer(tap)
        cell.bookImagexp.isUserInteractionEnabled = true
        
        
        
        return cell
    }
    
    
    
    
    
    @objc func clickPos(_ sender: UITapGestureRecognizer)
    {
        let touch = sender.location(in: self.collectionView)
        let indexPathx =  self.collectionView.indexPathForItem(at: touch)
        let indx = indexPathx?.row;
        let xyz : Organization_CollModal = catListArr[(indexPathx?.row)!]
        
        /*  let signUp = self.storyboard?.instantiateViewController(withIdentifier: "ExploreChannelVC") as! ExploreChannelVC
         signUp.expID = xyz.CatID;
         signUp.ExploreChannelCatName = xyz.CatName;
         self.navigationController?.pushViewController(signUp, animated: true)
         */
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ExploreChannelVC") as! ExploreChannelVC
        nextViewController.expID = xyz.CatID;
        nextViewController.ExploreChannelCatName = xyz.CatName;
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    
    
    func getExploreListData()
    {
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.mode = MBProgressHUDMode.indeterminate
        progressHUD.label.text = "Loading"
        //MBProgressHUD.hide(for: self.view, animated: false)
        
        let requestURL = BASEURL + getChannelCategory;
        let parameters: [String:String] = ["channel_id": channel_id ,"token": token]
        print("Request URL- \(requestURL)  Param --- \(parameters)")
        
        UCUtil.requestPOSTURLWithFormData(requestURL, params: parameters, headers: nil, success: {(response) -> Void in
            
            let success = response["success"].boolValue
            print("xxxxxxxx   ---- \(success)--")
            if success
            {
                let abc = response["data"].arrayObject;
                self.parse(json: response)
                
            }
        }, failure:  {
            (error) -> Void in
            print("error===\(error)")
            MBProgressHUD.hide(for: self.view, animated: false)
        })
    }
    
    
    func parse(json: JSON) {
        self.catListArr.removeAll();
        for result in json["data"].arrayValue {
            let id = result["id"].stringValue
            let name = result["name"].stringValue
            let photoUrl = result["photoUrl"].stringValue
            let count_channel = result["count_channel"].stringValue
            print("id -- \(id) -- name --- \(name) ---- \(photoUrl)---\(count_channel)")
            let catNAme1 : Organization_CollModal = Organization_CollModal(catID: id, catName: name , umgName: photoUrl , totalChannel: count_channel);
            catListArr.append(catNAme1);
        }
        
        self.collectionView.reloadData();
        MBProgressHUD.hide(for: self.view, animated: false)
    }
    
    
}









extension UIDevice {
    var iPhoneX: Bool {
        return UIScreen.main.nativeBounds.height == 2436
    }
    var iPhone: Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
    enum ScreenType: String {
        case iPhone4_4S = "960" // "iPhone 4 or iPhone 4S"
        case iPhones_5_5s_5c_SE = "1136" //iPhone 5, iPhone 5s, iPhone 5c or iPhone SE"
        case iPhones_6_6s_7_8 = "1334" //iPhone 6, iPhone 6S, iPhone 7 or iPhone 8"
        case iPhones_6Plus_6sPlus = "1920" // "iPhone 6 Plus, iPhone 6S Plus, iPhone 7 Plus or iPhone 8 Plus"
        case iPhones_7Plus_8Plus = "2208"
        case iPhoneX = "2436" //iPhone X"
        case unknown = "";
    }
    var screenTypeWithHeight: ScreenType {
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return  .iPhone4_4S
        case 1136:
            return .iPhones_5_5s_5c_SE
        case 1334:
            return .iPhones_6_6s_7_8
        case 1920:
            return .iPhones_6Plus_6sPlus
        case 2208:
            return .iPhones_7Plus_8Plus
        case 2436:
            return .iPhoneX
        default:
            return .unknown
        }
    }
}

public var screenWidth: CGFloat {
    return UIScreen.main.bounds.width
}

// Screen height.
public var screenHeight: CGFloat {
    return UIScreen.main.bounds.height
}



